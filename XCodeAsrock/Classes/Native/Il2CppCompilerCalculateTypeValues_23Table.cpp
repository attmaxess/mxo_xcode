﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tango_TangoXYZij1772940297.h"
#include "AssemblyU2DCSharp_Tango_TangoEvent3674277405.h"
#include "AssemblyU2DCSharp_Tango_TangoCameraIntrinsics464275052.h"
#include "AssemblyU2DCSharp_Tango_TangoPoseData192582348.h"
#include "AssemblyU2DCSharp_Tango_TangoUnityImageData4168844635.h"
#include "AssemblyU2DCSharp_Tango_TangoPointCloudData511956092.h"
#include "AssemblyU2DCSharp_Tango_TangoUnityDepth2545959683.h"
#include "AssemblyU2DCSharp_Tango_OnAreaDescriptionImportEven117915755.h"
#include "AssemblyU2DCSharp_Tango_OnAreaDescriptionExportEven795586882.h"
#include "AssemblyU2DCSharp_Tango_AreaDescriptionEventListen1921626273.h"
#include "AssemblyU2DCSharp_Tango_OnPointCloudAvailableEventH934212769.h"
#include "AssemblyU2DCSharp_Tango_OnPointCloudMultithreadedA3985220823.h"
#include "AssemblyU2DCSharp_Tango_OnTangoDepthAvailableEvent2179186224.h"
#include "AssemblyU2DCSharp_Tango_OnTangoDepthMulithreadedAvai82925278.h"
#include "AssemblyU2DCSharp_Tango_DepthListener1230635105.h"
#include "AssemblyU2DCSharp_Tango_DepthListenerWrapper2399943874.h"
#include "AssemblyU2DCSharp_Tango_OnTangoPoseAvailableEventH1363242156.h"
#include "AssemblyU2DCSharp_Tango_PoseListener4247666351.h"
#include "AssemblyU2DCSharp_Tango_OnTangoEventAvailableEvent3985195121.h"
#include "AssemblyU2DCSharp_Tango_TangoEventListener3965451773.h"
#include "AssemblyU2DCSharp_Tango_OnTangoImageAvailableEvent1456428726.h"
#include "AssemblyU2DCSharp_Tango_OnTangoCameraTextureAvaila1960798139.h"
#include "AssemblyU2DCSharp_Tango_OnTangoImageMultithreadedAv762238536.h"
#include "AssemblyU2DCSharp_Tango_VideoOverlayListener1537797687.h"
#include "AssemblyU2DCSharp_Tango_AreaDescription1434786909.h"
#include "AssemblyU2DCSharp_Tango_AreaDescription_Metadata134815774.h"
#include "AssemblyU2DCSharp_Tango_AreaDescription_AreaDescri2284920636.h"
#include "AssemblyU2DCSharp_Tango_AreaDescription_U3CForUUID3284976360.h"
#include "AssemblyU2DCSharp_Tango_DepthProvider2169900366.h"
#include "AssemblyU2DCSharp_Tango_DepthProvider_APIOnPointCl1407154221.h"
#include "AssemblyU2DCSharp_Tango_DepthProvider_API1434177264.h"
#include "AssemblyU2DCSharp_Tango_PoseProvider3518289390.h"
#include "AssemblyU2DCSharp_Tango_PoseProvider_APIOnPoseAvail896491865.h"
#include "AssemblyU2DCSharp_Tango_PoseProvider_API3222650810.h"
#include "AssemblyU2DCSharp_Tango_OnTango3DReconstructionGri4194210309.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction1839722436.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_Stat2679548312.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_Updat407952626.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_APICo210321224.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_Grid1635028266.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_Sign2474492307.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_APIC1534945845.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_APIIma15424157.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_APIM2170263161.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_APIM4229960961.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_APIM2481716188.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_APIP1341431879.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_APIPo580197307.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_API786738202.h"
#include "AssemblyU2DCSharp_Tango_OnDisplayChangedEventHandl1075383569.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication278578959.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_OnTangoBin173513017.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_OnTangoCo1579449014.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_OnTangoDi1172501576.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_OnAndroid2564384318.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_TangoAppli848537215.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_TangoDepth827397893.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_TangoEven3622516502.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_TangoEven3783119041.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_TangoEven2049946070.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_TangoEven1986028433.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_TangoEven3614973302.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_TangoEven3196680443.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_API3639790937.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_TangoSetu3504735734.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_Permissio3665688688.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_Permissio1914853856.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_TangoPerm3095458629.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_AndroidMes591434573.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_AndroidMes132712285.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_TangoAndr2861932349.h"
#include "AssemblyU2DCSharp_Tango_TangoConfig3878449435.h"
#include "AssemblyU2DCSharp_Tango_TangoConfig_DepthMode1660370495.h"
#include "AssemblyU2DCSharp_Tango_TangoConfig_Keys2044654961.h"
#include "AssemblyU2DCSharp_Tango_TangoConfig_TangoConfigAPI3801312892.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (TangoXYZij_t1772940297), sizeof(TangoXYZij_t1772940297_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2300[8] = 
{
	TangoXYZij_t1772940297::get_offset_of_version_0(),
	TangoXYZij_t1772940297::get_offset_of_timestamp_1(),
	TangoXYZij_t1772940297::get_offset_of_xyz_count_2(),
	TangoXYZij_t1772940297::get_offset_of_xyz_3(),
	TangoXYZij_t1772940297::get_offset_of_ij_rows_4(),
	TangoXYZij_t1772940297::get_offset_of_ij_cols_5(),
	TangoXYZij_t1772940297::get_offset_of_ij_6(),
	TangoXYZij_t1772940297::get_offset_of_color_image_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (TangoEvent_t3674277405), sizeof(TangoEvent_t3674277405_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2301[4] = 
{
	TangoEvent_t3674277405::get_offset_of_timestamp_0(),
	TangoEvent_t3674277405::get_offset_of_type_1(),
	TangoEvent_t3674277405::get_offset_of_event_key_2(),
	TangoEvent_t3674277405::get_offset_of_event_value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (TangoCameraIntrinsics_t464275052), sizeof(TangoCameraIntrinsics_t464275052_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2302[13] = 
{
	TangoCameraIntrinsics_t464275052::get_offset_of_camera_id_0(),
	TangoCameraIntrinsics_t464275052::get_offset_of_calibration_type_1(),
	TangoCameraIntrinsics_t464275052::get_offset_of_width_2(),
	TangoCameraIntrinsics_t464275052::get_offset_of_height_3(),
	TangoCameraIntrinsics_t464275052::get_offset_of_fx_4(),
	TangoCameraIntrinsics_t464275052::get_offset_of_fy_5(),
	TangoCameraIntrinsics_t464275052::get_offset_of_cx_6(),
	TangoCameraIntrinsics_t464275052::get_offset_of_cy_7(),
	TangoCameraIntrinsics_t464275052::get_offset_of_distortion0_8(),
	TangoCameraIntrinsics_t464275052::get_offset_of_distortion1_9(),
	TangoCameraIntrinsics_t464275052::get_offset_of_distortion2_10(),
	TangoCameraIntrinsics_t464275052::get_offset_of_distortion3_11(),
	TangoCameraIntrinsics_t464275052::get_offset_of_distortion4_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (TangoPoseData_t192582348), sizeof(TangoPoseData_t192582348_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2303[8] = 
{
	TangoPoseData_t192582348::get_offset_of_version_0(),
	TangoPoseData_t192582348::get_offset_of_timestamp_1(),
	TangoPoseData_t192582348::get_offset_of_orientation_2(),
	TangoPoseData_t192582348::get_offset_of_translation_3(),
	TangoPoseData_t192582348::get_offset_of_status_code_4(),
	TangoPoseData_t192582348::get_offset_of_framePair_5(),
	TangoPoseData_t192582348::get_offset_of_confidence_6(),
	TangoPoseData_t192582348::get_offset_of_accuracy_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (TangoUnityImageData_t4168844635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[7] = 
{
	TangoUnityImageData_t4168844635::get_offset_of_width_0(),
	TangoUnityImageData_t4168844635::get_offset_of_height_1(),
	TangoUnityImageData_t4168844635::get_offset_of_stride_2(),
	TangoUnityImageData_t4168844635::get_offset_of_timestamp_3(),
	TangoUnityImageData_t4168844635::get_offset_of_frame_number_4(),
	TangoUnityImageData_t4168844635::get_offset_of_format_5(),
	TangoUnityImageData_t4168844635::get_offset_of_data_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (TangoPointCloudData_t511956092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2305[3] = 
{
	TangoPointCloudData_t511956092::get_offset_of_m_timestamp_0(),
	TangoPointCloudData_t511956092::get_offset_of_m_numPoints_1(),
	TangoPointCloudData_t511956092::get_offset_of_m_points_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (TangoUnityDepth_t2545959683), -1, sizeof(TangoUnityDepth_t2545959683_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2306[9] = 
{
	TangoUnityDepth_t2545959683_StaticFields::get_offset_of_MAX_POINTS_ARRAY_SIZE_0(),
	TangoUnityDepth_t2545959683_StaticFields::get_offset_of_MAX_IJ_ARRAY_SIZE_1(),
	TangoUnityDepth_t2545959683::get_offset_of_m_version_2(),
	TangoUnityDepth_t2545959683::get_offset_of_m_pointCount_3(),
	TangoUnityDepth_t2545959683::get_offset_of_m_points_4(),
	TangoUnityDepth_t2545959683::get_offset_of_m_timestamp_5(),
	TangoUnityDepth_t2545959683::get_offset_of_m_ijRows_6(),
	TangoUnityDepth_t2545959683::get_offset_of_m_ijColumns_7(),
	TangoUnityDepth_t2545959683::get_offset_of_m_ij_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (OnAreaDescriptionImportEventHandler_t117915755), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (OnAreaDescriptionExportEventHandler_t795586882), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (AreaDescriptionEventListener_t1921626273), -1, sizeof(AreaDescriptionEventListener_t1921626273_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2323[10] = 
{
	AreaDescriptionEventListener_t1921626273_StaticFields::get_offset_of_m_lockObject_0(),
	AreaDescriptionEventListener_t1921626273_StaticFields::get_offset_of_m_isCallbackSet_1(),
	AreaDescriptionEventListener_t1921626273_StaticFields::get_offset_of_m_isImportFinished_2(),
	AreaDescriptionEventListener_t1921626273_StaticFields::get_offset_of_m_isExportFinished_3(),
	AreaDescriptionEventListener_t1921626273_StaticFields::get_offset_of_m_eventString_4(),
	AreaDescriptionEventListener_t1921626273_StaticFields::get_offset_of_m_isSuccessful_5(),
	AreaDescriptionEventListener_t1921626273_StaticFields::get_offset_of_m_onTangoAreaDescriptionImported_6(),
	AreaDescriptionEventListener_t1921626273_StaticFields::get_offset_of_m_onTangoAreaDescriptionExported_7(),
	AreaDescriptionEventListener_t1921626273_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_8(),
	AreaDescriptionEventListener_t1921626273_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (OnPointCloudAvailableEventHandler_t934212769), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (OnPointCloudMultithreadedAvailableEventHandler_t3985220823), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (OnTangoDepthAvailableEventHandler_t2179186224), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (OnTangoDepthMulithreadedAvailableEventHandler_t82925278), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (DepthListener_t1230635105), -1, sizeof(DepthListener_t1230635105_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2329[10] = 
{
	DepthListener_t1230635105_StaticFields::get_offset_of_m_lockObject_0(),
	DepthListener_t1230635105_StaticFields::get_offset_of_m_onPointCloudAvailableCallback_1(),
	DepthListener_t1230635105_StaticFields::get_offset_of_m_isDirty_2(),
	DepthListener_t1230635105_StaticFields::get_offset_of_m_pointCloud_3(),
	DepthListener_t1230635105_StaticFields::get_offset_of_m_xyzPoints_4(),
	DepthListener_t1230635105_StaticFields::get_offset_of_m_maxNumReducedDepthPoints_5(),
	DepthListener_t1230635105_StaticFields::get_offset_of_m_onTangoDepthAvailable_6(),
	DepthListener_t1230635105_StaticFields::get_offset_of_m_onTangoDepthMultithreadedAvailable_7(),
	DepthListener_t1230635105_StaticFields::get_offset_of_m_onPointCloudAvailable_8(),
	DepthListener_t1230635105_StaticFields::get_offset_of_m_onPointCloudMultithreadedAvailable_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (DepthListenerWrapper_t2399943874), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (OnTangoPoseAvailableEventHandler_t1363242156), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (PoseListener_t4247666351), -1, sizeof(PoseListener_t4247666351_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2332[11] = 
{
	PoseListener_t4247666351_StaticFields::get_offset_of_m_lockObject_0(),
	PoseListener_t4247666351_StaticFields::get_offset_of_m_poseAvailableCallback_1(),
	PoseListener_t4247666351_StaticFields::get_offset_of_m_motionTrackingData_2(),
	PoseListener_t4247666351_StaticFields::get_offset_of_m_areaLearningData_3(),
	PoseListener_t4247666351_StaticFields::get_offset_of_m_relocalizationData_4(),
	PoseListener_t4247666351_StaticFields::get_offset_of_m_cloudPoseData_5(),
	PoseListener_t4247666351_StaticFields::get_offset_of_m_onTangoPoseAvailable_6(),
	PoseListener_t4247666351_StaticFields::get_offset_of_m_isMotionTrackingPoseAvailable_7(),
	PoseListener_t4247666351_StaticFields::get_offset_of_m_isAreaLearningPoseAvailable_8(),
	PoseListener_t4247666351_StaticFields::get_offset_of_m_isRelocalizationPoseAvailable_9(),
	PoseListener_t4247666351_StaticFields::get_offset_of_m_isCloudPoseAvailable_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (OnTangoEventAvailableEventHandler_t3985195121), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (TangoEventListener_t3965451773), -1, sizeof(TangoEventListener_t3965451773_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2334[6] = 
{
	TangoEventListener_t3965451773_StaticFields::get_offset_of_m_lockObject_0(),
	TangoEventListener_t3965451773_StaticFields::get_offset_of_m_onEventAvailableCallback_1(),
	TangoEventListener_t3965451773_StaticFields::get_offset_of_m_onTangoEventAvailable_2(),
	TangoEventListener_t3965451773_StaticFields::get_offset_of_m_onTangoEventMultithreadedAvailable_3(),
	TangoEventListener_t3965451773_StaticFields::get_offset_of_m_tangoEvent_4(),
	TangoEventListener_t3965451773_StaticFields::get_offset_of_m_isDirty_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (OnTangoImageAvailableEventHandler_t1456428726), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (OnTangoCameraTextureAvailableEventHandler_t1960798139), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (OnTangoImageMultithreadedAvailableEventHandler_t762238536), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (VideoOverlayListener_t1537797687), -1, sizeof(VideoOverlayListener_t1537797687_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2338[13] = 
{
	0,
	VideoOverlayListener_t1537797687_StaticFields::get_offset_of_m_lockObject_1(),
	VideoOverlayListener_t1537797687_StaticFields::get_offset_of_m_onImageAvailable_2(),
	VideoOverlayListener_t1537797687_StaticFields::get_offset_of_m_onTextureAvailable_3(),
	VideoOverlayListener_t1537797687_StaticFields::get_offset_of_m_onYUVTextureAvailable_4(),
	VideoOverlayListener_t1537797687_StaticFields::get_offset_of_m_previousImageBuffer_5(),
	VideoOverlayListener_t1537797687_StaticFields::get_offset_of_m_shouldSendTextureMethodEvent_6(),
	VideoOverlayListener_t1537797687_StaticFields::get_offset_of_m_shouldSendByteBufferMethodEvent_7(),
	VideoOverlayListener_t1537797687_StaticFields::get_offset_of_m_shouldSendYUVTextureIdMethodEvent_8(),
	VideoOverlayListener_t1537797687_StaticFields::get_offset_of_m_onTangoImageAvailable_9(),
	VideoOverlayListener_t1537797687_StaticFields::get_offset_of_m_onTangoCameraTextureAvailable_10(),
	VideoOverlayListener_t1537797687_StaticFields::get_offset_of_m_onTangoYUVTextureAvailable_11(),
	VideoOverlayListener_t1537797687_StaticFields::get_offset_of_m_onTangoImageMultithreadedAvailable_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (AreaDescription_t1434786909), -1, sizeof(AreaDescription_t1434786909_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2339[5] = 
{
	AreaDescription_t1434786909::get_offset_of_m_uuid_0(),
	0,
	0,
	0,
	AreaDescription_t1434786909_StaticFields::get_offset_of_METADATA_EPOCH_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (Metadata_t134815774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[4] = 
{
	Metadata_t134815774::get_offset_of_m_name_0(),
	Metadata_t134815774::get_offset_of_m_dateTime_1(),
	Metadata_t134815774::get_offset_of_m_transformationPosition_2(),
	Metadata_t134815774::get_offset_of_m_transformationRotation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (AreaDescriptionAPI_t2284920636), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (U3CForUUIDU3Ec__AnonStorey0_t3284976360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2342[1] = 
{
	U3CForUUIDU3Ec__AnonStorey0_t3284976360::get_offset_of_uuid_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (DepthProvider_t2169900366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2343[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (APIOnPointCloudAvailable_t1407154221), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (API_t1434177264)+ sizeof (Il2CppObject), sizeof(API_t1434177264 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (PoseProvider_t3518289390), -1, sizeof(PoseProvider_t3518289390_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2347[3] = 
{
	0,
	0,
	PoseProvider_t3518289390_StaticFields::get_offset_of_CLASS_NAME_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (APIOnPoseAvailable_t896491865), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (API_t3222650810)+ sizeof (Il2CppObject), sizeof(API_t3222650810 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (OnTango3DReconstructionGridIndiciesDirtyEventHandler_t4194210309), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (Tango3DReconstruction_t1839722436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2351[13] = 
{
	Tango3DReconstruction_t1839722436::get_offset_of_m_useAreaDescriptionPose_0(),
	Tango3DReconstruction_t1839722436::get_offset_of_m_sendColorToUpdate_1(),
	Tango3DReconstruction_t1839722436::get_offset_of_m_context_2(),
	Tango3DReconstruction_t1839722436::get_offset_of_m_updatedIndices_3(),
	Tango3DReconstruction_t1839722436::get_offset_of_m_lockObject_4(),
	Tango3DReconstruction_t1839722436::get_offset_of_m_onGridIndicesDirty_5(),
	Tango3DReconstruction_t1839722436::get_offset_of_m_device_T_depthCamera_6(),
	Tango3DReconstruction_t1839722436::get_offset_of_m_device_T_colorCamera_7(),
	Tango3DReconstruction_t1839722436::get_offset_of_m_mostRecentDepth_8(),
	Tango3DReconstruction_t1839722436::get_offset_of_m_mostRecentDepthPoints_9(),
	Tango3DReconstruction_t1839722436::get_offset_of_m_mostRecentDepthPose_10(),
	Tango3DReconstruction_t1839722436::get_offset_of_m_mostRecentDepthIsValid_11(),
	Tango3DReconstruction_t1839722436::get_offset_of_m_enabled_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (Status_t2679548312)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2352[5] = 
{
	Status_t2679548312::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (UpdateMethod_t407952626)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2353[3] = 
{
	UpdateMethod_t407952626::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (APIConfigType_t210321224)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2354[3] = 
{
	APIConfigType_t210321224::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (GridIndex_t1635028266)+ sizeof (Il2CppObject), sizeof(GridIndex_t1635028266 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2355[3] = 
{
	GridIndex_t1635028266::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GridIndex_t1635028266::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GridIndex_t1635028266::get_offset_of_z_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (SignedDistanceVoxel_t2474492307)+ sizeof (Il2CppObject), sizeof(SignedDistanceVoxel_t2474492307 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2356[2] = 
{
	SignedDistanceVoxel_t2474492307::get_offset_of_sdf_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SignedDistanceVoxel_t2474492307::get_offset_of_weight_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (APICameraCalibration_t1534945845)+ sizeof (Il2CppObject), sizeof(APICameraCalibration_t1534945845 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2357[12] = 
{
	APICameraCalibration_t1534945845::get_offset_of_calibration_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APICameraCalibration_t1534945845::get_offset_of_width_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APICameraCalibration_t1534945845::get_offset_of_height_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APICameraCalibration_t1534945845::get_offset_of_fx_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APICameraCalibration_t1534945845::get_offset_of_fy_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APICameraCalibration_t1534945845::get_offset_of_cx_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APICameraCalibration_t1534945845::get_offset_of_cy_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APICameraCalibration_t1534945845::get_offset_of_distortion0_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APICameraCalibration_t1534945845::get_offset_of_distortion1_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APICameraCalibration_t1534945845::get_offset_of_distortion2_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APICameraCalibration_t1534945845::get_offset_of_distortion3_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APICameraCalibration_t1534945845::get_offset_of_distortion4_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (APIImageBuffer_t15424157)+ sizeof (Il2CppObject), sizeof(APIImageBuffer_t15424157 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2358[6] = 
{
	APIImageBuffer_t15424157::get_offset_of_width_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIImageBuffer_t15424157::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIImageBuffer_t15424157::get_offset_of_stride_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIImageBuffer_t15424157::get_offset_of_timestamp_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIImageBuffer_t15424157::get_offset_of_format_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIImageBuffer_t15424157::get_offset_of_data_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (APIMatrix3x3_t2170263161)+ sizeof (Il2CppObject), sizeof(APIMatrix3x3_t2170263161 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2359[9] = 
{
	APIMatrix3x3_t2170263161::get_offset_of_r00_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMatrix3x3_t2170263161::get_offset_of_r10_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMatrix3x3_t2170263161::get_offset_of_r20_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMatrix3x3_t2170263161::get_offset_of_r01_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMatrix3x3_t2170263161::get_offset_of_r11_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMatrix3x3_t2170263161::get_offset_of_r21_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMatrix3x3_t2170263161::get_offset_of_r02_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMatrix3x3_t2170263161::get_offset_of_r12_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMatrix3x3_t2170263161::get_offset_of_r22_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (APIMesh_t4229960961)+ sizeof (Il2CppObject), sizeof(APIMesh_t4229960961 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2360[13] = 
{
	APIMesh_t4229960961::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMesh_t4229960961::get_offset_of_numVertices_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMesh_t4229960961::get_offset_of_numFaces_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMesh_t4229960961::get_offset_of_numTextures_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMesh_t4229960961::get_offset_of_maxNumVertices_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMesh_t4229960961::get_offset_of_maxNumFaces_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMesh_t4229960961::get_offset_of_maxNumTextures_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMesh_t4229960961::get_offset_of_vertices_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMesh_t4229960961::get_offset_of_faces_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMesh_t4229960961::get_offset_of_normals_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMesh_t4229960961::get_offset_of_colors_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMesh_t4229960961::get_offset_of_textureCoords_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMesh_t4229960961::get_offset_of_textures_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (APIMeshGCHandles_t2481716188)+ sizeof (Il2CppObject), sizeof(APIMeshGCHandles_t2481716188 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2361[4] = 
{
	APIMeshGCHandles_t2481716188::get_offset_of_m_verticesGCHandle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMeshGCHandles_t2481716188::get_offset_of_m_facesGCHandle_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMeshGCHandles_t2481716188::get_offset_of_m_normalsGCHandle_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMeshGCHandles_t2481716188::get_offset_of_m_colorsGCHandle_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (APIPointCloud_t1341431879)+ sizeof (Il2CppObject), sizeof(APIPointCloud_t1341431879 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2362[3] = 
{
	APIPointCloud_t1341431879::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIPointCloud_t1341431879::get_offset_of_numPoints_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIPointCloud_t1341431879::get_offset_of_points_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (APIPose_t580197307)+ sizeof (Il2CppObject), sizeof(APIPose_t580197307 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2363[7] = 
{
	APIPose_t580197307::get_offset_of_translation0_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIPose_t580197307::get_offset_of_translation1_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIPose_t580197307::get_offset_of_translation2_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIPose_t580197307::get_offset_of_orientation0_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIPose_t580197307::get_offset_of_orientation1_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIPose_t580197307::get_offset_of_orientation2_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIPose_t580197307::get_offset_of_orientation3_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (API_t786738202), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (OnDisplayChangedEventHandler_t1075383569), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (TangoApplication_t278578959), -1, sizeof(TangoApplication_t278578959_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2367[49] = 
{
	TangoApplication_t278578959::get_offset_of_m_autoConnectToService_2(),
	TangoApplication_t278578959::get_offset_of_m_allowOutOfDateTangoAPI_3(),
	TangoApplication_t278578959::get_offset_of_m_enableMotionTracking_4(),
	TangoApplication_t278578959::get_offset_of_m_motionTrackingAutoReset_5(),
	TangoApplication_t278578959::get_offset_of_m_enableAreaDescriptions_6(),
	TangoApplication_t278578959::get_offset_of_m_areaDescriptionLearningMode_7(),
	TangoApplication_t278578959::get_offset_of_m_enableDriftCorrection_8(),
	TangoApplication_t278578959::get_offset_of_m_enableDepth_9(),
	TangoApplication_t278578959::get_offset_of_m_enableAreaLearning_10(),
	TangoApplication_t278578959::get_offset_of_m_enableADFLoading_11(),
	TangoApplication_t278578959::get_offset_of_m_enableCloudADF_12(),
	TangoApplication_t278578959::get_offset_of_m_enable3DReconstruction_13(),
	TangoApplication_t278578959::get_offset_of_m_3drResolutionMeters_14(),
	TangoApplication_t278578959::get_offset_of_m_3drGenerateColor_15(),
	TangoApplication_t278578959::get_offset_of_m_3drGenerateNormal_16(),
	TangoApplication_t278578959::get_offset_of_m_3drGenerateTexCoord_17(),
	TangoApplication_t278578959::get_offset_of_m_3drSpaceClearing_18(),
	TangoApplication_t278578959::get_offset_of_m_3drUseAreaDescriptionPose_19(),
	TangoApplication_t278578959::get_offset_of_m_3drMinNumVertices_20(),
	TangoApplication_t278578959::get_offset_of_m_3drUpdateMethod_21(),
	TangoApplication_t278578959::get_offset_of_m_enableVideoOverlay_22(),
	TangoApplication_t278578959::get_offset_of_m_videoOverlayUseTextureMethod_23(),
	TangoApplication_t278578959::get_offset_of_m_videoOverlayUseYUVTextureIdMethod_24(),
	TangoApplication_t278578959::get_offset_of_m_videoOverlayUseByteBufferMethod_25(),
	TangoApplication_t278578959::get_offset_of_m_keepScreenAwake_26(),
	TangoApplication_t278578959::get_offset_of_m_adjustScreenResolution_27(),
	TangoApplication_t278578959::get_offset_of_m_targetResolution_28(),
	TangoApplication_t278578959::get_offset_of_m_allowOversizedScreenResolutions_29(),
	TangoApplication_t278578959::get_offset_of_m_initialPointCloudMaxPoints_30(),
	TangoApplication_t278578959::get_offset_of_OnDisplayChanged_31(),
	0,
	0,
	TangoApplication_t278578959_StaticFields::get_offset_of_SCREEN_SLEEPABLE_UX_EXCEPTIONS_34(),
	TangoApplication_t278578959_StaticFields::get_offset_of_m_tangoServiceVersion_35(),
	TangoApplication_t278578959_StaticFields::get_offset_of_m_screenLandscapeAspectRatio_36(),
	TangoApplication_t278578959::get_offset_of_m_tango3DReconstruction_37(),
	TangoApplication_t278578959::get_offset_of_m_unresolvedUxExceptions_38(),
	TangoApplication_t278578959::get_offset_of_m_yuvTexture_39(),
	TangoApplication_t278578959::get_offset_of_m_androidPermissionsEvent_40(),
	TangoApplication_t278578959::get_offset_of_m_tangoBindEvent_41(),
	TangoApplication_t278578959::get_offset_of_m_globalTLocal_42(),
	TangoApplication_t278578959::get_offset_of_m_tangoConnectEvent_43(),
	TangoApplication_t278578959::get_offset_of_m_tangoDisconnectEvent_44(),
	TangoApplication_t278578959::get_offset_of_m_androidMessageManager_45(),
	TangoApplication_t278578959::get_offset_of_m_applicationState_46(),
	TangoApplication_t278578959::get_offset_of_m_eventRegistrationManager_47(),
	TangoApplication_t278578959::get_offset_of_m_permissionsManager_48(),
	TangoApplication_t278578959::get_offset_of_m_setupTeardownManager_49(),
	TangoApplication_t278578959::get_offset_of_m_tangoDepthCameraManager_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (OnTangoBindDelegate_t173513017), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (OnTangoConnectDelegate_t1579449014), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (OnTangoDisconnectDelegate_t1172501576), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (OnAndroidPermissionsDelegate_t2564384318), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (TangoApplicationState_t848537215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2372[4] = 
{
	TangoApplicationState_t848537215::get_offset_of_m_tangoConfig_0(),
	TangoApplicationState_t848537215::get_offset_of_m_tangoRuntimeConfig_1(),
	TangoApplicationState_t848537215::get_offset_of_U3CIsTangoUpToDateU3Ek__BackingField_2(),
	TangoApplicationState_t848537215::get_offset_of_U3CIsTangoStartedU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (TangoDepthCameraManager_t827397893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[3] = 
{
	TangoDepthCameraManager_t827397893::get_offset_of_m_tangoApplicationState_0(),
	TangoDepthCameraManager_t827397893::get_offset_of_m_depthListener_1(),
	TangoDepthCameraManager_t827397893::get_offset_of_U3CLastSetDepthCameraRateU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (TangoEventRegistrationManager_t3622516502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2376[1] = 
{
	TangoEventRegistrationManager_t3622516502::get_offset_of_m_tangoApplication_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (U3C_RegistrationChangeDefaultU3Ec__AnonStorey0_t3783119041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2377[2] = 
{
	U3C_RegistrationChangeDefaultU3Ec__AnonStorey0_t3783119041::get_offset_of_isRegister_0(),
	U3C_RegistrationChangeDefaultU3Ec__AnonStorey0_t3783119041::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (U3C_RegistrationChangeMotionTrackingU3Ec__AnonStorey1_t2049946070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[1] = 
{
	U3C_RegistrationChangeMotionTrackingU3Ec__AnonStorey1_t2049946070::get_offset_of_isRegister_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (U3C_RegistrationChangeDepthU3Ec__AnonStorey2_t1986028433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[1] = 
{
	U3C_RegistrationChangeDepthU3Ec__AnonStorey2_t1986028433::get_offset_of_isRegister_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (U3C_RegistrationChangeVideoOverlayU3Ec__AnonStorey3_t3614973302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[1] = 
{
	U3C_RegistrationChangeVideoOverlayU3Ec__AnonStorey3_t3614973302::get_offset_of_isRegister_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (U3C_RegistrationChange3DReconstructionU3Ec__AnonStorey4_t3196680443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2381[2] = 
{
	U3C_RegistrationChange3DReconstructionU3Ec__AnonStorey4_t3196680443::get_offset_of_isRegister_0(),
	U3C_RegistrationChange3DReconstructionU3Ec__AnonStorey4_t3196680443::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (API_t3639790937)+ sizeof (Il2CppObject), sizeof(API_t3639790937 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (TangoSetupTeardownManager_t3504735734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[8] = 
{
	TangoSetupTeardownManager_t3504735734::get_offset_of_m_applicationSettings_0(),
	TangoSetupTeardownManager_t3504735734::get_offset_of_m_applicationState_1(),
	TangoSetupTeardownManager_t3504735734::get_offset_of_m_tangoUx_2(),
	TangoSetupTeardownManager_t3504735734::get_offset_of_m_fireTangeConnectEvent_3(),
	TangoSetupTeardownManager_t3504735734::get_offset_of_m_fireTangoDisconnectEvent_4(),
	TangoSetupTeardownManager_t3504735734::get_offset_of_m_yuvTexture_5(),
	TangoSetupTeardownManager_t3504735734::get_offset_of_m_tangoLifecycleLock_6(),
	TangoSetupTeardownManager_t3504735734::get_offset_of_m_isApplicationPaused_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (PermissionsTypes_t3665688688)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2385[6] = 
{
	PermissionsTypes_t3665688688::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (PermissionRequestState_t1914853856)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2386[7] = 
{
	PermissionRequestState_t1914853856::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (TangoPermissionsManager_t3095458629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[6] = 
{
	TangoPermissionsManager_t3095458629::get_offset_of_m_tangoApplicationSettings_0(),
	TangoPermissionsManager_t3095458629::get_offset_of_m_androidHelper_1(),
	TangoPermissionsManager_t3095458629::get_offset_of_m_onAndroidPermissionsResolved_2(),
	TangoPermissionsManager_t3095458629::get_offset_of_m_onTangoBindResolved_3(),
	TangoPermissionsManager_t3095458629::get_offset_of_U3CPendingRequiredPermissionsU3Ek__BackingField_4(),
	TangoPermissionsManager_t3095458629::get_offset_of_U3CPermissionRequestStateU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (AndroidMessageType_t591434573)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2389[9] = 
{
	AndroidMessageType_t591434573::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (AndroidMessage_t132712285)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[2] = 
{
	AndroidMessage_t132712285::get_offset_of_m_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AndroidMessage_t132712285::get_offset_of_m_messages_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (TangoAndroidMessageManager_t2861932349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[10] = 
{
	TangoAndroidMessageManager_t2861932349::get_offset_of_m_messageQueueLock_0(),
	TangoAndroidMessageManager_t2861932349::get_offset_of_m_androidMessageQueue_1(),
	TangoAndroidMessageManager_t2861932349::get_offset_of_m_depthCameraManager_2(),
	TangoAndroidMessageManager_t2861932349::get_offset_of_m_applicationState_3(),
	TangoAndroidMessageManager_t2861932349::get_offset_of_m_permissionsManager_4(),
	TangoAndroidMessageManager_t2861932349::get_offset_of_m_onAndroidPauseResumeAsync_5(),
	TangoAndroidMessageManager_t2861932349::get_offset_of_m_onDisplayChanged_6(),
	TangoAndroidMessageManager_t2861932349::get_offset_of_m_androidHelper_7(),
	TangoAndroidMessageManager_t2861932349::get_offset_of_m_isPaused_8(),
	TangoAndroidMessageManager_t2861932349::get_offset_of_savedDepthCameraRate_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (TangoConfig_t3878449435), -1, sizeof(TangoConfig_t3878449435_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2394[4] = 
{
	0,
	TangoConfig_t3878449435_StaticFields::get_offset_of_CLASS_NAME_1(),
	TangoConfig_t3878449435_StaticFields::get_offset_of_NO_CONFIG_FOUND_2(),
	TangoConfig_t3878449435::get_offset_of_m_configHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (DepthMode_t1660370495)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2397[2] = 
{
	DepthMode_t1660370495::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (Keys_t2044654961)+ sizeof (Il2CppObject), sizeof(Keys_t2044654961 ), sizeof(Keys_t2044654961_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2398[17] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Keys_t2044654961_StaticFields::get_offset_of_ENABLE_DATASET_RECORDING_14(),
	Keys_t2044654961_StaticFields::get_offset_of_GET_TANGO_SERVICE_VERSION_STRING_15(),
	Keys_t2044654961_StaticFields::get_offset_of_RUNTIME_DEPTH_FRAMERATE_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (TangoConfigAPI_t3801312892)+ sizeof (Il2CppObject), sizeof(TangoConfigAPI_t3801312892 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
