﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Tango_TangoUxEnums_UxHoldPosture3601679880.h"

// Tango.TangoApplication
struct TangoApplication_t278578959;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoUx
struct  TangoUx_t746396440  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Tango.TangoUx::m_enableUXLibrary
	bool ___m_enableUXLibrary_2;
	// System.Boolean Tango.TangoUx::m_drawDefaultUXExceptions
	bool ___m_drawDefaultUXExceptions_3;
	// System.Boolean Tango.TangoUx::m_showConnectionScreen
	bool ___m_showConnectionScreen_4;
	// Tango.TangoUxEnums/UxHoldPostureType Tango.TangoUx::m_holdPosture
	int32_t ___m_holdPosture_5;
	// Tango.TangoApplication Tango.TangoUx::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_6;
	// System.Boolean Tango.TangoUx::m_isTangoUxStarted
	bool ___m_isTangoUxStarted_7;

public:
	inline static int32_t get_offset_of_m_enableUXLibrary_2() { return static_cast<int32_t>(offsetof(TangoUx_t746396440, ___m_enableUXLibrary_2)); }
	inline bool get_m_enableUXLibrary_2() const { return ___m_enableUXLibrary_2; }
	inline bool* get_address_of_m_enableUXLibrary_2() { return &___m_enableUXLibrary_2; }
	inline void set_m_enableUXLibrary_2(bool value)
	{
		___m_enableUXLibrary_2 = value;
	}

	inline static int32_t get_offset_of_m_drawDefaultUXExceptions_3() { return static_cast<int32_t>(offsetof(TangoUx_t746396440, ___m_drawDefaultUXExceptions_3)); }
	inline bool get_m_drawDefaultUXExceptions_3() const { return ___m_drawDefaultUXExceptions_3; }
	inline bool* get_address_of_m_drawDefaultUXExceptions_3() { return &___m_drawDefaultUXExceptions_3; }
	inline void set_m_drawDefaultUXExceptions_3(bool value)
	{
		___m_drawDefaultUXExceptions_3 = value;
	}

	inline static int32_t get_offset_of_m_showConnectionScreen_4() { return static_cast<int32_t>(offsetof(TangoUx_t746396440, ___m_showConnectionScreen_4)); }
	inline bool get_m_showConnectionScreen_4() const { return ___m_showConnectionScreen_4; }
	inline bool* get_address_of_m_showConnectionScreen_4() { return &___m_showConnectionScreen_4; }
	inline void set_m_showConnectionScreen_4(bool value)
	{
		___m_showConnectionScreen_4 = value;
	}

	inline static int32_t get_offset_of_m_holdPosture_5() { return static_cast<int32_t>(offsetof(TangoUx_t746396440, ___m_holdPosture_5)); }
	inline int32_t get_m_holdPosture_5() const { return ___m_holdPosture_5; }
	inline int32_t* get_address_of_m_holdPosture_5() { return &___m_holdPosture_5; }
	inline void set_m_holdPosture_5(int32_t value)
	{
		___m_holdPosture_5 = value;
	}

	inline static int32_t get_offset_of_m_tangoApplication_6() { return static_cast<int32_t>(offsetof(TangoUx_t746396440, ___m_tangoApplication_6)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_6() const { return ___m_tangoApplication_6; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_6() { return &___m_tangoApplication_6; }
	inline void set_m_tangoApplication_6(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_6, value);
	}

	inline static int32_t get_offset_of_m_isTangoUxStarted_7() { return static_cast<int32_t>(offsetof(TangoUx_t746396440, ___m_isTangoUxStarted_7)); }
	inline bool get_m_isTangoUxStarted_7() const { return ___m_isTangoUxStarted_7; }
	inline bool* get_address_of_m_isTangoUxStarted_7() { return &___m_isTangoUxStarted_7; }
	inline void set_m_isTangoUxStarted_7(bool value)
	{
		___m_isTangoUxStarted_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
