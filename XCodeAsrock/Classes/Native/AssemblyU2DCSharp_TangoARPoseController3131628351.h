﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoPoseStatus1112875089.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"

// TangoARScreen
struct TangoARScreen_t2366305298;
// Tango.TangoApplication
struct TangoApplication_t278578959;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TangoARPoseController
struct  TangoARPoseController_t3131628351  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TangoARPoseController::m_useAreaDescriptionPose
	bool ___m_useAreaDescriptionPose_2;
	// System.Boolean TangoARPoseController::m_syncToARScreen
	bool ___m_syncToARScreen_3;
	// System.Int32 TangoARPoseController::m_poseCount
	int32_t ___m_poseCount_4;
	// Tango.TangoEnums/TangoPoseStatusType TangoARPoseController::m_poseStatus
	int32_t ___m_poseStatus_5;
	// System.Double TangoARPoseController::m_poseTimestamp
	double ___m_poseTimestamp_6;
	// UnityEngine.Vector3 TangoARPoseController::m_tangoPosition
	Vector3_t2243707580  ___m_tangoPosition_7;
	// UnityEngine.Quaternion TangoARPoseController::m_tangoRotation
	Quaternion_t4030073918  ___m_tangoRotation_8;
	// UnityEngine.Matrix4x4 TangoARPoseController::m_dTuc
	Matrix4x4_t2933234003  ___m_dTuc_9;
	// TangoARScreen TangoARPoseController::m_tangoARScreen
	TangoARScreen_t2366305298 * ___m_tangoARScreen_10;
	// Tango.TangoApplication TangoARPoseController::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_11;

public:
	inline static int32_t get_offset_of_m_useAreaDescriptionPose_2() { return static_cast<int32_t>(offsetof(TangoARPoseController_t3131628351, ___m_useAreaDescriptionPose_2)); }
	inline bool get_m_useAreaDescriptionPose_2() const { return ___m_useAreaDescriptionPose_2; }
	inline bool* get_address_of_m_useAreaDescriptionPose_2() { return &___m_useAreaDescriptionPose_2; }
	inline void set_m_useAreaDescriptionPose_2(bool value)
	{
		___m_useAreaDescriptionPose_2 = value;
	}

	inline static int32_t get_offset_of_m_syncToARScreen_3() { return static_cast<int32_t>(offsetof(TangoARPoseController_t3131628351, ___m_syncToARScreen_3)); }
	inline bool get_m_syncToARScreen_3() const { return ___m_syncToARScreen_3; }
	inline bool* get_address_of_m_syncToARScreen_3() { return &___m_syncToARScreen_3; }
	inline void set_m_syncToARScreen_3(bool value)
	{
		___m_syncToARScreen_3 = value;
	}

	inline static int32_t get_offset_of_m_poseCount_4() { return static_cast<int32_t>(offsetof(TangoARPoseController_t3131628351, ___m_poseCount_4)); }
	inline int32_t get_m_poseCount_4() const { return ___m_poseCount_4; }
	inline int32_t* get_address_of_m_poseCount_4() { return &___m_poseCount_4; }
	inline void set_m_poseCount_4(int32_t value)
	{
		___m_poseCount_4 = value;
	}

	inline static int32_t get_offset_of_m_poseStatus_5() { return static_cast<int32_t>(offsetof(TangoARPoseController_t3131628351, ___m_poseStatus_5)); }
	inline int32_t get_m_poseStatus_5() const { return ___m_poseStatus_5; }
	inline int32_t* get_address_of_m_poseStatus_5() { return &___m_poseStatus_5; }
	inline void set_m_poseStatus_5(int32_t value)
	{
		___m_poseStatus_5 = value;
	}

	inline static int32_t get_offset_of_m_poseTimestamp_6() { return static_cast<int32_t>(offsetof(TangoARPoseController_t3131628351, ___m_poseTimestamp_6)); }
	inline double get_m_poseTimestamp_6() const { return ___m_poseTimestamp_6; }
	inline double* get_address_of_m_poseTimestamp_6() { return &___m_poseTimestamp_6; }
	inline void set_m_poseTimestamp_6(double value)
	{
		___m_poseTimestamp_6 = value;
	}

	inline static int32_t get_offset_of_m_tangoPosition_7() { return static_cast<int32_t>(offsetof(TangoARPoseController_t3131628351, ___m_tangoPosition_7)); }
	inline Vector3_t2243707580  get_m_tangoPosition_7() const { return ___m_tangoPosition_7; }
	inline Vector3_t2243707580 * get_address_of_m_tangoPosition_7() { return &___m_tangoPosition_7; }
	inline void set_m_tangoPosition_7(Vector3_t2243707580  value)
	{
		___m_tangoPosition_7 = value;
	}

	inline static int32_t get_offset_of_m_tangoRotation_8() { return static_cast<int32_t>(offsetof(TangoARPoseController_t3131628351, ___m_tangoRotation_8)); }
	inline Quaternion_t4030073918  get_m_tangoRotation_8() const { return ___m_tangoRotation_8; }
	inline Quaternion_t4030073918 * get_address_of_m_tangoRotation_8() { return &___m_tangoRotation_8; }
	inline void set_m_tangoRotation_8(Quaternion_t4030073918  value)
	{
		___m_tangoRotation_8 = value;
	}

	inline static int32_t get_offset_of_m_dTuc_9() { return static_cast<int32_t>(offsetof(TangoARPoseController_t3131628351, ___m_dTuc_9)); }
	inline Matrix4x4_t2933234003  get_m_dTuc_9() const { return ___m_dTuc_9; }
	inline Matrix4x4_t2933234003 * get_address_of_m_dTuc_9() { return &___m_dTuc_9; }
	inline void set_m_dTuc_9(Matrix4x4_t2933234003  value)
	{
		___m_dTuc_9 = value;
	}

	inline static int32_t get_offset_of_m_tangoARScreen_10() { return static_cast<int32_t>(offsetof(TangoARPoseController_t3131628351, ___m_tangoARScreen_10)); }
	inline TangoARScreen_t2366305298 * get_m_tangoARScreen_10() const { return ___m_tangoARScreen_10; }
	inline TangoARScreen_t2366305298 ** get_address_of_m_tangoARScreen_10() { return &___m_tangoARScreen_10; }
	inline void set_m_tangoARScreen_10(TangoARScreen_t2366305298 * value)
	{
		___m_tangoARScreen_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoARScreen_10, value);
	}

	inline static int32_t get_offset_of_m_tangoApplication_11() { return static_cast<int32_t>(offsetof(TangoARPoseController_t3131628351, ___m_tangoApplication_11)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_11() const { return ___m_tangoApplication_11; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_11() { return &___m_tangoApplication_11; }
	inline void set_m_tangoApplication_11(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
