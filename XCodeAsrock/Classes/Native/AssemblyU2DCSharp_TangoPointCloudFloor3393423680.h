﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Tango.TangoApplication
struct TangoApplication_t278578959;
// TangoPointCloud
struct TangoPointCloud_t374155286;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TangoPointCloudFloor
struct  TangoPointCloudFloor_t3393423680  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TangoPointCloudFloor::m_turnOffDepthCamera
	bool ___m_turnOffDepthCamera_2;
	// System.Boolean TangoPointCloudFloor::m_floorFound
	bool ___m_floorFound_3;
	// Tango.TangoApplication TangoPointCloudFloor::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_4;
	// TangoPointCloud TangoPointCloudFloor::m_pointCloud
	TangoPointCloud_t374155286 * ___m_pointCloud_5;
	// System.Boolean TangoPointCloudFloor::m_depthTriggered
	bool ___m_depthTriggered_6;

public:
	inline static int32_t get_offset_of_m_turnOffDepthCamera_2() { return static_cast<int32_t>(offsetof(TangoPointCloudFloor_t3393423680, ___m_turnOffDepthCamera_2)); }
	inline bool get_m_turnOffDepthCamera_2() const { return ___m_turnOffDepthCamera_2; }
	inline bool* get_address_of_m_turnOffDepthCamera_2() { return &___m_turnOffDepthCamera_2; }
	inline void set_m_turnOffDepthCamera_2(bool value)
	{
		___m_turnOffDepthCamera_2 = value;
	}

	inline static int32_t get_offset_of_m_floorFound_3() { return static_cast<int32_t>(offsetof(TangoPointCloudFloor_t3393423680, ___m_floorFound_3)); }
	inline bool get_m_floorFound_3() const { return ___m_floorFound_3; }
	inline bool* get_address_of_m_floorFound_3() { return &___m_floorFound_3; }
	inline void set_m_floorFound_3(bool value)
	{
		___m_floorFound_3 = value;
	}

	inline static int32_t get_offset_of_m_tangoApplication_4() { return static_cast<int32_t>(offsetof(TangoPointCloudFloor_t3393423680, ___m_tangoApplication_4)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_4() const { return ___m_tangoApplication_4; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_4() { return &___m_tangoApplication_4; }
	inline void set_m_tangoApplication_4(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_4, value);
	}

	inline static int32_t get_offset_of_m_pointCloud_5() { return static_cast<int32_t>(offsetof(TangoPointCloudFloor_t3393423680, ___m_pointCloud_5)); }
	inline TangoPointCloud_t374155286 * get_m_pointCloud_5() const { return ___m_pointCloud_5; }
	inline TangoPointCloud_t374155286 ** get_address_of_m_pointCloud_5() { return &___m_pointCloud_5; }
	inline void set_m_pointCloud_5(TangoPointCloud_t374155286 * value)
	{
		___m_pointCloud_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_pointCloud_5, value);
	}

	inline static int32_t get_offset_of_m_depthTriggered_6() { return static_cast<int32_t>(offsetof(TangoPointCloudFloor_t3393423680, ___m_depthTriggered_6)); }
	inline bool get_m_depthTriggered_6() const { return ___m_depthTriggered_6; }
	inline bool* get_address_of_m_depthTriggered_6() { return &___m_depthTriggered_6; }
	inline void set_m_depthTriggered_6(bool value)
	{
		___m_depthTriggered_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
