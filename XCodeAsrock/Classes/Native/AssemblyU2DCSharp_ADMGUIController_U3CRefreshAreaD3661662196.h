﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Tango.AreaDescription
struct AreaDescription_t1434786909;
// ADMGUIController
struct ADMGUIController_t1723854221;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ADMGUIController/<RefreshAreaDescriptionList>c__AnonStorey3
struct  U3CRefreshAreaDescriptionListU3Ec__AnonStorey3_t3661662196  : public Il2CppObject
{
public:
	// Tango.AreaDescription ADMGUIController/<RefreshAreaDescriptionList>c__AnonStorey3::lambdaParam
	AreaDescription_t1434786909 * ___lambdaParam_0;
	// ADMGUIController ADMGUIController/<RefreshAreaDescriptionList>c__AnonStorey3::$this
	ADMGUIController_t1723854221 * ___U24this_1;

public:
	inline static int32_t get_offset_of_lambdaParam_0() { return static_cast<int32_t>(offsetof(U3CRefreshAreaDescriptionListU3Ec__AnonStorey3_t3661662196, ___lambdaParam_0)); }
	inline AreaDescription_t1434786909 * get_lambdaParam_0() const { return ___lambdaParam_0; }
	inline AreaDescription_t1434786909 ** get_address_of_lambdaParam_0() { return &___lambdaParam_0; }
	inline void set_lambdaParam_0(AreaDescription_t1434786909 * value)
	{
		___lambdaParam_0 = value;
		Il2CppCodeGenWriteBarrier(&___lambdaParam_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRefreshAreaDescriptionListU3Ec__AnonStorey3_t3661662196, ___U24this_1)); }
	inline ADMGUIController_t1723854221 * get_U24this_1() const { return ___U24this_1; }
	inline ADMGUIController_t1723854221 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ADMGUIController_t1723854221 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
