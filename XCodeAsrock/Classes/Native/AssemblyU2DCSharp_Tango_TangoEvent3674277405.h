﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoEventType2750756310.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoEvent
struct  TangoEvent_t3674277405  : public Il2CppObject
{
public:
	// System.Double Tango.TangoEvent::timestamp
	double ___timestamp_0;
	// Tango.TangoEnums/TangoEventType Tango.TangoEvent::type
	int32_t ___type_1;
	// System.String Tango.TangoEvent::event_key
	String_t* ___event_key_2;
	// System.String Tango.TangoEvent::event_value
	String_t* ___event_value_3;

public:
	inline static int32_t get_offset_of_timestamp_0() { return static_cast<int32_t>(offsetof(TangoEvent_t3674277405, ___timestamp_0)); }
	inline double get_timestamp_0() const { return ___timestamp_0; }
	inline double* get_address_of_timestamp_0() { return &___timestamp_0; }
	inline void set_timestamp_0(double value)
	{
		___timestamp_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(TangoEvent_t3674277405, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_event_key_2() { return static_cast<int32_t>(offsetof(TangoEvent_t3674277405, ___event_key_2)); }
	inline String_t* get_event_key_2() const { return ___event_key_2; }
	inline String_t** get_address_of_event_key_2() { return &___event_key_2; }
	inline void set_event_key_2(String_t* value)
	{
		___event_key_2 = value;
		Il2CppCodeGenWriteBarrier(&___event_key_2, value);
	}

	inline static int32_t get_offset_of_event_value_3() { return static_cast<int32_t>(offsetof(TangoEvent_t3674277405, ___event_value_3)); }
	inline String_t* get_event_value_3() const { return ___event_value_3; }
	inline String_t** get_address_of_event_value_3() { return &___event_value_3; }
	inline void set_event_value_3(String_t* value)
	{
		___event_value_3 = value;
		Il2CppCodeGenWriteBarrier(&___event_value_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Tango.TangoEvent
struct TangoEvent_t3674277405_marshaled_pinvoke
{
	double ___timestamp_0;
	int32_t ___type_1;
	char* ___event_key_2;
	char* ___event_value_3;
};
// Native definition for COM marshalling of Tango.TangoEvent
struct TangoEvent_t3674277405_marshaled_com
{
	double ___timestamp_0;
	int32_t ___type_1;
	char* ___event_key_2;
	char* ___event_value_3;
};
