﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_i2580192745.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U2901866349.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U2645079618.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoCameraId106182192.h"

struct TangoPointCloudIntPtr_t1795497939 ;
struct TangoPoseData_t192582348_marshaled_pinvoke;
struct TangoEvent_t3674277405_marshaled_pinvoke;
struct TangoImage_t3319528210 ;
struct TangoCameraMetadata_t1412457087 ;




extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_DeflateStream_UnmanagedRead_m3972503109(intptr_t ___buffer0, int32_t ___length1, intptr_t ___data2);
extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_DeflateStream_UnmanagedWrite_m2053312812(intptr_t ___buffer0, int32_t ___length1, intptr_t ___data2);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__frame_update_m127370(internal_UnityARCamera_t2580192745  ___camera0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__ar_tracking_changed_m234370619(internal_UnityARCamera_t2580192745  ___camera0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__anchor_added_m3584567327(UnityARAnchorData_t2901866349  ___anchor0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__anchor_updated_m1970308864(UnityARAnchorData_t2901866349  ___anchor0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__anchor_removed_m4240378233(UnityARAnchorData_t2901866349  ___anchor0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__user_anchor_added_m3500265430(UnityARUserAnchorData_t2645079618  ___anchor0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__user_anchor_updated_m1546456361(UnityARUserAnchorData_t2645079618  ___anchor0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__user_anchor_removed_m2358695840(UnityARUserAnchorData_t2645079618  ___anchor0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__ar_session_failed_m2515310284(char* ___error0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__ar_session_interrupted_m3455489303();
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__ar_session_interruption_ended_m406108599();
extern "C" void CDECL ReversePInvokeWrapper_DepthListener__OnPointCloudAvailable_m3154749545(intptr_t ___callbackContext0, TangoPointCloudIntPtr_t1795497939 * ___rawPointCloud1);
extern "C" void CDECL ReversePInvokeWrapper_PoseListener__OnPoseAvailable_m2661102674(intptr_t ___callbackContext0, TangoPoseData_t192582348_marshaled_pinvoke* ___pose1);
extern "C" void CDECL ReversePInvokeWrapper_TangoEventListener__OnEventAvailable_m1145406868(intptr_t ___callbackContext0, TangoEvent_t3674277405_marshaled_pinvoke* ___tangoEvent1);
extern "C" void CDECL ReversePInvokeWrapper_VideoOverlayListener__OnImageAvailable_m3305723086(intptr_t ___callbackContext0, int32_t ___cameraId1, TangoImage_t3319528210 * ___image2, TangoCameraMetadata_t1412457087 * ___cameraMetadata3);
extern "C" void CDECL ReversePInvokeWrapper_VideoOverlayListener__OnTangoCameraTextureAvailable_m27262193(intptr_t ___callbackContext0, int32_t ___cameraId1);
extern "C" void CDECL ReversePInvokeWrapper_VideoOverlayListener__OnTangoYUVTextureAvailable_m3740454070(intptr_t ___callbackContext0, int32_t ___cameraId1);
extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[19] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DeflateStream_UnmanagedRead_m3972503109),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DeflateStream_UnmanagedWrite_m2053312812),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityARSessionNativeInterface__frame_update_m127370),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityARSessionNativeInterface__ar_tracking_changed_m234370619),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityARSessionNativeInterface__anchor_added_m3584567327),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityARSessionNativeInterface__anchor_updated_m1970308864),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityARSessionNativeInterface__anchor_removed_m4240378233),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityARSessionNativeInterface__user_anchor_added_m3500265430),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityARSessionNativeInterface__user_anchor_updated_m1546456361),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityARSessionNativeInterface__user_anchor_removed_m2358695840),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityARSessionNativeInterface__ar_session_failed_m2515310284),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityARSessionNativeInterface__ar_session_interrupted_m3455489303),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityARSessionNativeInterface__ar_session_interruption_ended_m406108599),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DepthListener__OnPointCloudAvailable_m3154749545),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_PoseListener__OnPoseAvailable_m2661102674),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_TangoEventListener__OnEventAvailable_m1145406868),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_VideoOverlayListener__OnImageAvailable_m3305723086),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_VideoOverlayListener__OnTangoCameraTextureAvailable_m27262193),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_VideoOverlayListener__OnTangoYUVTextureAvailable_m3740454070),
};
