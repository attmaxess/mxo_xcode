﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_IntPtr2504060609.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoSupport/APIMarkerList
struct  APIMarkerList_t1654514504 
{
public:
	// System.IntPtr Tango.TangoSupport/APIMarkerList::markers
	IntPtr_t ___markers_0;
	// System.Int32 Tango.TangoSupport/APIMarkerList::markerCount
	int32_t ___markerCount_1;

public:
	inline static int32_t get_offset_of_markers_0() { return static_cast<int32_t>(offsetof(APIMarkerList_t1654514504, ___markers_0)); }
	inline IntPtr_t get_markers_0() const { return ___markers_0; }
	inline IntPtr_t* get_address_of_markers_0() { return &___markers_0; }
	inline void set_markers_0(IntPtr_t value)
	{
		___markers_0 = value;
	}

	inline static int32_t get_offset_of_markerCount_1() { return static_cast<int32_t>(offsetof(APIMarkerList_t1654514504, ___markerCount_1)); }
	inline int32_t get_markerCount_1() const { return ___markerCount_1; }
	inline int32_t* get_address_of_markerCount_1() { return &___markerCount_1; }
	inline void set_markerCount_1(int32_t value)
	{
		___markerCount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
