﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Shader
struct Shader_t2430389951;
// Tango.TangoApplication
struct TangoApplication_t278578959;
// ARCameraPostProcess
struct ARCameraPostProcess_t3075820819;
// UnityEngine.Camera
struct Camera_t189460977;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TangoARScreen
struct  TangoARScreen_t2366305298  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TangoARScreen::m_enableOcclusion
	bool ___m_enableOcclusion_2;
	// UnityEngine.Shader TangoARScreen::m_occlusionShader
	Shader_t2430389951 * ___m_occlusionShader_3;
	// System.Double TangoARScreen::m_screenUpdateTime
	double ___m_screenUpdateTime_4;
	// Tango.TangoApplication TangoARScreen::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_5;
	// ARCameraPostProcess TangoARScreen::m_arCameraPostProcess
	ARCameraPostProcess_t3075820819 * ___m_arCameraPostProcess_6;
	// UnityEngine.Camera TangoARScreen::m_camera
	Camera_t189460977 * ___m_camera_7;
	// System.Single TangoARScreen::m_uOffset
	float ___m_uOffset_8;
	// System.Single TangoARScreen::m_vOffset
	float ___m_vOffset_9;
	// System.Boolean TangoARScreen::<IsRendering>k__BackingField
	bool ___U3CIsRenderingU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_m_enableOcclusion_2() { return static_cast<int32_t>(offsetof(TangoARScreen_t2366305298, ___m_enableOcclusion_2)); }
	inline bool get_m_enableOcclusion_2() const { return ___m_enableOcclusion_2; }
	inline bool* get_address_of_m_enableOcclusion_2() { return &___m_enableOcclusion_2; }
	inline void set_m_enableOcclusion_2(bool value)
	{
		___m_enableOcclusion_2 = value;
	}

	inline static int32_t get_offset_of_m_occlusionShader_3() { return static_cast<int32_t>(offsetof(TangoARScreen_t2366305298, ___m_occlusionShader_3)); }
	inline Shader_t2430389951 * get_m_occlusionShader_3() const { return ___m_occlusionShader_3; }
	inline Shader_t2430389951 ** get_address_of_m_occlusionShader_3() { return &___m_occlusionShader_3; }
	inline void set_m_occlusionShader_3(Shader_t2430389951 * value)
	{
		___m_occlusionShader_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_occlusionShader_3, value);
	}

	inline static int32_t get_offset_of_m_screenUpdateTime_4() { return static_cast<int32_t>(offsetof(TangoARScreen_t2366305298, ___m_screenUpdateTime_4)); }
	inline double get_m_screenUpdateTime_4() const { return ___m_screenUpdateTime_4; }
	inline double* get_address_of_m_screenUpdateTime_4() { return &___m_screenUpdateTime_4; }
	inline void set_m_screenUpdateTime_4(double value)
	{
		___m_screenUpdateTime_4 = value;
	}

	inline static int32_t get_offset_of_m_tangoApplication_5() { return static_cast<int32_t>(offsetof(TangoARScreen_t2366305298, ___m_tangoApplication_5)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_5() const { return ___m_tangoApplication_5; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_5() { return &___m_tangoApplication_5; }
	inline void set_m_tangoApplication_5(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_5, value);
	}

	inline static int32_t get_offset_of_m_arCameraPostProcess_6() { return static_cast<int32_t>(offsetof(TangoARScreen_t2366305298, ___m_arCameraPostProcess_6)); }
	inline ARCameraPostProcess_t3075820819 * get_m_arCameraPostProcess_6() const { return ___m_arCameraPostProcess_6; }
	inline ARCameraPostProcess_t3075820819 ** get_address_of_m_arCameraPostProcess_6() { return &___m_arCameraPostProcess_6; }
	inline void set_m_arCameraPostProcess_6(ARCameraPostProcess_t3075820819 * value)
	{
		___m_arCameraPostProcess_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_arCameraPostProcess_6, value);
	}

	inline static int32_t get_offset_of_m_camera_7() { return static_cast<int32_t>(offsetof(TangoARScreen_t2366305298, ___m_camera_7)); }
	inline Camera_t189460977 * get_m_camera_7() const { return ___m_camera_7; }
	inline Camera_t189460977 ** get_address_of_m_camera_7() { return &___m_camera_7; }
	inline void set_m_camera_7(Camera_t189460977 * value)
	{
		___m_camera_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_camera_7, value);
	}

	inline static int32_t get_offset_of_m_uOffset_8() { return static_cast<int32_t>(offsetof(TangoARScreen_t2366305298, ___m_uOffset_8)); }
	inline float get_m_uOffset_8() const { return ___m_uOffset_8; }
	inline float* get_address_of_m_uOffset_8() { return &___m_uOffset_8; }
	inline void set_m_uOffset_8(float value)
	{
		___m_uOffset_8 = value;
	}

	inline static int32_t get_offset_of_m_vOffset_9() { return static_cast<int32_t>(offsetof(TangoARScreen_t2366305298, ___m_vOffset_9)); }
	inline float get_m_vOffset_9() const { return ___m_vOffset_9; }
	inline float* get_address_of_m_vOffset_9() { return &___m_vOffset_9; }
	inline void set_m_vOffset_9(float value)
	{
		___m_vOffset_9 = value;
	}

	inline static int32_t get_offset_of_U3CIsRenderingU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(TangoARScreen_t2366305298, ___U3CIsRenderingU3Ek__BackingField_10)); }
	inline bool get_U3CIsRenderingU3Ek__BackingField_10() const { return ___U3CIsRenderingU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CIsRenderingU3Ek__BackingField_10() { return &___U3CIsRenderingU3Ek__BackingField_10; }
	inline void set_U3CIsRenderingU3Ek__BackingField_10(bool value)
	{
		___U3CIsRenderingU3Ek__BackingField_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
