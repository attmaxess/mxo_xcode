﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.LineRenderer
struct LineRenderer_t849157671;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerVisualizationObject
struct  MarkerVisualizationObject_t2038234659  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.LineRenderer MarkerVisualizationObject::m_rect
	LineRenderer_t849157671 * ___m_rect_2;

public:
	inline static int32_t get_offset_of_m_rect_2() { return static_cast<int32_t>(offsetof(MarkerVisualizationObject_t2038234659, ___m_rect_2)); }
	inline LineRenderer_t849157671 * get_m_rect_2() const { return ___m_rect_2; }
	inline LineRenderer_t849157671 ** get_address_of_m_rect_2() { return &___m_rect_2; }
	inline void set_m_rect_2(LineRenderer_t849157671 * value)
	{
		___m_rect_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_rect_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
