﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeshOcclusionAreaDescriptionListElement
struct  MeshOcclusionAreaDescriptionListElement_t3382731569  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Toggle MeshOcclusionAreaDescriptionListElement::m_toggle
	Toggle_t3976754468 * ___m_toggle_2;
	// UnityEngine.UI.Text MeshOcclusionAreaDescriptionListElement::m_areaDescriptionName
	Text_t356221433 * ___m_areaDescriptionName_3;
	// UnityEngine.UI.Text MeshOcclusionAreaDescriptionListElement::m_areaDescriptionUUID
	Text_t356221433 * ___m_areaDescriptionUUID_4;
	// UnityEngine.UI.Text MeshOcclusionAreaDescriptionListElement::m_hasMeshData
	Text_t356221433 * ___m_hasMeshData_5;

public:
	inline static int32_t get_offset_of_m_toggle_2() { return static_cast<int32_t>(offsetof(MeshOcclusionAreaDescriptionListElement_t3382731569, ___m_toggle_2)); }
	inline Toggle_t3976754468 * get_m_toggle_2() const { return ___m_toggle_2; }
	inline Toggle_t3976754468 ** get_address_of_m_toggle_2() { return &___m_toggle_2; }
	inline void set_m_toggle_2(Toggle_t3976754468 * value)
	{
		___m_toggle_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_toggle_2, value);
	}

	inline static int32_t get_offset_of_m_areaDescriptionName_3() { return static_cast<int32_t>(offsetof(MeshOcclusionAreaDescriptionListElement_t3382731569, ___m_areaDescriptionName_3)); }
	inline Text_t356221433 * get_m_areaDescriptionName_3() const { return ___m_areaDescriptionName_3; }
	inline Text_t356221433 ** get_address_of_m_areaDescriptionName_3() { return &___m_areaDescriptionName_3; }
	inline void set_m_areaDescriptionName_3(Text_t356221433 * value)
	{
		___m_areaDescriptionName_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_areaDescriptionName_3, value);
	}

	inline static int32_t get_offset_of_m_areaDescriptionUUID_4() { return static_cast<int32_t>(offsetof(MeshOcclusionAreaDescriptionListElement_t3382731569, ___m_areaDescriptionUUID_4)); }
	inline Text_t356221433 * get_m_areaDescriptionUUID_4() const { return ___m_areaDescriptionUUID_4; }
	inline Text_t356221433 ** get_address_of_m_areaDescriptionUUID_4() { return &___m_areaDescriptionUUID_4; }
	inline void set_m_areaDescriptionUUID_4(Text_t356221433 * value)
	{
		___m_areaDescriptionUUID_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_areaDescriptionUUID_4, value);
	}

	inline static int32_t get_offset_of_m_hasMeshData_5() { return static_cast<int32_t>(offsetof(MeshOcclusionAreaDescriptionListElement_t3382731569, ___m_hasMeshData_5)); }
	inline Text_t356221433 * get_m_hasMeshData_5() const { return ___m_hasMeshData_5; }
	inline Text_t356221433 ** get_address_of_m_hasMeshData_5() { return &___m_hasMeshData_5; }
	inline void set_m_hasMeshData_5(Text_t356221433 * value)
	{
		___m_hasMeshData_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_hasMeshData_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
