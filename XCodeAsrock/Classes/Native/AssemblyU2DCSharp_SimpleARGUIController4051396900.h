﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// TangoPoseController
struct TangoPoseController_t4427816;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleARGUIController
struct  SimpleARGUIController_t4051396900  : public MonoBehaviour_t1158329972
{
public:
	// TangoPoseController SimpleARGUIController::m_poseController
	TangoPoseController_t4427816 * ___m_poseController_2;

public:
	inline static int32_t get_offset_of_m_poseController_2() { return static_cast<int32_t>(offsetof(SimpleARGUIController_t4051396900, ___m_poseController_2)); }
	inline TangoPoseController_t4427816 * get_m_poseController_2() const { return ___m_poseController_2; }
	inline TangoPoseController_t4427816 ** get_address_of_m_poseController_2() { return &___m_poseController_2; }
	inline void set_m_poseController_2(TangoPoseController_t4427816 * value)
	{
		___m_poseController_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_poseController_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
