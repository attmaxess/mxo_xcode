﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoApplication/TangoEventRegistrationManager/<_RegistrationChangeVideoOverlay>c__AnonStorey3
struct  U3C_RegistrationChangeVideoOverlayU3Ec__AnonStorey3_t3614973302  : public Il2CppObject
{
public:
	// System.Boolean Tango.TangoApplication/TangoEventRegistrationManager/<_RegistrationChangeVideoOverlay>c__AnonStorey3::isRegister
	bool ___isRegister_0;

public:
	inline static int32_t get_offset_of_isRegister_0() { return static_cast<int32_t>(offsetof(U3C_RegistrationChangeVideoOverlayU3Ec__AnonStorey3_t3614973302, ___isRegister_0)); }
	inline bool get_isRegister_0() const { return ___isRegister_0; }
	inline bool* get_address_of_isRegister_0() { return &___isRegister_0; }
	inline void set_isRegister_0(bool value)
	{
		___isRegister_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
