﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.Tango3DReconstruction/SignedDistanceVoxel
struct  SignedDistanceVoxel_t2474492307 
{
public:
	// System.Int16 Tango.Tango3DReconstruction/SignedDistanceVoxel::sdf
	int16_t ___sdf_0;
	// System.UInt16 Tango.Tango3DReconstruction/SignedDistanceVoxel::weight
	uint16_t ___weight_1;

public:
	inline static int32_t get_offset_of_sdf_0() { return static_cast<int32_t>(offsetof(SignedDistanceVoxel_t2474492307, ___sdf_0)); }
	inline int16_t get_sdf_0() const { return ___sdf_0; }
	inline int16_t* get_address_of_sdf_0() { return &___sdf_0; }
	inline void set_sdf_0(int16_t value)
	{
		___sdf_0 = value;
	}

	inline static int32_t get_offset_of_weight_1() { return static_cast<int32_t>(offsetof(SignedDistanceVoxel_t2474492307, ___weight_1)); }
	inline uint16_t get_weight_1() const { return ___weight_1; }
	inline uint16_t* get_address_of_weight_1() { return &___weight_1; }
	inline void set_weight_1(uint16_t value)
	{
		___weight_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
