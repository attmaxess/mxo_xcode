﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_AndroidJavaProxy4274989947.h"

// OnStartEventHandler
struct OnStartEventHandler_t4250010451;
// OnStopEventHandler
struct OnStopEventHandler_t1144032567;
// OnPauseEventHandler
struct OnPauseEventHandler_t2696015029;
// OnResumeEventHandler
struct OnResumeEventHandler_t3883967602;
// OnActivityResultEventHandler
struct OnActivityResultEventHandler_t2943688375;
// OnDisplayChangedEventHandler
struct OnDisplayChangedEventHandler_t4254774819;
// OnRequestPermissionsResultHandler
struct OnRequestPermissionsResultHandler_t856552163;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidLifecycleCallbacks
struct  AndroidLifecycleCallbacks_t1531251569  : public AndroidJavaProxy_t4274989947
{
public:

public:
};

struct AndroidLifecycleCallbacks_t1531251569_StaticFields
{
public:
	// OnStartEventHandler AndroidLifecycleCallbacks::m_onStartEvent
	OnStartEventHandler_t4250010451 * ___m_onStartEvent_1;
	// OnStopEventHandler AndroidLifecycleCallbacks::m_onStopEvent
	OnStopEventHandler_t1144032567 * ___m_onStopEvent_2;
	// OnPauseEventHandler AndroidLifecycleCallbacks::m_onPauseEvent
	OnPauseEventHandler_t2696015029 * ___m_onPauseEvent_3;
	// OnResumeEventHandler AndroidLifecycleCallbacks::m_onResumeEvent
	OnResumeEventHandler_t3883967602 * ___m_onResumeEvent_4;
	// OnActivityResultEventHandler AndroidLifecycleCallbacks::m_onActivityResultEvent
	OnActivityResultEventHandler_t2943688375 * ___m_onActivityResultEvent_5;
	// OnDisplayChangedEventHandler AndroidLifecycleCallbacks::m_onDisplayChangedEvent
	OnDisplayChangedEventHandler_t4254774819 * ___m_onDisplayChangedEvent_6;
	// OnRequestPermissionsResultHandler AndroidLifecycleCallbacks::m_onRequestPermissionsResultEvent
	OnRequestPermissionsResultHandler_t856552163 * ___m_onRequestPermissionsResultEvent_7;

public:
	inline static int32_t get_offset_of_m_onStartEvent_1() { return static_cast<int32_t>(offsetof(AndroidLifecycleCallbacks_t1531251569_StaticFields, ___m_onStartEvent_1)); }
	inline OnStartEventHandler_t4250010451 * get_m_onStartEvent_1() const { return ___m_onStartEvent_1; }
	inline OnStartEventHandler_t4250010451 ** get_address_of_m_onStartEvent_1() { return &___m_onStartEvent_1; }
	inline void set_m_onStartEvent_1(OnStartEventHandler_t4250010451 * value)
	{
		___m_onStartEvent_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_onStartEvent_1, value);
	}

	inline static int32_t get_offset_of_m_onStopEvent_2() { return static_cast<int32_t>(offsetof(AndroidLifecycleCallbacks_t1531251569_StaticFields, ___m_onStopEvent_2)); }
	inline OnStopEventHandler_t1144032567 * get_m_onStopEvent_2() const { return ___m_onStopEvent_2; }
	inline OnStopEventHandler_t1144032567 ** get_address_of_m_onStopEvent_2() { return &___m_onStopEvent_2; }
	inline void set_m_onStopEvent_2(OnStopEventHandler_t1144032567 * value)
	{
		___m_onStopEvent_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_onStopEvent_2, value);
	}

	inline static int32_t get_offset_of_m_onPauseEvent_3() { return static_cast<int32_t>(offsetof(AndroidLifecycleCallbacks_t1531251569_StaticFields, ___m_onPauseEvent_3)); }
	inline OnPauseEventHandler_t2696015029 * get_m_onPauseEvent_3() const { return ___m_onPauseEvent_3; }
	inline OnPauseEventHandler_t2696015029 ** get_address_of_m_onPauseEvent_3() { return &___m_onPauseEvent_3; }
	inline void set_m_onPauseEvent_3(OnPauseEventHandler_t2696015029 * value)
	{
		___m_onPauseEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_onPauseEvent_3, value);
	}

	inline static int32_t get_offset_of_m_onResumeEvent_4() { return static_cast<int32_t>(offsetof(AndroidLifecycleCallbacks_t1531251569_StaticFields, ___m_onResumeEvent_4)); }
	inline OnResumeEventHandler_t3883967602 * get_m_onResumeEvent_4() const { return ___m_onResumeEvent_4; }
	inline OnResumeEventHandler_t3883967602 ** get_address_of_m_onResumeEvent_4() { return &___m_onResumeEvent_4; }
	inline void set_m_onResumeEvent_4(OnResumeEventHandler_t3883967602 * value)
	{
		___m_onResumeEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_onResumeEvent_4, value);
	}

	inline static int32_t get_offset_of_m_onActivityResultEvent_5() { return static_cast<int32_t>(offsetof(AndroidLifecycleCallbacks_t1531251569_StaticFields, ___m_onActivityResultEvent_5)); }
	inline OnActivityResultEventHandler_t2943688375 * get_m_onActivityResultEvent_5() const { return ___m_onActivityResultEvent_5; }
	inline OnActivityResultEventHandler_t2943688375 ** get_address_of_m_onActivityResultEvent_5() { return &___m_onActivityResultEvent_5; }
	inline void set_m_onActivityResultEvent_5(OnActivityResultEventHandler_t2943688375 * value)
	{
		___m_onActivityResultEvent_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_onActivityResultEvent_5, value);
	}

	inline static int32_t get_offset_of_m_onDisplayChangedEvent_6() { return static_cast<int32_t>(offsetof(AndroidLifecycleCallbacks_t1531251569_StaticFields, ___m_onDisplayChangedEvent_6)); }
	inline OnDisplayChangedEventHandler_t4254774819 * get_m_onDisplayChangedEvent_6() const { return ___m_onDisplayChangedEvent_6; }
	inline OnDisplayChangedEventHandler_t4254774819 ** get_address_of_m_onDisplayChangedEvent_6() { return &___m_onDisplayChangedEvent_6; }
	inline void set_m_onDisplayChangedEvent_6(OnDisplayChangedEventHandler_t4254774819 * value)
	{
		___m_onDisplayChangedEvent_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_onDisplayChangedEvent_6, value);
	}

	inline static int32_t get_offset_of_m_onRequestPermissionsResultEvent_7() { return static_cast<int32_t>(offsetof(AndroidLifecycleCallbacks_t1531251569_StaticFields, ___m_onRequestPermissionsResultEvent_7)); }
	inline OnRequestPermissionsResultHandler_t856552163 * get_m_onRequestPermissionsResultEvent_7() const { return ___m_onRequestPermissionsResultEvent_7; }
	inline OnRequestPermissionsResultHandler_t856552163 ** get_address_of_m_onRequestPermissionsResultEvent_7() { return &___m_onRequestPermissionsResultEvent_7; }
	inline void set_m_onRequestPermissionsResultEvent_7(OnRequestPermissionsResultHandler_t856552163 * value)
	{
		___m_onRequestPermissionsResultEvent_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_onRequestPermissionsResultEvent_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
