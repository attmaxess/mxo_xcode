﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoCoordinate3800473787.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoCoordinateFramePair
struct  TangoCoordinateFramePair_t3148559008 
{
public:
	// Tango.TangoEnums/TangoCoordinateFrameType Tango.TangoCoordinateFramePair::baseFrame
	int32_t ___baseFrame_0;
	// Tango.TangoEnums/TangoCoordinateFrameType Tango.TangoCoordinateFramePair::targetFrame
	int32_t ___targetFrame_1;

public:
	inline static int32_t get_offset_of_baseFrame_0() { return static_cast<int32_t>(offsetof(TangoCoordinateFramePair_t3148559008, ___baseFrame_0)); }
	inline int32_t get_baseFrame_0() const { return ___baseFrame_0; }
	inline int32_t* get_address_of_baseFrame_0() { return &___baseFrame_0; }
	inline void set_baseFrame_0(int32_t value)
	{
		___baseFrame_0 = value;
	}

	inline static int32_t get_offset_of_targetFrame_1() { return static_cast<int32_t>(offsetof(TangoCoordinateFramePair_t3148559008, ___targetFrame_1)); }
	inline int32_t get_targetFrame_1() const { return ___targetFrame_1; }
	inline int32_t* get_address_of_targetFrame_1() { return &___targetFrame_1; }
	inline void set_targetFrame_1(int32_t value)
	{
		___targetFrame_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
