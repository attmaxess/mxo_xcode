﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// TangoPointCloud
struct TangoPointCloud_t374155286;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;
// Tango.TangoApplication
struct TangoApplication_t278578959;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PointToPointGUIController
struct  PointToPointGUIController_t1162740098  : public MonoBehaviour_t1158329972
{
public:
	// TangoPointCloud PointToPointGUIController::m_pointCloud
	TangoPointCloud_t374155286 * ___m_pointCloud_6;
	// UnityEngine.LineRenderer PointToPointGUIController::m_lineRenderer
	LineRenderer_t849157671 * ___m_lineRenderer_7;
	// Tango.TangoApplication PointToPointGUIController::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_8;
	// System.Boolean PointToPointGUIController::m_waitingForDepth
	bool ___m_waitingForDepth_9;
	// UnityEngine.Vector3 PointToPointGUIController::m_startPoint
	Vector3_t2243707580  ___m_startPoint_10;
	// UnityEngine.Vector3 PointToPointGUIController::m_endPoint
	Vector3_t2243707580  ___m_endPoint_11;
	// System.Single PointToPointGUIController::m_distance
	float ___m_distance_12;
	// System.String PointToPointGUIController::m_distanceText
	String_t* ___m_distanceText_13;

public:
	inline static int32_t get_offset_of_m_pointCloud_6() { return static_cast<int32_t>(offsetof(PointToPointGUIController_t1162740098, ___m_pointCloud_6)); }
	inline TangoPointCloud_t374155286 * get_m_pointCloud_6() const { return ___m_pointCloud_6; }
	inline TangoPointCloud_t374155286 ** get_address_of_m_pointCloud_6() { return &___m_pointCloud_6; }
	inline void set_m_pointCloud_6(TangoPointCloud_t374155286 * value)
	{
		___m_pointCloud_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_pointCloud_6, value);
	}

	inline static int32_t get_offset_of_m_lineRenderer_7() { return static_cast<int32_t>(offsetof(PointToPointGUIController_t1162740098, ___m_lineRenderer_7)); }
	inline LineRenderer_t849157671 * get_m_lineRenderer_7() const { return ___m_lineRenderer_7; }
	inline LineRenderer_t849157671 ** get_address_of_m_lineRenderer_7() { return &___m_lineRenderer_7; }
	inline void set_m_lineRenderer_7(LineRenderer_t849157671 * value)
	{
		___m_lineRenderer_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_lineRenderer_7, value);
	}

	inline static int32_t get_offset_of_m_tangoApplication_8() { return static_cast<int32_t>(offsetof(PointToPointGUIController_t1162740098, ___m_tangoApplication_8)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_8() const { return ___m_tangoApplication_8; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_8() { return &___m_tangoApplication_8; }
	inline void set_m_tangoApplication_8(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_8, value);
	}

	inline static int32_t get_offset_of_m_waitingForDepth_9() { return static_cast<int32_t>(offsetof(PointToPointGUIController_t1162740098, ___m_waitingForDepth_9)); }
	inline bool get_m_waitingForDepth_9() const { return ___m_waitingForDepth_9; }
	inline bool* get_address_of_m_waitingForDepth_9() { return &___m_waitingForDepth_9; }
	inline void set_m_waitingForDepth_9(bool value)
	{
		___m_waitingForDepth_9 = value;
	}

	inline static int32_t get_offset_of_m_startPoint_10() { return static_cast<int32_t>(offsetof(PointToPointGUIController_t1162740098, ___m_startPoint_10)); }
	inline Vector3_t2243707580  get_m_startPoint_10() const { return ___m_startPoint_10; }
	inline Vector3_t2243707580 * get_address_of_m_startPoint_10() { return &___m_startPoint_10; }
	inline void set_m_startPoint_10(Vector3_t2243707580  value)
	{
		___m_startPoint_10 = value;
	}

	inline static int32_t get_offset_of_m_endPoint_11() { return static_cast<int32_t>(offsetof(PointToPointGUIController_t1162740098, ___m_endPoint_11)); }
	inline Vector3_t2243707580  get_m_endPoint_11() const { return ___m_endPoint_11; }
	inline Vector3_t2243707580 * get_address_of_m_endPoint_11() { return &___m_endPoint_11; }
	inline void set_m_endPoint_11(Vector3_t2243707580  value)
	{
		___m_endPoint_11 = value;
	}

	inline static int32_t get_offset_of_m_distance_12() { return static_cast<int32_t>(offsetof(PointToPointGUIController_t1162740098, ___m_distance_12)); }
	inline float get_m_distance_12() const { return ___m_distance_12; }
	inline float* get_address_of_m_distance_12() { return &___m_distance_12; }
	inline void set_m_distance_12(float value)
	{
		___m_distance_12 = value;
	}

	inline static int32_t get_offset_of_m_distanceText_13() { return static_cast<int32_t>(offsetof(PointToPointGUIController_t1162740098, ___m_distanceText_13)); }
	inline String_t* get_m_distanceText_13() const { return ___m_distanceText_13; }
	inline String_t** get_address_of_m_distanceText_13() { return &___m_distanceText_13; }
	inline void set_m_distanceText_13(String_t* value)
	{
		___m_distanceText_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_distanceText_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
