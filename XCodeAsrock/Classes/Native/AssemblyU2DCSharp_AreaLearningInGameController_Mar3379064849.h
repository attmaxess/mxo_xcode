﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AreaLearningInGameController/MarkerData
struct  MarkerData_t3379064849  : public Il2CppObject
{
public:
	// System.Int32 AreaLearningInGameController/MarkerData::m_type
	int32_t ___m_type_0;
	// UnityEngine.Vector3 AreaLearningInGameController/MarkerData::m_position
	Vector3_t2243707580  ___m_position_1;
	// UnityEngine.Quaternion AreaLearningInGameController/MarkerData::m_orientation
	Quaternion_t4030073918  ___m_orientation_2;

public:
	inline static int32_t get_offset_of_m_type_0() { return static_cast<int32_t>(offsetof(MarkerData_t3379064849, ___m_type_0)); }
	inline int32_t get_m_type_0() const { return ___m_type_0; }
	inline int32_t* get_address_of_m_type_0() { return &___m_type_0; }
	inline void set_m_type_0(int32_t value)
	{
		___m_type_0 = value;
	}

	inline static int32_t get_offset_of_m_position_1() { return static_cast<int32_t>(offsetof(MarkerData_t3379064849, ___m_position_1)); }
	inline Vector3_t2243707580  get_m_position_1() const { return ___m_position_1; }
	inline Vector3_t2243707580 * get_address_of_m_position_1() { return &___m_position_1; }
	inline void set_m_position_1(Vector3_t2243707580  value)
	{
		___m_position_1 = value;
	}

	inline static int32_t get_offset_of_m_orientation_2() { return static_cast<int32_t>(offsetof(MarkerData_t3379064849, ___m_orientation_2)); }
	inline Quaternion_t4030073918  get_m_orientation_2() const { return ___m_orientation_2; }
	inline Quaternion_t4030073918 * get_address_of_m_orientation_2() { return &___m_orientation_2; }
	inline void set_m_orientation_2(Quaternion_t4030073918  value)
	{
		___m_orientation_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
