﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Net.Sockets.UdpClient
struct UdpClient_t1278197702;
// System.Net.IPEndPoint
struct IPEndPoint_t2615413766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharingBroadcast
struct  SharingBroadcast_t3699410277  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject SharingBroadcast::qrcodePlane
	GameObject_t1756533147 * ___qrcodePlane_2;
	// System.Net.Sockets.UdpClient SharingBroadcast::udpBroadcast
	UdpClient_t1278197702 * ___udpBroadcast_3;

public:
	inline static int32_t get_offset_of_qrcodePlane_2() { return static_cast<int32_t>(offsetof(SharingBroadcast_t3699410277, ___qrcodePlane_2)); }
	inline GameObject_t1756533147 * get_qrcodePlane_2() const { return ___qrcodePlane_2; }
	inline GameObject_t1756533147 ** get_address_of_qrcodePlane_2() { return &___qrcodePlane_2; }
	inline void set_qrcodePlane_2(GameObject_t1756533147 * value)
	{
		___qrcodePlane_2 = value;
		Il2CppCodeGenWriteBarrier(&___qrcodePlane_2, value);
	}

	inline static int32_t get_offset_of_udpBroadcast_3() { return static_cast<int32_t>(offsetof(SharingBroadcast_t3699410277, ___udpBroadcast_3)); }
	inline UdpClient_t1278197702 * get_udpBroadcast_3() const { return ___udpBroadcast_3; }
	inline UdpClient_t1278197702 ** get_address_of_udpBroadcast_3() { return &___udpBroadcast_3; }
	inline void set_udpBroadcast_3(UdpClient_t1278197702 * value)
	{
		___udpBroadcast_3 = value;
		Il2CppCodeGenWriteBarrier(&___udpBroadcast_3, value);
	}
};

struct SharingBroadcast_t3699410277_StaticFields
{
public:
	// System.Net.IPEndPoint SharingBroadcast::udpEndPoint
	IPEndPoint_t2615413766 * ___udpEndPoint_4;

public:
	inline static int32_t get_offset_of_udpEndPoint_4() { return static_cast<int32_t>(offsetof(SharingBroadcast_t3699410277_StaticFields, ___udpEndPoint_4)); }
	inline IPEndPoint_t2615413766 * get_udpEndPoint_4() const { return ___udpEndPoint_4; }
	inline IPEndPoint_t2615413766 ** get_address_of_udpEndPoint_4() { return &___udpEndPoint_4; }
	inline void set_udpEndPoint_4(IPEndPoint_t2615413766 * value)
	{
		___udpEndPoint_4 = value;
		Il2CppCodeGenWriteBarrier(&___udpEndPoint_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
