﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// Tango.OnAreaDescriptionImportEventHandler
struct OnAreaDescriptionImportEventHandler_t117915755;
// Tango.OnAreaDescriptionExportEventHandler
struct OnAreaDescriptionExportEventHandler_t795586882;
// OnActivityResultEventHandler
struct OnActivityResultEventHandler_t2943688375;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.AreaDescriptionEventListener
struct  AreaDescriptionEventListener_t1921626273  : public Il2CppObject
{
public:

public:
};

struct AreaDescriptionEventListener_t1921626273_StaticFields
{
public:
	// System.Object Tango.AreaDescriptionEventListener::m_lockObject
	Il2CppObject * ___m_lockObject_0;
	// System.Boolean Tango.AreaDescriptionEventListener::m_isCallbackSet
	bool ___m_isCallbackSet_1;
	// System.Boolean Tango.AreaDescriptionEventListener::m_isImportFinished
	bool ___m_isImportFinished_2;
	// System.Boolean Tango.AreaDescriptionEventListener::m_isExportFinished
	bool ___m_isExportFinished_3;
	// System.String Tango.AreaDescriptionEventListener::m_eventString
	String_t* ___m_eventString_4;
	// System.Boolean Tango.AreaDescriptionEventListener::m_isSuccessful
	bool ___m_isSuccessful_5;
	// Tango.OnAreaDescriptionImportEventHandler Tango.AreaDescriptionEventListener::m_onTangoAreaDescriptionImported
	OnAreaDescriptionImportEventHandler_t117915755 * ___m_onTangoAreaDescriptionImported_6;
	// Tango.OnAreaDescriptionExportEventHandler Tango.AreaDescriptionEventListener::m_onTangoAreaDescriptionExported
	OnAreaDescriptionExportEventHandler_t795586882 * ___m_onTangoAreaDescriptionExported_7;
	// OnActivityResultEventHandler Tango.AreaDescriptionEventListener::<>f__mg$cache0
	OnActivityResultEventHandler_t2943688375 * ___U3CU3Ef__mgU24cache0_8;
	// OnActivityResultEventHandler Tango.AreaDescriptionEventListener::<>f__mg$cache1
	OnActivityResultEventHandler_t2943688375 * ___U3CU3Ef__mgU24cache1_9;

public:
	inline static int32_t get_offset_of_m_lockObject_0() { return static_cast<int32_t>(offsetof(AreaDescriptionEventListener_t1921626273_StaticFields, ___m_lockObject_0)); }
	inline Il2CppObject * get_m_lockObject_0() const { return ___m_lockObject_0; }
	inline Il2CppObject ** get_address_of_m_lockObject_0() { return &___m_lockObject_0; }
	inline void set_m_lockObject_0(Il2CppObject * value)
	{
		___m_lockObject_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_lockObject_0, value);
	}

	inline static int32_t get_offset_of_m_isCallbackSet_1() { return static_cast<int32_t>(offsetof(AreaDescriptionEventListener_t1921626273_StaticFields, ___m_isCallbackSet_1)); }
	inline bool get_m_isCallbackSet_1() const { return ___m_isCallbackSet_1; }
	inline bool* get_address_of_m_isCallbackSet_1() { return &___m_isCallbackSet_1; }
	inline void set_m_isCallbackSet_1(bool value)
	{
		___m_isCallbackSet_1 = value;
	}

	inline static int32_t get_offset_of_m_isImportFinished_2() { return static_cast<int32_t>(offsetof(AreaDescriptionEventListener_t1921626273_StaticFields, ___m_isImportFinished_2)); }
	inline bool get_m_isImportFinished_2() const { return ___m_isImportFinished_2; }
	inline bool* get_address_of_m_isImportFinished_2() { return &___m_isImportFinished_2; }
	inline void set_m_isImportFinished_2(bool value)
	{
		___m_isImportFinished_2 = value;
	}

	inline static int32_t get_offset_of_m_isExportFinished_3() { return static_cast<int32_t>(offsetof(AreaDescriptionEventListener_t1921626273_StaticFields, ___m_isExportFinished_3)); }
	inline bool get_m_isExportFinished_3() const { return ___m_isExportFinished_3; }
	inline bool* get_address_of_m_isExportFinished_3() { return &___m_isExportFinished_3; }
	inline void set_m_isExportFinished_3(bool value)
	{
		___m_isExportFinished_3 = value;
	}

	inline static int32_t get_offset_of_m_eventString_4() { return static_cast<int32_t>(offsetof(AreaDescriptionEventListener_t1921626273_StaticFields, ___m_eventString_4)); }
	inline String_t* get_m_eventString_4() const { return ___m_eventString_4; }
	inline String_t** get_address_of_m_eventString_4() { return &___m_eventString_4; }
	inline void set_m_eventString_4(String_t* value)
	{
		___m_eventString_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_eventString_4, value);
	}

	inline static int32_t get_offset_of_m_isSuccessful_5() { return static_cast<int32_t>(offsetof(AreaDescriptionEventListener_t1921626273_StaticFields, ___m_isSuccessful_5)); }
	inline bool get_m_isSuccessful_5() const { return ___m_isSuccessful_5; }
	inline bool* get_address_of_m_isSuccessful_5() { return &___m_isSuccessful_5; }
	inline void set_m_isSuccessful_5(bool value)
	{
		___m_isSuccessful_5 = value;
	}

	inline static int32_t get_offset_of_m_onTangoAreaDescriptionImported_6() { return static_cast<int32_t>(offsetof(AreaDescriptionEventListener_t1921626273_StaticFields, ___m_onTangoAreaDescriptionImported_6)); }
	inline OnAreaDescriptionImportEventHandler_t117915755 * get_m_onTangoAreaDescriptionImported_6() const { return ___m_onTangoAreaDescriptionImported_6; }
	inline OnAreaDescriptionImportEventHandler_t117915755 ** get_address_of_m_onTangoAreaDescriptionImported_6() { return &___m_onTangoAreaDescriptionImported_6; }
	inline void set_m_onTangoAreaDescriptionImported_6(OnAreaDescriptionImportEventHandler_t117915755 * value)
	{
		___m_onTangoAreaDescriptionImported_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_onTangoAreaDescriptionImported_6, value);
	}

	inline static int32_t get_offset_of_m_onTangoAreaDescriptionExported_7() { return static_cast<int32_t>(offsetof(AreaDescriptionEventListener_t1921626273_StaticFields, ___m_onTangoAreaDescriptionExported_7)); }
	inline OnAreaDescriptionExportEventHandler_t795586882 * get_m_onTangoAreaDescriptionExported_7() const { return ___m_onTangoAreaDescriptionExported_7; }
	inline OnAreaDescriptionExportEventHandler_t795586882 ** get_address_of_m_onTangoAreaDescriptionExported_7() { return &___m_onTangoAreaDescriptionExported_7; }
	inline void set_m_onTangoAreaDescriptionExported_7(OnAreaDescriptionExportEventHandler_t795586882 * value)
	{
		___m_onTangoAreaDescriptionExported_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_onTangoAreaDescriptionExported_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_8() { return static_cast<int32_t>(offsetof(AreaDescriptionEventListener_t1921626273_StaticFields, ___U3CU3Ef__mgU24cache0_8)); }
	inline OnActivityResultEventHandler_t2943688375 * get_U3CU3Ef__mgU24cache0_8() const { return ___U3CU3Ef__mgU24cache0_8; }
	inline OnActivityResultEventHandler_t2943688375 ** get_address_of_U3CU3Ef__mgU24cache0_8() { return &___U3CU3Ef__mgU24cache0_8; }
	inline void set_U3CU3Ef__mgU24cache0_8(OnActivityResultEventHandler_t2943688375 * value)
	{
		___U3CU3Ef__mgU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_9() { return static_cast<int32_t>(offsetof(AreaDescriptionEventListener_t1921626273_StaticFields, ___U3CU3Ef__mgU24cache1_9)); }
	inline OnActivityResultEventHandler_t2943688375 * get_U3CU3Ef__mgU24cache1_9() const { return ___U3CU3Ef__mgU24cache1_9; }
	inline OnActivityResultEventHandler_t2943688375 ** get_address_of_U3CU3Ef__mgU24cache1_9() { return &___U3CU3Ef__mgU24cache1_9; }
	inline void set_U3CU3Ef__mgU24cache1_9(OnActivityResultEventHandler_t2943688375 * value)
	{
		___U3CU3Ef__mgU24cache1_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache1_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
