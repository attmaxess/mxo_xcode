﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Tango_DMatrix4x443123165.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"

// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// Tango.TangoApplication
struct TangoApplication_t278578959;
// Tango.TangoCameraIntrinsics
struct TangoCameraIntrinsics_t464275052;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.Renderer
struct Renderer_t257310565;
// TangoDeltaPoseController
struct TangoDeltaPoseController_t1881166960;
// System.Collections.Generic.Dictionary`2<System.Single,System.Int32>
struct Dictionary_2_t2921398135;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;
// Tango.TangoPointCloudData
struct TangoPointCloudData_t511956092;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TangoPointCloud
struct  TangoPointCloud_t374155286  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TangoPointCloud::m_useAreaDescriptionPose
	bool ___m_useAreaDescriptionPose_2;
	// System.Boolean TangoPointCloud::m_updatePointsMesh
	bool ___m_updatePointsMesh_3;
	// UnityEngine.Vector3[] TangoPointCloud::m_points
	Vector3U5BU5D_t1172311765* ___m_points_4;
	// System.Int32 TangoPointCloud::m_pointsCount
	int32_t ___m_pointsCount_5;
	// System.Double TangoPointCloud::m_depthTimestamp
	double ___m_depthTimestamp_6;
	// System.Single TangoPointCloud::m_overallZ
	float ___m_overallZ_7;
	// System.Single TangoPointCloud::m_depthDeltaTime
	float ___m_depthDeltaTime_8;
	// System.Single TangoPointCloud::m_floorPlaneY
	float ___m_floorPlaneY_9;
	// System.Boolean TangoPointCloud::m_floorFound
	bool ___m_floorFound_10;
	// Tango.TangoApplication TangoPointCloud::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_15;
	// Tango.DMatrix4x4 TangoPointCloud::m_imuTDevice
	DMatrix4x4_t43123165  ___m_imuTDevice_16;
	// Tango.DMatrix4x4 TangoPointCloud::m_imuTDepthCamera
	DMatrix4x4_t43123165  ___m_imuTDepthCamera_17;
	// Tango.DMatrix4x4 TangoPointCloud::m_deviceTDepthCamera
	DMatrix4x4_t43123165  ___m_deviceTDepthCamera_18;
	// Tango.TangoCameraIntrinsics TangoPointCloud::m_colorCameraIntrinsics
	TangoCameraIntrinsics_t464275052 * ___m_colorCameraIntrinsics_19;
	// System.Boolean TangoPointCloud::m_cameraDataSetUp
	bool ___m_cameraDataSetUp_20;
	// UnityEngine.Mesh TangoPointCloud::m_mesh
	Mesh_t1356156583 * ___m_mesh_21;
	// UnityEngine.Renderer TangoPointCloud::m_renderer
	Renderer_t257310565 * ___m_renderer_22;
	// TangoDeltaPoseController TangoPointCloud::m_tangoDeltaPoseController
	TangoDeltaPoseController_t1881166960 * ___m_tangoDeltaPoseController_23;
	// System.Boolean TangoPointCloud::m_findFloorWithDepth
	bool ___m_findFloorWithDepth_24;
	// System.Collections.Generic.Dictionary`2<System.Single,System.Int32> TangoPointCloud::m_numPointsAtY
	Dictionary_2_t2921398135 * ___m_numPointsAtY_25;
	// System.Collections.Generic.List`1<System.Single> TangoPointCloud::m_nonNoiseBuckets
	List_1_t1445631064 * ___m_nonNoiseBuckets_26;
	// Tango.TangoPointCloudData TangoPointCloud::m_mostRecentPointCloud
	TangoPointCloudData_t511956092 * ___m_mostRecentPointCloud_27;
	// UnityEngine.Matrix4x4 TangoPointCloud::m_mostRecentUnityWorldTDepthCamera
	Matrix4x4_t2933234003  ___m_mostRecentUnityWorldTDepthCamera_28;

public:
	inline static int32_t get_offset_of_m_useAreaDescriptionPose_2() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_useAreaDescriptionPose_2)); }
	inline bool get_m_useAreaDescriptionPose_2() const { return ___m_useAreaDescriptionPose_2; }
	inline bool* get_address_of_m_useAreaDescriptionPose_2() { return &___m_useAreaDescriptionPose_2; }
	inline void set_m_useAreaDescriptionPose_2(bool value)
	{
		___m_useAreaDescriptionPose_2 = value;
	}

	inline static int32_t get_offset_of_m_updatePointsMesh_3() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_updatePointsMesh_3)); }
	inline bool get_m_updatePointsMesh_3() const { return ___m_updatePointsMesh_3; }
	inline bool* get_address_of_m_updatePointsMesh_3() { return &___m_updatePointsMesh_3; }
	inline void set_m_updatePointsMesh_3(bool value)
	{
		___m_updatePointsMesh_3 = value;
	}

	inline static int32_t get_offset_of_m_points_4() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_points_4)); }
	inline Vector3U5BU5D_t1172311765* get_m_points_4() const { return ___m_points_4; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_points_4() { return &___m_points_4; }
	inline void set_m_points_4(Vector3U5BU5D_t1172311765* value)
	{
		___m_points_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_points_4, value);
	}

	inline static int32_t get_offset_of_m_pointsCount_5() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_pointsCount_5)); }
	inline int32_t get_m_pointsCount_5() const { return ___m_pointsCount_5; }
	inline int32_t* get_address_of_m_pointsCount_5() { return &___m_pointsCount_5; }
	inline void set_m_pointsCount_5(int32_t value)
	{
		___m_pointsCount_5 = value;
	}

	inline static int32_t get_offset_of_m_depthTimestamp_6() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_depthTimestamp_6)); }
	inline double get_m_depthTimestamp_6() const { return ___m_depthTimestamp_6; }
	inline double* get_address_of_m_depthTimestamp_6() { return &___m_depthTimestamp_6; }
	inline void set_m_depthTimestamp_6(double value)
	{
		___m_depthTimestamp_6 = value;
	}

	inline static int32_t get_offset_of_m_overallZ_7() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_overallZ_7)); }
	inline float get_m_overallZ_7() const { return ___m_overallZ_7; }
	inline float* get_address_of_m_overallZ_7() { return &___m_overallZ_7; }
	inline void set_m_overallZ_7(float value)
	{
		___m_overallZ_7 = value;
	}

	inline static int32_t get_offset_of_m_depthDeltaTime_8() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_depthDeltaTime_8)); }
	inline float get_m_depthDeltaTime_8() const { return ___m_depthDeltaTime_8; }
	inline float* get_address_of_m_depthDeltaTime_8() { return &___m_depthDeltaTime_8; }
	inline void set_m_depthDeltaTime_8(float value)
	{
		___m_depthDeltaTime_8 = value;
	}

	inline static int32_t get_offset_of_m_floorPlaneY_9() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_floorPlaneY_9)); }
	inline float get_m_floorPlaneY_9() const { return ___m_floorPlaneY_9; }
	inline float* get_address_of_m_floorPlaneY_9() { return &___m_floorPlaneY_9; }
	inline void set_m_floorPlaneY_9(float value)
	{
		___m_floorPlaneY_9 = value;
	}

	inline static int32_t get_offset_of_m_floorFound_10() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_floorFound_10)); }
	inline bool get_m_floorFound_10() const { return ___m_floorFound_10; }
	inline bool* get_address_of_m_floorFound_10() { return &___m_floorFound_10; }
	inline void set_m_floorFound_10(bool value)
	{
		___m_floorFound_10 = value;
	}

	inline static int32_t get_offset_of_m_tangoApplication_15() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_tangoApplication_15)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_15() const { return ___m_tangoApplication_15; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_15() { return &___m_tangoApplication_15; }
	inline void set_m_tangoApplication_15(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_15, value);
	}

	inline static int32_t get_offset_of_m_imuTDevice_16() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_imuTDevice_16)); }
	inline DMatrix4x4_t43123165  get_m_imuTDevice_16() const { return ___m_imuTDevice_16; }
	inline DMatrix4x4_t43123165 * get_address_of_m_imuTDevice_16() { return &___m_imuTDevice_16; }
	inline void set_m_imuTDevice_16(DMatrix4x4_t43123165  value)
	{
		___m_imuTDevice_16 = value;
	}

	inline static int32_t get_offset_of_m_imuTDepthCamera_17() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_imuTDepthCamera_17)); }
	inline DMatrix4x4_t43123165  get_m_imuTDepthCamera_17() const { return ___m_imuTDepthCamera_17; }
	inline DMatrix4x4_t43123165 * get_address_of_m_imuTDepthCamera_17() { return &___m_imuTDepthCamera_17; }
	inline void set_m_imuTDepthCamera_17(DMatrix4x4_t43123165  value)
	{
		___m_imuTDepthCamera_17 = value;
	}

	inline static int32_t get_offset_of_m_deviceTDepthCamera_18() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_deviceTDepthCamera_18)); }
	inline DMatrix4x4_t43123165  get_m_deviceTDepthCamera_18() const { return ___m_deviceTDepthCamera_18; }
	inline DMatrix4x4_t43123165 * get_address_of_m_deviceTDepthCamera_18() { return &___m_deviceTDepthCamera_18; }
	inline void set_m_deviceTDepthCamera_18(DMatrix4x4_t43123165  value)
	{
		___m_deviceTDepthCamera_18 = value;
	}

	inline static int32_t get_offset_of_m_colorCameraIntrinsics_19() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_colorCameraIntrinsics_19)); }
	inline TangoCameraIntrinsics_t464275052 * get_m_colorCameraIntrinsics_19() const { return ___m_colorCameraIntrinsics_19; }
	inline TangoCameraIntrinsics_t464275052 ** get_address_of_m_colorCameraIntrinsics_19() { return &___m_colorCameraIntrinsics_19; }
	inline void set_m_colorCameraIntrinsics_19(TangoCameraIntrinsics_t464275052 * value)
	{
		___m_colorCameraIntrinsics_19 = value;
		Il2CppCodeGenWriteBarrier(&___m_colorCameraIntrinsics_19, value);
	}

	inline static int32_t get_offset_of_m_cameraDataSetUp_20() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_cameraDataSetUp_20)); }
	inline bool get_m_cameraDataSetUp_20() const { return ___m_cameraDataSetUp_20; }
	inline bool* get_address_of_m_cameraDataSetUp_20() { return &___m_cameraDataSetUp_20; }
	inline void set_m_cameraDataSetUp_20(bool value)
	{
		___m_cameraDataSetUp_20 = value;
	}

	inline static int32_t get_offset_of_m_mesh_21() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_mesh_21)); }
	inline Mesh_t1356156583 * get_m_mesh_21() const { return ___m_mesh_21; }
	inline Mesh_t1356156583 ** get_address_of_m_mesh_21() { return &___m_mesh_21; }
	inline void set_m_mesh_21(Mesh_t1356156583 * value)
	{
		___m_mesh_21 = value;
		Il2CppCodeGenWriteBarrier(&___m_mesh_21, value);
	}

	inline static int32_t get_offset_of_m_renderer_22() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_renderer_22)); }
	inline Renderer_t257310565 * get_m_renderer_22() const { return ___m_renderer_22; }
	inline Renderer_t257310565 ** get_address_of_m_renderer_22() { return &___m_renderer_22; }
	inline void set_m_renderer_22(Renderer_t257310565 * value)
	{
		___m_renderer_22 = value;
		Il2CppCodeGenWriteBarrier(&___m_renderer_22, value);
	}

	inline static int32_t get_offset_of_m_tangoDeltaPoseController_23() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_tangoDeltaPoseController_23)); }
	inline TangoDeltaPoseController_t1881166960 * get_m_tangoDeltaPoseController_23() const { return ___m_tangoDeltaPoseController_23; }
	inline TangoDeltaPoseController_t1881166960 ** get_address_of_m_tangoDeltaPoseController_23() { return &___m_tangoDeltaPoseController_23; }
	inline void set_m_tangoDeltaPoseController_23(TangoDeltaPoseController_t1881166960 * value)
	{
		___m_tangoDeltaPoseController_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoDeltaPoseController_23, value);
	}

	inline static int32_t get_offset_of_m_findFloorWithDepth_24() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_findFloorWithDepth_24)); }
	inline bool get_m_findFloorWithDepth_24() const { return ___m_findFloorWithDepth_24; }
	inline bool* get_address_of_m_findFloorWithDepth_24() { return &___m_findFloorWithDepth_24; }
	inline void set_m_findFloorWithDepth_24(bool value)
	{
		___m_findFloorWithDepth_24 = value;
	}

	inline static int32_t get_offset_of_m_numPointsAtY_25() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_numPointsAtY_25)); }
	inline Dictionary_2_t2921398135 * get_m_numPointsAtY_25() const { return ___m_numPointsAtY_25; }
	inline Dictionary_2_t2921398135 ** get_address_of_m_numPointsAtY_25() { return &___m_numPointsAtY_25; }
	inline void set_m_numPointsAtY_25(Dictionary_2_t2921398135 * value)
	{
		___m_numPointsAtY_25 = value;
		Il2CppCodeGenWriteBarrier(&___m_numPointsAtY_25, value);
	}

	inline static int32_t get_offset_of_m_nonNoiseBuckets_26() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_nonNoiseBuckets_26)); }
	inline List_1_t1445631064 * get_m_nonNoiseBuckets_26() const { return ___m_nonNoiseBuckets_26; }
	inline List_1_t1445631064 ** get_address_of_m_nonNoiseBuckets_26() { return &___m_nonNoiseBuckets_26; }
	inline void set_m_nonNoiseBuckets_26(List_1_t1445631064 * value)
	{
		___m_nonNoiseBuckets_26 = value;
		Il2CppCodeGenWriteBarrier(&___m_nonNoiseBuckets_26, value);
	}

	inline static int32_t get_offset_of_m_mostRecentPointCloud_27() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_mostRecentPointCloud_27)); }
	inline TangoPointCloudData_t511956092 * get_m_mostRecentPointCloud_27() const { return ___m_mostRecentPointCloud_27; }
	inline TangoPointCloudData_t511956092 ** get_address_of_m_mostRecentPointCloud_27() { return &___m_mostRecentPointCloud_27; }
	inline void set_m_mostRecentPointCloud_27(TangoPointCloudData_t511956092 * value)
	{
		___m_mostRecentPointCloud_27 = value;
		Il2CppCodeGenWriteBarrier(&___m_mostRecentPointCloud_27, value);
	}

	inline static int32_t get_offset_of_m_mostRecentUnityWorldTDepthCamera_28() { return static_cast<int32_t>(offsetof(TangoPointCloud_t374155286, ___m_mostRecentUnityWorldTDepthCamera_28)); }
	inline Matrix4x4_t2933234003  get_m_mostRecentUnityWorldTDepthCamera_28() const { return ___m_mostRecentUnityWorldTDepthCamera_28; }
	inline Matrix4x4_t2933234003 * get_address_of_m_mostRecentUnityWorldTDepthCamera_28() { return &___m_mostRecentUnityWorldTDepthCamera_28; }
	inline void set_m_mostRecentUnityWorldTDepthCamera_28(Matrix4x4_t2933234003  value)
	{
		___m_mostRecentUnityWorldTDepthCamera_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
