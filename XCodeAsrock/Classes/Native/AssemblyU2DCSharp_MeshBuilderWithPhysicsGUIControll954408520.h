﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Tango.TangoApplication
struct TangoApplication_t278578959;
// TangoDynamicMesh
struct TangoDynamicMesh_t714838395;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeshBuilderWithPhysicsGUIController
struct  MeshBuilderWithPhysicsGUIController_t954408520  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean MeshBuilderWithPhysicsGUIController::m_enableSelectiveMeshing
	bool ___m_enableSelectiveMeshing_2;
	// System.Boolean MeshBuilderWithPhysicsGUIController::m_isEnabled
	bool ___m_isEnabled_3;
	// Tango.TangoApplication MeshBuilderWithPhysicsGUIController::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_4;
	// TangoDynamicMesh MeshBuilderWithPhysicsGUIController::m_dynamicMesh
	TangoDynamicMesh_t714838395 * ___m_dynamicMesh_5;

public:
	inline static int32_t get_offset_of_m_enableSelectiveMeshing_2() { return static_cast<int32_t>(offsetof(MeshBuilderWithPhysicsGUIController_t954408520, ___m_enableSelectiveMeshing_2)); }
	inline bool get_m_enableSelectiveMeshing_2() const { return ___m_enableSelectiveMeshing_2; }
	inline bool* get_address_of_m_enableSelectiveMeshing_2() { return &___m_enableSelectiveMeshing_2; }
	inline void set_m_enableSelectiveMeshing_2(bool value)
	{
		___m_enableSelectiveMeshing_2 = value;
	}

	inline static int32_t get_offset_of_m_isEnabled_3() { return static_cast<int32_t>(offsetof(MeshBuilderWithPhysicsGUIController_t954408520, ___m_isEnabled_3)); }
	inline bool get_m_isEnabled_3() const { return ___m_isEnabled_3; }
	inline bool* get_address_of_m_isEnabled_3() { return &___m_isEnabled_3; }
	inline void set_m_isEnabled_3(bool value)
	{
		___m_isEnabled_3 = value;
	}

	inline static int32_t get_offset_of_m_tangoApplication_4() { return static_cast<int32_t>(offsetof(MeshBuilderWithPhysicsGUIController_t954408520, ___m_tangoApplication_4)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_4() const { return ___m_tangoApplication_4; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_4() { return &___m_tangoApplication_4; }
	inline void set_m_tangoApplication_4(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_4, value);
	}

	inline static int32_t get_offset_of_m_dynamicMesh_5() { return static_cast<int32_t>(offsetof(MeshBuilderWithPhysicsGUIController_t954408520, ___m_dynamicMesh_5)); }
	inline TangoDynamicMesh_t714838395 * get_m_dynamicMesh_5() const { return ___m_dynamicMesh_5; }
	inline TangoDynamicMesh_t714838395 ** get_address_of_m_dynamicMesh_5() { return &___m_dynamicMesh_5; }
	inline void set_m_dynamicMesh_5(TangoDynamicMesh_t714838395 * value)
	{
		___m_dynamicMesh_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_dynamicMesh_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
