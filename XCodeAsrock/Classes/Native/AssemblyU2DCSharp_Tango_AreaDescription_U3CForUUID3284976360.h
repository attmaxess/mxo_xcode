﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.AreaDescription/<ForUUID>c__AnonStorey0
struct  U3CForUUIDU3Ec__AnonStorey0_t3284976360  : public Il2CppObject
{
public:
	// System.String Tango.AreaDescription/<ForUUID>c__AnonStorey0::uuid
	String_t* ___uuid_0;

public:
	inline static int32_t get_offset_of_uuid_0() { return static_cast<int32_t>(offsetof(U3CForUUIDU3Ec__AnonStorey0_t3284976360, ___uuid_0)); }
	inline String_t* get_uuid_0() const { return ___uuid_0; }
	inline String_t** get_address_of_uuid_0() { return &___uuid_0; }
	inline void set_uuid_0(String_t* value)
	{
		___uuid_0 = value;
		Il2CppCodeGenWriteBarrier(&___uuid_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
