﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_TangoPoseController_BaseFrameSele171456315.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"

// TangoARScreen
struct TangoARScreen_t2366305298;
// UnityEngine.CharacterController
struct CharacterController_t4094781467;
// Tango.TangoApplication
struct TangoApplication_t278578959;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TangoPoseController
struct  TangoPoseController_t4427816  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TangoPoseController::m_clutchEnabled
	bool ___m_clutchEnabled_2;
	// TangoPoseController/BaseFrameSelectionModeEnum TangoPoseController::m_baseFrameMode
	int32_t ___m_baseFrameMode_3;
	// TangoARScreen TangoPoseController::m_tangoARScreen
	TangoARScreen_t2366305298 * ___m_tangoARScreen_4;
	// UnityEngine.CharacterController TangoPoseController::m_characterController
	CharacterController_t4094781467 * ___m_characterController_5;
	// UnityEngine.Matrix4x4 TangoPoseController::m_unityWorld_T_unityCamera
	Matrix4x4_t2933234003  ___m_unityWorld_T_unityCamera_6;
	// UnityEngine.Matrix4x4 TangoPoseController::m_unityWorldTransformOffset_T_unityWorld
	Matrix4x4_t2933234003  ___m_unityWorldTransformOffset_T_unityWorld_7;
	// Tango.TangoApplication TangoPoseController::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_8;
	// System.Double TangoPoseController::<LastPoseTimestamp>k__BackingField
	double ___U3CLastPoseTimestampU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_m_clutchEnabled_2() { return static_cast<int32_t>(offsetof(TangoPoseController_t4427816, ___m_clutchEnabled_2)); }
	inline bool get_m_clutchEnabled_2() const { return ___m_clutchEnabled_2; }
	inline bool* get_address_of_m_clutchEnabled_2() { return &___m_clutchEnabled_2; }
	inline void set_m_clutchEnabled_2(bool value)
	{
		___m_clutchEnabled_2 = value;
	}

	inline static int32_t get_offset_of_m_baseFrameMode_3() { return static_cast<int32_t>(offsetof(TangoPoseController_t4427816, ___m_baseFrameMode_3)); }
	inline int32_t get_m_baseFrameMode_3() const { return ___m_baseFrameMode_3; }
	inline int32_t* get_address_of_m_baseFrameMode_3() { return &___m_baseFrameMode_3; }
	inline void set_m_baseFrameMode_3(int32_t value)
	{
		___m_baseFrameMode_3 = value;
	}

	inline static int32_t get_offset_of_m_tangoARScreen_4() { return static_cast<int32_t>(offsetof(TangoPoseController_t4427816, ___m_tangoARScreen_4)); }
	inline TangoARScreen_t2366305298 * get_m_tangoARScreen_4() const { return ___m_tangoARScreen_4; }
	inline TangoARScreen_t2366305298 ** get_address_of_m_tangoARScreen_4() { return &___m_tangoARScreen_4; }
	inline void set_m_tangoARScreen_4(TangoARScreen_t2366305298 * value)
	{
		___m_tangoARScreen_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoARScreen_4, value);
	}

	inline static int32_t get_offset_of_m_characterController_5() { return static_cast<int32_t>(offsetof(TangoPoseController_t4427816, ___m_characterController_5)); }
	inline CharacterController_t4094781467 * get_m_characterController_5() const { return ___m_characterController_5; }
	inline CharacterController_t4094781467 ** get_address_of_m_characterController_5() { return &___m_characterController_5; }
	inline void set_m_characterController_5(CharacterController_t4094781467 * value)
	{
		___m_characterController_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_characterController_5, value);
	}

	inline static int32_t get_offset_of_m_unityWorld_T_unityCamera_6() { return static_cast<int32_t>(offsetof(TangoPoseController_t4427816, ___m_unityWorld_T_unityCamera_6)); }
	inline Matrix4x4_t2933234003  get_m_unityWorld_T_unityCamera_6() const { return ___m_unityWorld_T_unityCamera_6; }
	inline Matrix4x4_t2933234003 * get_address_of_m_unityWorld_T_unityCamera_6() { return &___m_unityWorld_T_unityCamera_6; }
	inline void set_m_unityWorld_T_unityCamera_6(Matrix4x4_t2933234003  value)
	{
		___m_unityWorld_T_unityCamera_6 = value;
	}

	inline static int32_t get_offset_of_m_unityWorldTransformOffset_T_unityWorld_7() { return static_cast<int32_t>(offsetof(TangoPoseController_t4427816, ___m_unityWorldTransformOffset_T_unityWorld_7)); }
	inline Matrix4x4_t2933234003  get_m_unityWorldTransformOffset_T_unityWorld_7() const { return ___m_unityWorldTransformOffset_T_unityWorld_7; }
	inline Matrix4x4_t2933234003 * get_address_of_m_unityWorldTransformOffset_T_unityWorld_7() { return &___m_unityWorldTransformOffset_T_unityWorld_7; }
	inline void set_m_unityWorldTransformOffset_T_unityWorld_7(Matrix4x4_t2933234003  value)
	{
		___m_unityWorldTransformOffset_T_unityWorld_7 = value;
	}

	inline static int32_t get_offset_of_m_tangoApplication_8() { return static_cast<int32_t>(offsetof(TangoPoseController_t4427816, ___m_tangoApplication_8)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_8() const { return ___m_tangoApplication_8; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_8() { return &___m_tangoApplication_8; }
	inline void set_m_tangoApplication_8(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_8, value);
	}

	inline static int32_t get_offset_of_U3CLastPoseTimestampU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TangoPoseController_t4427816, ___U3CLastPoseTimestampU3Ek__BackingField_9)); }
	inline double get_U3CLastPoseTimestampU3Ek__BackingField_9() const { return ___U3CLastPoseTimestampU3Ek__BackingField_9; }
	inline double* get_address_of_U3CLastPoseTimestampU3Ek__BackingField_9() { return &___U3CLastPoseTimestampU3Ek__BackingField_9; }
	inline void set_U3CLastPoseTimestampU3Ek__BackingField_9(double value)
	{
		___U3CLastPoseTimestampU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
