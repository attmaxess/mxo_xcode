﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Canvas
struct Canvas_t209405766;
// TangoPointCloud
struct TangoPointCloud_t374155286;
// Tango.TangoApplication
struct TangoApplication_t278578959;
// TangoARPoseController
struct TangoARPoseController_t3131628351;
// ARCameraPostProcess
struct ARCameraPostProcess_t3075820819;
// ARMarker
struct ARMarker_t1554260723;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARGUIController
struct  ARGUIController_t3357238990  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ARGUIController::m_prefabMarker
	GameObject_t1756533147 * ___m_prefabMarker_28;
	// UnityEngine.RectTransform ARGUIController::m_prefabTouchEffect
	RectTransform_t3349966182 * ___m_prefabTouchEffect_29;
	// UnityEngine.Canvas ARGUIController::m_canvas
	Canvas_t209405766 * ___m_canvas_30;
	// TangoPointCloud ARGUIController::m_pointCloud
	TangoPointCloud_t374155286 * ___m_pointCloud_31;
	// System.String ARGUIController::m_fpsText
	String_t* ___m_fpsText_33;
	// System.Int32 ARGUIController::m_currentFPS
	int32_t ___m_currentFPS_34;
	// System.Int32 ARGUIController::m_framesSinceUpdate
	int32_t ___m_framesSinceUpdate_35;
	// System.Single ARGUIController::m_accumulation
	float ___m_accumulation_36;
	// System.Single ARGUIController::m_currentTime
	float ___m_currentTime_37;
	// Tango.TangoApplication ARGUIController::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_38;
	// TangoARPoseController ARGUIController::m_tangoPose
	TangoARPoseController_t3131628351 * ___m_tangoPose_39;
	// System.String ARGUIController::m_tangoServiceVersion
	String_t* ___m_tangoServiceVersion_40;
	// ARCameraPostProcess ARGUIController::m_arCameraPostProcess
	ARCameraPostProcess_t3075820819 * ___m_arCameraPostProcess_41;
	// System.Boolean ARGUIController::m_findPlaneWaitingForDepth
	bool ___m_findPlaneWaitingForDepth_42;
	// ARMarker ARGUIController::m_selectedMarker
	ARMarker_t1554260723 * ___m_selectedMarker_43;
	// UnityEngine.Rect ARGUIController::m_selectedRect
	Rect_t3681755626  ___m_selectedRect_44;
	// UnityEngine.Rect ARGUIController::m_hideAllRect
	Rect_t3681755626  ___m_hideAllRect_45;
	// System.Boolean ARGUIController::m_showDebug
	bool ___m_showDebug_46;

public:
	inline static int32_t get_offset_of_m_prefabMarker_28() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_prefabMarker_28)); }
	inline GameObject_t1756533147 * get_m_prefabMarker_28() const { return ___m_prefabMarker_28; }
	inline GameObject_t1756533147 ** get_address_of_m_prefabMarker_28() { return &___m_prefabMarker_28; }
	inline void set_m_prefabMarker_28(GameObject_t1756533147 * value)
	{
		___m_prefabMarker_28 = value;
		Il2CppCodeGenWriteBarrier(&___m_prefabMarker_28, value);
	}

	inline static int32_t get_offset_of_m_prefabTouchEffect_29() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_prefabTouchEffect_29)); }
	inline RectTransform_t3349966182 * get_m_prefabTouchEffect_29() const { return ___m_prefabTouchEffect_29; }
	inline RectTransform_t3349966182 ** get_address_of_m_prefabTouchEffect_29() { return &___m_prefabTouchEffect_29; }
	inline void set_m_prefabTouchEffect_29(RectTransform_t3349966182 * value)
	{
		___m_prefabTouchEffect_29 = value;
		Il2CppCodeGenWriteBarrier(&___m_prefabTouchEffect_29, value);
	}

	inline static int32_t get_offset_of_m_canvas_30() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_canvas_30)); }
	inline Canvas_t209405766 * get_m_canvas_30() const { return ___m_canvas_30; }
	inline Canvas_t209405766 ** get_address_of_m_canvas_30() { return &___m_canvas_30; }
	inline void set_m_canvas_30(Canvas_t209405766 * value)
	{
		___m_canvas_30 = value;
		Il2CppCodeGenWriteBarrier(&___m_canvas_30, value);
	}

	inline static int32_t get_offset_of_m_pointCloud_31() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_pointCloud_31)); }
	inline TangoPointCloud_t374155286 * get_m_pointCloud_31() const { return ___m_pointCloud_31; }
	inline TangoPointCloud_t374155286 ** get_address_of_m_pointCloud_31() { return &___m_pointCloud_31; }
	inline void set_m_pointCloud_31(TangoPointCloud_t374155286 * value)
	{
		___m_pointCloud_31 = value;
		Il2CppCodeGenWriteBarrier(&___m_pointCloud_31, value);
	}

	inline static int32_t get_offset_of_m_fpsText_33() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_fpsText_33)); }
	inline String_t* get_m_fpsText_33() const { return ___m_fpsText_33; }
	inline String_t** get_address_of_m_fpsText_33() { return &___m_fpsText_33; }
	inline void set_m_fpsText_33(String_t* value)
	{
		___m_fpsText_33 = value;
		Il2CppCodeGenWriteBarrier(&___m_fpsText_33, value);
	}

	inline static int32_t get_offset_of_m_currentFPS_34() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_currentFPS_34)); }
	inline int32_t get_m_currentFPS_34() const { return ___m_currentFPS_34; }
	inline int32_t* get_address_of_m_currentFPS_34() { return &___m_currentFPS_34; }
	inline void set_m_currentFPS_34(int32_t value)
	{
		___m_currentFPS_34 = value;
	}

	inline static int32_t get_offset_of_m_framesSinceUpdate_35() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_framesSinceUpdate_35)); }
	inline int32_t get_m_framesSinceUpdate_35() const { return ___m_framesSinceUpdate_35; }
	inline int32_t* get_address_of_m_framesSinceUpdate_35() { return &___m_framesSinceUpdate_35; }
	inline void set_m_framesSinceUpdate_35(int32_t value)
	{
		___m_framesSinceUpdate_35 = value;
	}

	inline static int32_t get_offset_of_m_accumulation_36() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_accumulation_36)); }
	inline float get_m_accumulation_36() const { return ___m_accumulation_36; }
	inline float* get_address_of_m_accumulation_36() { return &___m_accumulation_36; }
	inline void set_m_accumulation_36(float value)
	{
		___m_accumulation_36 = value;
	}

	inline static int32_t get_offset_of_m_currentTime_37() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_currentTime_37)); }
	inline float get_m_currentTime_37() const { return ___m_currentTime_37; }
	inline float* get_address_of_m_currentTime_37() { return &___m_currentTime_37; }
	inline void set_m_currentTime_37(float value)
	{
		___m_currentTime_37 = value;
	}

	inline static int32_t get_offset_of_m_tangoApplication_38() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_tangoApplication_38)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_38() const { return ___m_tangoApplication_38; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_38() { return &___m_tangoApplication_38; }
	inline void set_m_tangoApplication_38(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_38 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_38, value);
	}

	inline static int32_t get_offset_of_m_tangoPose_39() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_tangoPose_39)); }
	inline TangoARPoseController_t3131628351 * get_m_tangoPose_39() const { return ___m_tangoPose_39; }
	inline TangoARPoseController_t3131628351 ** get_address_of_m_tangoPose_39() { return &___m_tangoPose_39; }
	inline void set_m_tangoPose_39(TangoARPoseController_t3131628351 * value)
	{
		___m_tangoPose_39 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoPose_39, value);
	}

	inline static int32_t get_offset_of_m_tangoServiceVersion_40() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_tangoServiceVersion_40)); }
	inline String_t* get_m_tangoServiceVersion_40() const { return ___m_tangoServiceVersion_40; }
	inline String_t** get_address_of_m_tangoServiceVersion_40() { return &___m_tangoServiceVersion_40; }
	inline void set_m_tangoServiceVersion_40(String_t* value)
	{
		___m_tangoServiceVersion_40 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoServiceVersion_40, value);
	}

	inline static int32_t get_offset_of_m_arCameraPostProcess_41() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_arCameraPostProcess_41)); }
	inline ARCameraPostProcess_t3075820819 * get_m_arCameraPostProcess_41() const { return ___m_arCameraPostProcess_41; }
	inline ARCameraPostProcess_t3075820819 ** get_address_of_m_arCameraPostProcess_41() { return &___m_arCameraPostProcess_41; }
	inline void set_m_arCameraPostProcess_41(ARCameraPostProcess_t3075820819 * value)
	{
		___m_arCameraPostProcess_41 = value;
		Il2CppCodeGenWriteBarrier(&___m_arCameraPostProcess_41, value);
	}

	inline static int32_t get_offset_of_m_findPlaneWaitingForDepth_42() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_findPlaneWaitingForDepth_42)); }
	inline bool get_m_findPlaneWaitingForDepth_42() const { return ___m_findPlaneWaitingForDepth_42; }
	inline bool* get_address_of_m_findPlaneWaitingForDepth_42() { return &___m_findPlaneWaitingForDepth_42; }
	inline void set_m_findPlaneWaitingForDepth_42(bool value)
	{
		___m_findPlaneWaitingForDepth_42 = value;
	}

	inline static int32_t get_offset_of_m_selectedMarker_43() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_selectedMarker_43)); }
	inline ARMarker_t1554260723 * get_m_selectedMarker_43() const { return ___m_selectedMarker_43; }
	inline ARMarker_t1554260723 ** get_address_of_m_selectedMarker_43() { return &___m_selectedMarker_43; }
	inline void set_m_selectedMarker_43(ARMarker_t1554260723 * value)
	{
		___m_selectedMarker_43 = value;
		Il2CppCodeGenWriteBarrier(&___m_selectedMarker_43, value);
	}

	inline static int32_t get_offset_of_m_selectedRect_44() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_selectedRect_44)); }
	inline Rect_t3681755626  get_m_selectedRect_44() const { return ___m_selectedRect_44; }
	inline Rect_t3681755626 * get_address_of_m_selectedRect_44() { return &___m_selectedRect_44; }
	inline void set_m_selectedRect_44(Rect_t3681755626  value)
	{
		___m_selectedRect_44 = value;
	}

	inline static int32_t get_offset_of_m_hideAllRect_45() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_hideAllRect_45)); }
	inline Rect_t3681755626  get_m_hideAllRect_45() const { return ___m_hideAllRect_45; }
	inline Rect_t3681755626 * get_address_of_m_hideAllRect_45() { return &___m_hideAllRect_45; }
	inline void set_m_hideAllRect_45(Rect_t3681755626  value)
	{
		___m_hideAllRect_45 = value;
	}

	inline static int32_t get_offset_of_m_showDebug_46() { return static_cast<int32_t>(offsetof(ARGUIController_t3357238990, ___m_showDebug_46)); }
	inline bool get_m_showDebug_46() const { return ___m_showDebug_46; }
	inline bool* get_address_of_m_showDebug_46() { return &___m_showDebug_46; }
	inline void set_m_showDebug_46(bool value)
	{
		___m_showDebug_46 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
