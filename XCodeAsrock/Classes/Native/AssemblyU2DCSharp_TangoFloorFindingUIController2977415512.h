﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// Tango.TangoApplication
struct TangoApplication_t278578959;
// TangoPointCloud
struct TangoPointCloud_t374155286;
// TangoPointCloudFloor
struct TangoPointCloudFloor_t3393423680;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TangoFloorFindingUIController
struct  TangoFloorFindingUIController_t2977415512  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject TangoFloorFindingUIController::m_marker
	GameObject_t1756533147 * ___m_marker_2;
	// Tango.TangoApplication TangoFloorFindingUIController::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_3;
	// TangoPointCloud TangoFloorFindingUIController::m_pointCloud
	TangoPointCloud_t374155286 * ___m_pointCloud_4;
	// TangoPointCloudFloor TangoFloorFindingUIController::m_pointCloudFloor
	TangoPointCloudFloor_t3393423680 * ___m_pointCloudFloor_5;
	// System.Boolean TangoFloorFindingUIController::m_findingFloor
	bool ___m_findingFloor_6;

public:
	inline static int32_t get_offset_of_m_marker_2() { return static_cast<int32_t>(offsetof(TangoFloorFindingUIController_t2977415512, ___m_marker_2)); }
	inline GameObject_t1756533147 * get_m_marker_2() const { return ___m_marker_2; }
	inline GameObject_t1756533147 ** get_address_of_m_marker_2() { return &___m_marker_2; }
	inline void set_m_marker_2(GameObject_t1756533147 * value)
	{
		___m_marker_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_marker_2, value);
	}

	inline static int32_t get_offset_of_m_tangoApplication_3() { return static_cast<int32_t>(offsetof(TangoFloorFindingUIController_t2977415512, ___m_tangoApplication_3)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_3() const { return ___m_tangoApplication_3; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_3() { return &___m_tangoApplication_3; }
	inline void set_m_tangoApplication_3(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_3, value);
	}

	inline static int32_t get_offset_of_m_pointCloud_4() { return static_cast<int32_t>(offsetof(TangoFloorFindingUIController_t2977415512, ___m_pointCloud_4)); }
	inline TangoPointCloud_t374155286 * get_m_pointCloud_4() const { return ___m_pointCloud_4; }
	inline TangoPointCloud_t374155286 ** get_address_of_m_pointCloud_4() { return &___m_pointCloud_4; }
	inline void set_m_pointCloud_4(TangoPointCloud_t374155286 * value)
	{
		___m_pointCloud_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_pointCloud_4, value);
	}

	inline static int32_t get_offset_of_m_pointCloudFloor_5() { return static_cast<int32_t>(offsetof(TangoFloorFindingUIController_t2977415512, ___m_pointCloudFloor_5)); }
	inline TangoPointCloudFloor_t3393423680 * get_m_pointCloudFloor_5() const { return ___m_pointCloudFloor_5; }
	inline TangoPointCloudFloor_t3393423680 ** get_address_of_m_pointCloudFloor_5() { return &___m_pointCloudFloor_5; }
	inline void set_m_pointCloudFloor_5(TangoPointCloudFloor_t3393423680 * value)
	{
		___m_pointCloudFloor_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_pointCloudFloor_5, value);
	}

	inline static int32_t get_offset_of_m_findingFloor_6() { return static_cast<int32_t>(offsetof(TangoFloorFindingUIController_t2977415512, ___m_findingFloor_6)); }
	inline bool get_m_findingFloor_6() const { return ___m_findingFloor_6; }
	inline bool* get_address_of_m_findingFloor_6() { return &___m_findingFloor_6; }
	inline void set_m_findingFloor_6(bool value)
	{
		___m_findingFloor_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
