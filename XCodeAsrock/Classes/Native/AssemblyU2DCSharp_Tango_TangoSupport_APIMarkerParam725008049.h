﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_Tango_TangoSupport_MarkerType583861412.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoSupport/APIMarkerParam
struct  APIMarkerParam_t725008049 
{
public:
	// Tango.TangoSupport/MarkerType Tango.TangoSupport/APIMarkerParam::m_type
	int32_t ___m_type_0;
	// System.Double Tango.TangoSupport/APIMarkerParam::m_markerSize
	double ___m_markerSize_1;

public:
	inline static int32_t get_offset_of_m_type_0() { return static_cast<int32_t>(offsetof(APIMarkerParam_t725008049, ___m_type_0)); }
	inline int32_t get_m_type_0() const { return ___m_type_0; }
	inline int32_t* get_address_of_m_type_0() { return &___m_type_0; }
	inline void set_m_type_0(int32_t value)
	{
		___m_type_0 = value;
	}

	inline static int32_t get_offset_of_m_markerSize_1() { return static_cast<int32_t>(offsetof(APIMarkerParam_t725008049, ___m_markerSize_1)); }
	inline double get_m_markerSize_1() const { return ___m_markerSize_1; }
	inline double* get_address_of_m_markerSize_1() { return &___m_markerSize_1; }
	inline void set_m_markerSize_1(double value)
	{
		___m_markerSize_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
