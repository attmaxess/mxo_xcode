﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t601950206;
// AreaLearningInGameController
struct AreaLearningInGameController_t4091715694;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AreaLearningInGameController/<_DoSaveCurrentAreaDescription>c__Iterator0
struct  U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073  : public Il2CppObject
{
public:
	// UnityEngine.TouchScreenKeyboard AreaLearningInGameController/<_DoSaveCurrentAreaDescription>c__Iterator0::<kb>__0
	TouchScreenKeyboard_t601950206 * ___U3CkbU3E__0_0;
	// System.Boolean AreaLearningInGameController/<_DoSaveCurrentAreaDescription>c__Iterator0::<saveConfirmed>__0
	bool ___U3CsaveConfirmedU3E__0_1;
	// AreaLearningInGameController AreaLearningInGameController/<_DoSaveCurrentAreaDescription>c__Iterator0::$this
	AreaLearningInGameController_t4091715694 * ___U24this_2;
	// System.Object AreaLearningInGameController/<_DoSaveCurrentAreaDescription>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean AreaLearningInGameController/<_DoSaveCurrentAreaDescription>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 AreaLearningInGameController/<_DoSaveCurrentAreaDescription>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CkbU3E__0_0() { return static_cast<int32_t>(offsetof(U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073, ___U3CkbU3E__0_0)); }
	inline TouchScreenKeyboard_t601950206 * get_U3CkbU3E__0_0() const { return ___U3CkbU3E__0_0; }
	inline TouchScreenKeyboard_t601950206 ** get_address_of_U3CkbU3E__0_0() { return &___U3CkbU3E__0_0; }
	inline void set_U3CkbU3E__0_0(TouchScreenKeyboard_t601950206 * value)
	{
		___U3CkbU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CkbU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CsaveConfirmedU3E__0_1() { return static_cast<int32_t>(offsetof(U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073, ___U3CsaveConfirmedU3E__0_1)); }
	inline bool get_U3CsaveConfirmedU3E__0_1() const { return ___U3CsaveConfirmedU3E__0_1; }
	inline bool* get_address_of_U3CsaveConfirmedU3E__0_1() { return &___U3CsaveConfirmedU3E__0_1; }
	inline void set_U3CsaveConfirmedU3E__0_1(bool value)
	{
		___U3CsaveConfirmedU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073, ___U24this_2)); }
	inline AreaLearningInGameController_t4091715694 * get_U24this_2() const { return ___U24this_2; }
	inline AreaLearningInGameController_t4091715694 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AreaLearningInGameController_t4091715694 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
