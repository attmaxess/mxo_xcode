﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.DMatrix4x4
struct  DMatrix4x4_t43123165 
{
public:
	// System.Double Tango.DMatrix4x4::m00
	double ___m00_0;
	// System.Double Tango.DMatrix4x4::m10
	double ___m10_1;
	// System.Double Tango.DMatrix4x4::m20
	double ___m20_2;
	// System.Double Tango.DMatrix4x4::m30
	double ___m30_3;
	// System.Double Tango.DMatrix4x4::m01
	double ___m01_4;
	// System.Double Tango.DMatrix4x4::m11
	double ___m11_5;
	// System.Double Tango.DMatrix4x4::m21
	double ___m21_6;
	// System.Double Tango.DMatrix4x4::m31
	double ___m31_7;
	// System.Double Tango.DMatrix4x4::m02
	double ___m02_8;
	// System.Double Tango.DMatrix4x4::m12
	double ___m12_9;
	// System.Double Tango.DMatrix4x4::m22
	double ___m22_10;
	// System.Double Tango.DMatrix4x4::m32
	double ___m32_11;
	// System.Double Tango.DMatrix4x4::m03
	double ___m03_12;
	// System.Double Tango.DMatrix4x4::m13
	double ___m13_13;
	// System.Double Tango.DMatrix4x4::m23
	double ___m23_14;
	// System.Double Tango.DMatrix4x4::m33
	double ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(DMatrix4x4_t43123165, ___m00_0)); }
	inline double get_m00_0() const { return ___m00_0; }
	inline double* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(double value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(DMatrix4x4_t43123165, ___m10_1)); }
	inline double get_m10_1() const { return ___m10_1; }
	inline double* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(double value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(DMatrix4x4_t43123165, ___m20_2)); }
	inline double get_m20_2() const { return ___m20_2; }
	inline double* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(double value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(DMatrix4x4_t43123165, ___m30_3)); }
	inline double get_m30_3() const { return ___m30_3; }
	inline double* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(double value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(DMatrix4x4_t43123165, ___m01_4)); }
	inline double get_m01_4() const { return ___m01_4; }
	inline double* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(double value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(DMatrix4x4_t43123165, ___m11_5)); }
	inline double get_m11_5() const { return ___m11_5; }
	inline double* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(double value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(DMatrix4x4_t43123165, ___m21_6)); }
	inline double get_m21_6() const { return ___m21_6; }
	inline double* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(double value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(DMatrix4x4_t43123165, ___m31_7)); }
	inline double get_m31_7() const { return ___m31_7; }
	inline double* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(double value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(DMatrix4x4_t43123165, ___m02_8)); }
	inline double get_m02_8() const { return ___m02_8; }
	inline double* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(double value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(DMatrix4x4_t43123165, ___m12_9)); }
	inline double get_m12_9() const { return ___m12_9; }
	inline double* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(double value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(DMatrix4x4_t43123165, ___m22_10)); }
	inline double get_m22_10() const { return ___m22_10; }
	inline double* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(double value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(DMatrix4x4_t43123165, ___m32_11)); }
	inline double get_m32_11() const { return ___m32_11; }
	inline double* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(double value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(DMatrix4x4_t43123165, ___m03_12)); }
	inline double get_m03_12() const { return ___m03_12; }
	inline double* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(double value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(DMatrix4x4_t43123165, ___m13_13)); }
	inline double get_m13_13() const { return ___m13_13; }
	inline double* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(double value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(DMatrix4x4_t43123165, ___m23_14)); }
	inline double get_m23_14() const { return ___m23_14; }
	inline double* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(double value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(DMatrix4x4_t43123165, ___m33_15)); }
	inline double get_m33_15() const { return ___m33_15; }
	inline double* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(double value)
	{
		___m33_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
