﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// AndroidHelperWrapper
struct AndroidHelperWrapper_t864207182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidHelperWrapper
struct  AndroidHelperWrapper_t864207182  : public Il2CppObject
{
public:

public:
};

struct AndroidHelperWrapper_t864207182_StaticFields
{
public:
	// AndroidHelperWrapper AndroidHelperWrapper::m_instance
	AndroidHelperWrapper_t864207182 * ___m_instance_0;

public:
	inline static int32_t get_offset_of_m_instance_0() { return static_cast<int32_t>(offsetof(AndroidHelperWrapper_t864207182_StaticFields, ___m_instance_0)); }
	inline AndroidHelperWrapper_t864207182 * get_m_instance_0() const { return ___m_instance_0; }
	inline AndroidHelperWrapper_t864207182 ** get_address_of_m_instance_0() { return &___m_instance_0; }
	inline void set_m_instance_0(AndroidHelperWrapper_t864207182 * value)
	{
		___m_instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
