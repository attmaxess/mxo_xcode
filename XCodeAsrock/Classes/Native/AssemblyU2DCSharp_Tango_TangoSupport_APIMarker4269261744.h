﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_Tango_TangoSupport_MarkerType583861412.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_Tango_DVector33583584176.h"
#include "AssemblyU2DCSharp_Tango_DVector42017500235.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoSupport/APIMarker
struct  APIMarker_t4269261744 
{
public:
	// Tango.TangoSupport/MarkerType Tango.TangoSupport/APIMarker::m_type
	int32_t ___m_type_0;
	// System.Double Tango.TangoSupport/APIMarker::m_timestamp
	double ___m_timestamp_1;
	// System.String Tango.TangoSupport/APIMarker::m_content
	String_t* ___m_content_2;
	// System.Int32 Tango.TangoSupport/APIMarker::m_contentSize
	int32_t ___m_contentSize_3;
	// UnityEngine.Vector2 Tango.TangoSupport/APIMarker::m_corner2DP0
	Vector2_t2243707579  ___m_corner2DP0_4;
	// UnityEngine.Vector2 Tango.TangoSupport/APIMarker::m_corner2DP1
	Vector2_t2243707579  ___m_corner2DP1_5;
	// UnityEngine.Vector2 Tango.TangoSupport/APIMarker::m_corner2DP2
	Vector2_t2243707579  ___m_corner2DP2_6;
	// UnityEngine.Vector2 Tango.TangoSupport/APIMarker::m_corner2DP3
	Vector2_t2243707579  ___m_corner2DP3_7;
	// UnityEngine.Vector3 Tango.TangoSupport/APIMarker::m_corner3DP0
	Vector3_t2243707580  ___m_corner3DP0_8;
	// UnityEngine.Vector3 Tango.TangoSupport/APIMarker::m_corner3DP1
	Vector3_t2243707580  ___m_corner3DP1_9;
	// UnityEngine.Vector3 Tango.TangoSupport/APIMarker::m_corner3DP2
	Vector3_t2243707580  ___m_corner3DP2_10;
	// UnityEngine.Vector3 Tango.TangoSupport/APIMarker::m_corner3DP3
	Vector3_t2243707580  ___m_corner3DP3_11;
	// Tango.DVector3 Tango.TangoSupport/APIMarker::m_translation
	DVector3_t3583584176  ___m_translation_12;
	// Tango.DVector4 Tango.TangoSupport/APIMarker::m_rotation
	DVector4_t2017500235  ___m_rotation_13;

public:
	inline static int32_t get_offset_of_m_type_0() { return static_cast<int32_t>(offsetof(APIMarker_t4269261744, ___m_type_0)); }
	inline int32_t get_m_type_0() const { return ___m_type_0; }
	inline int32_t* get_address_of_m_type_0() { return &___m_type_0; }
	inline void set_m_type_0(int32_t value)
	{
		___m_type_0 = value;
	}

	inline static int32_t get_offset_of_m_timestamp_1() { return static_cast<int32_t>(offsetof(APIMarker_t4269261744, ___m_timestamp_1)); }
	inline double get_m_timestamp_1() const { return ___m_timestamp_1; }
	inline double* get_address_of_m_timestamp_1() { return &___m_timestamp_1; }
	inline void set_m_timestamp_1(double value)
	{
		___m_timestamp_1 = value;
	}

	inline static int32_t get_offset_of_m_content_2() { return static_cast<int32_t>(offsetof(APIMarker_t4269261744, ___m_content_2)); }
	inline String_t* get_m_content_2() const { return ___m_content_2; }
	inline String_t** get_address_of_m_content_2() { return &___m_content_2; }
	inline void set_m_content_2(String_t* value)
	{
		___m_content_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_content_2, value);
	}

	inline static int32_t get_offset_of_m_contentSize_3() { return static_cast<int32_t>(offsetof(APIMarker_t4269261744, ___m_contentSize_3)); }
	inline int32_t get_m_contentSize_3() const { return ___m_contentSize_3; }
	inline int32_t* get_address_of_m_contentSize_3() { return &___m_contentSize_3; }
	inline void set_m_contentSize_3(int32_t value)
	{
		___m_contentSize_3 = value;
	}

	inline static int32_t get_offset_of_m_corner2DP0_4() { return static_cast<int32_t>(offsetof(APIMarker_t4269261744, ___m_corner2DP0_4)); }
	inline Vector2_t2243707579  get_m_corner2DP0_4() const { return ___m_corner2DP0_4; }
	inline Vector2_t2243707579 * get_address_of_m_corner2DP0_4() { return &___m_corner2DP0_4; }
	inline void set_m_corner2DP0_4(Vector2_t2243707579  value)
	{
		___m_corner2DP0_4 = value;
	}

	inline static int32_t get_offset_of_m_corner2DP1_5() { return static_cast<int32_t>(offsetof(APIMarker_t4269261744, ___m_corner2DP1_5)); }
	inline Vector2_t2243707579  get_m_corner2DP1_5() const { return ___m_corner2DP1_5; }
	inline Vector2_t2243707579 * get_address_of_m_corner2DP1_5() { return &___m_corner2DP1_5; }
	inline void set_m_corner2DP1_5(Vector2_t2243707579  value)
	{
		___m_corner2DP1_5 = value;
	}

	inline static int32_t get_offset_of_m_corner2DP2_6() { return static_cast<int32_t>(offsetof(APIMarker_t4269261744, ___m_corner2DP2_6)); }
	inline Vector2_t2243707579  get_m_corner2DP2_6() const { return ___m_corner2DP2_6; }
	inline Vector2_t2243707579 * get_address_of_m_corner2DP2_6() { return &___m_corner2DP2_6; }
	inline void set_m_corner2DP2_6(Vector2_t2243707579  value)
	{
		___m_corner2DP2_6 = value;
	}

	inline static int32_t get_offset_of_m_corner2DP3_7() { return static_cast<int32_t>(offsetof(APIMarker_t4269261744, ___m_corner2DP3_7)); }
	inline Vector2_t2243707579  get_m_corner2DP3_7() const { return ___m_corner2DP3_7; }
	inline Vector2_t2243707579 * get_address_of_m_corner2DP3_7() { return &___m_corner2DP3_7; }
	inline void set_m_corner2DP3_7(Vector2_t2243707579  value)
	{
		___m_corner2DP3_7 = value;
	}

	inline static int32_t get_offset_of_m_corner3DP0_8() { return static_cast<int32_t>(offsetof(APIMarker_t4269261744, ___m_corner3DP0_8)); }
	inline Vector3_t2243707580  get_m_corner3DP0_8() const { return ___m_corner3DP0_8; }
	inline Vector3_t2243707580 * get_address_of_m_corner3DP0_8() { return &___m_corner3DP0_8; }
	inline void set_m_corner3DP0_8(Vector3_t2243707580  value)
	{
		___m_corner3DP0_8 = value;
	}

	inline static int32_t get_offset_of_m_corner3DP1_9() { return static_cast<int32_t>(offsetof(APIMarker_t4269261744, ___m_corner3DP1_9)); }
	inline Vector3_t2243707580  get_m_corner3DP1_9() const { return ___m_corner3DP1_9; }
	inline Vector3_t2243707580 * get_address_of_m_corner3DP1_9() { return &___m_corner3DP1_9; }
	inline void set_m_corner3DP1_9(Vector3_t2243707580  value)
	{
		___m_corner3DP1_9 = value;
	}

	inline static int32_t get_offset_of_m_corner3DP2_10() { return static_cast<int32_t>(offsetof(APIMarker_t4269261744, ___m_corner3DP2_10)); }
	inline Vector3_t2243707580  get_m_corner3DP2_10() const { return ___m_corner3DP2_10; }
	inline Vector3_t2243707580 * get_address_of_m_corner3DP2_10() { return &___m_corner3DP2_10; }
	inline void set_m_corner3DP2_10(Vector3_t2243707580  value)
	{
		___m_corner3DP2_10 = value;
	}

	inline static int32_t get_offset_of_m_corner3DP3_11() { return static_cast<int32_t>(offsetof(APIMarker_t4269261744, ___m_corner3DP3_11)); }
	inline Vector3_t2243707580  get_m_corner3DP3_11() const { return ___m_corner3DP3_11; }
	inline Vector3_t2243707580 * get_address_of_m_corner3DP3_11() { return &___m_corner3DP3_11; }
	inline void set_m_corner3DP3_11(Vector3_t2243707580  value)
	{
		___m_corner3DP3_11 = value;
	}

	inline static int32_t get_offset_of_m_translation_12() { return static_cast<int32_t>(offsetof(APIMarker_t4269261744, ___m_translation_12)); }
	inline DVector3_t3583584176  get_m_translation_12() const { return ___m_translation_12; }
	inline DVector3_t3583584176 * get_address_of_m_translation_12() { return &___m_translation_12; }
	inline void set_m_translation_12(DVector3_t3583584176  value)
	{
		___m_translation_12 = value;
	}

	inline static int32_t get_offset_of_m_rotation_13() { return static_cast<int32_t>(offsetof(APIMarker_t4269261744, ___m_rotation_13)); }
	inline DVector4_t2017500235  get_m_rotation_13() const { return ___m_rotation_13; }
	inline DVector4_t2017500235 * get_address_of_m_rotation_13() { return &___m_rotation_13; }
	inline void set_m_rotation_13(DVector4_t2017500235  value)
	{
		___m_rotation_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Tango.TangoSupport/APIMarker
struct APIMarker_t4269261744_marshaled_pinvoke
{
	int32_t ___m_type_0;
	double ___m_timestamp_1;
	char* ___m_content_2;
	int32_t ___m_contentSize_3;
	Vector2_t2243707579  ___m_corner2DP0_4;
	Vector2_t2243707579  ___m_corner2DP1_5;
	Vector2_t2243707579  ___m_corner2DP2_6;
	Vector2_t2243707579  ___m_corner2DP3_7;
	Vector3_t2243707580  ___m_corner3DP0_8;
	Vector3_t2243707580  ___m_corner3DP1_9;
	Vector3_t2243707580  ___m_corner3DP2_10;
	Vector3_t2243707580  ___m_corner3DP3_11;
	DVector3_t3583584176  ___m_translation_12;
	DVector4_t2017500235  ___m_rotation_13;
};
// Native definition for COM marshalling of Tango.TangoSupport/APIMarker
struct APIMarker_t4269261744_marshaled_com
{
	int32_t ___m_type_0;
	double ___m_timestamp_1;
	char* ___m_content_2;
	int32_t ___m_contentSize_3;
	Vector2_t2243707579  ___m_corner2DP0_4;
	Vector2_t2243707579  ___m_corner2DP1_5;
	Vector2_t2243707579  ___m_corner2DP2_6;
	Vector2_t2243707579  ___m_corner2DP3_7;
	Vector3_t2243707580  ___m_corner3DP0_8;
	Vector3_t2243707580  ___m_corner3DP1_9;
	Vector3_t2243707580  ___m_corner3DP2_10;
	Vector3_t2243707580  ___m_corner3DP3_11;
	DVector3_t3583584176  ___m_translation_12;
	DVector4_t2017500235  ___m_rotation_13;
};
