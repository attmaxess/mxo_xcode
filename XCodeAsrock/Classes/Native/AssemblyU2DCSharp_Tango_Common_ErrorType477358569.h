﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.Common/ErrorType
struct  ErrorType_t477358569 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ErrorType_t477358569__padding[1];
	};

public:
};

struct ErrorType_t477358569_StaticFields
{
public:
	// System.Int32 Tango.Common/ErrorType::TANGO_NO_CAMERA_PERMISSION
	int32_t ___TANGO_NO_CAMERA_PERMISSION_0;
	// System.Int32 Tango.Common/ErrorType::TANGO_NO_ADF_PERMISSION
	int32_t ___TANGO_NO_ADF_PERMISSION_1;
	// System.Int32 Tango.Common/ErrorType::TANGO_NO_MOTION_TRACKING_PERMISSION
	int32_t ___TANGO_NO_MOTION_TRACKING_PERMISSION_2;
	// System.Int32 Tango.Common/ErrorType::TANGO_INVALID
	int32_t ___TANGO_INVALID_3;
	// System.Int32 Tango.Common/ErrorType::TANGO_ERROR
	int32_t ___TANGO_ERROR_4;
	// System.Int32 Tango.Common/ErrorType::TANGO_SUCCESS
	int32_t ___TANGO_SUCCESS_5;

public:
	inline static int32_t get_offset_of_TANGO_NO_CAMERA_PERMISSION_0() { return static_cast<int32_t>(offsetof(ErrorType_t477358569_StaticFields, ___TANGO_NO_CAMERA_PERMISSION_0)); }
	inline int32_t get_TANGO_NO_CAMERA_PERMISSION_0() const { return ___TANGO_NO_CAMERA_PERMISSION_0; }
	inline int32_t* get_address_of_TANGO_NO_CAMERA_PERMISSION_0() { return &___TANGO_NO_CAMERA_PERMISSION_0; }
	inline void set_TANGO_NO_CAMERA_PERMISSION_0(int32_t value)
	{
		___TANGO_NO_CAMERA_PERMISSION_0 = value;
	}

	inline static int32_t get_offset_of_TANGO_NO_ADF_PERMISSION_1() { return static_cast<int32_t>(offsetof(ErrorType_t477358569_StaticFields, ___TANGO_NO_ADF_PERMISSION_1)); }
	inline int32_t get_TANGO_NO_ADF_PERMISSION_1() const { return ___TANGO_NO_ADF_PERMISSION_1; }
	inline int32_t* get_address_of_TANGO_NO_ADF_PERMISSION_1() { return &___TANGO_NO_ADF_PERMISSION_1; }
	inline void set_TANGO_NO_ADF_PERMISSION_1(int32_t value)
	{
		___TANGO_NO_ADF_PERMISSION_1 = value;
	}

	inline static int32_t get_offset_of_TANGO_NO_MOTION_TRACKING_PERMISSION_2() { return static_cast<int32_t>(offsetof(ErrorType_t477358569_StaticFields, ___TANGO_NO_MOTION_TRACKING_PERMISSION_2)); }
	inline int32_t get_TANGO_NO_MOTION_TRACKING_PERMISSION_2() const { return ___TANGO_NO_MOTION_TRACKING_PERMISSION_2; }
	inline int32_t* get_address_of_TANGO_NO_MOTION_TRACKING_PERMISSION_2() { return &___TANGO_NO_MOTION_TRACKING_PERMISSION_2; }
	inline void set_TANGO_NO_MOTION_TRACKING_PERMISSION_2(int32_t value)
	{
		___TANGO_NO_MOTION_TRACKING_PERMISSION_2 = value;
	}

	inline static int32_t get_offset_of_TANGO_INVALID_3() { return static_cast<int32_t>(offsetof(ErrorType_t477358569_StaticFields, ___TANGO_INVALID_3)); }
	inline int32_t get_TANGO_INVALID_3() const { return ___TANGO_INVALID_3; }
	inline int32_t* get_address_of_TANGO_INVALID_3() { return &___TANGO_INVALID_3; }
	inline void set_TANGO_INVALID_3(int32_t value)
	{
		___TANGO_INVALID_3 = value;
	}

	inline static int32_t get_offset_of_TANGO_ERROR_4() { return static_cast<int32_t>(offsetof(ErrorType_t477358569_StaticFields, ___TANGO_ERROR_4)); }
	inline int32_t get_TANGO_ERROR_4() const { return ___TANGO_ERROR_4; }
	inline int32_t* get_address_of_TANGO_ERROR_4() { return &___TANGO_ERROR_4; }
	inline void set_TANGO_ERROR_4(int32_t value)
	{
		___TANGO_ERROR_4 = value;
	}

	inline static int32_t get_offset_of_TANGO_SUCCESS_5() { return static_cast<int32_t>(offsetof(ErrorType_t477358569_StaticFields, ___TANGO_SUCCESS_5)); }
	inline int32_t get_TANGO_SUCCESS_5() const { return ___TANGO_SUCCESS_5; }
	inline int32_t* get_address_of_TANGO_SUCCESS_5() { return &___TANGO_SUCCESS_5; }
	inline void set_TANGO_SUCCESS_5(int32_t value)
	{
		___TANGO_SUCCESS_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
