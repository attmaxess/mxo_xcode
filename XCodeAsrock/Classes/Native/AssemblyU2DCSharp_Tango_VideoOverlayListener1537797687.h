﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoCameraId106182192.h"

// System.Object
struct Il2CppObject;
// Tango.VideoOverlayProvider/APIOnImageAvailable
struct APIOnImageAvailable_t3522420021;
// Tango.VideoOverlayProvider/APIOnTextureAvailable
struct APIOnTextureAvailable_t2067566777;
// Tango.TangoUnityImageData
struct TangoUnityImageData_t4168844635;
// Tango.OnTangoImageAvailableEventHandler
struct OnTangoImageAvailableEventHandler_t1456428726;
// Tango.OnTangoCameraTextureAvailableEventHandler
struct OnTangoCameraTextureAvailableEventHandler_t1960798139;
// Tango.OnTangoImageMultithreadedAvailableEventHandler
struct OnTangoImageMultithreadedAvailableEventHandler_t762238536;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.VideoOverlayListener
struct  VideoOverlayListener_t1537797687  : public Il2CppObject
{
public:

public:
};

struct VideoOverlayListener_t1537797687_StaticFields
{
public:
	// System.Object Tango.VideoOverlayListener::m_lockObject
	Il2CppObject * ___m_lockObject_1;
	// Tango.VideoOverlayProvider/APIOnImageAvailable Tango.VideoOverlayListener::m_onImageAvailable
	APIOnImageAvailable_t3522420021 * ___m_onImageAvailable_2;
	// Tango.VideoOverlayProvider/APIOnTextureAvailable Tango.VideoOverlayListener::m_onTextureAvailable
	APIOnTextureAvailable_t2067566777 * ___m_onTextureAvailable_3;
	// Tango.VideoOverlayProvider/APIOnTextureAvailable Tango.VideoOverlayListener::m_onYUVTextureAvailable
	APIOnTextureAvailable_t2067566777 * ___m_onYUVTextureAvailable_4;
	// Tango.TangoUnityImageData Tango.VideoOverlayListener::m_previousImageBuffer
	TangoUnityImageData_t4168844635 * ___m_previousImageBuffer_5;
	// System.Boolean Tango.VideoOverlayListener::m_shouldSendTextureMethodEvent
	bool ___m_shouldSendTextureMethodEvent_6;
	// System.Boolean Tango.VideoOverlayListener::m_shouldSendByteBufferMethodEvent
	bool ___m_shouldSendByteBufferMethodEvent_7;
	// System.Boolean Tango.VideoOverlayListener::m_shouldSendYUVTextureIdMethodEvent
	bool ___m_shouldSendYUVTextureIdMethodEvent_8;
	// Tango.OnTangoImageAvailableEventHandler Tango.VideoOverlayListener::m_onTangoImageAvailable
	OnTangoImageAvailableEventHandler_t1456428726 * ___m_onTangoImageAvailable_9;
	// Tango.OnTangoCameraTextureAvailableEventHandler Tango.VideoOverlayListener::m_onTangoCameraTextureAvailable
	OnTangoCameraTextureAvailableEventHandler_t1960798139 * ___m_onTangoCameraTextureAvailable_10;
	// Tango.OnTangoCameraTextureAvailableEventHandler Tango.VideoOverlayListener::m_onTangoYUVTextureAvailable
	OnTangoCameraTextureAvailableEventHandler_t1960798139 * ___m_onTangoYUVTextureAvailable_11;
	// Tango.OnTangoImageMultithreadedAvailableEventHandler Tango.VideoOverlayListener::m_onTangoImageMultithreadedAvailable
	OnTangoImageMultithreadedAvailableEventHandler_t762238536 * ___m_onTangoImageMultithreadedAvailable_12;

public:
	inline static int32_t get_offset_of_m_lockObject_1() { return static_cast<int32_t>(offsetof(VideoOverlayListener_t1537797687_StaticFields, ___m_lockObject_1)); }
	inline Il2CppObject * get_m_lockObject_1() const { return ___m_lockObject_1; }
	inline Il2CppObject ** get_address_of_m_lockObject_1() { return &___m_lockObject_1; }
	inline void set_m_lockObject_1(Il2CppObject * value)
	{
		___m_lockObject_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_lockObject_1, value);
	}

	inline static int32_t get_offset_of_m_onImageAvailable_2() { return static_cast<int32_t>(offsetof(VideoOverlayListener_t1537797687_StaticFields, ___m_onImageAvailable_2)); }
	inline APIOnImageAvailable_t3522420021 * get_m_onImageAvailable_2() const { return ___m_onImageAvailable_2; }
	inline APIOnImageAvailable_t3522420021 ** get_address_of_m_onImageAvailable_2() { return &___m_onImageAvailable_2; }
	inline void set_m_onImageAvailable_2(APIOnImageAvailable_t3522420021 * value)
	{
		___m_onImageAvailable_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_onImageAvailable_2, value);
	}

	inline static int32_t get_offset_of_m_onTextureAvailable_3() { return static_cast<int32_t>(offsetof(VideoOverlayListener_t1537797687_StaticFields, ___m_onTextureAvailable_3)); }
	inline APIOnTextureAvailable_t2067566777 * get_m_onTextureAvailable_3() const { return ___m_onTextureAvailable_3; }
	inline APIOnTextureAvailable_t2067566777 ** get_address_of_m_onTextureAvailable_3() { return &___m_onTextureAvailable_3; }
	inline void set_m_onTextureAvailable_3(APIOnTextureAvailable_t2067566777 * value)
	{
		___m_onTextureAvailable_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_onTextureAvailable_3, value);
	}

	inline static int32_t get_offset_of_m_onYUVTextureAvailable_4() { return static_cast<int32_t>(offsetof(VideoOverlayListener_t1537797687_StaticFields, ___m_onYUVTextureAvailable_4)); }
	inline APIOnTextureAvailable_t2067566777 * get_m_onYUVTextureAvailable_4() const { return ___m_onYUVTextureAvailable_4; }
	inline APIOnTextureAvailable_t2067566777 ** get_address_of_m_onYUVTextureAvailable_4() { return &___m_onYUVTextureAvailable_4; }
	inline void set_m_onYUVTextureAvailable_4(APIOnTextureAvailable_t2067566777 * value)
	{
		___m_onYUVTextureAvailable_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_onYUVTextureAvailable_4, value);
	}

	inline static int32_t get_offset_of_m_previousImageBuffer_5() { return static_cast<int32_t>(offsetof(VideoOverlayListener_t1537797687_StaticFields, ___m_previousImageBuffer_5)); }
	inline TangoUnityImageData_t4168844635 * get_m_previousImageBuffer_5() const { return ___m_previousImageBuffer_5; }
	inline TangoUnityImageData_t4168844635 ** get_address_of_m_previousImageBuffer_5() { return &___m_previousImageBuffer_5; }
	inline void set_m_previousImageBuffer_5(TangoUnityImageData_t4168844635 * value)
	{
		___m_previousImageBuffer_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_previousImageBuffer_5, value);
	}

	inline static int32_t get_offset_of_m_shouldSendTextureMethodEvent_6() { return static_cast<int32_t>(offsetof(VideoOverlayListener_t1537797687_StaticFields, ___m_shouldSendTextureMethodEvent_6)); }
	inline bool get_m_shouldSendTextureMethodEvent_6() const { return ___m_shouldSendTextureMethodEvent_6; }
	inline bool* get_address_of_m_shouldSendTextureMethodEvent_6() { return &___m_shouldSendTextureMethodEvent_6; }
	inline void set_m_shouldSendTextureMethodEvent_6(bool value)
	{
		___m_shouldSendTextureMethodEvent_6 = value;
	}

	inline static int32_t get_offset_of_m_shouldSendByteBufferMethodEvent_7() { return static_cast<int32_t>(offsetof(VideoOverlayListener_t1537797687_StaticFields, ___m_shouldSendByteBufferMethodEvent_7)); }
	inline bool get_m_shouldSendByteBufferMethodEvent_7() const { return ___m_shouldSendByteBufferMethodEvent_7; }
	inline bool* get_address_of_m_shouldSendByteBufferMethodEvent_7() { return &___m_shouldSendByteBufferMethodEvent_7; }
	inline void set_m_shouldSendByteBufferMethodEvent_7(bool value)
	{
		___m_shouldSendByteBufferMethodEvent_7 = value;
	}

	inline static int32_t get_offset_of_m_shouldSendYUVTextureIdMethodEvent_8() { return static_cast<int32_t>(offsetof(VideoOverlayListener_t1537797687_StaticFields, ___m_shouldSendYUVTextureIdMethodEvent_8)); }
	inline bool get_m_shouldSendYUVTextureIdMethodEvent_8() const { return ___m_shouldSendYUVTextureIdMethodEvent_8; }
	inline bool* get_address_of_m_shouldSendYUVTextureIdMethodEvent_8() { return &___m_shouldSendYUVTextureIdMethodEvent_8; }
	inline void set_m_shouldSendYUVTextureIdMethodEvent_8(bool value)
	{
		___m_shouldSendYUVTextureIdMethodEvent_8 = value;
	}

	inline static int32_t get_offset_of_m_onTangoImageAvailable_9() { return static_cast<int32_t>(offsetof(VideoOverlayListener_t1537797687_StaticFields, ___m_onTangoImageAvailable_9)); }
	inline OnTangoImageAvailableEventHandler_t1456428726 * get_m_onTangoImageAvailable_9() const { return ___m_onTangoImageAvailable_9; }
	inline OnTangoImageAvailableEventHandler_t1456428726 ** get_address_of_m_onTangoImageAvailable_9() { return &___m_onTangoImageAvailable_9; }
	inline void set_m_onTangoImageAvailable_9(OnTangoImageAvailableEventHandler_t1456428726 * value)
	{
		___m_onTangoImageAvailable_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_onTangoImageAvailable_9, value);
	}

	inline static int32_t get_offset_of_m_onTangoCameraTextureAvailable_10() { return static_cast<int32_t>(offsetof(VideoOverlayListener_t1537797687_StaticFields, ___m_onTangoCameraTextureAvailable_10)); }
	inline OnTangoCameraTextureAvailableEventHandler_t1960798139 * get_m_onTangoCameraTextureAvailable_10() const { return ___m_onTangoCameraTextureAvailable_10; }
	inline OnTangoCameraTextureAvailableEventHandler_t1960798139 ** get_address_of_m_onTangoCameraTextureAvailable_10() { return &___m_onTangoCameraTextureAvailable_10; }
	inline void set_m_onTangoCameraTextureAvailable_10(OnTangoCameraTextureAvailableEventHandler_t1960798139 * value)
	{
		___m_onTangoCameraTextureAvailable_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_onTangoCameraTextureAvailable_10, value);
	}

	inline static int32_t get_offset_of_m_onTangoYUVTextureAvailable_11() { return static_cast<int32_t>(offsetof(VideoOverlayListener_t1537797687_StaticFields, ___m_onTangoYUVTextureAvailable_11)); }
	inline OnTangoCameraTextureAvailableEventHandler_t1960798139 * get_m_onTangoYUVTextureAvailable_11() const { return ___m_onTangoYUVTextureAvailable_11; }
	inline OnTangoCameraTextureAvailableEventHandler_t1960798139 ** get_address_of_m_onTangoYUVTextureAvailable_11() { return &___m_onTangoYUVTextureAvailable_11; }
	inline void set_m_onTangoYUVTextureAvailable_11(OnTangoCameraTextureAvailableEventHandler_t1960798139 * value)
	{
		___m_onTangoYUVTextureAvailable_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_onTangoYUVTextureAvailable_11, value);
	}

	inline static int32_t get_offset_of_m_onTangoImageMultithreadedAvailable_12() { return static_cast<int32_t>(offsetof(VideoOverlayListener_t1537797687_StaticFields, ___m_onTangoImageMultithreadedAvailable_12)); }
	inline OnTangoImageMultithreadedAvailableEventHandler_t762238536 * get_m_onTangoImageMultithreadedAvailable_12() const { return ___m_onTangoImageMultithreadedAvailable_12; }
	inline OnTangoImageMultithreadedAvailableEventHandler_t762238536 ** get_address_of_m_onTangoImageMultithreadedAvailable_12() { return &___m_onTangoImageMultithreadedAvailable_12; }
	inline void set_m_onTangoImageMultithreadedAvailable_12(OnTangoImageMultithreadedAvailableEventHandler_t762238536 * value)
	{
		___m_onTangoImageMultithreadedAvailable_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_onTangoImageMultithreadedAvailable_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
