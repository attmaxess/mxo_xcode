﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.Tango3DReconstruction/APIPose
struct  APIPose_t580197307 
{
public:
	// System.Double Tango.Tango3DReconstruction/APIPose::translation0
	double ___translation0_0;
	// System.Double Tango.Tango3DReconstruction/APIPose::translation1
	double ___translation1_1;
	// System.Double Tango.Tango3DReconstruction/APIPose::translation2
	double ___translation2_2;
	// System.Double Tango.Tango3DReconstruction/APIPose::orientation0
	double ___orientation0_3;
	// System.Double Tango.Tango3DReconstruction/APIPose::orientation1
	double ___orientation1_4;
	// System.Double Tango.Tango3DReconstruction/APIPose::orientation2
	double ___orientation2_5;
	// System.Double Tango.Tango3DReconstruction/APIPose::orientation3
	double ___orientation3_6;

public:
	inline static int32_t get_offset_of_translation0_0() { return static_cast<int32_t>(offsetof(APIPose_t580197307, ___translation0_0)); }
	inline double get_translation0_0() const { return ___translation0_0; }
	inline double* get_address_of_translation0_0() { return &___translation0_0; }
	inline void set_translation0_0(double value)
	{
		___translation0_0 = value;
	}

	inline static int32_t get_offset_of_translation1_1() { return static_cast<int32_t>(offsetof(APIPose_t580197307, ___translation1_1)); }
	inline double get_translation1_1() const { return ___translation1_1; }
	inline double* get_address_of_translation1_1() { return &___translation1_1; }
	inline void set_translation1_1(double value)
	{
		___translation1_1 = value;
	}

	inline static int32_t get_offset_of_translation2_2() { return static_cast<int32_t>(offsetof(APIPose_t580197307, ___translation2_2)); }
	inline double get_translation2_2() const { return ___translation2_2; }
	inline double* get_address_of_translation2_2() { return &___translation2_2; }
	inline void set_translation2_2(double value)
	{
		___translation2_2 = value;
	}

	inline static int32_t get_offset_of_orientation0_3() { return static_cast<int32_t>(offsetof(APIPose_t580197307, ___orientation0_3)); }
	inline double get_orientation0_3() const { return ___orientation0_3; }
	inline double* get_address_of_orientation0_3() { return &___orientation0_3; }
	inline void set_orientation0_3(double value)
	{
		___orientation0_3 = value;
	}

	inline static int32_t get_offset_of_orientation1_4() { return static_cast<int32_t>(offsetof(APIPose_t580197307, ___orientation1_4)); }
	inline double get_orientation1_4() const { return ___orientation1_4; }
	inline double* get_address_of_orientation1_4() { return &___orientation1_4; }
	inline void set_orientation1_4(double value)
	{
		___orientation1_4 = value;
	}

	inline static int32_t get_offset_of_orientation2_5() { return static_cast<int32_t>(offsetof(APIPose_t580197307, ___orientation2_5)); }
	inline double get_orientation2_5() const { return ___orientation2_5; }
	inline double* get_address_of_orientation2_5() { return &___orientation2_5; }
	inline void set_orientation2_5(double value)
	{
		___orientation2_5 = value;
	}

	inline static int32_t get_offset_of_orientation3_6() { return static_cast<int32_t>(offsetof(APIPose_t580197307, ___orientation3_6)); }
	inline double get_orientation3_6() const { return ___orientation3_6; }
	inline double* get_address_of_orientation3_6() { return &___orientation3_6; }
	inline void set_orientation3_6(double value)
	{
		___orientation3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
