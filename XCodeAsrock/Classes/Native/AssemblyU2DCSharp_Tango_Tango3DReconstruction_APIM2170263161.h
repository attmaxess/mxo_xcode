﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.Tango3DReconstruction/APIMatrix3x3
struct  APIMatrix3x3_t2170263161 
{
public:
	// System.Double Tango.Tango3DReconstruction/APIMatrix3x3::r00
	double ___r00_0;
	// System.Double Tango.Tango3DReconstruction/APIMatrix3x3::r10
	double ___r10_1;
	// System.Double Tango.Tango3DReconstruction/APIMatrix3x3::r20
	double ___r20_2;
	// System.Double Tango.Tango3DReconstruction/APIMatrix3x3::r01
	double ___r01_3;
	// System.Double Tango.Tango3DReconstruction/APIMatrix3x3::r11
	double ___r11_4;
	// System.Double Tango.Tango3DReconstruction/APIMatrix3x3::r21
	double ___r21_5;
	// System.Double Tango.Tango3DReconstruction/APIMatrix3x3::r02
	double ___r02_6;
	// System.Double Tango.Tango3DReconstruction/APIMatrix3x3::r12
	double ___r12_7;
	// System.Double Tango.Tango3DReconstruction/APIMatrix3x3::r22
	double ___r22_8;

public:
	inline static int32_t get_offset_of_r00_0() { return static_cast<int32_t>(offsetof(APIMatrix3x3_t2170263161, ___r00_0)); }
	inline double get_r00_0() const { return ___r00_0; }
	inline double* get_address_of_r00_0() { return &___r00_0; }
	inline void set_r00_0(double value)
	{
		___r00_0 = value;
	}

	inline static int32_t get_offset_of_r10_1() { return static_cast<int32_t>(offsetof(APIMatrix3x3_t2170263161, ___r10_1)); }
	inline double get_r10_1() const { return ___r10_1; }
	inline double* get_address_of_r10_1() { return &___r10_1; }
	inline void set_r10_1(double value)
	{
		___r10_1 = value;
	}

	inline static int32_t get_offset_of_r20_2() { return static_cast<int32_t>(offsetof(APIMatrix3x3_t2170263161, ___r20_2)); }
	inline double get_r20_2() const { return ___r20_2; }
	inline double* get_address_of_r20_2() { return &___r20_2; }
	inline void set_r20_2(double value)
	{
		___r20_2 = value;
	}

	inline static int32_t get_offset_of_r01_3() { return static_cast<int32_t>(offsetof(APIMatrix3x3_t2170263161, ___r01_3)); }
	inline double get_r01_3() const { return ___r01_3; }
	inline double* get_address_of_r01_3() { return &___r01_3; }
	inline void set_r01_3(double value)
	{
		___r01_3 = value;
	}

	inline static int32_t get_offset_of_r11_4() { return static_cast<int32_t>(offsetof(APIMatrix3x3_t2170263161, ___r11_4)); }
	inline double get_r11_4() const { return ___r11_4; }
	inline double* get_address_of_r11_4() { return &___r11_4; }
	inline void set_r11_4(double value)
	{
		___r11_4 = value;
	}

	inline static int32_t get_offset_of_r21_5() { return static_cast<int32_t>(offsetof(APIMatrix3x3_t2170263161, ___r21_5)); }
	inline double get_r21_5() const { return ___r21_5; }
	inline double* get_address_of_r21_5() { return &___r21_5; }
	inline void set_r21_5(double value)
	{
		___r21_5 = value;
	}

	inline static int32_t get_offset_of_r02_6() { return static_cast<int32_t>(offsetof(APIMatrix3x3_t2170263161, ___r02_6)); }
	inline double get_r02_6() const { return ___r02_6; }
	inline double* get_address_of_r02_6() { return &___r02_6; }
	inline void set_r02_6(double value)
	{
		___r02_6 = value;
	}

	inline static int32_t get_offset_of_r12_7() { return static_cast<int32_t>(offsetof(APIMatrix3x3_t2170263161, ___r12_7)); }
	inline double get_r12_7() const { return ___r12_7; }
	inline double* get_address_of_r12_7() { return &___r12_7; }
	inline void set_r12_7(double value)
	{
		___r12_7 = value;
	}

	inline static int32_t get_offset_of_r22_8() { return static_cast<int32_t>(offsetof(APIMatrix3x3_t2170263161, ___r22_8)); }
	inline double get_r22_8() const { return ___r22_8; }
	inline double* get_address_of_r22_8() { return &___r22_8; }
	inline void set_r22_8(double value)
	{
		___r22_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
