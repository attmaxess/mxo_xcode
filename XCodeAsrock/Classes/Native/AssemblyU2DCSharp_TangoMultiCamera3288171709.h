﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_TangoMultiCamera_CameraType2660248723.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TangoMultiCamera
struct  TangoMultiCamera_t3288171709  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject TangoMultiCamera::m_targetFollowingObject
	GameObject_t1756533147 * ___m_targetFollowingObject_2;
	// System.Boolean TangoMultiCamera::m_enableCameraTypeUI
	bool ___m_enableCameraTypeUI_3;
	// TangoMultiCamera/CameraType TangoMultiCamera::m_defaultCameraType
	int32_t ___m_defaultCameraType_4;
	// TangoMultiCamera/CameraType TangoMultiCamera::m_currentCamera
	int32_t ___m_currentCamera_15;
	// UnityEngine.Vector2 TangoMultiCamera::m_touchStartPosition
	Vector2_t2243707579  ___m_touchStartPosition_16;
	// System.Single TangoMultiCamera::m_touchStartDistance
	float ___m_touchStartDistance_17;
	// UnityEngine.Vector3 TangoMultiCamera::m_thirdPersonRotationEuler
	Vector3_t2243707580  ___m_thirdPersonRotationEuler_18;
	// UnityEngine.Vector3 TangoMultiCamera::m_thirdPersonRotationEulerStart
	Vector3_t2243707580  ___m_thirdPersonRotationEulerStart_19;
	// System.Single TangoMultiCamera::m_thirdPersonDistance
	float ___m_thirdPersonDistance_20;
	// System.Single TangoMultiCamera::m_thirdPersonDistanceStart
	float ___m_thirdPersonDistanceStart_21;
	// UnityEngine.Vector3 TangoMultiCamera::m_topDownOffset
	Vector3_t2243707580  ___m_topDownOffset_22;
	// UnityEngine.Vector3 TangoMultiCamera::m_topDownOffsetStart
	Vector3_t2243707580  ___m_topDownOffsetStart_23;

public:
	inline static int32_t get_offset_of_m_targetFollowingObject_2() { return static_cast<int32_t>(offsetof(TangoMultiCamera_t3288171709, ___m_targetFollowingObject_2)); }
	inline GameObject_t1756533147 * get_m_targetFollowingObject_2() const { return ___m_targetFollowingObject_2; }
	inline GameObject_t1756533147 ** get_address_of_m_targetFollowingObject_2() { return &___m_targetFollowingObject_2; }
	inline void set_m_targetFollowingObject_2(GameObject_t1756533147 * value)
	{
		___m_targetFollowingObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_targetFollowingObject_2, value);
	}

	inline static int32_t get_offset_of_m_enableCameraTypeUI_3() { return static_cast<int32_t>(offsetof(TangoMultiCamera_t3288171709, ___m_enableCameraTypeUI_3)); }
	inline bool get_m_enableCameraTypeUI_3() const { return ___m_enableCameraTypeUI_3; }
	inline bool* get_address_of_m_enableCameraTypeUI_3() { return &___m_enableCameraTypeUI_3; }
	inline void set_m_enableCameraTypeUI_3(bool value)
	{
		___m_enableCameraTypeUI_3 = value;
	}

	inline static int32_t get_offset_of_m_defaultCameraType_4() { return static_cast<int32_t>(offsetof(TangoMultiCamera_t3288171709, ___m_defaultCameraType_4)); }
	inline int32_t get_m_defaultCameraType_4() const { return ___m_defaultCameraType_4; }
	inline int32_t* get_address_of_m_defaultCameraType_4() { return &___m_defaultCameraType_4; }
	inline void set_m_defaultCameraType_4(int32_t value)
	{
		___m_defaultCameraType_4 = value;
	}

	inline static int32_t get_offset_of_m_currentCamera_15() { return static_cast<int32_t>(offsetof(TangoMultiCamera_t3288171709, ___m_currentCamera_15)); }
	inline int32_t get_m_currentCamera_15() const { return ___m_currentCamera_15; }
	inline int32_t* get_address_of_m_currentCamera_15() { return &___m_currentCamera_15; }
	inline void set_m_currentCamera_15(int32_t value)
	{
		___m_currentCamera_15 = value;
	}

	inline static int32_t get_offset_of_m_touchStartPosition_16() { return static_cast<int32_t>(offsetof(TangoMultiCamera_t3288171709, ___m_touchStartPosition_16)); }
	inline Vector2_t2243707579  get_m_touchStartPosition_16() const { return ___m_touchStartPosition_16; }
	inline Vector2_t2243707579 * get_address_of_m_touchStartPosition_16() { return &___m_touchStartPosition_16; }
	inline void set_m_touchStartPosition_16(Vector2_t2243707579  value)
	{
		___m_touchStartPosition_16 = value;
	}

	inline static int32_t get_offset_of_m_touchStartDistance_17() { return static_cast<int32_t>(offsetof(TangoMultiCamera_t3288171709, ___m_touchStartDistance_17)); }
	inline float get_m_touchStartDistance_17() const { return ___m_touchStartDistance_17; }
	inline float* get_address_of_m_touchStartDistance_17() { return &___m_touchStartDistance_17; }
	inline void set_m_touchStartDistance_17(float value)
	{
		___m_touchStartDistance_17 = value;
	}

	inline static int32_t get_offset_of_m_thirdPersonRotationEuler_18() { return static_cast<int32_t>(offsetof(TangoMultiCamera_t3288171709, ___m_thirdPersonRotationEuler_18)); }
	inline Vector3_t2243707580  get_m_thirdPersonRotationEuler_18() const { return ___m_thirdPersonRotationEuler_18; }
	inline Vector3_t2243707580 * get_address_of_m_thirdPersonRotationEuler_18() { return &___m_thirdPersonRotationEuler_18; }
	inline void set_m_thirdPersonRotationEuler_18(Vector3_t2243707580  value)
	{
		___m_thirdPersonRotationEuler_18 = value;
	}

	inline static int32_t get_offset_of_m_thirdPersonRotationEulerStart_19() { return static_cast<int32_t>(offsetof(TangoMultiCamera_t3288171709, ___m_thirdPersonRotationEulerStart_19)); }
	inline Vector3_t2243707580  get_m_thirdPersonRotationEulerStart_19() const { return ___m_thirdPersonRotationEulerStart_19; }
	inline Vector3_t2243707580 * get_address_of_m_thirdPersonRotationEulerStart_19() { return &___m_thirdPersonRotationEulerStart_19; }
	inline void set_m_thirdPersonRotationEulerStart_19(Vector3_t2243707580  value)
	{
		___m_thirdPersonRotationEulerStart_19 = value;
	}

	inline static int32_t get_offset_of_m_thirdPersonDistance_20() { return static_cast<int32_t>(offsetof(TangoMultiCamera_t3288171709, ___m_thirdPersonDistance_20)); }
	inline float get_m_thirdPersonDistance_20() const { return ___m_thirdPersonDistance_20; }
	inline float* get_address_of_m_thirdPersonDistance_20() { return &___m_thirdPersonDistance_20; }
	inline void set_m_thirdPersonDistance_20(float value)
	{
		___m_thirdPersonDistance_20 = value;
	}

	inline static int32_t get_offset_of_m_thirdPersonDistanceStart_21() { return static_cast<int32_t>(offsetof(TangoMultiCamera_t3288171709, ___m_thirdPersonDistanceStart_21)); }
	inline float get_m_thirdPersonDistanceStart_21() const { return ___m_thirdPersonDistanceStart_21; }
	inline float* get_address_of_m_thirdPersonDistanceStart_21() { return &___m_thirdPersonDistanceStart_21; }
	inline void set_m_thirdPersonDistanceStart_21(float value)
	{
		___m_thirdPersonDistanceStart_21 = value;
	}

	inline static int32_t get_offset_of_m_topDownOffset_22() { return static_cast<int32_t>(offsetof(TangoMultiCamera_t3288171709, ___m_topDownOffset_22)); }
	inline Vector3_t2243707580  get_m_topDownOffset_22() const { return ___m_topDownOffset_22; }
	inline Vector3_t2243707580 * get_address_of_m_topDownOffset_22() { return &___m_topDownOffset_22; }
	inline void set_m_topDownOffset_22(Vector3_t2243707580  value)
	{
		___m_topDownOffset_22 = value;
	}

	inline static int32_t get_offset_of_m_topDownOffsetStart_23() { return static_cast<int32_t>(offsetof(TangoMultiCamera_t3288171709, ___m_topDownOffsetStart_23)); }
	inline Vector3_t2243707580  get_m_topDownOffsetStart_23() const { return ___m_topDownOffsetStart_23; }
	inline Vector3_t2243707580 * get_address_of_m_topDownOffsetStart_23() { return &___m_topDownOffsetStart_23; }
	inline void set_m_topDownOffsetStart_23(Vector3_t2243707580  value)
	{
		___m_topDownOffsetStart_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
