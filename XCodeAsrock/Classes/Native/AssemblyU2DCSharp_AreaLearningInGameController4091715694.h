﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// TangoPointCloud
struct TangoPointCloud_t374155286;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.Text
struct Text_t356221433;
// Tango.AreaDescription
struct AreaDescription_t1434786909;
// TangoPoseController
struct TangoPoseController_t4427816;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// ARMarker
struct ARMarker_t1554260723;
// Tango.TangoApplication
struct TangoApplication_t278578959;
// System.Threading.Thread
struct Thread_t241561612;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AreaLearningInGameController
struct  AreaLearningInGameController_t4091715694  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] AreaLearningInGameController::m_markPrefabs
	GameObjectU5BU5D_t3057952154* ___m_markPrefabs_2;
	// TangoPointCloud AreaLearningInGameController::m_pointCloud
	TangoPointCloud_t374155286 * ___m_pointCloud_3;
	// UnityEngine.Canvas AreaLearningInGameController::m_canvas
	Canvas_t209405766 * ___m_canvas_4;
	// UnityEngine.RectTransform AreaLearningInGameController::m_prefabTouchEffect
	RectTransform_t3349966182 * ___m_prefabTouchEffect_5;
	// UnityEngine.UI.Text AreaLearningInGameController::m_savingText
	Text_t356221433 * ___m_savingText_6;
	// Tango.AreaDescription AreaLearningInGameController::m_curAreaDescription
	AreaDescription_t1434786909 * ___m_curAreaDescription_7;
	// System.Boolean AreaLearningInGameController::m_findPlaneWaitingForDepth
	bool ___m_findPlaneWaitingForDepth_8;
	// TangoPoseController AreaLearningInGameController::m_poseController
	TangoPoseController_t4427816 * ___m_poseController_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> AreaLearningInGameController::m_markerList
	List_1_t1125654279 * ___m_markerList_10;
	// UnityEngine.GameObject AreaLearningInGameController::newMarkObject
	GameObject_t1756533147 * ___newMarkObject_11;
	// System.Int32 AreaLearningInGameController::m_currentMarkType
	int32_t ___m_currentMarkType_12;
	// ARMarker AreaLearningInGameController::m_selectedMarker
	ARMarker_t1554260723 * ___m_selectedMarker_13;
	// UnityEngine.Rect AreaLearningInGameController::m_selectedRect
	Rect_t3681755626  ___m_selectedRect_14;
	// System.Boolean AreaLearningInGameController::m_initialized
	bool ___m_initialized_15;
	// Tango.TangoApplication AreaLearningInGameController::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_16;
	// System.Threading.Thread AreaLearningInGameController::m_saveThread
	Thread_t241561612 * ___m_saveThread_17;

public:
	inline static int32_t get_offset_of_m_markPrefabs_2() { return static_cast<int32_t>(offsetof(AreaLearningInGameController_t4091715694, ___m_markPrefabs_2)); }
	inline GameObjectU5BU5D_t3057952154* get_m_markPrefabs_2() const { return ___m_markPrefabs_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_markPrefabs_2() { return &___m_markPrefabs_2; }
	inline void set_m_markPrefabs_2(GameObjectU5BU5D_t3057952154* value)
	{
		___m_markPrefabs_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_markPrefabs_2, value);
	}

	inline static int32_t get_offset_of_m_pointCloud_3() { return static_cast<int32_t>(offsetof(AreaLearningInGameController_t4091715694, ___m_pointCloud_3)); }
	inline TangoPointCloud_t374155286 * get_m_pointCloud_3() const { return ___m_pointCloud_3; }
	inline TangoPointCloud_t374155286 ** get_address_of_m_pointCloud_3() { return &___m_pointCloud_3; }
	inline void set_m_pointCloud_3(TangoPointCloud_t374155286 * value)
	{
		___m_pointCloud_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_pointCloud_3, value);
	}

	inline static int32_t get_offset_of_m_canvas_4() { return static_cast<int32_t>(offsetof(AreaLearningInGameController_t4091715694, ___m_canvas_4)); }
	inline Canvas_t209405766 * get_m_canvas_4() const { return ___m_canvas_4; }
	inline Canvas_t209405766 ** get_address_of_m_canvas_4() { return &___m_canvas_4; }
	inline void set_m_canvas_4(Canvas_t209405766 * value)
	{
		___m_canvas_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_canvas_4, value);
	}

	inline static int32_t get_offset_of_m_prefabTouchEffect_5() { return static_cast<int32_t>(offsetof(AreaLearningInGameController_t4091715694, ___m_prefabTouchEffect_5)); }
	inline RectTransform_t3349966182 * get_m_prefabTouchEffect_5() const { return ___m_prefabTouchEffect_5; }
	inline RectTransform_t3349966182 ** get_address_of_m_prefabTouchEffect_5() { return &___m_prefabTouchEffect_5; }
	inline void set_m_prefabTouchEffect_5(RectTransform_t3349966182 * value)
	{
		___m_prefabTouchEffect_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_prefabTouchEffect_5, value);
	}

	inline static int32_t get_offset_of_m_savingText_6() { return static_cast<int32_t>(offsetof(AreaLearningInGameController_t4091715694, ___m_savingText_6)); }
	inline Text_t356221433 * get_m_savingText_6() const { return ___m_savingText_6; }
	inline Text_t356221433 ** get_address_of_m_savingText_6() { return &___m_savingText_6; }
	inline void set_m_savingText_6(Text_t356221433 * value)
	{
		___m_savingText_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_savingText_6, value);
	}

	inline static int32_t get_offset_of_m_curAreaDescription_7() { return static_cast<int32_t>(offsetof(AreaLearningInGameController_t4091715694, ___m_curAreaDescription_7)); }
	inline AreaDescription_t1434786909 * get_m_curAreaDescription_7() const { return ___m_curAreaDescription_7; }
	inline AreaDescription_t1434786909 ** get_address_of_m_curAreaDescription_7() { return &___m_curAreaDescription_7; }
	inline void set_m_curAreaDescription_7(AreaDescription_t1434786909 * value)
	{
		___m_curAreaDescription_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_curAreaDescription_7, value);
	}

	inline static int32_t get_offset_of_m_findPlaneWaitingForDepth_8() { return static_cast<int32_t>(offsetof(AreaLearningInGameController_t4091715694, ___m_findPlaneWaitingForDepth_8)); }
	inline bool get_m_findPlaneWaitingForDepth_8() const { return ___m_findPlaneWaitingForDepth_8; }
	inline bool* get_address_of_m_findPlaneWaitingForDepth_8() { return &___m_findPlaneWaitingForDepth_8; }
	inline void set_m_findPlaneWaitingForDepth_8(bool value)
	{
		___m_findPlaneWaitingForDepth_8 = value;
	}

	inline static int32_t get_offset_of_m_poseController_9() { return static_cast<int32_t>(offsetof(AreaLearningInGameController_t4091715694, ___m_poseController_9)); }
	inline TangoPoseController_t4427816 * get_m_poseController_9() const { return ___m_poseController_9; }
	inline TangoPoseController_t4427816 ** get_address_of_m_poseController_9() { return &___m_poseController_9; }
	inline void set_m_poseController_9(TangoPoseController_t4427816 * value)
	{
		___m_poseController_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_poseController_9, value);
	}

	inline static int32_t get_offset_of_m_markerList_10() { return static_cast<int32_t>(offsetof(AreaLearningInGameController_t4091715694, ___m_markerList_10)); }
	inline List_1_t1125654279 * get_m_markerList_10() const { return ___m_markerList_10; }
	inline List_1_t1125654279 ** get_address_of_m_markerList_10() { return &___m_markerList_10; }
	inline void set_m_markerList_10(List_1_t1125654279 * value)
	{
		___m_markerList_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_markerList_10, value);
	}

	inline static int32_t get_offset_of_newMarkObject_11() { return static_cast<int32_t>(offsetof(AreaLearningInGameController_t4091715694, ___newMarkObject_11)); }
	inline GameObject_t1756533147 * get_newMarkObject_11() const { return ___newMarkObject_11; }
	inline GameObject_t1756533147 ** get_address_of_newMarkObject_11() { return &___newMarkObject_11; }
	inline void set_newMarkObject_11(GameObject_t1756533147 * value)
	{
		___newMarkObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___newMarkObject_11, value);
	}

	inline static int32_t get_offset_of_m_currentMarkType_12() { return static_cast<int32_t>(offsetof(AreaLearningInGameController_t4091715694, ___m_currentMarkType_12)); }
	inline int32_t get_m_currentMarkType_12() const { return ___m_currentMarkType_12; }
	inline int32_t* get_address_of_m_currentMarkType_12() { return &___m_currentMarkType_12; }
	inline void set_m_currentMarkType_12(int32_t value)
	{
		___m_currentMarkType_12 = value;
	}

	inline static int32_t get_offset_of_m_selectedMarker_13() { return static_cast<int32_t>(offsetof(AreaLearningInGameController_t4091715694, ___m_selectedMarker_13)); }
	inline ARMarker_t1554260723 * get_m_selectedMarker_13() const { return ___m_selectedMarker_13; }
	inline ARMarker_t1554260723 ** get_address_of_m_selectedMarker_13() { return &___m_selectedMarker_13; }
	inline void set_m_selectedMarker_13(ARMarker_t1554260723 * value)
	{
		___m_selectedMarker_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_selectedMarker_13, value);
	}

	inline static int32_t get_offset_of_m_selectedRect_14() { return static_cast<int32_t>(offsetof(AreaLearningInGameController_t4091715694, ___m_selectedRect_14)); }
	inline Rect_t3681755626  get_m_selectedRect_14() const { return ___m_selectedRect_14; }
	inline Rect_t3681755626 * get_address_of_m_selectedRect_14() { return &___m_selectedRect_14; }
	inline void set_m_selectedRect_14(Rect_t3681755626  value)
	{
		___m_selectedRect_14 = value;
	}

	inline static int32_t get_offset_of_m_initialized_15() { return static_cast<int32_t>(offsetof(AreaLearningInGameController_t4091715694, ___m_initialized_15)); }
	inline bool get_m_initialized_15() const { return ___m_initialized_15; }
	inline bool* get_address_of_m_initialized_15() { return &___m_initialized_15; }
	inline void set_m_initialized_15(bool value)
	{
		___m_initialized_15 = value;
	}

	inline static int32_t get_offset_of_m_tangoApplication_16() { return static_cast<int32_t>(offsetof(AreaLearningInGameController_t4091715694, ___m_tangoApplication_16)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_16() const { return ___m_tangoApplication_16; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_16() { return &___m_tangoApplication_16; }
	inline void set_m_tangoApplication_16(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_16, value);
	}

	inline static int32_t get_offset_of_m_saveThread_17() { return static_cast<int32_t>(offsetof(AreaLearningInGameController_t4091715694, ___m_saveThread_17)); }
	inline Thread_t241561612 * get_m_saveThread_17() const { return ___m_saveThread_17; }
	inline Thread_t241561612 ** get_address_of_m_saveThread_17() { return &___m_saveThread_17; }
	inline void set_m_saveThread_17(Thread_t241561612 * value)
	{
		___m_saveThread_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_saveThread_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
