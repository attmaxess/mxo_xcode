﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoCameraMetadata
struct  TangoCameraMetadata_t1412457087 
{
public:
	// System.Int64 Tango.TangoCameraMetadata::m_timestampNs
	int64_t ___m_timestampNs_0;
	// System.Int64 Tango.TangoCameraMetadata::m_frameNumber
	int64_t ___m_frameNumber_1;
	// System.Int64 Tango.TangoCameraMetadata::m_exposureDurationNs
	int64_t ___m_exposureDurationNs_2;

public:
	inline static int32_t get_offset_of_m_timestampNs_0() { return static_cast<int32_t>(offsetof(TangoCameraMetadata_t1412457087, ___m_timestampNs_0)); }
	inline int64_t get_m_timestampNs_0() const { return ___m_timestampNs_0; }
	inline int64_t* get_address_of_m_timestampNs_0() { return &___m_timestampNs_0; }
	inline void set_m_timestampNs_0(int64_t value)
	{
		___m_timestampNs_0 = value;
	}

	inline static int32_t get_offset_of_m_frameNumber_1() { return static_cast<int32_t>(offsetof(TangoCameraMetadata_t1412457087, ___m_frameNumber_1)); }
	inline int64_t get_m_frameNumber_1() const { return ___m_frameNumber_1; }
	inline int64_t* get_address_of_m_frameNumber_1() { return &___m_frameNumber_1; }
	inline void set_m_frameNumber_1(int64_t value)
	{
		___m_frameNumber_1 = value;
	}

	inline static int32_t get_offset_of_m_exposureDurationNs_2() { return static_cast<int32_t>(offsetof(TangoCameraMetadata_t1412457087, ___m_exposureDurationNs_2)); }
	inline int64_t get_m_exposureDurationNs_2() const { return ___m_exposureDurationNs_2; }
	inline int64_t* get_address_of_m_exposureDurationNs_2() { return &___m_exposureDurationNs_2; }
	inline void set_m_exposureDurationNs_2(int64_t value)
	{
		___m_exposureDurationNs_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
