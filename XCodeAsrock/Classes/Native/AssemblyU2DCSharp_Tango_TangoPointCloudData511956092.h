﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Single[]
struct SingleU5BU5D_t577127397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoPointCloudData
struct  TangoPointCloudData_t511956092  : public Il2CppObject
{
public:
	// System.Double Tango.TangoPointCloudData::m_timestamp
	double ___m_timestamp_0;
	// System.Int32 Tango.TangoPointCloudData::m_numPoints
	int32_t ___m_numPoints_1;
	// System.Single[] Tango.TangoPointCloudData::m_points
	SingleU5BU5D_t577127397* ___m_points_2;

public:
	inline static int32_t get_offset_of_m_timestamp_0() { return static_cast<int32_t>(offsetof(TangoPointCloudData_t511956092, ___m_timestamp_0)); }
	inline double get_m_timestamp_0() const { return ___m_timestamp_0; }
	inline double* get_address_of_m_timestamp_0() { return &___m_timestamp_0; }
	inline void set_m_timestamp_0(double value)
	{
		___m_timestamp_0 = value;
	}

	inline static int32_t get_offset_of_m_numPoints_1() { return static_cast<int32_t>(offsetof(TangoPointCloudData_t511956092, ___m_numPoints_1)); }
	inline int32_t get_m_numPoints_1() const { return ___m_numPoints_1; }
	inline int32_t* get_address_of_m_numPoints_1() { return &___m_numPoints_1; }
	inline void set_m_numPoints_1(int32_t value)
	{
		___m_numPoints_1 = value;
	}

	inline static int32_t get_offset_of_m_points_2() { return static_cast<int32_t>(offsetof(TangoPointCloudData_t511956092, ___m_points_2)); }
	inline SingleU5BU5D_t577127397* get_m_points_2() const { return ___m_points_2; }
	inline SingleU5BU5D_t577127397** get_address_of_m_points_2() { return &___m_points_2; }
	inline void set_m_points_2(SingleU5BU5D_t577127397* value)
	{
		___m_points_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_points_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
