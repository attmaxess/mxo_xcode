﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Tango.ITangoConfig
struct ITangoConfig_t1873940398;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoApplication/TangoApplicationState
struct  TangoApplicationState_t848537215  : public Il2CppObject
{
public:
	// Tango.ITangoConfig Tango.TangoApplication/TangoApplicationState::m_tangoConfig
	Il2CppObject * ___m_tangoConfig_0;
	// Tango.ITangoConfig Tango.TangoApplication/TangoApplicationState::m_tangoRuntimeConfig
	Il2CppObject * ___m_tangoRuntimeConfig_1;
	// System.Boolean Tango.TangoApplication/TangoApplicationState::<IsTangoUpToDate>k__BackingField
	bool ___U3CIsTangoUpToDateU3Ek__BackingField_2;
	// System.Boolean Tango.TangoApplication/TangoApplicationState::<IsTangoStarted>k__BackingField
	bool ___U3CIsTangoStartedU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_tangoConfig_0() { return static_cast<int32_t>(offsetof(TangoApplicationState_t848537215, ___m_tangoConfig_0)); }
	inline Il2CppObject * get_m_tangoConfig_0() const { return ___m_tangoConfig_0; }
	inline Il2CppObject ** get_address_of_m_tangoConfig_0() { return &___m_tangoConfig_0; }
	inline void set_m_tangoConfig_0(Il2CppObject * value)
	{
		___m_tangoConfig_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoConfig_0, value);
	}

	inline static int32_t get_offset_of_m_tangoRuntimeConfig_1() { return static_cast<int32_t>(offsetof(TangoApplicationState_t848537215, ___m_tangoRuntimeConfig_1)); }
	inline Il2CppObject * get_m_tangoRuntimeConfig_1() const { return ___m_tangoRuntimeConfig_1; }
	inline Il2CppObject ** get_address_of_m_tangoRuntimeConfig_1() { return &___m_tangoRuntimeConfig_1; }
	inline void set_m_tangoRuntimeConfig_1(Il2CppObject * value)
	{
		___m_tangoRuntimeConfig_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoRuntimeConfig_1, value);
	}

	inline static int32_t get_offset_of_U3CIsTangoUpToDateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TangoApplicationState_t848537215, ___U3CIsTangoUpToDateU3Ek__BackingField_2)); }
	inline bool get_U3CIsTangoUpToDateU3Ek__BackingField_2() const { return ___U3CIsTangoUpToDateU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsTangoUpToDateU3Ek__BackingField_2() { return &___U3CIsTangoUpToDateU3Ek__BackingField_2; }
	inline void set_U3CIsTangoUpToDateU3Ek__BackingField_2(bool value)
	{
		___U3CIsTangoUpToDateU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CIsTangoStartedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TangoApplicationState_t848537215, ___U3CIsTangoStartedU3Ek__BackingField_3)); }
	inline bool get_U3CIsTangoStartedU3Ek__BackingField_3() const { return ___U3CIsTangoStartedU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsTangoStartedU3Ek__BackingField_3() { return &___U3CIsTangoStartedU3Ek__BackingField_3; }
	inline void set_U3CIsTangoStartedU3Ek__BackingField_3(bool value)
	{
		___U3CIsTangoStartedU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
