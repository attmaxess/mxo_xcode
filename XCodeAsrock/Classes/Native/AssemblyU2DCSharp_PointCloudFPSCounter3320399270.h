﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// Tango.TangoApplication
struct TangoApplication_t278578959;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PointCloudFPSCounter
struct  PointCloudFPSCounter_t3320399270  : public MonoBehaviour_t1158329972
{
public:
	// System.Single PointCloudFPSCounter::m_updateFrequency
	float ___m_updateFrequency_2;
	// System.String PointCloudFPSCounter::m_FPSText
	String_t* ___m_FPSText_3;
	// System.Int32 PointCloudFPSCounter::m_currentFPS
	int32_t ___m_currentFPS_4;
	// System.Int32 PointCloudFPSCounter::m_framesSinceUpdate
	int32_t ___m_framesSinceUpdate_5;
	// System.Single PointCloudFPSCounter::m_accumulation
	float ___m_accumulation_6;
	// System.Single PointCloudFPSCounter::m_currentTime
	float ___m_currentTime_7;
	// Tango.TangoApplication PointCloudFPSCounter::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_8;

public:
	inline static int32_t get_offset_of_m_updateFrequency_2() { return static_cast<int32_t>(offsetof(PointCloudFPSCounter_t3320399270, ___m_updateFrequency_2)); }
	inline float get_m_updateFrequency_2() const { return ___m_updateFrequency_2; }
	inline float* get_address_of_m_updateFrequency_2() { return &___m_updateFrequency_2; }
	inline void set_m_updateFrequency_2(float value)
	{
		___m_updateFrequency_2 = value;
	}

	inline static int32_t get_offset_of_m_FPSText_3() { return static_cast<int32_t>(offsetof(PointCloudFPSCounter_t3320399270, ___m_FPSText_3)); }
	inline String_t* get_m_FPSText_3() const { return ___m_FPSText_3; }
	inline String_t** get_address_of_m_FPSText_3() { return &___m_FPSText_3; }
	inline void set_m_FPSText_3(String_t* value)
	{
		___m_FPSText_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_FPSText_3, value);
	}

	inline static int32_t get_offset_of_m_currentFPS_4() { return static_cast<int32_t>(offsetof(PointCloudFPSCounter_t3320399270, ___m_currentFPS_4)); }
	inline int32_t get_m_currentFPS_4() const { return ___m_currentFPS_4; }
	inline int32_t* get_address_of_m_currentFPS_4() { return &___m_currentFPS_4; }
	inline void set_m_currentFPS_4(int32_t value)
	{
		___m_currentFPS_4 = value;
	}

	inline static int32_t get_offset_of_m_framesSinceUpdate_5() { return static_cast<int32_t>(offsetof(PointCloudFPSCounter_t3320399270, ___m_framesSinceUpdate_5)); }
	inline int32_t get_m_framesSinceUpdate_5() const { return ___m_framesSinceUpdate_5; }
	inline int32_t* get_address_of_m_framesSinceUpdate_5() { return &___m_framesSinceUpdate_5; }
	inline void set_m_framesSinceUpdate_5(int32_t value)
	{
		___m_framesSinceUpdate_5 = value;
	}

	inline static int32_t get_offset_of_m_accumulation_6() { return static_cast<int32_t>(offsetof(PointCloudFPSCounter_t3320399270, ___m_accumulation_6)); }
	inline float get_m_accumulation_6() const { return ___m_accumulation_6; }
	inline float* get_address_of_m_accumulation_6() { return &___m_accumulation_6; }
	inline void set_m_accumulation_6(float value)
	{
		___m_accumulation_6 = value;
	}

	inline static int32_t get_offset_of_m_currentTime_7() { return static_cast<int32_t>(offsetof(PointCloudFPSCounter_t3320399270, ___m_currentTime_7)); }
	inline float get_m_currentTime_7() const { return ___m_currentTime_7; }
	inline float* get_address_of_m_currentTime_7() { return &___m_currentTime_7; }
	inline void set_m_currentTime_7(float value)
	{
		___m_currentTime_7 = value;
	}

	inline static int32_t get_offset_of_m_tangoApplication_8() { return static_cast<int32_t>(offsetof(PointCloudFPSCounter_t3320399270, ___m_tangoApplication_8)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_8() const { return ___m_tangoApplication_8; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_8() { return &___m_tangoApplication_8; }
	inline void set_m_tangoApplication_8(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
