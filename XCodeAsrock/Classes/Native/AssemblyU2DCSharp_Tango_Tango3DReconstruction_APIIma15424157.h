﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_IntPtr2504060609.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.Tango3DReconstruction/APIImageBuffer
struct  APIImageBuffer_t15424157 
{
public:
	// System.UInt32 Tango.Tango3DReconstruction/APIImageBuffer::width
	uint32_t ___width_0;
	// System.UInt32 Tango.Tango3DReconstruction/APIImageBuffer::height
	uint32_t ___height_1;
	// System.UInt32 Tango.Tango3DReconstruction/APIImageBuffer::stride
	uint32_t ___stride_2;
	// System.Double Tango.Tango3DReconstruction/APIImageBuffer::timestamp
	double ___timestamp_3;
	// System.Int32 Tango.Tango3DReconstruction/APIImageBuffer::format
	int32_t ___format_4;
	// System.IntPtr Tango.Tango3DReconstruction/APIImageBuffer::data
	IntPtr_t ___data_5;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(APIImageBuffer_t15424157, ___width_0)); }
	inline uint32_t get_width_0() const { return ___width_0; }
	inline uint32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(uint32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(APIImageBuffer_t15424157, ___height_1)); }
	inline uint32_t get_height_1() const { return ___height_1; }
	inline uint32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(uint32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_stride_2() { return static_cast<int32_t>(offsetof(APIImageBuffer_t15424157, ___stride_2)); }
	inline uint32_t get_stride_2() const { return ___stride_2; }
	inline uint32_t* get_address_of_stride_2() { return &___stride_2; }
	inline void set_stride_2(uint32_t value)
	{
		___stride_2 = value;
	}

	inline static int32_t get_offset_of_timestamp_3() { return static_cast<int32_t>(offsetof(APIImageBuffer_t15424157, ___timestamp_3)); }
	inline double get_timestamp_3() const { return ___timestamp_3; }
	inline double* get_address_of_timestamp_3() { return &___timestamp_3; }
	inline void set_timestamp_3(double value)
	{
		___timestamp_3 = value;
	}

	inline static int32_t get_offset_of_format_4() { return static_cast<int32_t>(offsetof(APIImageBuffer_t15424157, ___format_4)); }
	inline int32_t get_format_4() const { return ___format_4; }
	inline int32_t* get_address_of_format_4() { return &___format_4; }
	inline void set_format_4(int32_t value)
	{
		___format_4 = value;
	}

	inline static int32_t get_offset_of_data_5() { return static_cast<int32_t>(offsetof(APIImageBuffer_t15424157, ___data_5)); }
	inline IntPtr_t get_data_5() const { return ___data_5; }
	inline IntPtr_t* get_address_of_data_5() { return &___data_5; }
	inline void set_data_5(IntPtr_t value)
	{
		___data_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
