﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallThrower
struct  BallThrower_t1574671556  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject BallThrower::ballPrefab
	GameObject_t1756533147 * ___ballPrefab_2;
	// UnityEngine.Camera BallThrower::mainCamera
	Camera_t189460977 * ___mainCamera_3;
	// System.Single BallThrower::forwardVelocity
	float ___forwardVelocity_4;
	// UnityEngine.GameObject[] BallThrower::ballArray
	GameObjectU5BU5D_t3057952154* ___ballArray_5;
	// System.Int32 BallThrower::currentBallID
	int32_t ___currentBallID_6;

public:
	inline static int32_t get_offset_of_ballPrefab_2() { return static_cast<int32_t>(offsetof(BallThrower_t1574671556, ___ballPrefab_2)); }
	inline GameObject_t1756533147 * get_ballPrefab_2() const { return ___ballPrefab_2; }
	inline GameObject_t1756533147 ** get_address_of_ballPrefab_2() { return &___ballPrefab_2; }
	inline void set_ballPrefab_2(GameObject_t1756533147 * value)
	{
		___ballPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___ballPrefab_2, value);
	}

	inline static int32_t get_offset_of_mainCamera_3() { return static_cast<int32_t>(offsetof(BallThrower_t1574671556, ___mainCamera_3)); }
	inline Camera_t189460977 * get_mainCamera_3() const { return ___mainCamera_3; }
	inline Camera_t189460977 ** get_address_of_mainCamera_3() { return &___mainCamera_3; }
	inline void set_mainCamera_3(Camera_t189460977 * value)
	{
		___mainCamera_3 = value;
		Il2CppCodeGenWriteBarrier(&___mainCamera_3, value);
	}

	inline static int32_t get_offset_of_forwardVelocity_4() { return static_cast<int32_t>(offsetof(BallThrower_t1574671556, ___forwardVelocity_4)); }
	inline float get_forwardVelocity_4() const { return ___forwardVelocity_4; }
	inline float* get_address_of_forwardVelocity_4() { return &___forwardVelocity_4; }
	inline void set_forwardVelocity_4(float value)
	{
		___forwardVelocity_4 = value;
	}

	inline static int32_t get_offset_of_ballArray_5() { return static_cast<int32_t>(offsetof(BallThrower_t1574671556, ___ballArray_5)); }
	inline GameObjectU5BU5D_t3057952154* get_ballArray_5() const { return ___ballArray_5; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_ballArray_5() { return &___ballArray_5; }
	inline void set_ballArray_5(GameObjectU5BU5D_t3057952154* value)
	{
		___ballArray_5 = value;
		Il2CppCodeGenWriteBarrier(&___ballArray_5, value);
	}

	inline static int32_t get_offset_of_currentBallID_6() { return static_cast<int32_t>(offsetof(BallThrower_t1574671556, ___currentBallID_6)); }
	inline int32_t get_currentBallID_6() const { return ___currentBallID_6; }
	inline int32_t* get_address_of_currentBallID_6() { return &___currentBallID_6; }
	inline void set_currentBallID_6(int32_t value)
	{
		___currentBallID_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
