﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoCameraId106182192.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoCalibratio1064312954.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoCameraIntrinsics
struct  TangoCameraIntrinsics_t464275052  : public Il2CppObject
{
public:
	// Tango.TangoEnums/TangoCameraId Tango.TangoCameraIntrinsics::camera_id
	int32_t ___camera_id_0;
	// Tango.TangoEnums/TangoCalibrationType Tango.TangoCameraIntrinsics::calibration_type
	int32_t ___calibration_type_1;
	// System.UInt32 Tango.TangoCameraIntrinsics::width
	uint32_t ___width_2;
	// System.UInt32 Tango.TangoCameraIntrinsics::height
	uint32_t ___height_3;
	// System.Double Tango.TangoCameraIntrinsics::fx
	double ___fx_4;
	// System.Double Tango.TangoCameraIntrinsics::fy
	double ___fy_5;
	// System.Double Tango.TangoCameraIntrinsics::cx
	double ___cx_6;
	// System.Double Tango.TangoCameraIntrinsics::cy
	double ___cy_7;
	// System.Double Tango.TangoCameraIntrinsics::distortion0
	double ___distortion0_8;
	// System.Double Tango.TangoCameraIntrinsics::distortion1
	double ___distortion1_9;
	// System.Double Tango.TangoCameraIntrinsics::distortion2
	double ___distortion2_10;
	// System.Double Tango.TangoCameraIntrinsics::distortion3
	double ___distortion3_11;
	// System.Double Tango.TangoCameraIntrinsics::distortion4
	double ___distortion4_12;

public:
	inline static int32_t get_offset_of_camera_id_0() { return static_cast<int32_t>(offsetof(TangoCameraIntrinsics_t464275052, ___camera_id_0)); }
	inline int32_t get_camera_id_0() const { return ___camera_id_0; }
	inline int32_t* get_address_of_camera_id_0() { return &___camera_id_0; }
	inline void set_camera_id_0(int32_t value)
	{
		___camera_id_0 = value;
	}

	inline static int32_t get_offset_of_calibration_type_1() { return static_cast<int32_t>(offsetof(TangoCameraIntrinsics_t464275052, ___calibration_type_1)); }
	inline int32_t get_calibration_type_1() const { return ___calibration_type_1; }
	inline int32_t* get_address_of_calibration_type_1() { return &___calibration_type_1; }
	inline void set_calibration_type_1(int32_t value)
	{
		___calibration_type_1 = value;
	}

	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(TangoCameraIntrinsics_t464275052, ___width_2)); }
	inline uint32_t get_width_2() const { return ___width_2; }
	inline uint32_t* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(uint32_t value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(TangoCameraIntrinsics_t464275052, ___height_3)); }
	inline uint32_t get_height_3() const { return ___height_3; }
	inline uint32_t* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(uint32_t value)
	{
		___height_3 = value;
	}

	inline static int32_t get_offset_of_fx_4() { return static_cast<int32_t>(offsetof(TangoCameraIntrinsics_t464275052, ___fx_4)); }
	inline double get_fx_4() const { return ___fx_4; }
	inline double* get_address_of_fx_4() { return &___fx_4; }
	inline void set_fx_4(double value)
	{
		___fx_4 = value;
	}

	inline static int32_t get_offset_of_fy_5() { return static_cast<int32_t>(offsetof(TangoCameraIntrinsics_t464275052, ___fy_5)); }
	inline double get_fy_5() const { return ___fy_5; }
	inline double* get_address_of_fy_5() { return &___fy_5; }
	inline void set_fy_5(double value)
	{
		___fy_5 = value;
	}

	inline static int32_t get_offset_of_cx_6() { return static_cast<int32_t>(offsetof(TangoCameraIntrinsics_t464275052, ___cx_6)); }
	inline double get_cx_6() const { return ___cx_6; }
	inline double* get_address_of_cx_6() { return &___cx_6; }
	inline void set_cx_6(double value)
	{
		___cx_6 = value;
	}

	inline static int32_t get_offset_of_cy_7() { return static_cast<int32_t>(offsetof(TangoCameraIntrinsics_t464275052, ___cy_7)); }
	inline double get_cy_7() const { return ___cy_7; }
	inline double* get_address_of_cy_7() { return &___cy_7; }
	inline void set_cy_7(double value)
	{
		___cy_7 = value;
	}

	inline static int32_t get_offset_of_distortion0_8() { return static_cast<int32_t>(offsetof(TangoCameraIntrinsics_t464275052, ___distortion0_8)); }
	inline double get_distortion0_8() const { return ___distortion0_8; }
	inline double* get_address_of_distortion0_8() { return &___distortion0_8; }
	inline void set_distortion0_8(double value)
	{
		___distortion0_8 = value;
	}

	inline static int32_t get_offset_of_distortion1_9() { return static_cast<int32_t>(offsetof(TangoCameraIntrinsics_t464275052, ___distortion1_9)); }
	inline double get_distortion1_9() const { return ___distortion1_9; }
	inline double* get_address_of_distortion1_9() { return &___distortion1_9; }
	inline void set_distortion1_9(double value)
	{
		___distortion1_9 = value;
	}

	inline static int32_t get_offset_of_distortion2_10() { return static_cast<int32_t>(offsetof(TangoCameraIntrinsics_t464275052, ___distortion2_10)); }
	inline double get_distortion2_10() const { return ___distortion2_10; }
	inline double* get_address_of_distortion2_10() { return &___distortion2_10; }
	inline void set_distortion2_10(double value)
	{
		___distortion2_10 = value;
	}

	inline static int32_t get_offset_of_distortion3_11() { return static_cast<int32_t>(offsetof(TangoCameraIntrinsics_t464275052, ___distortion3_11)); }
	inline double get_distortion3_11() const { return ___distortion3_11; }
	inline double* get_address_of_distortion3_11() { return &___distortion3_11; }
	inline void set_distortion3_11(double value)
	{
		___distortion3_11 = value;
	}

	inline static int32_t get_offset_of_distortion4_12() { return static_cast<int32_t>(offsetof(TangoCameraIntrinsics_t464275052, ___distortion4_12)); }
	inline double get_distortion4_12() const { return ___distortion4_12; }
	inline double* get_address_of_distortion4_12() { return &___distortion4_12; }
	inline void set_distortion4_12(double value)
	{
		___distortion4_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Tango.TangoCameraIntrinsics
struct TangoCameraIntrinsics_t464275052_marshaled_pinvoke
{
	int32_t ___camera_id_0;
	int32_t ___calibration_type_1;
	int32_t ___width_2;
	int32_t ___height_3;
	double ___fx_4;
	double ___fy_5;
	double ___cx_6;
	double ___cy_7;
	double ___distortion0_8;
	double ___distortion1_9;
	double ___distortion2_10;
	double ___distortion3_11;
	double ___distortion4_12;
};
// Native definition for COM marshalling of Tango.TangoCameraIntrinsics
struct TangoCameraIntrinsics_t464275052_marshaled_com
{
	int32_t ___camera_id_0;
	int32_t ___calibration_type_1;
	int32_t ___width_2;
	int32_t ___height_3;
	double ___fx_4;
	double ___fy_5;
	double ___cx_6;
	double ___cy_7;
	double ___distortion0_8;
	double ___distortion1_9;
	double ___distortion2_10;
	double ___distortion3_11;
	double ___distortion4_12;
};
