﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoImageForma3932021136.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoUnityImageData
struct  TangoUnityImageData_t4168844635  : public Il2CppObject
{
public:
	// System.UInt32 Tango.TangoUnityImageData::width
	uint32_t ___width_0;
	// System.UInt32 Tango.TangoUnityImageData::height
	uint32_t ___height_1;
	// System.UInt32 Tango.TangoUnityImageData::stride
	uint32_t ___stride_2;
	// System.Double Tango.TangoUnityImageData::timestamp
	double ___timestamp_3;
	// System.Int64 Tango.TangoUnityImageData::frame_number
	int64_t ___frame_number_4;
	// Tango.TangoEnums/TangoImageFormatType Tango.TangoUnityImageData::format
	int32_t ___format_5;
	// System.Byte[] Tango.TangoUnityImageData::data
	ByteU5BU5D_t3397334013* ___data_6;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(TangoUnityImageData_t4168844635, ___width_0)); }
	inline uint32_t get_width_0() const { return ___width_0; }
	inline uint32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(uint32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(TangoUnityImageData_t4168844635, ___height_1)); }
	inline uint32_t get_height_1() const { return ___height_1; }
	inline uint32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(uint32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_stride_2() { return static_cast<int32_t>(offsetof(TangoUnityImageData_t4168844635, ___stride_2)); }
	inline uint32_t get_stride_2() const { return ___stride_2; }
	inline uint32_t* get_address_of_stride_2() { return &___stride_2; }
	inline void set_stride_2(uint32_t value)
	{
		___stride_2 = value;
	}

	inline static int32_t get_offset_of_timestamp_3() { return static_cast<int32_t>(offsetof(TangoUnityImageData_t4168844635, ___timestamp_3)); }
	inline double get_timestamp_3() const { return ___timestamp_3; }
	inline double* get_address_of_timestamp_3() { return &___timestamp_3; }
	inline void set_timestamp_3(double value)
	{
		___timestamp_3 = value;
	}

	inline static int32_t get_offset_of_frame_number_4() { return static_cast<int32_t>(offsetof(TangoUnityImageData_t4168844635, ___frame_number_4)); }
	inline int64_t get_frame_number_4() const { return ___frame_number_4; }
	inline int64_t* get_address_of_frame_number_4() { return &___frame_number_4; }
	inline void set_frame_number_4(int64_t value)
	{
		___frame_number_4 = value;
	}

	inline static int32_t get_offset_of_format_5() { return static_cast<int32_t>(offsetof(TangoUnityImageData_t4168844635, ___format_5)); }
	inline int32_t get_format_5() const { return ___format_5; }
	inline int32_t* get_address_of_format_5() { return &___format_5; }
	inline void set_format_5(int32_t value)
	{
		___format_5 = value;
	}

	inline static int32_t get_offset_of_data_6() { return static_cast<int32_t>(offsetof(TangoUnityImageData_t4168844635, ___data_6)); }
	inline ByteU5BU5D_t3397334013* get_data_6() const { return ___data_6; }
	inline ByteU5BU5D_t3397334013** get_address_of_data_6() { return &___data_6; }
	inline void set_data_6(ByteU5BU5D_t3397334013* value)
	{
		___data_6 = value;
		Il2CppCodeGenWriteBarrier(&___data_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
