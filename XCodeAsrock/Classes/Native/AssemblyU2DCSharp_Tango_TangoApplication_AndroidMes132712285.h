﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_AndroidMes591434573.h"

// System.Object[]
struct ObjectU5BU5D_t3614634134;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoApplication/AndroidMessage
struct  AndroidMessage_t132712285 
{
public:
	// Tango.TangoApplication/AndroidMessageType Tango.TangoApplication/AndroidMessage::m_type
	int32_t ___m_type_0;
	// System.Object[] Tango.TangoApplication/AndroidMessage::m_messages
	ObjectU5BU5D_t3614634134* ___m_messages_1;

public:
	inline static int32_t get_offset_of_m_type_0() { return static_cast<int32_t>(offsetof(AndroidMessage_t132712285, ___m_type_0)); }
	inline int32_t get_m_type_0() const { return ___m_type_0; }
	inline int32_t* get_address_of_m_type_0() { return &___m_type_0; }
	inline void set_m_type_0(int32_t value)
	{
		___m_type_0 = value;
	}

	inline static int32_t get_offset_of_m_messages_1() { return static_cast<int32_t>(offsetof(AndroidMessage_t132712285, ___m_messages_1)); }
	inline ObjectU5BU5D_t3614634134* get_m_messages_1() const { return ___m_messages_1; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_messages_1() { return &___m_messages_1; }
	inline void set_m_messages_1(ObjectU5BU5D_t3614634134* value)
	{
		___m_messages_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_messages_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Tango.TangoApplication/AndroidMessage
struct AndroidMessage_t132712285_marshaled_pinvoke
{
	int32_t ___m_type_0;
	ObjectU5BU5D_t3614634134* ___m_messages_1;
};
// Native definition for COM marshalling of Tango.TangoApplication/AndroidMessage
struct AndroidMessage_t132712285_marshaled_com
{
	int32_t ___m_type_0;
	ObjectU5BU5D_t3614634134* ___m_messages_1;
};
