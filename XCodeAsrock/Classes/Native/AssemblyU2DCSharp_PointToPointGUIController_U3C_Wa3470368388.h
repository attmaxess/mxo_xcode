﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.Camera
struct Camera_t189460977;
// PointToPointGUIController
struct PointToPointGUIController_t1162740098;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PointToPointGUIController/<_WaitForDepth>c__Iterator0
struct  U3C_WaitForDepthU3Ec__Iterator0_t3470368388  : public Il2CppObject
{
public:
	// UnityEngine.Camera PointToPointGUIController/<_WaitForDepth>c__Iterator0::<cam>__0
	Camera_t189460977 * ___U3CcamU3E__0_0;
	// UnityEngine.Vector2 PointToPointGUIController/<_WaitForDepth>c__Iterator0::touchPosition
	Vector2_t2243707579  ___touchPosition_1;
	// System.Int32 PointToPointGUIController/<_WaitForDepth>c__Iterator0::<pointIndex>__0
	int32_t ___U3CpointIndexU3E__0_2;
	// PointToPointGUIController PointToPointGUIController/<_WaitForDepth>c__Iterator0::$this
	PointToPointGUIController_t1162740098 * ___U24this_3;
	// System.Object PointToPointGUIController/<_WaitForDepth>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean PointToPointGUIController/<_WaitForDepth>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 PointToPointGUIController/<_WaitForDepth>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CcamU3E__0_0() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthU3Ec__Iterator0_t3470368388, ___U3CcamU3E__0_0)); }
	inline Camera_t189460977 * get_U3CcamU3E__0_0() const { return ___U3CcamU3E__0_0; }
	inline Camera_t189460977 ** get_address_of_U3CcamU3E__0_0() { return &___U3CcamU3E__0_0; }
	inline void set_U3CcamU3E__0_0(Camera_t189460977 * value)
	{
		___U3CcamU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcamU3E__0_0, value);
	}

	inline static int32_t get_offset_of_touchPosition_1() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthU3Ec__Iterator0_t3470368388, ___touchPosition_1)); }
	inline Vector2_t2243707579  get_touchPosition_1() const { return ___touchPosition_1; }
	inline Vector2_t2243707579 * get_address_of_touchPosition_1() { return &___touchPosition_1; }
	inline void set_touchPosition_1(Vector2_t2243707579  value)
	{
		___touchPosition_1 = value;
	}

	inline static int32_t get_offset_of_U3CpointIndexU3E__0_2() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthU3Ec__Iterator0_t3470368388, ___U3CpointIndexU3E__0_2)); }
	inline int32_t get_U3CpointIndexU3E__0_2() const { return ___U3CpointIndexU3E__0_2; }
	inline int32_t* get_address_of_U3CpointIndexU3E__0_2() { return &___U3CpointIndexU3E__0_2; }
	inline void set_U3CpointIndexU3E__0_2(int32_t value)
	{
		___U3CpointIndexU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthU3Ec__Iterator0_t3470368388, ___U24this_3)); }
	inline PointToPointGUIController_t1162740098 * get_U24this_3() const { return ___U24this_3; }
	inline PointToPointGUIController_t1162740098 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(PointToPointGUIController_t1162740098 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthU3Ec__Iterator0_t3470368388, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthU3Ec__Iterator0_t3470368388, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthU3Ec__Iterator0_t3470368388, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
