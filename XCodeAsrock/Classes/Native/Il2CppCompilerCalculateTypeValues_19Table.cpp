﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlDateTimeSerializationMode137774893.h"
#include "System_Xml_System_Xml_XmlDeclaration1545359137.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlDocumentFragment3083262362.h"
#include "System_Xml_System_Xml_XmlDocumentType824160610.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"
#include "System_Xml_System_Xml_XmlEntity4027255380.h"
#include "System_Xml_System_Xml_XmlEntityReference3053868353.h"
#include "System_Xml_System_Xml_XmlException4188277960.h"
#include "System_Xml_System_Xml_XmlImplementation1664517635.h"
#include "System_Xml_System_Xml_XmlStreamReader2725532304.h"
#include "System_Xml_System_Xml_NonBlockingStreamReader3963211903.h"
#include "System_Xml_System_Xml_XmlInputStream2650744719.h"
#include "System_Xml_System_Xml_XmlLinkedNode1287616130.h"
#include "System_Xml_System_Xml_XmlNameEntry3745551716.h"
#include "System_Xml_System_Xml_XmlNameEntryCache3855584002.h"
#include "System_Xml_System_Xml_XmlNameTable1345805268.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap145210370.h"
#include "System_Xml_System_Xml_XmlNamespaceManager486731501.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl3210081295.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope2513625351.h"
#include "System_Xml_System_Xml_XmlNode616554813.h"
#include "System_Xml_System_Xml_XmlNode_EmptyNodeList1718403287.h"
#include "System_Xml_System_Xml_XmlNodeChangedAction1188489541.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventArgs4036174778.h"
#include "System_Xml_System_Xml_XmlNodeList497326455.h"
#include "System_Xml_System_Xml_XmlNodeListChildren2811458520.h"
#include "System_Xml_System_Xml_XmlNodeListChildren_Enumerato569056069.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "System_Xml_System_Xml_XmlNotation206561061.h"
#include "System_Xml_System_Xml_XmlOutputMethod2267235953.h"
#include "System_Xml_System_Xml_XmlParserContext2728039553.h"
#include "System_Xml_System_Xml_XmlParserContext_ContextItem1262420678.h"
#include "System_Xml_System_Xml_XmlParserInput2366782760.h"
#include "System_Xml_System_Xml_XmlParserInput_XmlParserInputS25800784.h"
#include "System_Xml_System_Xml_XmlProcessingInstruction431557540.h"
#include "System_Xml_System_Xml_XmlQualifiedName1944712516.h"
#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport1548133672.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_Comma1644897369.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_CharG1955031820.h"
#include "System_Xml_System_Xml_XmlReaderSettings1578612233.h"
#include "System_Xml_System_Xml_XmlResolver2024571559.h"
#include "System_Xml_System_Xml_XmlSignificantWhitespace1224054391.h"
#include "System_Xml_System_Xml_XmlSpace2880376877.h"
#include "System_Xml_System_Xml_XmlText4111601336.h"
#include "System_Xml_Mono_Xml2_XmlTextReader511376973.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlTokenInfo254587324.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlAttributeTok3353594030.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName2340974457.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputState3313602765.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputStateSt3023928423.h"
#include "System_Xml_System_Xml_XmlTextReader3514170725.h"
#include "System_Xml_System_Xml_XmlTextWriter2527250655.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlNodeInfo3709371029.h"
#include "System_Xml_System_Xml_XmlTextWriter_StringUtil2068578019.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlDeclState3530111136.h"
#include "System_Xml_System_Xml_XmlTokenizedType1619571710.h"
#include "System_Xml_System_Xml_XmlUrlResolver896669594.h"
#include "System_Xml_System_Xml_XmlWhitespace2557770518.h"
#include "System_Xml_System_Xml_XmlWriter1048088568.h"
#include "System_Xml_System_Xml_XmlWriterSettings924210539.h"
#include "System_Xml_System_Xml_Serialization_UnreferencedOb3651715031.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeEv1549396895.h"
#include "System_Xml_System_Xml_Serialization_XmlElementEven1680966409.h"
#include "System_Xml_System_Xml_Serialization_XmlNodeEventHa2205849959.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati3113026122.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati4030450962.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati3162327146.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati3502762961.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventHandler2964483403.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3672778802.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1957337327.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2038352954.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar628910058.h"
#include "UnityEngine_UI_U3CModuleU3E3783534214.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle942672932.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg1967201810.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3959312622.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3365010046.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (XmlDateTimeSerializationMode_t137774893)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1900[5] = 
{
	XmlDateTimeSerializationMode_t137774893::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (XmlDeclaration_t1545359137), -1, sizeof(XmlDeclaration_t1545359137_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1901[4] = 
{
	XmlDeclaration_t1545359137::get_offset_of_encoding_6(),
	XmlDeclaration_t1545359137::get_offset_of_standalone_7(),
	XmlDeclaration_t1545359137::get_offset_of_version_8(),
	XmlDeclaration_t1545359137_StaticFields::get_offset_of_U3CU3Ef__switchU24map4A_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (XmlDocument_t3649534162), -1, sizeof(XmlDocument_t3649534162_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1902[19] = 
{
	XmlDocument_t3649534162_StaticFields::get_offset_of_optimal_create_types_5(),
	XmlDocument_t3649534162::get_offset_of_optimal_create_element_6(),
	XmlDocument_t3649534162::get_offset_of_optimal_create_attribute_7(),
	XmlDocument_t3649534162::get_offset_of_nameTable_8(),
	XmlDocument_t3649534162::get_offset_of_baseURI_9(),
	XmlDocument_t3649534162::get_offset_of_implementation_10(),
	XmlDocument_t3649534162::get_offset_of_preserveWhitespace_11(),
	XmlDocument_t3649534162::get_offset_of_resolver_12(),
	XmlDocument_t3649534162::get_offset_of_idTable_13(),
	XmlDocument_t3649534162::get_offset_of_nameCache_14(),
	XmlDocument_t3649534162::get_offset_of_lastLinkedChild_15(),
	XmlDocument_t3649534162::get_offset_of_schemaInfo_16(),
	XmlDocument_t3649534162::get_offset_of_loadMode_17(),
	XmlDocument_t3649534162::get_offset_of_NodeChanged_18(),
	XmlDocument_t3649534162::get_offset_of_NodeChanging_19(),
	XmlDocument_t3649534162::get_offset_of_NodeInserted_20(),
	XmlDocument_t3649534162::get_offset_of_NodeInserting_21(),
	XmlDocument_t3649534162::get_offset_of_NodeRemoved_22(),
	XmlDocument_t3649534162::get_offset_of_NodeRemoving_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (XmlDocumentFragment_t3083262362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1903[1] = 
{
	XmlDocumentFragment_t3083262362::get_offset_of_lastLinkedChild_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (XmlDocumentType_t824160610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1904[3] = 
{
	XmlDocumentType_t824160610::get_offset_of_entities_6(),
	XmlDocumentType_t824160610::get_offset_of_notations_7(),
	XmlDocumentType_t824160610::get_offset_of_dtd_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (XmlElement_t2877111883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[5] = 
{
	XmlElement_t2877111883::get_offset_of_attributes_6(),
	XmlElement_t2877111883::get_offset_of_name_7(),
	XmlElement_t2877111883::get_offset_of_lastLinkedChild_8(),
	XmlElement_t2877111883::get_offset_of_isNotEmpty_9(),
	XmlElement_t2877111883::get_offset_of_schemaInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (XmlEntity_t4027255380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1906[7] = 
{
	XmlEntity_t4027255380::get_offset_of_name_5(),
	XmlEntity_t4027255380::get_offset_of_NDATA_6(),
	XmlEntity_t4027255380::get_offset_of_publicId_7(),
	XmlEntity_t4027255380::get_offset_of_systemId_8(),
	XmlEntity_t4027255380::get_offset_of_baseUri_9(),
	XmlEntity_t4027255380::get_offset_of_lastLinkedChild_10(),
	XmlEntity_t4027255380::get_offset_of_contentAlreadySet_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (XmlEntityReference_t3053868353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1907[2] = 
{
	XmlEntityReference_t3053868353::get_offset_of_entityName_6(),
	XmlEntityReference_t3053868353::get_offset_of_lastLinkedChild_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (XmlException_t4188277960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[5] = 
{
	XmlException_t4188277960::get_offset_of_lineNumber_11(),
	XmlException_t4188277960::get_offset_of_linePosition_12(),
	XmlException_t4188277960::get_offset_of_sourceUri_13(),
	XmlException_t4188277960::get_offset_of_res_14(),
	XmlException_t4188277960::get_offset_of_messages_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (XmlImplementation_t1664517635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1909[1] = 
{
	XmlImplementation_t1664517635::get_offset_of_InternalNameTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (XmlStreamReader_t2725532304), -1, sizeof(XmlStreamReader_t2725532304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1910[2] = 
{
	XmlStreamReader_t2725532304::get_offset_of_input_12(),
	XmlStreamReader_t2725532304_StaticFields::get_offset_of_invalidDataException_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (NonBlockingStreamReader_t3963211903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1911[11] = 
{
	NonBlockingStreamReader_t3963211903::get_offset_of_input_buffer_1(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoded_buffer_2(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoded_count_3(),
	NonBlockingStreamReader_t3963211903::get_offset_of_pos_4(),
	NonBlockingStreamReader_t3963211903::get_offset_of_buffer_size_5(),
	NonBlockingStreamReader_t3963211903::get_offset_of_encoding_6(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoder_7(),
	NonBlockingStreamReader_t3963211903::get_offset_of_base_stream_8(),
	NonBlockingStreamReader_t3963211903::get_offset_of_mayBlock_9(),
	NonBlockingStreamReader_t3963211903::get_offset_of_line_builder_10(),
	NonBlockingStreamReader_t3963211903::get_offset_of_foundCR_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (XmlInputStream_t2650744719), -1, sizeof(XmlInputStream_t2650744719_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1912[7] = 
{
	XmlInputStream_t2650744719_StaticFields::get_offset_of_StrictUTF8_1(),
	XmlInputStream_t2650744719::get_offset_of_enc_2(),
	XmlInputStream_t2650744719::get_offset_of_stream_3(),
	XmlInputStream_t2650744719::get_offset_of_buffer_4(),
	XmlInputStream_t2650744719::get_offset_of_bufLength_5(),
	XmlInputStream_t2650744719::get_offset_of_bufPos_6(),
	XmlInputStream_t2650744719_StaticFields::get_offset_of_encodingException_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (XmlLinkedNode_t1287616130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1913[1] = 
{
	XmlLinkedNode_t1287616130::get_offset_of_nextSibling_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (XmlNameEntry_t3745551716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1914[5] = 
{
	XmlNameEntry_t3745551716::get_offset_of_Prefix_0(),
	XmlNameEntry_t3745551716::get_offset_of_LocalName_1(),
	XmlNameEntry_t3745551716::get_offset_of_NS_2(),
	XmlNameEntry_t3745551716::get_offset_of_Hash_3(),
	XmlNameEntry_t3745551716::get_offset_of_prefixed_name_cache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (XmlNameEntryCache_t3855584002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1915[4] = 
{
	XmlNameEntryCache_t3855584002::get_offset_of_table_0(),
	XmlNameEntryCache_t3855584002::get_offset_of_nameTable_1(),
	XmlNameEntryCache_t3855584002::get_offset_of_dummy_2(),
	XmlNameEntryCache_t3855584002::get_offset_of_cacheBuffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (XmlNameTable_t1345805268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (XmlNamedNodeMap_t145210370), -1, sizeof(XmlNamedNodeMap_t145210370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1917[4] = 
{
	XmlNamedNodeMap_t145210370_StaticFields::get_offset_of_emptyEnumerator_0(),
	XmlNamedNodeMap_t145210370::get_offset_of_parent_1(),
	XmlNamedNodeMap_t145210370::get_offset_of_nodeList_2(),
	XmlNamedNodeMap_t145210370::get_offset_of_readOnly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (XmlNamespaceManager_t486731501), -1, sizeof(XmlNamespaceManager_t486731501_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1918[9] = 
{
	XmlNamespaceManager_t486731501::get_offset_of_decls_0(),
	XmlNamespaceManager_t486731501::get_offset_of_declPos_1(),
	XmlNamespaceManager_t486731501::get_offset_of_scopes_2(),
	XmlNamespaceManager_t486731501::get_offset_of_scopePos_3(),
	XmlNamespaceManager_t486731501::get_offset_of_defaultNamespace_4(),
	XmlNamespaceManager_t486731501::get_offset_of_count_5(),
	XmlNamespaceManager_t486731501::get_offset_of_nameTable_6(),
	XmlNamespaceManager_t486731501::get_offset_of_internalAtomizedNames_7(),
	XmlNamespaceManager_t486731501_StaticFields::get_offset_of_U3CU3Ef__switchU24map28_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (NsDecl_t3210081295)+ sizeof (Il2CppObject), sizeof(NsDecl_t3210081295_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1919[2] = 
{
	NsDecl_t3210081295::get_offset_of_Prefix_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsDecl_t3210081295::get_offset_of_Uri_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (NsScope_t2513625351)+ sizeof (Il2CppObject), sizeof(NsScope_t2513625351_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1920[2] = 
{
	NsScope_t2513625351::get_offset_of_DeclCount_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsScope_t2513625351::get_offset_of_DefaultNamespace_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (XmlNode_t616554813), -1, sizeof(XmlNode_t616554813_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1921[5] = 
{
	XmlNode_t616554813_StaticFields::get_offset_of_emptyList_0(),
	XmlNode_t616554813::get_offset_of_ownerDocument_1(),
	XmlNode_t616554813::get_offset_of_parentNode_2(),
	XmlNode_t616554813::get_offset_of_childNodes_3(),
	XmlNode_t616554813_StaticFields::get_offset_of_U3CU3Ef__switchU24map44_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (EmptyNodeList_t1718403287), -1, sizeof(EmptyNodeList_t1718403287_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1922[1] = 
{
	EmptyNodeList_t1718403287_StaticFields::get_offset_of_emptyEnumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (XmlNodeChangedAction_t1188489541)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1923[4] = 
{
	XmlNodeChangedAction_t1188489541::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (XmlNodeChangedEventArgs_t4036174778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[6] = 
{
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__oldParent_1(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__newParent_2(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__action_3(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__node_4(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__oldValue_5(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__newValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (XmlNodeList_t497326455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (XmlNodeListChildren_t2811458520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1926[1] = 
{
	XmlNodeListChildren_t2811458520::get_offset_of_parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (Enumerator_t569056069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1927[3] = 
{
	Enumerator_t569056069::get_offset_of_parent_0(),
	Enumerator_t569056069::get_offset_of_currentChild_1(),
	Enumerator_t569056069::get_offset_of_passedLastNode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (XmlNodeType_t739504597)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1928[19] = 
{
	XmlNodeType_t739504597::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (XmlNotation_t206561061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1929[4] = 
{
	XmlNotation_t206561061::get_offset_of_localName_5(),
	XmlNotation_t206561061::get_offset_of_publicId_6(),
	XmlNotation_t206561061::get_offset_of_systemId_7(),
	XmlNotation_t206561061::get_offset_of_prefix_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (XmlOutputMethod_t2267235953)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1930[5] = 
{
	XmlOutputMethod_t2267235953::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (XmlParserContext_t2728039553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1931[13] = 
{
	XmlParserContext_t2728039553::get_offset_of_baseURI_0(),
	XmlParserContext_t2728039553::get_offset_of_docTypeName_1(),
	XmlParserContext_t2728039553::get_offset_of_encoding_2(),
	XmlParserContext_t2728039553::get_offset_of_internalSubset_3(),
	XmlParserContext_t2728039553::get_offset_of_namespaceManager_4(),
	XmlParserContext_t2728039553::get_offset_of_nameTable_5(),
	XmlParserContext_t2728039553::get_offset_of_publicID_6(),
	XmlParserContext_t2728039553::get_offset_of_systemID_7(),
	XmlParserContext_t2728039553::get_offset_of_xmlLang_8(),
	XmlParserContext_t2728039553::get_offset_of_xmlSpace_9(),
	XmlParserContext_t2728039553::get_offset_of_contextItems_10(),
	XmlParserContext_t2728039553::get_offset_of_contextItemCount_11(),
	XmlParserContext_t2728039553::get_offset_of_dtd_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (ContextItem_t1262420678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1932[3] = 
{
	ContextItem_t1262420678::get_offset_of_BaseURI_0(),
	ContextItem_t1262420678::get_offset_of_XmlLang_1(),
	ContextItem_t1262420678::get_offset_of_XmlSpace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (XmlParserInput_t2366782760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1933[5] = 
{
	XmlParserInput_t2366782760::get_offset_of_sourceStack_0(),
	XmlParserInput_t2366782760::get_offset_of_source_1(),
	XmlParserInput_t2366782760::get_offset_of_has_peek_2(),
	XmlParserInput_t2366782760::get_offset_of_peek_char_3(),
	XmlParserInput_t2366782760::get_offset_of_allowTextDecl_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (XmlParserInputSource_t25800784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1934[6] = 
{
	XmlParserInputSource_t25800784::get_offset_of_BaseURI_0(),
	XmlParserInputSource_t25800784::get_offset_of_reader_1(),
	XmlParserInputSource_t25800784::get_offset_of_state_2(),
	XmlParserInputSource_t25800784::get_offset_of_isPE_3(),
	XmlParserInputSource_t25800784::get_offset_of_line_4(),
	XmlParserInputSource_t25800784::get_offset_of_column_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (XmlProcessingInstruction_t431557540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1935[2] = 
{
	XmlProcessingInstruction_t431557540::get_offset_of_target_6(),
	XmlProcessingInstruction_t431557540::get_offset_of_data_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (XmlQualifiedName_t1944712516), -1, sizeof(XmlQualifiedName_t1944712516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1936[4] = 
{
	XmlQualifiedName_t1944712516_StaticFields::get_offset_of_Empty_0(),
	XmlQualifiedName_t1944712516::get_offset_of_name_1(),
	XmlQualifiedName_t1944712516::get_offset_of_ns_2(),
	XmlQualifiedName_t1944712516::get_offset_of_hash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (XmlReader_t3675626668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1937[3] = 
{
	XmlReader_t3675626668::get_offset_of_readStringBuffer_0(),
	XmlReader_t3675626668::get_offset_of_binary_1(),
	XmlReader_t3675626668::get_offset_of_settings_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (XmlReaderBinarySupport_t1548133672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1938[5] = 
{
	XmlReaderBinarySupport_t1548133672::get_offset_of_reader_0(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_base64CacheStartsAt_1(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_state_2(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_hasCache_3(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_dontReset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (CommandState_t1644897369)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1939[6] = 
{
	CommandState_t1644897369::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (CharGetter_t1955031820), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (XmlReaderSettings_t1578612233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1941[2] = 
{
	XmlReaderSettings_t1578612233::get_offset_of_checkCharacters_0(),
	XmlReaderSettings_t1578612233::get_offset_of_conformance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (XmlResolver_t2024571559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (XmlSignificantWhitespace_t1224054391), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (XmlSpace_t2880376877)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1944[4] = 
{
	XmlSpace_t2880376877::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (XmlText_t4111601336), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (XmlTextReader_t511376973), -1, sizeof(XmlTextReader_t511376973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1946[54] = 
{
	XmlTextReader_t511376973::get_offset_of_cursorToken_3(),
	XmlTextReader_t511376973::get_offset_of_currentToken_4(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeToken_5(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeValueToken_6(),
	XmlTextReader_t511376973::get_offset_of_attributeTokens_7(),
	XmlTextReader_t511376973::get_offset_of_attributeValueTokens_8(),
	XmlTextReader_t511376973::get_offset_of_currentAttribute_9(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeValue_10(),
	XmlTextReader_t511376973::get_offset_of_attributeCount_11(),
	XmlTextReader_t511376973::get_offset_of_parserContext_12(),
	XmlTextReader_t511376973::get_offset_of_nameTable_13(),
	XmlTextReader_t511376973::get_offset_of_nsmgr_14(),
	XmlTextReader_t511376973::get_offset_of_readState_15(),
	XmlTextReader_t511376973::get_offset_of_disallowReset_16(),
	XmlTextReader_t511376973::get_offset_of_depth_17(),
	XmlTextReader_t511376973::get_offset_of_elementDepth_18(),
	XmlTextReader_t511376973::get_offset_of_depthUp_19(),
	XmlTextReader_t511376973::get_offset_of_popScope_20(),
	XmlTextReader_t511376973::get_offset_of_elementNames_21(),
	XmlTextReader_t511376973::get_offset_of_elementNameStackPos_22(),
	XmlTextReader_t511376973::get_offset_of_allowMultipleRoot_23(),
	XmlTextReader_t511376973::get_offset_of_isStandalone_24(),
	XmlTextReader_t511376973::get_offset_of_returnEntityReference_25(),
	XmlTextReader_t511376973::get_offset_of_entityReferenceName_26(),
	XmlTextReader_t511376973::get_offset_of_valueBuffer_27(),
	XmlTextReader_t511376973::get_offset_of_reader_28(),
	XmlTextReader_t511376973::get_offset_of_peekChars_29(),
	XmlTextReader_t511376973::get_offset_of_peekCharsIndex_30(),
	XmlTextReader_t511376973::get_offset_of_peekCharsLength_31(),
	XmlTextReader_t511376973::get_offset_of_curNodePeekIndex_32(),
	XmlTextReader_t511376973::get_offset_of_preserveCurrentTag_33(),
	XmlTextReader_t511376973::get_offset_of_line_34(),
	XmlTextReader_t511376973::get_offset_of_column_35(),
	XmlTextReader_t511376973::get_offset_of_currentLinkedNodeLineNumber_36(),
	XmlTextReader_t511376973::get_offset_of_currentLinkedNodeLinePosition_37(),
	XmlTextReader_t511376973::get_offset_of_useProceedingLineInfo_38(),
	XmlTextReader_t511376973::get_offset_of_startNodeType_39(),
	XmlTextReader_t511376973::get_offset_of_currentState_40(),
	XmlTextReader_t511376973::get_offset_of_nestLevel_41(),
	XmlTextReader_t511376973::get_offset_of_readCharsInProgress_42(),
	XmlTextReader_t511376973::get_offset_of_binaryCharGetter_43(),
	XmlTextReader_t511376973::get_offset_of_namespaces_44(),
	XmlTextReader_t511376973::get_offset_of_whitespaceHandling_45(),
	XmlTextReader_t511376973::get_offset_of_resolver_46(),
	XmlTextReader_t511376973::get_offset_of_normalization_47(),
	XmlTextReader_t511376973::get_offset_of_checkCharacters_48(),
	XmlTextReader_t511376973::get_offset_of_prohibitDtd_49(),
	XmlTextReader_t511376973::get_offset_of_closeInput_50(),
	XmlTextReader_t511376973::get_offset_of_entityHandling_51(),
	XmlTextReader_t511376973::get_offset_of_whitespacePool_52(),
	XmlTextReader_t511376973::get_offset_of_whitespaceCache_53(),
	XmlTextReader_t511376973::get_offset_of_stateStack_54(),
	XmlTextReader_t511376973_StaticFields::get_offset_of_U3CU3Ef__switchU24map51_55(),
	XmlTextReader_t511376973_StaticFields::get_offset_of_U3CU3Ef__switchU24map52_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (XmlTokenInfo_t254587324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1947[13] = 
{
	XmlTokenInfo_t254587324::get_offset_of_valueCache_0(),
	XmlTokenInfo_t254587324::get_offset_of_Reader_1(),
	XmlTokenInfo_t254587324::get_offset_of_Name_2(),
	XmlTokenInfo_t254587324::get_offset_of_LocalName_3(),
	XmlTokenInfo_t254587324::get_offset_of_Prefix_4(),
	XmlTokenInfo_t254587324::get_offset_of_NamespaceURI_5(),
	XmlTokenInfo_t254587324::get_offset_of_IsEmptyElement_6(),
	XmlTokenInfo_t254587324::get_offset_of_QuoteChar_7(),
	XmlTokenInfo_t254587324::get_offset_of_LineNumber_8(),
	XmlTokenInfo_t254587324::get_offset_of_LinePosition_9(),
	XmlTokenInfo_t254587324::get_offset_of_ValueBufferStart_10(),
	XmlTokenInfo_t254587324::get_offset_of_ValueBufferEnd_11(),
	XmlTokenInfo_t254587324::get_offset_of_NodeType_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (XmlAttributeTokenInfo_t3353594030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1948[4] = 
{
	XmlAttributeTokenInfo_t3353594030::get_offset_of_ValueTokenStartIndex_13(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_ValueTokenEndIndex_14(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_valueCache_15(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_tmpBuilder_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (TagName_t2340974457)+ sizeof (Il2CppObject), sizeof(TagName_t2340974457_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1949[3] = 
{
	TagName_t2340974457::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2340974457::get_offset_of_LocalName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2340974457::get_offset_of_Prefix_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (DtdInputState_t3313602765)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1950[10] = 
{
	DtdInputState_t3313602765::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (DtdInputStateStack_t3023928423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[1] = 
{
	DtdInputStateStack_t3023928423::get_offset_of_intern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (XmlTextReader_t3514170725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1952[5] = 
{
	XmlTextReader_t3514170725::get_offset_of_entity_3(),
	XmlTextReader_t3514170725::get_offset_of_source_4(),
	XmlTextReader_t3514170725::get_offset_of_entityInsideAttribute_5(),
	XmlTextReader_t3514170725::get_offset_of_insideAttribute_6(),
	XmlTextReader_t3514170725::get_offset_of_entityNameStack_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (XmlTextWriter_t2527250655), -1, sizeof(XmlTextWriter_t2527250655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1953[35] = 
{
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_unmarked_utf8encoding_1(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_escaped_text_chars_2(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_escaped_attr_chars_3(),
	XmlTextWriter_t2527250655::get_offset_of_base_stream_4(),
	XmlTextWriter_t2527250655::get_offset_of_source_5(),
	XmlTextWriter_t2527250655::get_offset_of_writer_6(),
	XmlTextWriter_t2527250655::get_offset_of_preserver_7(),
	XmlTextWriter_t2527250655::get_offset_of_preserved_name_8(),
	XmlTextWriter_t2527250655::get_offset_of_is_preserved_xmlns_9(),
	XmlTextWriter_t2527250655::get_offset_of_allow_doc_fragment_10(),
	XmlTextWriter_t2527250655::get_offset_of_close_output_stream_11(),
	XmlTextWriter_t2527250655::get_offset_of_ignore_encoding_12(),
	XmlTextWriter_t2527250655::get_offset_of_namespaces_13(),
	XmlTextWriter_t2527250655::get_offset_of_xmldecl_state_14(),
	XmlTextWriter_t2527250655::get_offset_of_check_character_validity_15(),
	XmlTextWriter_t2527250655::get_offset_of_newline_handling_16(),
	XmlTextWriter_t2527250655::get_offset_of_is_document_entity_17(),
	XmlTextWriter_t2527250655::get_offset_of_state_18(),
	XmlTextWriter_t2527250655::get_offset_of_node_state_19(),
	XmlTextWriter_t2527250655::get_offset_of_nsmanager_20(),
	XmlTextWriter_t2527250655::get_offset_of_open_count_21(),
	XmlTextWriter_t2527250655::get_offset_of_elements_22(),
	XmlTextWriter_t2527250655::get_offset_of_new_local_namespaces_23(),
	XmlTextWriter_t2527250655::get_offset_of_explicit_nsdecls_24(),
	XmlTextWriter_t2527250655::get_offset_of_namespace_handling_25(),
	XmlTextWriter_t2527250655::get_offset_of_indent_26(),
	XmlTextWriter_t2527250655::get_offset_of_indent_count_27(),
	XmlTextWriter_t2527250655::get_offset_of_indent_char_28(),
	XmlTextWriter_t2527250655::get_offset_of_indent_string_29(),
	XmlTextWriter_t2527250655::get_offset_of_newline_30(),
	XmlTextWriter_t2527250655::get_offset_of_indent_attributes_31(),
	XmlTextWriter_t2527250655::get_offset_of_quote_char_32(),
	XmlTextWriter_t2527250655::get_offset_of_v2_33(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_U3CU3Ef__switchU24map53_34(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_U3CU3Ef__switchU24map54_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (XmlNodeInfo_t3709371029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1954[7] = 
{
	XmlNodeInfo_t3709371029::get_offset_of_Prefix_0(),
	XmlNodeInfo_t3709371029::get_offset_of_LocalName_1(),
	XmlNodeInfo_t3709371029::get_offset_of_NS_2(),
	XmlNodeInfo_t3709371029::get_offset_of_HasSimple_3(),
	XmlNodeInfo_t3709371029::get_offset_of_HasElements_4(),
	XmlNodeInfo_t3709371029::get_offset_of_XmlLang_5(),
	XmlNodeInfo_t3709371029::get_offset_of_XmlSpace_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (StringUtil_t2068578019), -1, sizeof(StringUtil_t2068578019_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1955[2] = 
{
	StringUtil_t2068578019_StaticFields::get_offset_of_cul_0(),
	StringUtil_t2068578019_StaticFields::get_offset_of_cmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (XmlDeclState_t3530111136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1956[5] = 
{
	XmlDeclState_t3530111136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (XmlTokenizedType_t1619571710)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1957[14] = 
{
	XmlTokenizedType_t1619571710::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (XmlUrlResolver_t896669594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1958[1] = 
{
	XmlUrlResolver_t896669594::get_offset_of_credential_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (XmlWhitespace_t2557770518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (XmlWriter_t1048088568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1960[1] = 
{
	XmlWriter_t1048088568::get_offset_of_settings_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (XmlWriterSettings_t924210539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1961[11] = 
{
	XmlWriterSettings_t924210539::get_offset_of_checkCharacters_0(),
	XmlWriterSettings_t924210539::get_offset_of_closeOutput_1(),
	XmlWriterSettings_t924210539::get_offset_of_conformance_2(),
	XmlWriterSettings_t924210539::get_offset_of_encoding_3(),
	XmlWriterSettings_t924210539::get_offset_of_indent_4(),
	XmlWriterSettings_t924210539::get_offset_of_indentChars_5(),
	XmlWriterSettings_t924210539::get_offset_of_newLineChars_6(),
	XmlWriterSettings_t924210539::get_offset_of_newLineOnAttributes_7(),
	XmlWriterSettings_t924210539::get_offset_of_newLineHandling_8(),
	XmlWriterSettings_t924210539::get_offset_of_omitXmlDeclaration_9(),
	XmlWriterSettings_t924210539::get_offset_of_outputMethod_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (UnreferencedObjectEventHandler_t3651715031), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (XmlAttributeEventHandler_t1549396895), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (XmlElementEventHandler_t1680966409), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (XmlNodeEventHandler_t2205849959), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (XmlSerializationCollectionFixupCallback_t3113026122), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (XmlSerializationFixupCallback_t4030450962), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (XmlSerializationReadCallback_t3162327146), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (XmlSerializationWriteCallback_t3502762961), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (XmlNodeChangedEventHandler_t2964483403), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1971[7] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D23_0(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D26_1(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D27_2(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D28_3(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D29_4(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D43_5(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D44_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (U24ArrayTypeU2412_t3672778807)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778807 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (U24ArrayTypeU248_t1957337328)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t1957337328 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (U24ArrayTypeU24256_t2038352957)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t2038352957 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (U24ArrayTypeU241280_t628910058)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241280_t628910058 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (EventHandle_t942672932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1977[3] = 
{
	EventHandle_t942672932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (EventSystem_t3466835263), -1, sizeof(EventSystem_t3466835263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1996[12] = 
{
	EventSystem_t3466835263::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t3466835263::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_4(),
	EventSystem_t3466835263::get_offset_of_m_FirstSelected_5(),
	EventSystem_t3466835263::get_offset_of_m_sendNavigationEvents_6(),
	EventSystem_t3466835263::get_offset_of_m_DragThreshold_7(),
	EventSystem_t3466835263::get_offset_of_m_CurrentSelected_8(),
	EventSystem_t3466835263::get_offset_of_m_Paused_9(),
	EventSystem_t3466835263::get_offset_of_m_SelectionGuard_10(),
	EventSystem_t3466835263::get_offset_of_m_DummyData_11(),
	EventSystem_t3466835263_StaticFields::get_offset_of_s_RaycastComparer_12(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (EventTrigger_t1967201810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1997[2] = 
{
	EventTrigger_t1967201810::get_offset_of_m_Delegates_2(),
	EventTrigger_t1967201810::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (TriggerEvent_t3959312622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (Entry_t3365010046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1999[2] = 
{
	Entry_t3365010046::get_offset_of_eventID_0(),
	Entry_t3365010046::get_offset_of_callback_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
