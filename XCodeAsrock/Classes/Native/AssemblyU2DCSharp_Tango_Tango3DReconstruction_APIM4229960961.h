﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_IntPtr2504060609.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.Tango3DReconstruction/APIMesh
struct  APIMesh_t4229960961 
{
public:
	// System.Double Tango.Tango3DReconstruction/APIMesh::timestamp
	double ___timestamp_0;
	// System.Int32 Tango.Tango3DReconstruction/APIMesh::numVertices
	int32_t ___numVertices_1;
	// System.Int32 Tango.Tango3DReconstruction/APIMesh::numFaces
	int32_t ___numFaces_2;
	// System.Int32 Tango.Tango3DReconstruction/APIMesh::numTextures
	int32_t ___numTextures_3;
	// System.Int32 Tango.Tango3DReconstruction/APIMesh::maxNumVertices
	int32_t ___maxNumVertices_4;
	// System.Int32 Tango.Tango3DReconstruction/APIMesh::maxNumFaces
	int32_t ___maxNumFaces_5;
	// System.Int32 Tango.Tango3DReconstruction/APIMesh::maxNumTextures
	int32_t ___maxNumTextures_6;
	// System.IntPtr Tango.Tango3DReconstruction/APIMesh::vertices
	IntPtr_t ___vertices_7;
	// System.IntPtr Tango.Tango3DReconstruction/APIMesh::faces
	IntPtr_t ___faces_8;
	// System.IntPtr Tango.Tango3DReconstruction/APIMesh::normals
	IntPtr_t ___normals_9;
	// System.IntPtr Tango.Tango3DReconstruction/APIMesh::colors
	IntPtr_t ___colors_10;
	// System.IntPtr Tango.Tango3DReconstruction/APIMesh::textureCoords
	IntPtr_t ___textureCoords_11;
	// System.IntPtr Tango.Tango3DReconstruction/APIMesh::textures
	IntPtr_t ___textures_12;

public:
	inline static int32_t get_offset_of_timestamp_0() { return static_cast<int32_t>(offsetof(APIMesh_t4229960961, ___timestamp_0)); }
	inline double get_timestamp_0() const { return ___timestamp_0; }
	inline double* get_address_of_timestamp_0() { return &___timestamp_0; }
	inline void set_timestamp_0(double value)
	{
		___timestamp_0 = value;
	}

	inline static int32_t get_offset_of_numVertices_1() { return static_cast<int32_t>(offsetof(APIMesh_t4229960961, ___numVertices_1)); }
	inline int32_t get_numVertices_1() const { return ___numVertices_1; }
	inline int32_t* get_address_of_numVertices_1() { return &___numVertices_1; }
	inline void set_numVertices_1(int32_t value)
	{
		___numVertices_1 = value;
	}

	inline static int32_t get_offset_of_numFaces_2() { return static_cast<int32_t>(offsetof(APIMesh_t4229960961, ___numFaces_2)); }
	inline int32_t get_numFaces_2() const { return ___numFaces_2; }
	inline int32_t* get_address_of_numFaces_2() { return &___numFaces_2; }
	inline void set_numFaces_2(int32_t value)
	{
		___numFaces_2 = value;
	}

	inline static int32_t get_offset_of_numTextures_3() { return static_cast<int32_t>(offsetof(APIMesh_t4229960961, ___numTextures_3)); }
	inline int32_t get_numTextures_3() const { return ___numTextures_3; }
	inline int32_t* get_address_of_numTextures_3() { return &___numTextures_3; }
	inline void set_numTextures_3(int32_t value)
	{
		___numTextures_3 = value;
	}

	inline static int32_t get_offset_of_maxNumVertices_4() { return static_cast<int32_t>(offsetof(APIMesh_t4229960961, ___maxNumVertices_4)); }
	inline int32_t get_maxNumVertices_4() const { return ___maxNumVertices_4; }
	inline int32_t* get_address_of_maxNumVertices_4() { return &___maxNumVertices_4; }
	inline void set_maxNumVertices_4(int32_t value)
	{
		___maxNumVertices_4 = value;
	}

	inline static int32_t get_offset_of_maxNumFaces_5() { return static_cast<int32_t>(offsetof(APIMesh_t4229960961, ___maxNumFaces_5)); }
	inline int32_t get_maxNumFaces_5() const { return ___maxNumFaces_5; }
	inline int32_t* get_address_of_maxNumFaces_5() { return &___maxNumFaces_5; }
	inline void set_maxNumFaces_5(int32_t value)
	{
		___maxNumFaces_5 = value;
	}

	inline static int32_t get_offset_of_maxNumTextures_6() { return static_cast<int32_t>(offsetof(APIMesh_t4229960961, ___maxNumTextures_6)); }
	inline int32_t get_maxNumTextures_6() const { return ___maxNumTextures_6; }
	inline int32_t* get_address_of_maxNumTextures_6() { return &___maxNumTextures_6; }
	inline void set_maxNumTextures_6(int32_t value)
	{
		___maxNumTextures_6 = value;
	}

	inline static int32_t get_offset_of_vertices_7() { return static_cast<int32_t>(offsetof(APIMesh_t4229960961, ___vertices_7)); }
	inline IntPtr_t get_vertices_7() const { return ___vertices_7; }
	inline IntPtr_t* get_address_of_vertices_7() { return &___vertices_7; }
	inline void set_vertices_7(IntPtr_t value)
	{
		___vertices_7 = value;
	}

	inline static int32_t get_offset_of_faces_8() { return static_cast<int32_t>(offsetof(APIMesh_t4229960961, ___faces_8)); }
	inline IntPtr_t get_faces_8() const { return ___faces_8; }
	inline IntPtr_t* get_address_of_faces_8() { return &___faces_8; }
	inline void set_faces_8(IntPtr_t value)
	{
		___faces_8 = value;
	}

	inline static int32_t get_offset_of_normals_9() { return static_cast<int32_t>(offsetof(APIMesh_t4229960961, ___normals_9)); }
	inline IntPtr_t get_normals_9() const { return ___normals_9; }
	inline IntPtr_t* get_address_of_normals_9() { return &___normals_9; }
	inline void set_normals_9(IntPtr_t value)
	{
		___normals_9 = value;
	}

	inline static int32_t get_offset_of_colors_10() { return static_cast<int32_t>(offsetof(APIMesh_t4229960961, ___colors_10)); }
	inline IntPtr_t get_colors_10() const { return ___colors_10; }
	inline IntPtr_t* get_address_of_colors_10() { return &___colors_10; }
	inline void set_colors_10(IntPtr_t value)
	{
		___colors_10 = value;
	}

	inline static int32_t get_offset_of_textureCoords_11() { return static_cast<int32_t>(offsetof(APIMesh_t4229960961, ___textureCoords_11)); }
	inline IntPtr_t get_textureCoords_11() const { return ___textureCoords_11; }
	inline IntPtr_t* get_address_of_textureCoords_11() { return &___textureCoords_11; }
	inline void set_textureCoords_11(IntPtr_t value)
	{
		___textureCoords_11 = value;
	}

	inline static int32_t get_offset_of_textures_12() { return static_cast<int32_t>(offsetof(APIMesh_t4229960961, ___textures_12)); }
	inline IntPtr_t get_textures_12() const { return ___textures_12; }
	inline IntPtr_t* get_address_of_textures_12() { return &___textures_12; }
	inline void set_textures_12(IntPtr_t value)
	{
		___textures_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
