﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t1030026315;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// TangoPoseController
struct TangoPoseController_t4427816;
// AreaLearningInGameController
struct AreaLearningInGameController_t4091715694;
// Tango.TangoApplication
struct TangoApplication_t278578959;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AreaDescriptionPicker
struct  AreaDescriptionPicker_t696588977  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject AreaDescriptionPicker::m_listElement
	GameObject_t1756533147 * ___m_listElement_2;
	// UnityEngine.RectTransform AreaDescriptionPicker::m_listContentParent
	RectTransform_t3349966182 * ___m_listContentParent_3;
	// UnityEngine.UI.ToggleGroup AreaDescriptionPicker::m_toggleGroup
	ToggleGroup_t1030026315 * ___m_toggleGroup_4;
	// UnityEngine.UI.Toggle AreaDescriptionPicker::m_enableLearningToggle
	Toggle_t3976754468 * ___m_enableLearningToggle_5;
	// TangoPoseController AreaDescriptionPicker::m_poseController
	TangoPoseController_t4427816 * ___m_poseController_6;
	// UnityEngine.GameObject AreaDescriptionPicker::m_gameControlPanel
	GameObject_t1756533147 * ___m_gameControlPanel_7;
	// AreaLearningInGameController AreaDescriptionPicker::m_guiController
	AreaLearningInGameController_t4091715694 * ___m_guiController_8;
	// Tango.TangoApplication AreaDescriptionPicker::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_9;
	// System.String AreaDescriptionPicker::m_curAreaDescriptionUUID
	String_t* ___m_curAreaDescriptionUUID_10;

public:
	inline static int32_t get_offset_of_m_listElement_2() { return static_cast<int32_t>(offsetof(AreaDescriptionPicker_t696588977, ___m_listElement_2)); }
	inline GameObject_t1756533147 * get_m_listElement_2() const { return ___m_listElement_2; }
	inline GameObject_t1756533147 ** get_address_of_m_listElement_2() { return &___m_listElement_2; }
	inline void set_m_listElement_2(GameObject_t1756533147 * value)
	{
		___m_listElement_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_listElement_2, value);
	}

	inline static int32_t get_offset_of_m_listContentParent_3() { return static_cast<int32_t>(offsetof(AreaDescriptionPicker_t696588977, ___m_listContentParent_3)); }
	inline RectTransform_t3349966182 * get_m_listContentParent_3() const { return ___m_listContentParent_3; }
	inline RectTransform_t3349966182 ** get_address_of_m_listContentParent_3() { return &___m_listContentParent_3; }
	inline void set_m_listContentParent_3(RectTransform_t3349966182 * value)
	{
		___m_listContentParent_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_listContentParent_3, value);
	}

	inline static int32_t get_offset_of_m_toggleGroup_4() { return static_cast<int32_t>(offsetof(AreaDescriptionPicker_t696588977, ___m_toggleGroup_4)); }
	inline ToggleGroup_t1030026315 * get_m_toggleGroup_4() const { return ___m_toggleGroup_4; }
	inline ToggleGroup_t1030026315 ** get_address_of_m_toggleGroup_4() { return &___m_toggleGroup_4; }
	inline void set_m_toggleGroup_4(ToggleGroup_t1030026315 * value)
	{
		___m_toggleGroup_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_toggleGroup_4, value);
	}

	inline static int32_t get_offset_of_m_enableLearningToggle_5() { return static_cast<int32_t>(offsetof(AreaDescriptionPicker_t696588977, ___m_enableLearningToggle_5)); }
	inline Toggle_t3976754468 * get_m_enableLearningToggle_5() const { return ___m_enableLearningToggle_5; }
	inline Toggle_t3976754468 ** get_address_of_m_enableLearningToggle_5() { return &___m_enableLearningToggle_5; }
	inline void set_m_enableLearningToggle_5(Toggle_t3976754468 * value)
	{
		___m_enableLearningToggle_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_enableLearningToggle_5, value);
	}

	inline static int32_t get_offset_of_m_poseController_6() { return static_cast<int32_t>(offsetof(AreaDescriptionPicker_t696588977, ___m_poseController_6)); }
	inline TangoPoseController_t4427816 * get_m_poseController_6() const { return ___m_poseController_6; }
	inline TangoPoseController_t4427816 ** get_address_of_m_poseController_6() { return &___m_poseController_6; }
	inline void set_m_poseController_6(TangoPoseController_t4427816 * value)
	{
		___m_poseController_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_poseController_6, value);
	}

	inline static int32_t get_offset_of_m_gameControlPanel_7() { return static_cast<int32_t>(offsetof(AreaDescriptionPicker_t696588977, ___m_gameControlPanel_7)); }
	inline GameObject_t1756533147 * get_m_gameControlPanel_7() const { return ___m_gameControlPanel_7; }
	inline GameObject_t1756533147 ** get_address_of_m_gameControlPanel_7() { return &___m_gameControlPanel_7; }
	inline void set_m_gameControlPanel_7(GameObject_t1756533147 * value)
	{
		___m_gameControlPanel_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_gameControlPanel_7, value);
	}

	inline static int32_t get_offset_of_m_guiController_8() { return static_cast<int32_t>(offsetof(AreaDescriptionPicker_t696588977, ___m_guiController_8)); }
	inline AreaLearningInGameController_t4091715694 * get_m_guiController_8() const { return ___m_guiController_8; }
	inline AreaLearningInGameController_t4091715694 ** get_address_of_m_guiController_8() { return &___m_guiController_8; }
	inline void set_m_guiController_8(AreaLearningInGameController_t4091715694 * value)
	{
		___m_guiController_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_guiController_8, value);
	}

	inline static int32_t get_offset_of_m_tangoApplication_9() { return static_cast<int32_t>(offsetof(AreaDescriptionPicker_t696588977, ___m_tangoApplication_9)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_9() const { return ___m_tangoApplication_9; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_9() { return &___m_tangoApplication_9; }
	inline void set_m_tangoApplication_9(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_9, value);
	}

	inline static int32_t get_offset_of_m_curAreaDescriptionUUID_10() { return static_cast<int32_t>(offsetof(AreaDescriptionPicker_t696588977, ___m_curAreaDescriptionUUID_10)); }
	inline String_t* get_m_curAreaDescriptionUUID_10() const { return ___m_curAreaDescriptionUUID_10; }
	inline String_t** get_address_of_m_curAreaDescriptionUUID_10() { return &___m_curAreaDescriptionUUID_10; }
	inline void set_m_curAreaDescriptionUUID_10(String_t* value)
	{
		___m_curAreaDescriptionUUID_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_curAreaDescriptionUUID_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
