﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MulticastDelegate3201952435.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23097358910.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_Grid1635028266.h"

// TangoDynamicMesh/TangoSingleDynamicMesh
struct TangoSingleDynamicMesh_t2600495423;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Transform`1<Tango.Tango3DReconstruction/GridIndex,TangoDynamicMesh/TangoSingleDynamicMesh,System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,TangoDynamicMesh/TangoSingleDynamicMesh>>
struct  Transform_1_t443183155  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
