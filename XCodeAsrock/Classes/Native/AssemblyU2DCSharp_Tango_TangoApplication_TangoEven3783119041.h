﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Tango.TangoApplication/TangoEventRegistrationManager
struct TangoEventRegistrationManager_t3622516502;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoApplication/TangoEventRegistrationManager/<_RegistrationChangeDefault>c__AnonStorey0
struct  U3C_RegistrationChangeDefaultU3Ec__AnonStorey0_t3783119041  : public Il2CppObject
{
public:
	// System.Boolean Tango.TangoApplication/TangoEventRegistrationManager/<_RegistrationChangeDefault>c__AnonStorey0::isRegister
	bool ___isRegister_0;
	// Tango.TangoApplication/TangoEventRegistrationManager Tango.TangoApplication/TangoEventRegistrationManager/<_RegistrationChangeDefault>c__AnonStorey0::$this
	TangoEventRegistrationManager_t3622516502 * ___U24this_1;

public:
	inline static int32_t get_offset_of_isRegister_0() { return static_cast<int32_t>(offsetof(U3C_RegistrationChangeDefaultU3Ec__AnonStorey0_t3783119041, ___isRegister_0)); }
	inline bool get_isRegister_0() const { return ___isRegister_0; }
	inline bool* get_address_of_isRegister_0() { return &___isRegister_0; }
	inline void set_isRegister_0(bool value)
	{
		___isRegister_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3C_RegistrationChangeDefaultU3Ec__AnonStorey0_t3783119041, ___U24this_1)); }
	inline TangoEventRegistrationManager_t3622516502 * get_U24this_1() const { return ___U24this_1; }
	inline TangoEventRegistrationManager_t3622516502 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TangoEventRegistrationManager_t3622516502 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
