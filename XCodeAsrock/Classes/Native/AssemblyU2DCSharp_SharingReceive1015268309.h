﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t3671312409;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharingReceive
struct  SharingReceive_t1015268309  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject SharingReceive::qrcodePlane
	GameObject_t1756533147 * ___qrcodePlane_2;
	// UnityEngine.GameObject SharingReceive::avatarPrefab
	GameObject_t1756533147 * ___avatarPrefab_3;
	// System.Collections.Hashtable SharingReceive::devices
	Hashtable_t909839986 * ___devices_4;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> SharingReceive::avatars
	Dictionary_2_t3671312409 * ___avatars_5;

public:
	inline static int32_t get_offset_of_qrcodePlane_2() { return static_cast<int32_t>(offsetof(SharingReceive_t1015268309, ___qrcodePlane_2)); }
	inline GameObject_t1756533147 * get_qrcodePlane_2() const { return ___qrcodePlane_2; }
	inline GameObject_t1756533147 ** get_address_of_qrcodePlane_2() { return &___qrcodePlane_2; }
	inline void set_qrcodePlane_2(GameObject_t1756533147 * value)
	{
		___qrcodePlane_2 = value;
		Il2CppCodeGenWriteBarrier(&___qrcodePlane_2, value);
	}

	inline static int32_t get_offset_of_avatarPrefab_3() { return static_cast<int32_t>(offsetof(SharingReceive_t1015268309, ___avatarPrefab_3)); }
	inline GameObject_t1756533147 * get_avatarPrefab_3() const { return ___avatarPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_avatarPrefab_3() { return &___avatarPrefab_3; }
	inline void set_avatarPrefab_3(GameObject_t1756533147 * value)
	{
		___avatarPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___avatarPrefab_3, value);
	}

	inline static int32_t get_offset_of_devices_4() { return static_cast<int32_t>(offsetof(SharingReceive_t1015268309, ___devices_4)); }
	inline Hashtable_t909839986 * get_devices_4() const { return ___devices_4; }
	inline Hashtable_t909839986 ** get_address_of_devices_4() { return &___devices_4; }
	inline void set_devices_4(Hashtable_t909839986 * value)
	{
		___devices_4 = value;
		Il2CppCodeGenWriteBarrier(&___devices_4, value);
	}

	inline static int32_t get_offset_of_avatars_5() { return static_cast<int32_t>(offsetof(SharingReceive_t1015268309, ___avatars_5)); }
	inline Dictionary_2_t3671312409 * get_avatars_5() const { return ___avatars_5; }
	inline Dictionary_2_t3671312409 ** get_address_of_avatars_5() { return &___avatars_5; }
	inline void set_avatars_5(Dictionary_2_t3671312409 * value)
	{
		___avatars_5 = value;
		Il2CppCodeGenWriteBarrier(&___avatars_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
