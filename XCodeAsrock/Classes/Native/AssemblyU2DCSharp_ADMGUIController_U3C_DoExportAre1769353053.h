﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t601950206;
// Tango.AreaDescription
struct AreaDescription_t1434786909;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ADMGUIController/<_DoExportAreaDescription>c__Iterator1
struct  U3C_DoExportAreaDescriptionU3Ec__Iterator1_t1769353053  : public Il2CppObject
{
public:
	// UnityEngine.TouchScreenKeyboard ADMGUIController/<_DoExportAreaDescription>c__Iterator1::<kb>__0
	TouchScreenKeyboard_t601950206 * ___U3CkbU3E__0_0;
	// Tango.AreaDescription ADMGUIController/<_DoExportAreaDescription>c__Iterator1::areaDescription
	AreaDescription_t1434786909 * ___areaDescription_1;
	// System.Object ADMGUIController/<_DoExportAreaDescription>c__Iterator1::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean ADMGUIController/<_DoExportAreaDescription>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 ADMGUIController/<_DoExportAreaDescription>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CkbU3E__0_0() { return static_cast<int32_t>(offsetof(U3C_DoExportAreaDescriptionU3Ec__Iterator1_t1769353053, ___U3CkbU3E__0_0)); }
	inline TouchScreenKeyboard_t601950206 * get_U3CkbU3E__0_0() const { return ___U3CkbU3E__0_0; }
	inline TouchScreenKeyboard_t601950206 ** get_address_of_U3CkbU3E__0_0() { return &___U3CkbU3E__0_0; }
	inline void set_U3CkbU3E__0_0(TouchScreenKeyboard_t601950206 * value)
	{
		___U3CkbU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CkbU3E__0_0, value);
	}

	inline static int32_t get_offset_of_areaDescription_1() { return static_cast<int32_t>(offsetof(U3C_DoExportAreaDescriptionU3Ec__Iterator1_t1769353053, ___areaDescription_1)); }
	inline AreaDescription_t1434786909 * get_areaDescription_1() const { return ___areaDescription_1; }
	inline AreaDescription_t1434786909 ** get_address_of_areaDescription_1() { return &___areaDescription_1; }
	inline void set_areaDescription_1(AreaDescription_t1434786909 * value)
	{
		___areaDescription_1 = value;
		Il2CppCodeGenWriteBarrier(&___areaDescription_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3C_DoExportAreaDescriptionU3Ec__Iterator1_t1769353053, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3C_DoExportAreaDescriptionU3Ec__Iterator1_t1769353053, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3C_DoExportAreaDescriptionU3Ec__Iterator1_t1769353053, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
