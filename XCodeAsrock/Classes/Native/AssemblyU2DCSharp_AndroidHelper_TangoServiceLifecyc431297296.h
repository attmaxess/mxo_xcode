﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_AndroidJavaProxy4274989947.h"

// AndroidHelper/OnTangoServiceConnected
struct OnTangoServiceConnected_t4169241012;
// AndroidHelper/OnTangoServiceDisconnected
struct OnTangoServiceDisconnected_t1901329298;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidHelper/TangoServiceLifecycleListener
struct  TangoServiceLifecycleListener_t431297296  : public AndroidJavaProxy_t4274989947
{
public:
	// AndroidHelper/OnTangoServiceConnected AndroidHelper/TangoServiceLifecycleListener::m_onTangoServiceConnected
	OnTangoServiceConnected_t4169241012 * ___m_onTangoServiceConnected_1;
	// AndroidHelper/OnTangoServiceDisconnected AndroidHelper/TangoServiceLifecycleListener::m_onTangoServiceDisconnected
	OnTangoServiceDisconnected_t1901329298 * ___m_onTangoServiceDisconnected_2;

public:
	inline static int32_t get_offset_of_m_onTangoServiceConnected_1() { return static_cast<int32_t>(offsetof(TangoServiceLifecycleListener_t431297296, ___m_onTangoServiceConnected_1)); }
	inline OnTangoServiceConnected_t4169241012 * get_m_onTangoServiceConnected_1() const { return ___m_onTangoServiceConnected_1; }
	inline OnTangoServiceConnected_t4169241012 ** get_address_of_m_onTangoServiceConnected_1() { return &___m_onTangoServiceConnected_1; }
	inline void set_m_onTangoServiceConnected_1(OnTangoServiceConnected_t4169241012 * value)
	{
		___m_onTangoServiceConnected_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_onTangoServiceConnected_1, value);
	}

	inline static int32_t get_offset_of_m_onTangoServiceDisconnected_2() { return static_cast<int32_t>(offsetof(TangoServiceLifecycleListener_t431297296, ___m_onTangoServiceDisconnected_2)); }
	inline OnTangoServiceDisconnected_t1901329298 * get_m_onTangoServiceDisconnected_2() const { return ___m_onTangoServiceDisconnected_2; }
	inline OnTangoServiceDisconnected_t1901329298 ** get_address_of_m_onTangoServiceDisconnected_2() { return &___m_onTangoServiceDisconnected_2; }
	inline void set_m_onTangoServiceDisconnected_2(OnTangoServiceDisconnected_t1901329298 * value)
	{
		___m_onTangoServiceDisconnected_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_onTangoServiceDisconnected_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
