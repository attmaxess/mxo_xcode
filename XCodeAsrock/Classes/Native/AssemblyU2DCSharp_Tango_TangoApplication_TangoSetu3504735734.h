﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Tango.ITangoApplicationSettings
struct ITangoApplicationSettings_t1290032987;
// Tango.TangoApplication/TangoApplicationState
struct TangoApplicationState_t848537215;
// Tango.TangoUx
struct TangoUx_t746396440;
// System.Action
struct Action_t3226471752;
// Tango.YUVTexture
struct YUVTexture_t2576165397;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoApplication/TangoSetupTeardownManager
struct  TangoSetupTeardownManager_t3504735734  : public Il2CppObject
{
public:
	// Tango.ITangoApplicationSettings Tango.TangoApplication/TangoSetupTeardownManager::m_applicationSettings
	Il2CppObject * ___m_applicationSettings_0;
	// Tango.TangoApplication/TangoApplicationState Tango.TangoApplication/TangoSetupTeardownManager::m_applicationState
	TangoApplicationState_t848537215 * ___m_applicationState_1;
	// Tango.TangoUx Tango.TangoApplication/TangoSetupTeardownManager::m_tangoUx
	TangoUx_t746396440 * ___m_tangoUx_2;
	// System.Action Tango.TangoApplication/TangoSetupTeardownManager::m_fireTangeConnectEvent
	Action_t3226471752 * ___m_fireTangeConnectEvent_3;
	// System.Action Tango.TangoApplication/TangoSetupTeardownManager::m_fireTangoDisconnectEvent
	Action_t3226471752 * ___m_fireTangoDisconnectEvent_4;
	// Tango.YUVTexture Tango.TangoApplication/TangoSetupTeardownManager::m_yuvTexture
	YUVTexture_t2576165397 * ___m_yuvTexture_5;
	// System.Object Tango.TangoApplication/TangoSetupTeardownManager::m_tangoLifecycleLock
	Il2CppObject * ___m_tangoLifecycleLock_6;
	// System.Boolean Tango.TangoApplication/TangoSetupTeardownManager::m_isApplicationPaused
	bool ___m_isApplicationPaused_7;

public:
	inline static int32_t get_offset_of_m_applicationSettings_0() { return static_cast<int32_t>(offsetof(TangoSetupTeardownManager_t3504735734, ___m_applicationSettings_0)); }
	inline Il2CppObject * get_m_applicationSettings_0() const { return ___m_applicationSettings_0; }
	inline Il2CppObject ** get_address_of_m_applicationSettings_0() { return &___m_applicationSettings_0; }
	inline void set_m_applicationSettings_0(Il2CppObject * value)
	{
		___m_applicationSettings_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_applicationSettings_0, value);
	}

	inline static int32_t get_offset_of_m_applicationState_1() { return static_cast<int32_t>(offsetof(TangoSetupTeardownManager_t3504735734, ___m_applicationState_1)); }
	inline TangoApplicationState_t848537215 * get_m_applicationState_1() const { return ___m_applicationState_1; }
	inline TangoApplicationState_t848537215 ** get_address_of_m_applicationState_1() { return &___m_applicationState_1; }
	inline void set_m_applicationState_1(TangoApplicationState_t848537215 * value)
	{
		___m_applicationState_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_applicationState_1, value);
	}

	inline static int32_t get_offset_of_m_tangoUx_2() { return static_cast<int32_t>(offsetof(TangoSetupTeardownManager_t3504735734, ___m_tangoUx_2)); }
	inline TangoUx_t746396440 * get_m_tangoUx_2() const { return ___m_tangoUx_2; }
	inline TangoUx_t746396440 ** get_address_of_m_tangoUx_2() { return &___m_tangoUx_2; }
	inline void set_m_tangoUx_2(TangoUx_t746396440 * value)
	{
		___m_tangoUx_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoUx_2, value);
	}

	inline static int32_t get_offset_of_m_fireTangeConnectEvent_3() { return static_cast<int32_t>(offsetof(TangoSetupTeardownManager_t3504735734, ___m_fireTangeConnectEvent_3)); }
	inline Action_t3226471752 * get_m_fireTangeConnectEvent_3() const { return ___m_fireTangeConnectEvent_3; }
	inline Action_t3226471752 ** get_address_of_m_fireTangeConnectEvent_3() { return &___m_fireTangeConnectEvent_3; }
	inline void set_m_fireTangeConnectEvent_3(Action_t3226471752 * value)
	{
		___m_fireTangeConnectEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_fireTangeConnectEvent_3, value);
	}

	inline static int32_t get_offset_of_m_fireTangoDisconnectEvent_4() { return static_cast<int32_t>(offsetof(TangoSetupTeardownManager_t3504735734, ___m_fireTangoDisconnectEvent_4)); }
	inline Action_t3226471752 * get_m_fireTangoDisconnectEvent_4() const { return ___m_fireTangoDisconnectEvent_4; }
	inline Action_t3226471752 ** get_address_of_m_fireTangoDisconnectEvent_4() { return &___m_fireTangoDisconnectEvent_4; }
	inline void set_m_fireTangoDisconnectEvent_4(Action_t3226471752 * value)
	{
		___m_fireTangoDisconnectEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_fireTangoDisconnectEvent_4, value);
	}

	inline static int32_t get_offset_of_m_yuvTexture_5() { return static_cast<int32_t>(offsetof(TangoSetupTeardownManager_t3504735734, ___m_yuvTexture_5)); }
	inline YUVTexture_t2576165397 * get_m_yuvTexture_5() const { return ___m_yuvTexture_5; }
	inline YUVTexture_t2576165397 ** get_address_of_m_yuvTexture_5() { return &___m_yuvTexture_5; }
	inline void set_m_yuvTexture_5(YUVTexture_t2576165397 * value)
	{
		___m_yuvTexture_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_yuvTexture_5, value);
	}

	inline static int32_t get_offset_of_m_tangoLifecycleLock_6() { return static_cast<int32_t>(offsetof(TangoSetupTeardownManager_t3504735734, ___m_tangoLifecycleLock_6)); }
	inline Il2CppObject * get_m_tangoLifecycleLock_6() const { return ___m_tangoLifecycleLock_6; }
	inline Il2CppObject ** get_address_of_m_tangoLifecycleLock_6() { return &___m_tangoLifecycleLock_6; }
	inline void set_m_tangoLifecycleLock_6(Il2CppObject * value)
	{
		___m_tangoLifecycleLock_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoLifecycleLock_6, value);
	}

	inline static int32_t get_offset_of_m_isApplicationPaused_7() { return static_cast<int32_t>(offsetof(TangoSetupTeardownManager_t3504735734, ___m_isApplicationPaused_7)); }
	inline bool get_m_isApplicationPaused_7() const { return ___m_isApplicationPaused_7; }
	inline bool* get_address_of_m_isApplicationPaused_7() { return &___m_isApplicationPaused_7; }
	inline void set_m_isApplicationPaused_7(bool value)
	{
		___m_isApplicationPaused_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
