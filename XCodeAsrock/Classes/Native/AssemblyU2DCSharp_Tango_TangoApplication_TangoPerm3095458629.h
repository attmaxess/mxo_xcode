﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Tango_TangoApplication_Permissio1914853856.h"

// Tango.ITangoApplicationSettings
struct ITangoApplicationSettings_t1290032987;
// IAndroidHelperWrapper
struct IAndroidHelperWrapper_t128471629;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Collections.Generic.HashSet`1<Tango.TangoApplication/PermissionsTypes>
struct HashSet_1_t1999149542;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoApplication/TangoPermissionsManager
struct  TangoPermissionsManager_t3095458629  : public Il2CppObject
{
public:
	// Tango.ITangoApplicationSettings Tango.TangoApplication/TangoPermissionsManager::m_tangoApplicationSettings
	Il2CppObject * ___m_tangoApplicationSettings_0;
	// IAndroidHelperWrapper Tango.TangoApplication/TangoPermissionsManager::m_androidHelper
	Il2CppObject * ___m_androidHelper_1;
	// System.Action`1<System.Boolean> Tango.TangoApplication/TangoPermissionsManager::m_onAndroidPermissionsResolved
	Action_1_t3627374100 * ___m_onAndroidPermissionsResolved_2;
	// System.Action`1<System.Boolean> Tango.TangoApplication/TangoPermissionsManager::m_onTangoBindResolved
	Action_1_t3627374100 * ___m_onTangoBindResolved_3;
	// System.Collections.Generic.HashSet`1<Tango.TangoApplication/PermissionsTypes> Tango.TangoApplication/TangoPermissionsManager::<PendingRequiredPermissions>k__BackingField
	HashSet_1_t1999149542 * ___U3CPendingRequiredPermissionsU3Ek__BackingField_4;
	// Tango.TangoApplication/PermissionRequestState Tango.TangoApplication/TangoPermissionsManager::<PermissionRequestState>k__BackingField
	int32_t ___U3CPermissionRequestStateU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_m_tangoApplicationSettings_0() { return static_cast<int32_t>(offsetof(TangoPermissionsManager_t3095458629, ___m_tangoApplicationSettings_0)); }
	inline Il2CppObject * get_m_tangoApplicationSettings_0() const { return ___m_tangoApplicationSettings_0; }
	inline Il2CppObject ** get_address_of_m_tangoApplicationSettings_0() { return &___m_tangoApplicationSettings_0; }
	inline void set_m_tangoApplicationSettings_0(Il2CppObject * value)
	{
		___m_tangoApplicationSettings_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplicationSettings_0, value);
	}

	inline static int32_t get_offset_of_m_androidHelper_1() { return static_cast<int32_t>(offsetof(TangoPermissionsManager_t3095458629, ___m_androidHelper_1)); }
	inline Il2CppObject * get_m_androidHelper_1() const { return ___m_androidHelper_1; }
	inline Il2CppObject ** get_address_of_m_androidHelper_1() { return &___m_androidHelper_1; }
	inline void set_m_androidHelper_1(Il2CppObject * value)
	{
		___m_androidHelper_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_androidHelper_1, value);
	}

	inline static int32_t get_offset_of_m_onAndroidPermissionsResolved_2() { return static_cast<int32_t>(offsetof(TangoPermissionsManager_t3095458629, ___m_onAndroidPermissionsResolved_2)); }
	inline Action_1_t3627374100 * get_m_onAndroidPermissionsResolved_2() const { return ___m_onAndroidPermissionsResolved_2; }
	inline Action_1_t3627374100 ** get_address_of_m_onAndroidPermissionsResolved_2() { return &___m_onAndroidPermissionsResolved_2; }
	inline void set_m_onAndroidPermissionsResolved_2(Action_1_t3627374100 * value)
	{
		___m_onAndroidPermissionsResolved_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_onAndroidPermissionsResolved_2, value);
	}

	inline static int32_t get_offset_of_m_onTangoBindResolved_3() { return static_cast<int32_t>(offsetof(TangoPermissionsManager_t3095458629, ___m_onTangoBindResolved_3)); }
	inline Action_1_t3627374100 * get_m_onTangoBindResolved_3() const { return ___m_onTangoBindResolved_3; }
	inline Action_1_t3627374100 ** get_address_of_m_onTangoBindResolved_3() { return &___m_onTangoBindResolved_3; }
	inline void set_m_onTangoBindResolved_3(Action_1_t3627374100 * value)
	{
		___m_onTangoBindResolved_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_onTangoBindResolved_3, value);
	}

	inline static int32_t get_offset_of_U3CPendingRequiredPermissionsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TangoPermissionsManager_t3095458629, ___U3CPendingRequiredPermissionsU3Ek__BackingField_4)); }
	inline HashSet_1_t1999149542 * get_U3CPendingRequiredPermissionsU3Ek__BackingField_4() const { return ___U3CPendingRequiredPermissionsU3Ek__BackingField_4; }
	inline HashSet_1_t1999149542 ** get_address_of_U3CPendingRequiredPermissionsU3Ek__BackingField_4() { return &___U3CPendingRequiredPermissionsU3Ek__BackingField_4; }
	inline void set_U3CPendingRequiredPermissionsU3Ek__BackingField_4(HashSet_1_t1999149542 * value)
	{
		___U3CPendingRequiredPermissionsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPendingRequiredPermissionsU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CPermissionRequestStateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TangoPermissionsManager_t3095458629, ___U3CPermissionRequestStateU3Ek__BackingField_5)); }
	inline int32_t get_U3CPermissionRequestStateU3Ek__BackingField_5() const { return ___U3CPermissionRequestStateU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CPermissionRequestStateU3Ek__BackingField_5() { return &___U3CPermissionRequestStateU3Ek__BackingField_5; }
	inline void set_U3CPermissionRequestStateU3Ek__BackingField_5(int32_t value)
	{
		___U3CPermissionRequestStateU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
