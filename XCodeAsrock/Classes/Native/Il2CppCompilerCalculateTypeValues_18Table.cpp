﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributes1176289510.h"
#include "System_Xml_System_Xml_Serialization_XmlChoiceIdent3898228811.h"
#include "System_Xml_System_Xml_Serialization_XmlCustomForma3130931586.h"
#include "System_Xml_System_Xml_Serialization_XmlElementAttr2182839281.h"
#include "System_Xml_System_Xml_Serialization_XmlElementAttri724917664.h"
#include "System_Xml_System_Xml_Serialization_XmlElementEvent634537510.h"
#include "System_Xml_System_Xml_Serialization_XmlEnumAttribut919400678.h"
#include "System_Xml_System_Xml_Serialization_XmlIgnoreAttri2333915871.h"
#include "System_Xml_System_Xml_Serialization_XmlIncludeAttr3199808027.h"
#include "System_Xml_System_Xml_Serialization_XmlMapping1597064667.h"
#include "System_Xml_System_Xml_Serialization_ObjectMap1719332799.h"
#include "System_Xml_System_Xml_Serialization_SerializationF2842638831.h"
#include "System_Xml_System_Xml_Serialization_XmlMemberMappi2912176015.h"
#include "System_Xml_System_Xml_Serialization_XmlMembersMapp1818148568.h"
#include "System_Xml_System_Xml_Serialization_XmlNamespaceDe2069522403.h"
#include "System_Xml_System_Xml_Serialization_XmlNodeEventAr3585347612.h"
#include "System_Xml_System_Xml_Serialization_XmlReflectionI1815714992.h"
#include "System_Xml_System_Xml_Serialization_XmlReflectionM3763194608.h"
#include "System_Xml_System_Xml_Serialization_XmlRootAttribu3527426713.h"
#include "System_Xml_System_Xml_Serialization_XmlSchemaProvi2486667559.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializatio148621035.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati3551839386.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializatio825234753.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati3886809155.h"
#include "System_Xml_System_Xml_Serialization_XmlSerialization36825869.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati3196984506.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati2056927484.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializatio468691018.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati2453144241.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati2868308282.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati1544550059.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati4075956380.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati2309376514.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializer1255294979.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializer_3606741380.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializerI2663519657.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializerN3063656491.h"
#include "System_Xml_System_Xml_Serialization_XmlTextAttribu3321178844.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeAttribu1804457051.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapElem3381496463.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapElem1987994689.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb3057402259.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb3329971455.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb1864082097.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb3063640245.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMembe755220835.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb2275030030.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb1726696875.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMembe151515769.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMembe999867419.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapping315595419.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializable277695926.h"
#include "System_Xml_System_Xml_Serialization_ClassMap1647926812.h"
#include "System_Xml_System_Xml_Serialization_ListMap1787375712.h"
#include "System_Xml_System_Xml_Serialization_EnumMap3161685173.h"
#include "System_Xml_System_Xml_Serialization_EnumMap_EnumMa3814867081.h"
#include "System_Xml_System_Xml_ConformanceLevel3761201363.h"
#include "System_Xml_Mono_Xml_DTDAutomataFactory3605390810.h"
#include "System_Xml_Mono_Xml_DTDObjectModel1113953282.h"
#include "System_Xml_Mono_Xml_DictionaryBase1005937181.h"
#include "System_Xml_Mono_Xml_DictionaryBase_U3CU3Ec__Iterat3518389200.h"
#include "System_Xml_Mono_Xml_DTDCollectionBase2621362935.h"
#include "System_Xml_Mono_Xml_DTDElementDeclarationCollectio2224069626.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclarationCollection243645429.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclarationCollection1212505713.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclarationCollectio228085060.h"
#include "System_Xml_Mono_Xml_DTDContentModel445576364.h"
#include "System_Xml_Mono_Xml_DTDContentModelCollection3164170484.h"
#include "System_Xml_Mono_Xml_DTDNode1758286970.h"
#include "System_Xml_Mono_Xml_DTDElementDeclaration8748002.h"
#include "System_Xml_Mono_Xml_DTDAttributeDefinition3692870749.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclaration2272374839.h"
#include "System_Xml_Mono_Xml_DTDEntityBase2353758560.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclaration4283284771.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclaration1758408116.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclarationC3496720022.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclaration252230634.h"
#include "System_Xml_Mono_Xml_DTDContentOrderType3150259539.h"
#include "System_Xml_Mono_Xml_DTDOccurence99371501.h"
#include "System_Xml_System_Xml_DTDReader2453137441.h"
#include "System_Xml_System_Xml_EntityHandling3960499440.h"
#include "System_Xml_System_Xml_Formatting1126649075.h"
#include "System_Xml_System_Xml_NameTable594386929.h"
#include "System_Xml_System_Xml_NameTable_Entry2583369454.h"
#include "System_Xml_System_Xml_NamespaceHandling1452270444.h"
#include "System_Xml_System_Xml_NewLineHandling1737195169.h"
#include "System_Xml_System_Xml_ReadState3138905245.h"
#include "System_Xml_System_Xml_WhitespaceHandling3754063142.h"
#include "System_Xml_System_Xml_WriteState1534871862.h"
#include "System_Xml_System_Xml_XmlAttribute175731005.h"
#include "System_Xml_System_Xml_XmlAttributeCollection3359885287.h"
#include "System_Xml_System_Xml_XmlCDataSection1124775823.h"
#include "System_Xml_System_Xml_XmlChar1369421061.h"
#include "System_Xml_System_Xml_XmlCharacterData575748506.h"
#include "System_Xml_System_Xml_XmlComment3999331572.h"
#include "System_Xml_System_Xml_XmlConvert1936105738.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (XmlAttributes_t1176289510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[14] = 
{
	XmlAttributes_t1176289510::get_offset_of_xmlAnyAttribute_0(),
	XmlAttributes_t1176289510::get_offset_of_xmlAnyElements_1(),
	XmlAttributes_t1176289510::get_offset_of_xmlArray_2(),
	XmlAttributes_t1176289510::get_offset_of_xmlArrayItems_3(),
	XmlAttributes_t1176289510::get_offset_of_xmlAttribute_4(),
	XmlAttributes_t1176289510::get_offset_of_xmlChoiceIdentifier_5(),
	XmlAttributes_t1176289510::get_offset_of_xmlDefaultValue_6(),
	XmlAttributes_t1176289510::get_offset_of_xmlElements_7(),
	XmlAttributes_t1176289510::get_offset_of_xmlEnum_8(),
	XmlAttributes_t1176289510::get_offset_of_xmlIgnore_9(),
	XmlAttributes_t1176289510::get_offset_of_xmlns_10(),
	XmlAttributes_t1176289510::get_offset_of_xmlRoot_11(),
	XmlAttributes_t1176289510::get_offset_of_xmlText_12(),
	XmlAttributes_t1176289510::get_offset_of_xmlType_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (XmlChoiceIdentifierAttribute_t3898228811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[1] = 
{
	XmlChoiceIdentifierAttribute_t3898228811::get_offset_of_memberName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (XmlCustomFormatter_t3130931586), -1, sizeof(XmlCustomFormatter_t3130931586_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1802[2] = 
{
	XmlCustomFormatter_t3130931586_StaticFields::get_offset_of_U3CU3Ef__switchU24map3D_0(),
	XmlCustomFormatter_t3130931586_StaticFields::get_offset_of_U3CU3Ef__switchU24map3E_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (XmlElementAttribute_t2182839281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[7] = 
{
	XmlElementAttribute_t2182839281::get_offset_of_dataType_0(),
	XmlElementAttribute_t2182839281::get_offset_of_elementName_1(),
	XmlElementAttribute_t2182839281::get_offset_of_form_2(),
	XmlElementAttribute_t2182839281::get_offset_of_ns_3(),
	XmlElementAttribute_t2182839281::get_offset_of_isNullable_4(),
	XmlElementAttribute_t2182839281::get_offset_of_type_5(),
	XmlElementAttribute_t2182839281::get_offset_of_order_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (XmlElementAttributes_t724917664), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (XmlElementEventArgs_t634537510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[5] = 
{
	XmlElementEventArgs_t634537510::get_offset_of_attr_1(),
	XmlElementEventArgs_t634537510::get_offset_of_lineNumber_2(),
	XmlElementEventArgs_t634537510::get_offset_of_linePosition_3(),
	XmlElementEventArgs_t634537510::get_offset_of_obj_4(),
	XmlElementEventArgs_t634537510::get_offset_of_expectedElements_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (XmlEnumAttribute_t919400678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[1] = 
{
	XmlEnumAttribute_t919400678::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (XmlIgnoreAttribute_t2333915871), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (XmlIncludeAttribute_t3199808027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[1] = 
{
	XmlIncludeAttribute_t3199808027::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (XmlMapping_t1597064667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[7] = 
{
	XmlMapping_t1597064667::get_offset_of_map_0(),
	XmlMapping_t1597064667::get_offset_of_relatedMaps_1(),
	XmlMapping_t1597064667::get_offset_of_format_2(),
	XmlMapping_t1597064667::get_offset_of_source_3(),
	XmlMapping_t1597064667::get_offset_of__elementName_4(),
	XmlMapping_t1597064667::get_offset_of__namespace_5(),
	XmlMapping_t1597064667::get_offset_of_key_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (ObjectMap_t1719332799), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (SerializationFormat_t2842638831)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1811[3] = 
{
	SerializationFormat_t2842638831::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (XmlMemberMapping_t2912176015), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (XmlMembersMapping_t1818148568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[2] = 
{
	XmlMembersMapping_t1818148568::get_offset_of__hasWrapperElement_7(),
	XmlMembersMapping_t1818148568::get_offset_of__mapping_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (XmlNamespaceDeclarationsAttribute_t2069522403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (XmlNodeEventArgs_t3585347612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[8] = 
{
	XmlNodeEventArgs_t3585347612::get_offset_of_linenumber_1(),
	XmlNodeEventArgs_t3585347612::get_offset_of_lineposition_2(),
	XmlNodeEventArgs_t3585347612::get_offset_of_localname_3(),
	XmlNodeEventArgs_t3585347612::get_offset_of_name_4(),
	XmlNodeEventArgs_t3585347612::get_offset_of_nsuri_5(),
	XmlNodeEventArgs_t3585347612::get_offset_of_nodetype_6(),
	XmlNodeEventArgs_t3585347612::get_offset_of_source_7(),
	XmlNodeEventArgs_t3585347612::get_offset_of_text_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (XmlReflectionImporter_t1815714992), -1, sizeof(XmlReflectionImporter_t1815714992_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1816[9] = 
{
	XmlReflectionImporter_t1815714992::get_offset_of_initialDefaultNamespace_0(),
	XmlReflectionImporter_t1815714992::get_offset_of_attributeOverrides_1(),
	XmlReflectionImporter_t1815714992::get_offset_of_includedTypes_2(),
	XmlReflectionImporter_t1815714992::get_offset_of_helper_3(),
	XmlReflectionImporter_t1815714992::get_offset_of_arrayChoiceCount_4(),
	XmlReflectionImporter_t1815714992::get_offset_of_relatedMaps_5(),
	XmlReflectionImporter_t1815714992::get_offset_of_allowPrivateTypes_6(),
	XmlReflectionImporter_t1815714992_StaticFields::get_offset_of_errSimple_7(),
	XmlReflectionImporter_t1815714992_StaticFields::get_offset_of_errSimple2_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (XmlReflectionMember_t3763194608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[5] = 
{
	XmlReflectionMember_t3763194608::get_offset_of_isReturnValue_0(),
	XmlReflectionMember_t3763194608::get_offset_of_memberName_1(),
	XmlReflectionMember_t3763194608::get_offset_of_memberType_2(),
	XmlReflectionMember_t3763194608::get_offset_of_xmlAttributes_3(),
	XmlReflectionMember_t3763194608::get_offset_of_declaringType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (XmlRootAttribute_t3527426713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[4] = 
{
	XmlRootAttribute_t3527426713::get_offset_of_dataType_0(),
	XmlRootAttribute_t3527426713::get_offset_of_elementName_1(),
	XmlRootAttribute_t3527426713::get_offset_of_isNullable_2(),
	XmlRootAttribute_t3527426713::get_offset_of_ns_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (XmlSchemaProviderAttribute_t2486667559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[1] = 
{
	XmlSchemaProviderAttribute_t2486667559::get_offset_of__methodName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (XmlSerializationGeneratedCode_t148621035), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (XmlSerializationReader_t3551839386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[25] = 
{
	XmlSerializationReader_t3551839386::get_offset_of_document_0(),
	XmlSerializationReader_t3551839386::get_offset_of_reader_1(),
	XmlSerializationReader_t3551839386::get_offset_of_fixups_2(),
	XmlSerializationReader_t3551839386::get_offset_of_collFixups_3(),
	XmlSerializationReader_t3551839386::get_offset_of_collItemFixups_4(),
	XmlSerializationReader_t3551839386::get_offset_of_typesCallbacks_5(),
	XmlSerializationReader_t3551839386::get_offset_of_noIDTargets_6(),
	XmlSerializationReader_t3551839386::get_offset_of_targets_7(),
	XmlSerializationReader_t3551839386::get_offset_of_delayedListFixups_8(),
	XmlSerializationReader_t3551839386::get_offset_of_eventSource_9(),
	XmlSerializationReader_t3551839386::get_offset_of_delayedFixupId_10(),
	XmlSerializationReader_t3551839386::get_offset_of_referencedObjects_11(),
	XmlSerializationReader_t3551839386::get_offset_of_readCount_12(),
	XmlSerializationReader_t3551839386::get_offset_of_whileIterationCount_13(),
	XmlSerializationReader_t3551839386::get_offset_of_w3SchemaNS_14(),
	XmlSerializationReader_t3551839386::get_offset_of_w3InstanceNS_15(),
	XmlSerializationReader_t3551839386::get_offset_of_w3InstanceNS2000_16(),
	XmlSerializationReader_t3551839386::get_offset_of_w3InstanceNS1999_17(),
	XmlSerializationReader_t3551839386::get_offset_of_soapNS_18(),
	XmlSerializationReader_t3551839386::get_offset_of_wsdlNS_19(),
	XmlSerializationReader_t3551839386::get_offset_of_nullX_20(),
	XmlSerializationReader_t3551839386::get_offset_of_nil_21(),
	XmlSerializationReader_t3551839386::get_offset_of_typeX_22(),
	XmlSerializationReader_t3551839386::get_offset_of_arrayType_23(),
	XmlSerializationReader_t3551839386::get_offset_of_arrayQName_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (WriteCallbackInfo_t825234753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[4] = 
{
	WriteCallbackInfo_t825234753::get_offset_of_Type_0(),
	WriteCallbackInfo_t825234753::get_offset_of_TypeName_1(),
	WriteCallbackInfo_t825234753::get_offset_of_TypeNs_2(),
	WriteCallbackInfo_t825234753::get_offset_of_Callback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (CollectionFixup_t3886809155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[4] = 
{
	CollectionFixup_t3886809155::get_offset_of_callback_0(),
	CollectionFixup_t3886809155::get_offset_of_collection_1(),
	CollectionFixup_t3886809155::get_offset_of_collectionItems_2(),
	CollectionFixup_t3886809155::get_offset_of_id_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (Fixup_t36825869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1824[3] = 
{
	Fixup_t36825869::get_offset_of_source_0(),
	Fixup_t36825869::get_offset_of_ids_1(),
	Fixup_t36825869::get_offset_of_callback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (CollectionItemFixup_t3196984506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[3] = 
{
	CollectionItemFixup_t3196984506::get_offset_of_list_0(),
	CollectionItemFixup_t3196984506::get_offset_of_index_1(),
	CollectionItemFixup_t3196984506::get_offset_of_id_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (XmlSerializationReaderInterpreter_t2056927484), -1, sizeof(XmlSerializationReaderInterpreter_t2056927484_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1826[4] = 
{
	XmlSerializationReaderInterpreter_t2056927484::get_offset_of__typeMap_25(),
	XmlSerializationReaderInterpreter_t2056927484::get_offset_of__format_26(),
	XmlSerializationReaderInterpreter_t2056927484_StaticFields::get_offset_of_AnyType_27(),
	XmlSerializationReaderInterpreter_t2056927484_StaticFields::get_offset_of_empty_array_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (FixupCallbackInfo_t468691018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1827[3] = 
{
	FixupCallbackInfo_t468691018::get_offset_of__sri_0(),
	FixupCallbackInfo_t468691018::get_offset_of__map_1(),
	FixupCallbackInfo_t468691018::get_offset_of__isValueList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (ReaderCallbackInfo_t2453144241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1828[2] = 
{
	ReaderCallbackInfo_t2453144241::get_offset_of__sri_0(),
	ReaderCallbackInfo_t2453144241::get_offset_of__typeMap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (XmlSerializationWriter_t2868308282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1829[8] = 
{
	XmlSerializationWriter_t2868308282::get_offset_of_idGenerator_0(),
	XmlSerializationWriter_t2868308282::get_offset_of_qnameCount_1(),
	XmlSerializationWriter_t2868308282::get_offset_of_topLevelElement_2(),
	XmlSerializationWriter_t2868308282::get_offset_of_namespaces_3(),
	XmlSerializationWriter_t2868308282::get_offset_of_writer_4(),
	XmlSerializationWriter_t2868308282::get_offset_of_referencedElements_5(),
	XmlSerializationWriter_t2868308282::get_offset_of_callbacks_6(),
	XmlSerializationWriter_t2868308282::get_offset_of_serializedObjects_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (WriteCallbackInfo_t1544550059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[4] = 
{
	WriteCallbackInfo_t1544550059::get_offset_of_Type_0(),
	WriteCallbackInfo_t1544550059::get_offset_of_TypeName_1(),
	WriteCallbackInfo_t1544550059::get_offset_of_TypeNs_2(),
	WriteCallbackInfo_t1544550059::get_offset_of_Callback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (XmlSerializationWriterInterpreter_t4075956380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1831[2] = 
{
	XmlSerializationWriterInterpreter_t4075956380::get_offset_of__typeMap_8(),
	XmlSerializationWriterInterpreter_t4075956380::get_offset_of__format_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (CallbackInfo_t2309376514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1832[2] = 
{
	CallbackInfo_t2309376514::get_offset_of__swi_0(),
	CallbackInfo_t2309376514::get_offset_of__typeMap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (XmlSerializer_t1255294979), -1, sizeof(XmlSerializer_t1255294979_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1833[12] = 
{
	XmlSerializer_t1255294979_StaticFields::get_offset_of_generationThreshold_0(),
	XmlSerializer_t1255294979_StaticFields::get_offset_of_backgroundGeneration_1(),
	XmlSerializer_t1255294979_StaticFields::get_offset_of_deleteTempFiles_2(),
	XmlSerializer_t1255294979_StaticFields::get_offset_of_generatorFallback_3(),
	XmlSerializer_t1255294979::get_offset_of_customSerializer_4(),
	XmlSerializer_t1255294979::get_offset_of_typeMapping_5(),
	XmlSerializer_t1255294979::get_offset_of_serializerData_6(),
	XmlSerializer_t1255294979_StaticFields::get_offset_of_serializerTypes_7(),
	XmlSerializer_t1255294979::get_offset_of_onUnknownAttribute_8(),
	XmlSerializer_t1255294979::get_offset_of_onUnknownElement_9(),
	XmlSerializer_t1255294979::get_offset_of_onUnknownNode_10(),
	XmlSerializer_t1255294979::get_offset_of_onUnreferencedObject_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (SerializerData_t3606741380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1834[5] = 
{
	SerializerData_t3606741380::get_offset_of_ReaderType_0(),
	SerializerData_t3606741380::get_offset_of_ReaderMethod_1(),
	SerializerData_t3606741380::get_offset_of_WriterType_2(),
	SerializerData_t3606741380::get_offset_of_WriterMethod_3(),
	SerializerData_t3606741380::get_offset_of_Implementation_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (XmlSerializerImplementation_t2663519657), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (XmlSerializerNamespaces_t3063656491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1836[1] = 
{
	XmlSerializerNamespaces_t3063656491::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (XmlTextAttribute_t3321178844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[2] = 
{
	XmlTextAttribute_t3321178844::get_offset_of_dataType_0(),
	XmlTextAttribute_t3321178844::get_offset_of_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (XmlTypeAttribute_t1804457051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1838[3] = 
{
	XmlTypeAttribute_t1804457051::get_offset_of_includeInSchema_0(),
	XmlTypeAttribute_t1804457051::get_offset_of_ns_1(),
	XmlTypeAttribute_t1804457051::get_offset_of_typeName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (XmlTypeMapElementInfo_t3381496463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1839[10] = 
{
	XmlTypeMapElementInfo_t3381496463::get_offset_of__elementName_0(),
	XmlTypeMapElementInfo_t3381496463::get_offset_of__namespace_1(),
	XmlTypeMapElementInfo_t3381496463::get_offset_of__form_2(),
	XmlTypeMapElementInfo_t3381496463::get_offset_of__member_3(),
	XmlTypeMapElementInfo_t3381496463::get_offset_of__choiceValue_4(),
	XmlTypeMapElementInfo_t3381496463::get_offset_of__isNullable_5(),
	XmlTypeMapElementInfo_t3381496463::get_offset_of__nestingLevel_6(),
	XmlTypeMapElementInfo_t3381496463::get_offset_of__mappedType_7(),
	XmlTypeMapElementInfo_t3381496463::get_offset_of__type_8(),
	XmlTypeMapElementInfo_t3381496463::get_offset_of__wrappedElement_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (XmlTypeMapElementInfoList_t1987994689), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (XmlTypeMapMember_t3057402259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[8] = 
{
	XmlTypeMapMember_t3057402259::get_offset_of__name_0(),
	XmlTypeMapMember_t3057402259::get_offset_of__index_1(),
	XmlTypeMapMember_t3057402259::get_offset_of__globalIndex_2(),
	XmlTypeMapMember_t3057402259::get_offset_of__typeData_3(),
	XmlTypeMapMember_t3057402259::get_offset_of__member_4(),
	XmlTypeMapMember_t3057402259::get_offset_of__specifiedMember_5(),
	XmlTypeMapMember_t3057402259::get_offset_of__defaultValue_6(),
	XmlTypeMapMember_t3057402259::get_offset_of__flags_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (XmlTypeMapMemberAttribute_t3329971455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1842[4] = 
{
	XmlTypeMapMemberAttribute_t3329971455::get_offset_of__attributeName_8(),
	XmlTypeMapMemberAttribute_t3329971455::get_offset_of__namespace_9(),
	XmlTypeMapMemberAttribute_t3329971455::get_offset_of__form_10(),
	XmlTypeMapMemberAttribute_t3329971455::get_offset_of__mappedType_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (XmlTypeMapMemberElement_t1864082097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[4] = 
{
	XmlTypeMapMemberElement_t1864082097::get_offset_of__elementInfo_8(),
	XmlTypeMapMemberElement_t1864082097::get_offset_of__choiceMember_9(),
	XmlTypeMapMemberElement_t1864082097::get_offset_of__isTextCollector_10(),
	XmlTypeMapMemberElement_t1864082097::get_offset_of__choiceTypeData_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (XmlTypeMapMemberList_t3063640245), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (XmlTypeMapMemberExpandable_t755220835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[1] = 
{
	XmlTypeMapMemberExpandable_t755220835::get_offset_of__flatArrayIndex_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (XmlTypeMapMemberFlatList_t2275030030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[1] = 
{
	XmlTypeMapMemberFlatList_t2275030030::get_offset_of__listMap_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (XmlTypeMapMemberAnyElement_t1726696875), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (XmlTypeMapMemberAnyAttribute_t151515769), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (XmlTypeMapMemberNamespaces_t999867419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (XmlTypeMapping_t315595419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1850[8] = 
{
	XmlTypeMapping_t315595419::get_offset_of_xmlType_7(),
	XmlTypeMapping_t315595419::get_offset_of_xmlTypeNamespace_8(),
	XmlTypeMapping_t315595419::get_offset_of_type_9(),
	XmlTypeMapping_t315595419::get_offset_of_baseMap_10(),
	XmlTypeMapping_t315595419::get_offset_of_multiReferenceType_11(),
	XmlTypeMapping_t315595419::get_offset_of_includeInSchema_12(),
	XmlTypeMapping_t315595419::get_offset_of_isNullable_13(),
	XmlTypeMapping_t315595419::get_offset_of__derivedTypes_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (XmlSerializableMapping_t277695926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1851[3] = 
{
	XmlSerializableMapping_t277695926::get_offset_of__schema_15(),
	XmlSerializableMapping_t277695926::get_offset_of__schemaType_16(),
	XmlSerializableMapping_t277695926::get_offset_of__schemaTypeName_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (ClassMap_t1647926812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[16] = 
{
	ClassMap_t1647926812::get_offset_of__elements_0(),
	ClassMap_t1647926812::get_offset_of__elementMembers_1(),
	ClassMap_t1647926812::get_offset_of__attributeMembers_2(),
	ClassMap_t1647926812::get_offset_of__attributeMembersArray_3(),
	ClassMap_t1647926812::get_offset_of__elementsByIndex_4(),
	ClassMap_t1647926812::get_offset_of__flatLists_5(),
	ClassMap_t1647926812::get_offset_of__allMembers_6(),
	ClassMap_t1647926812::get_offset_of__membersWithDefault_7(),
	ClassMap_t1647926812::get_offset_of__listMembers_8(),
	ClassMap_t1647926812::get_offset_of__defaultAnyElement_9(),
	ClassMap_t1647926812::get_offset_of__defaultAnyAttribute_10(),
	ClassMap_t1647926812::get_offset_of__namespaceDeclarations_11(),
	ClassMap_t1647926812::get_offset_of__xmlTextCollector_12(),
	ClassMap_t1647926812::get_offset_of__returnMember_13(),
	ClassMap_t1647926812::get_offset_of__ignoreMemberNamespace_14(),
	ClassMap_t1647926812::get_offset_of__canBeSimpleType_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (ListMap_t1787375712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1853[2] = 
{
	ListMap_t1787375712::get_offset_of__itemInfo_0(),
	ListMap_t1787375712::get_offset_of__choiceMember_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (EnumMap_t3161685173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[5] = 
{
	EnumMap_t3161685173::get_offset_of__members_0(),
	EnumMap_t3161685173::get_offset_of__isFlags_1(),
	EnumMap_t3161685173::get_offset_of__enumNames_2(),
	EnumMap_t3161685173::get_offset_of__xmlNames_3(),
	EnumMap_t3161685173::get_offset_of__values_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (EnumMapMember_t3814867081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1855[3] = 
{
	EnumMapMember_t3814867081::get_offset_of__xmlName_0(),
	EnumMapMember_t3814867081::get_offset_of__enumName_1(),
	EnumMapMember_t3814867081::get_offset_of__value_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (ConformanceLevel_t3761201363)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1857[4] = 
{
	ConformanceLevel_t3761201363::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (DTDAutomataFactory_t3605390810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[3] = 
{
	DTDAutomataFactory_t3605390810::get_offset_of_root_0(),
	DTDAutomataFactory_t3605390810::get_offset_of_choiceTable_1(),
	DTDAutomataFactory_t3605390810::get_offset_of_sequenceTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (DTDObjectModel_t1113953282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1859[19] = 
{
	DTDObjectModel_t1113953282::get_offset_of_factory_0(),
	DTDObjectModel_t1113953282::get_offset_of_elementDecls_1(),
	DTDObjectModel_t1113953282::get_offset_of_attListDecls_2(),
	DTDObjectModel_t1113953282::get_offset_of_peDecls_3(),
	DTDObjectModel_t1113953282::get_offset_of_entityDecls_4(),
	DTDObjectModel_t1113953282::get_offset_of_notationDecls_5(),
	DTDObjectModel_t1113953282::get_offset_of_validationErrors_6(),
	DTDObjectModel_t1113953282::get_offset_of_resolver_7(),
	DTDObjectModel_t1113953282::get_offset_of_nameTable_8(),
	DTDObjectModel_t1113953282::get_offset_of_externalResources_9(),
	DTDObjectModel_t1113953282::get_offset_of_baseURI_10(),
	DTDObjectModel_t1113953282::get_offset_of_name_11(),
	DTDObjectModel_t1113953282::get_offset_of_publicId_12(),
	DTDObjectModel_t1113953282::get_offset_of_systemId_13(),
	DTDObjectModel_t1113953282::get_offset_of_intSubset_14(),
	DTDObjectModel_t1113953282::get_offset_of_intSubsetHasPERef_15(),
	DTDObjectModel_t1113953282::get_offset_of_isStandalone_16(),
	DTDObjectModel_t1113953282::get_offset_of_lineNumber_17(),
	DTDObjectModel_t1113953282::get_offset_of_linePosition_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (DictionaryBase_t1005937181), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (U3CU3Ec__Iterator3_t3518389200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1861[5] = 
{
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CU24s_431U3E__0_0(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CpU3E__1_1(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U24PC_2(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (DTDCollectionBase_t2621362935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1862[1] = 
{
	DTDCollectionBase_t2621362935::get_offset_of_root_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (DTDElementDeclarationCollection_t2224069626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (DTDAttListDeclarationCollection_t243645429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (DTDEntityDeclarationCollection_t1212505713), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (DTDNotationDeclarationCollection_t228085060), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (DTDContentModel_t445576364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1867[6] = 
{
	DTDContentModel_t445576364::get_offset_of_root_5(),
	DTDContentModel_t445576364::get_offset_of_ownerElementName_6(),
	DTDContentModel_t445576364::get_offset_of_elementName_7(),
	DTDContentModel_t445576364::get_offset_of_orderType_8(),
	DTDContentModel_t445576364::get_offset_of_childModels_9(),
	DTDContentModel_t445576364::get_offset_of_occurence_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (DTDContentModelCollection_t3164170484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1868[1] = 
{
	DTDContentModelCollection_t3164170484::get_offset_of_contentModel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (DTDNode_t1758286970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1869[5] = 
{
	DTDNode_t1758286970::get_offset_of_root_0(),
	DTDNode_t1758286970::get_offset_of_isInternalSubset_1(),
	DTDNode_t1758286970::get_offset_of_baseURI_2(),
	DTDNode_t1758286970::get_offset_of_lineNumber_3(),
	DTDNode_t1758286970::get_offset_of_linePosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (DTDElementDeclaration_t8748002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1870[6] = 
{
	DTDElementDeclaration_t8748002::get_offset_of_root_5(),
	DTDElementDeclaration_t8748002::get_offset_of_contentModel_6(),
	DTDElementDeclaration_t8748002::get_offset_of_name_7(),
	DTDElementDeclaration_t8748002::get_offset_of_isEmpty_8(),
	DTDElementDeclaration_t8748002::get_offset_of_isAny_9(),
	DTDElementDeclaration_t8748002::get_offset_of_isMixedContent_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (DTDAttributeDefinition_t3692870749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1871[4] = 
{
	DTDAttributeDefinition_t3692870749::get_offset_of_name_5(),
	DTDAttributeDefinition_t3692870749::get_offset_of_datatype_6(),
	DTDAttributeDefinition_t3692870749::get_offset_of_unresolvedDefault_7(),
	DTDAttributeDefinition_t3692870749::get_offset_of_resolvedDefaultValue_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (DTDAttListDeclaration_t2272374839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1872[3] = 
{
	DTDAttListDeclaration_t2272374839::get_offset_of_name_5(),
	DTDAttListDeclaration_t2272374839::get_offset_of_attributeOrders_6(),
	DTDAttListDeclaration_t2272374839::get_offset_of_attributes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (DTDEntityBase_t2353758560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1873[10] = 
{
	DTDEntityBase_t2353758560::get_offset_of_name_5(),
	DTDEntityBase_t2353758560::get_offset_of_publicId_6(),
	DTDEntityBase_t2353758560::get_offset_of_systemId_7(),
	DTDEntityBase_t2353758560::get_offset_of_literalValue_8(),
	DTDEntityBase_t2353758560::get_offset_of_replacementText_9(),
	DTDEntityBase_t2353758560::get_offset_of_uriString_10(),
	DTDEntityBase_t2353758560::get_offset_of_absUri_11(),
	DTDEntityBase_t2353758560::get_offset_of_isInvalid_12(),
	DTDEntityBase_t2353758560::get_offset_of_loadFailed_13(),
	DTDEntityBase_t2353758560::get_offset_of_resolver_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (DTDEntityDeclaration_t4283284771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1874[6] = 
{
	DTDEntityDeclaration_t4283284771::get_offset_of_entityValue_15(),
	DTDEntityDeclaration_t4283284771::get_offset_of_notationName_16(),
	DTDEntityDeclaration_t4283284771::get_offset_of_ReferencingEntities_17(),
	DTDEntityDeclaration_t4283284771::get_offset_of_scanned_18(),
	DTDEntityDeclaration_t4283284771::get_offset_of_recursed_19(),
	DTDEntityDeclaration_t4283284771::get_offset_of_hasExternalReference_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (DTDNotationDeclaration_t1758408116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1875[5] = 
{
	DTDNotationDeclaration_t1758408116::get_offset_of_name_5(),
	DTDNotationDeclaration_t1758408116::get_offset_of_localName_6(),
	DTDNotationDeclaration_t1758408116::get_offset_of_prefix_7(),
	DTDNotationDeclaration_t1758408116::get_offset_of_publicId_8(),
	DTDNotationDeclaration_t1758408116::get_offset_of_systemId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (DTDParameterEntityDeclarationCollection_t3496720022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1876[2] = 
{
	DTDParameterEntityDeclarationCollection_t3496720022::get_offset_of_peDecls_0(),
	DTDParameterEntityDeclarationCollection_t3496720022::get_offset_of_root_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (DTDParameterEntityDeclaration_t252230634), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (DTDContentOrderType_t3150259539)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1878[4] = 
{
	DTDContentOrderType_t3150259539::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (DTDOccurence_t99371501)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1879[5] = 
{
	DTDOccurence_t99371501::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (DTDReader_t2453137441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1880[14] = 
{
	DTDReader_t2453137441::get_offset_of_currentInput_0(),
	DTDReader_t2453137441::get_offset_of_parserInputStack_1(),
	DTDReader_t2453137441::get_offset_of_nameBuffer_2(),
	DTDReader_t2453137441::get_offset_of_nameLength_3(),
	DTDReader_t2453137441::get_offset_of_nameCapacity_4(),
	DTDReader_t2453137441::get_offset_of_valueBuffer_5(),
	DTDReader_t2453137441::get_offset_of_currentLinkedNodeLineNumber_6(),
	DTDReader_t2453137441::get_offset_of_currentLinkedNodeLinePosition_7(),
	DTDReader_t2453137441::get_offset_of_dtdIncludeSect_8(),
	DTDReader_t2453137441::get_offset_of_normalization_9(),
	DTDReader_t2453137441::get_offset_of_processingInternalSubset_10(),
	DTDReader_t2453137441::get_offset_of_cachedPublicId_11(),
	DTDReader_t2453137441::get_offset_of_cachedSystemId_12(),
	DTDReader_t2453137441::get_offset_of_DTD_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (EntityHandling_t3960499440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1881[3] = 
{
	EntityHandling_t3960499440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (Formatting_t1126649075)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1882[3] = 
{
	Formatting_t1126649075::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (NameTable_t594386929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1886[3] = 
{
	NameTable_t594386929::get_offset_of_count_0(),
	NameTable_t594386929::get_offset_of_buckets_1(),
	NameTable_t594386929::get_offset_of_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (Entry_t2583369454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[4] = 
{
	Entry_t2583369454::get_offset_of_str_0(),
	Entry_t2583369454::get_offset_of_hash_1(),
	Entry_t2583369454::get_offset_of_len_2(),
	Entry_t2583369454::get_offset_of_next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (NamespaceHandling_t1452270444)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1888[3] = 
{
	NamespaceHandling_t1452270444::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (NewLineHandling_t1737195169)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1889[4] = 
{
	NewLineHandling_t1737195169::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (ReadState_t3138905245)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1890[6] = 
{
	ReadState_t3138905245::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (WhitespaceHandling_t3754063142)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1891[4] = 
{
	WhitespaceHandling_t3754063142::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (WriteState_t1534871862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1892[8] = 
{
	WriteState_t1534871862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (XmlAttribute_t175731005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1893[4] = 
{
	XmlAttribute_t175731005::get_offset_of_name_5(),
	XmlAttribute_t175731005::get_offset_of_isDefault_6(),
	XmlAttribute_t175731005::get_offset_of_lastLinkedChild_7(),
	XmlAttribute_t175731005::get_offset_of_schemaInfo_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (XmlAttributeCollection_t3359885287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[2] = 
{
	XmlAttributeCollection_t3359885287::get_offset_of_ownerElement_4(),
	XmlAttributeCollection_t3359885287::get_offset_of_ownerDocument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (XmlCDataSection_t1124775823), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (XmlChar_t1369421061), -1, sizeof(XmlChar_t1369421061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1896[5] = 
{
	XmlChar_t1369421061_StaticFields::get_offset_of_WhitespaceChars_0(),
	XmlChar_t1369421061_StaticFields::get_offset_of_firstNamePages_1(),
	XmlChar_t1369421061_StaticFields::get_offset_of_namePages_2(),
	XmlChar_t1369421061_StaticFields::get_offset_of_nameBitmap_3(),
	XmlChar_t1369421061_StaticFields::get_offset_of_U3CU3Ef__switchU24map47_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (XmlCharacterData_t575748506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1897[1] = 
{
	XmlCharacterData_t575748506::get_offset_of_data_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (XmlComment_t3999331572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (XmlConvert_t1936105738), -1, sizeof(XmlConvert_t1936105738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1899[8] = 
{
	XmlConvert_t1936105738_StaticFields::get_offset_of_datetimeFormats_0(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_defaultDateTimeFormats_1(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_roundtripDateTimeFormats_2(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_localDateTimeFormats_3(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_utcDateTimeFormats_4(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_unspecifiedDateTimeFormats_5(),
	XmlConvert_t1936105738_StaticFields::get_offset_of__defaultStyle_6(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_U3CU3Ef__switchU24map49_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
