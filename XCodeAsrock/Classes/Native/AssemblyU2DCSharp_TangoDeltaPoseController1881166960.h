﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoPoseStatus1112875089.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"

// Tango.TangoApplication
struct TangoApplication_t278578959;
// UnityEngine.CharacterController
struct CharacterController_t4094781467;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TangoDeltaPoseController
struct  TangoDeltaPoseController_t1881166960  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TangoDeltaPoseController::m_poseDeltaTime
	float ___m_poseDeltaTime_2;
	// System.Int32 TangoDeltaPoseController::m_poseCount
	int32_t ___m_poseCount_3;
	// Tango.TangoEnums/TangoPoseStatusType TangoDeltaPoseController::m_poseStatus
	int32_t ___m_poseStatus_4;
	// System.Single TangoDeltaPoseController::m_poseTimestamp
	float ___m_poseTimestamp_5;
	// UnityEngine.Vector3 TangoDeltaPoseController::m_tangoPosition
	Vector3_t2243707580  ___m_tangoPosition_6;
	// UnityEngine.Quaternion TangoDeltaPoseController::m_tangoRotation
	Quaternion_t4030073918  ___m_tangoRotation_7;
	// System.Boolean TangoDeltaPoseController::m_characterMotion
	bool ___m_characterMotion_8;
	// System.Boolean TangoDeltaPoseController::m_enableClutchUI
	bool ___m_enableClutchUI_9;
	// System.Boolean TangoDeltaPoseController::m_useAreaDescriptionPose
	bool ___m_useAreaDescriptionPose_10;
	// Tango.TangoApplication TangoDeltaPoseController::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_11;
	// UnityEngine.Vector3 TangoDeltaPoseController::m_prevTangoPosition
	Vector3_t2243707580  ___m_prevTangoPosition_12;
	// UnityEngine.Quaternion TangoDeltaPoseController::m_prevTangoRotation
	Quaternion_t4030073918  ___m_prevTangoRotation_13;
	// System.Boolean TangoDeltaPoseController::m_clutchActive
	bool ___m_clutchActive_14;
	// UnityEngine.CharacterController TangoDeltaPoseController::m_characterController
	CharacterController_t4094781467 * ___m_characterController_15;
	// UnityEngine.Matrix4x4 TangoDeltaPoseController::m_uwTuc
	Matrix4x4_t2933234003  ___m_uwTuc_16;
	// UnityEngine.Matrix4x4 TangoDeltaPoseController::m_uwOffsetTuw
	Matrix4x4_t2933234003  ___m_uwOffsetTuw_17;

public:
	inline static int32_t get_offset_of_m_poseDeltaTime_2() { return static_cast<int32_t>(offsetof(TangoDeltaPoseController_t1881166960, ___m_poseDeltaTime_2)); }
	inline float get_m_poseDeltaTime_2() const { return ___m_poseDeltaTime_2; }
	inline float* get_address_of_m_poseDeltaTime_2() { return &___m_poseDeltaTime_2; }
	inline void set_m_poseDeltaTime_2(float value)
	{
		___m_poseDeltaTime_2 = value;
	}

	inline static int32_t get_offset_of_m_poseCount_3() { return static_cast<int32_t>(offsetof(TangoDeltaPoseController_t1881166960, ___m_poseCount_3)); }
	inline int32_t get_m_poseCount_3() const { return ___m_poseCount_3; }
	inline int32_t* get_address_of_m_poseCount_3() { return &___m_poseCount_3; }
	inline void set_m_poseCount_3(int32_t value)
	{
		___m_poseCount_3 = value;
	}

	inline static int32_t get_offset_of_m_poseStatus_4() { return static_cast<int32_t>(offsetof(TangoDeltaPoseController_t1881166960, ___m_poseStatus_4)); }
	inline int32_t get_m_poseStatus_4() const { return ___m_poseStatus_4; }
	inline int32_t* get_address_of_m_poseStatus_4() { return &___m_poseStatus_4; }
	inline void set_m_poseStatus_4(int32_t value)
	{
		___m_poseStatus_4 = value;
	}

	inline static int32_t get_offset_of_m_poseTimestamp_5() { return static_cast<int32_t>(offsetof(TangoDeltaPoseController_t1881166960, ___m_poseTimestamp_5)); }
	inline float get_m_poseTimestamp_5() const { return ___m_poseTimestamp_5; }
	inline float* get_address_of_m_poseTimestamp_5() { return &___m_poseTimestamp_5; }
	inline void set_m_poseTimestamp_5(float value)
	{
		___m_poseTimestamp_5 = value;
	}

	inline static int32_t get_offset_of_m_tangoPosition_6() { return static_cast<int32_t>(offsetof(TangoDeltaPoseController_t1881166960, ___m_tangoPosition_6)); }
	inline Vector3_t2243707580  get_m_tangoPosition_6() const { return ___m_tangoPosition_6; }
	inline Vector3_t2243707580 * get_address_of_m_tangoPosition_6() { return &___m_tangoPosition_6; }
	inline void set_m_tangoPosition_6(Vector3_t2243707580  value)
	{
		___m_tangoPosition_6 = value;
	}

	inline static int32_t get_offset_of_m_tangoRotation_7() { return static_cast<int32_t>(offsetof(TangoDeltaPoseController_t1881166960, ___m_tangoRotation_7)); }
	inline Quaternion_t4030073918  get_m_tangoRotation_7() const { return ___m_tangoRotation_7; }
	inline Quaternion_t4030073918 * get_address_of_m_tangoRotation_7() { return &___m_tangoRotation_7; }
	inline void set_m_tangoRotation_7(Quaternion_t4030073918  value)
	{
		___m_tangoRotation_7 = value;
	}

	inline static int32_t get_offset_of_m_characterMotion_8() { return static_cast<int32_t>(offsetof(TangoDeltaPoseController_t1881166960, ___m_characterMotion_8)); }
	inline bool get_m_characterMotion_8() const { return ___m_characterMotion_8; }
	inline bool* get_address_of_m_characterMotion_8() { return &___m_characterMotion_8; }
	inline void set_m_characterMotion_8(bool value)
	{
		___m_characterMotion_8 = value;
	}

	inline static int32_t get_offset_of_m_enableClutchUI_9() { return static_cast<int32_t>(offsetof(TangoDeltaPoseController_t1881166960, ___m_enableClutchUI_9)); }
	inline bool get_m_enableClutchUI_9() const { return ___m_enableClutchUI_9; }
	inline bool* get_address_of_m_enableClutchUI_9() { return &___m_enableClutchUI_9; }
	inline void set_m_enableClutchUI_9(bool value)
	{
		___m_enableClutchUI_9 = value;
	}

	inline static int32_t get_offset_of_m_useAreaDescriptionPose_10() { return static_cast<int32_t>(offsetof(TangoDeltaPoseController_t1881166960, ___m_useAreaDescriptionPose_10)); }
	inline bool get_m_useAreaDescriptionPose_10() const { return ___m_useAreaDescriptionPose_10; }
	inline bool* get_address_of_m_useAreaDescriptionPose_10() { return &___m_useAreaDescriptionPose_10; }
	inline void set_m_useAreaDescriptionPose_10(bool value)
	{
		___m_useAreaDescriptionPose_10 = value;
	}

	inline static int32_t get_offset_of_m_tangoApplication_11() { return static_cast<int32_t>(offsetof(TangoDeltaPoseController_t1881166960, ___m_tangoApplication_11)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_11() const { return ___m_tangoApplication_11; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_11() { return &___m_tangoApplication_11; }
	inline void set_m_tangoApplication_11(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_11, value);
	}

	inline static int32_t get_offset_of_m_prevTangoPosition_12() { return static_cast<int32_t>(offsetof(TangoDeltaPoseController_t1881166960, ___m_prevTangoPosition_12)); }
	inline Vector3_t2243707580  get_m_prevTangoPosition_12() const { return ___m_prevTangoPosition_12; }
	inline Vector3_t2243707580 * get_address_of_m_prevTangoPosition_12() { return &___m_prevTangoPosition_12; }
	inline void set_m_prevTangoPosition_12(Vector3_t2243707580  value)
	{
		___m_prevTangoPosition_12 = value;
	}

	inline static int32_t get_offset_of_m_prevTangoRotation_13() { return static_cast<int32_t>(offsetof(TangoDeltaPoseController_t1881166960, ___m_prevTangoRotation_13)); }
	inline Quaternion_t4030073918  get_m_prevTangoRotation_13() const { return ___m_prevTangoRotation_13; }
	inline Quaternion_t4030073918 * get_address_of_m_prevTangoRotation_13() { return &___m_prevTangoRotation_13; }
	inline void set_m_prevTangoRotation_13(Quaternion_t4030073918  value)
	{
		___m_prevTangoRotation_13 = value;
	}

	inline static int32_t get_offset_of_m_clutchActive_14() { return static_cast<int32_t>(offsetof(TangoDeltaPoseController_t1881166960, ___m_clutchActive_14)); }
	inline bool get_m_clutchActive_14() const { return ___m_clutchActive_14; }
	inline bool* get_address_of_m_clutchActive_14() { return &___m_clutchActive_14; }
	inline void set_m_clutchActive_14(bool value)
	{
		___m_clutchActive_14 = value;
	}

	inline static int32_t get_offset_of_m_characterController_15() { return static_cast<int32_t>(offsetof(TangoDeltaPoseController_t1881166960, ___m_characterController_15)); }
	inline CharacterController_t4094781467 * get_m_characterController_15() const { return ___m_characterController_15; }
	inline CharacterController_t4094781467 ** get_address_of_m_characterController_15() { return &___m_characterController_15; }
	inline void set_m_characterController_15(CharacterController_t4094781467 * value)
	{
		___m_characterController_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_characterController_15, value);
	}

	inline static int32_t get_offset_of_m_uwTuc_16() { return static_cast<int32_t>(offsetof(TangoDeltaPoseController_t1881166960, ___m_uwTuc_16)); }
	inline Matrix4x4_t2933234003  get_m_uwTuc_16() const { return ___m_uwTuc_16; }
	inline Matrix4x4_t2933234003 * get_address_of_m_uwTuc_16() { return &___m_uwTuc_16; }
	inline void set_m_uwTuc_16(Matrix4x4_t2933234003  value)
	{
		___m_uwTuc_16 = value;
	}

	inline static int32_t get_offset_of_m_uwOffsetTuw_17() { return static_cast<int32_t>(offsetof(TangoDeltaPoseController_t1881166960, ___m_uwOffsetTuw_17)); }
	inline Matrix4x4_t2933234003  get_m_uwOffsetTuw_17() const { return ___m_uwOffsetTuw_17; }
	inline Matrix4x4_t2933234003 * get_address_of_m_uwOffsetTuw_17() { return &___m_uwOffsetTuw_17; }
	inline void set_m_uwOffsetTuw_17(Matrix4x4_t2933234003  value)
	{
		___m_uwOffsetTuw_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
