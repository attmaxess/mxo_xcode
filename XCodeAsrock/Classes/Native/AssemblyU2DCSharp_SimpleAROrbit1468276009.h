﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleAROrbit
struct  SimpleAROrbit_t1468276009  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject SimpleAROrbit::m_earth
	GameObject_t1756533147 * ___m_earth_2;
	// System.Single SimpleAROrbit::m_radius
	float ___m_radius_3;
	// System.Single SimpleAROrbit::m_orbitSpeed
	float ___m_orbitSpeed_4;

public:
	inline static int32_t get_offset_of_m_earth_2() { return static_cast<int32_t>(offsetof(SimpleAROrbit_t1468276009, ___m_earth_2)); }
	inline GameObject_t1756533147 * get_m_earth_2() const { return ___m_earth_2; }
	inline GameObject_t1756533147 ** get_address_of_m_earth_2() { return &___m_earth_2; }
	inline void set_m_earth_2(GameObject_t1756533147 * value)
	{
		___m_earth_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_earth_2, value);
	}

	inline static int32_t get_offset_of_m_radius_3() { return static_cast<int32_t>(offsetof(SimpleAROrbit_t1468276009, ___m_radius_3)); }
	inline float get_m_radius_3() const { return ___m_radius_3; }
	inline float* get_address_of_m_radius_3() { return &___m_radius_3; }
	inline void set_m_radius_3(float value)
	{
		___m_radius_3 = value;
	}

	inline static int32_t get_offset_of_m_orbitSpeed_4() { return static_cast<int32_t>(offsetof(SimpleAROrbit_t1468276009, ___m_orbitSpeed_4)); }
	inline float get_m_orbitSpeed_4() const { return ___m_orbitSpeed_4; }
	inline float* get_address_of_m_orbitSpeed_4() { return &___m_orbitSpeed_4; }
	inline void set_m_orbitSpeed_4(float value)
	{
		___m_orbitSpeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
