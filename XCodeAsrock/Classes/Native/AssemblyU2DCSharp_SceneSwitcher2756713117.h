﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneSwitcher
struct  SceneSwitcher_t2756713117  : public MonoBehaviour_t1158329972
{
public:
	// System.Action`1<System.String> SceneSwitcher::m_onBeforeLoadScene
	Action_1_t1831019615 * ___m_onBeforeLoadScene_2;
	// System.String[] SceneSwitcher::m_sceneNames
	StringU5BU5D_t1642385972* ___m_sceneNames_7;

public:
	inline static int32_t get_offset_of_m_onBeforeLoadScene_2() { return static_cast<int32_t>(offsetof(SceneSwitcher_t2756713117, ___m_onBeforeLoadScene_2)); }
	inline Action_1_t1831019615 * get_m_onBeforeLoadScene_2() const { return ___m_onBeforeLoadScene_2; }
	inline Action_1_t1831019615 ** get_address_of_m_onBeforeLoadScene_2() { return &___m_onBeforeLoadScene_2; }
	inline void set_m_onBeforeLoadScene_2(Action_1_t1831019615 * value)
	{
		___m_onBeforeLoadScene_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_onBeforeLoadScene_2, value);
	}

	inline static int32_t get_offset_of_m_sceneNames_7() { return static_cast<int32_t>(offsetof(SceneSwitcher_t2756713117, ___m_sceneNames_7)); }
	inline StringU5BU5D_t1642385972* get_m_sceneNames_7() const { return ___m_sceneNames_7; }
	inline StringU5BU5D_t1642385972** get_address_of_m_sceneNames_7() { return &___m_sceneNames_7; }
	inline void set_m_sceneNames_7(StringU5BU5D_t1642385972* value)
	{
		___m_sceneNames_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_sceneNames_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
