﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngineInternal_GenericStack3718539591.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970.h"
#include "System_Xml_U3CModuleU3E3783534214.h"
#include "System_Xml_System_MonoTODOAttribute3487514019.h"
#include "System_Xml_Mono_Xml_Schema_XsdWhitespaceFacet2758097827.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnySimpleType1096449895.h"
#include "System_Xml_Mono_Xml_Schema_XdtAnyAtomicType3359210273.h"
#include "System_Xml_Mono_Xml_Schema_XdtUntypedAtomic2904699188.h"
#include "System_Xml_Mono_Xml_Schema_XsdString263933896.h"
#include "System_Xml_Mono_Xml_Schema_XsdNormalizedString2132216291.h"
#include "System_Xml_Mono_Xml_Schema_XsdToken2902215462.h"
#include "System_Xml_Mono_Xml_Schema_XsdLanguage3897851897.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMToken547135877.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMTokens1753890866.h"
#include "System_Xml_Mono_Xml_Schema_XsdName1409945702.h"
#include "System_Xml_Mono_Xml_Schema_XsdNCName414304927.h"
#include "System_Xml_Mono_Xml_Schema_XsdID1067193160.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRef1377311049.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRefs763119546.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntity2664305022.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntities1103053540.h"
#include "System_Xml_Mono_Xml_Schema_XsdNotation720379093.h"
#include "System_Xml_Mono_Xml_Schema_XsdDecimal2932251550.h"
#include "System_Xml_Mono_Xml_Schema_XsdInteger1330502157.h"
#include "System_Xml_Mono_Xml_Schema_XsdLong4179235589.h"
#include "System_Xml_Mono_Xml_Schema_XsdInt1488443894.h"
#include "System_Xml_Mono_Xml_Schema_XsdShort1778530041.h"
#include "System_Xml_Mono_Xml_Schema_XsdByte1120972221.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonNegativeInteger3587933853.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedLong137890294.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedInt2956447959.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedShort3693774826.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedByte3216355454.h"
#include "System_Xml_Mono_Xml_Schema_XsdPositiveInteger1896481288.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonPositiveInteger409343009.h"
#include "System_Xml_Mono_Xml_Schema_XsdNegativeInteger399444136.h"
#include "System_Xml_Mono_Xml_Schema_XsdFloat386143221.h"
#include "System_Xml_Mono_Xml_Schema_XsdDouble2510112208.h"
#include "System_Xml_Mono_Xml_Schema_XsdBase64Binary1094704629.h"
#include "System_Xml_Mono_Xml_Schema_XsdHexBinary3496718151.h"
#include "System_Xml_Mono_Xml_Schema_XsdQName930779123.h"
#include "System_Xml_Mono_Xml_Schema_XsdBoolean4126353587.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnyURI2527983239.h"
#include "System_Xml_Mono_Xml_Schema_XsdDuration1605638443.h"
#include "System_Xml_Mono_Xml_Schema_XdtDayTimeDuration2797717973.h"
#include "System_Xml_Mono_Xml_Schema_XdtYearMonthDuration1764328599.h"
#include "System_Xml_Mono_Xml_Schema_XsdDateTime1344468684.h"
#include "System_Xml_Mono_Xml_Schema_XsdDate919459387.h"
#include "System_Xml_Mono_Xml_Schema_XsdTime4165680512.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYearMonth1363357165.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonthDay3859978294.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYear3810382607.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonth4076673358.h"
#include "System_Xml_Mono_Xml_Schema_XsdGDay1914244270.h"
#include "System_Xml_System_Xml_Schema_XmlSchema880472818.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2082486936.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttribute4015859774.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCompilationS2971213394.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexType4086789226.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatype1195946242.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMe3165007540.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaElement2433337156.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet614309579.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet_Facet3019654938.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaForm1143227640.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaInfo2864028808.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObject2050913741.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle3365045970.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaPatternFacet2024909611.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSet313318308.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleType248156492.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeCo1606103299.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeLi2170323082.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeRe1099506232.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeUnio91327365.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaType1795078578.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUtil3576230726.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidity995929432.h"
#include "System_Xml_System_Xml_Serialization_CodeIdentifier3245527920.h"
#include "System_Xml_System_Xml_Serialization_KeyHelper2419695731.h"
#include "System_Xml_System_Xml_Serialization_ReflectionHelp3833447559.h"
#include "System_Xml_System_Xml_Serialization_SchemaTypes3045759914.h"
#include "System_Xml_System_Xml_Serialization_SerializationS1619243689.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeSeriali3069538444.h"
#include "System_Xml_System_Xml_Serialization_TypeData3979356678.h"
#include "System_Xml_System_Xml_Serialization_TypeMember1615994940.h"
#include "System_Xml_System_Xml_Serialization_TypeTranslator1077722680.h"
#include "System_Xml_System_Xml_Serialization_UnreferencedObj983274210.h"
#include "System_Xml_System_Xml_Serialization_XmlAnyAttribute713947513.h"
#include "System_Xml_System_Xml_Serialization_XmlAnyElementA2502375235.h"
#include "System_Xml_System_Xml_Serialization_XmlAnyElementA4274036600.h"
#include "System_Xml_System_Xml_Serialization_XmlArrayAttrib4163692084.h"
#include "System_Xml_System_Xml_Serialization_XmlArrayItemAt2615142271.h"
#include "System_Xml_System_Xml_Serialization_XmlArrayItemAt1923979634.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeAtt850813783.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeEv3859077982.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeOv3994942922.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (GenericStack_t3718539591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (NetFxCoreExtensions_t4275971970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (U3CModuleU3E_t3783534219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (MonoTODOAttribute_t3487514022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (XsdWhitespaceFacet_t2758097827)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1705[4] = 
{
	XsdWhitespaceFacet_t2758097827::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (XsdAnySimpleType_t1096449895), -1, sizeof(XsdAnySimpleType_t1096449895_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1706[6] = 
{
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_instance_55(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_whitespaceArray_56(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_booleanAllowedFacets_57(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_decimalAllowedFacets_58(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_durationAllowedFacets_59(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_stringAllowedFacets_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (XdtAnyAtomicType_t3359210273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (XdtUntypedAtomic_t2904699188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (XsdString_t263933896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (XsdNormalizedString_t2132216291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (XsdToken_t2902215462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (XsdLanguage_t3897851897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (XsdNMToken_t547135877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (XsdNMTokens_t1753890866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (XsdName_t1409945702), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (XsdNCName_t414304927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (XsdID_t1067193160), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (XsdIDRef_t1377311049), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (XsdIDRefs_t763119546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (XsdEntity_t2664305022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (XsdEntities_t1103053540), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (XsdNotation_t720379093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (XsdDecimal_t2932251550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (XsdInteger_t1330502157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (XsdLong_t4179235589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (XsdInt_t1488443894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (XsdShort_t1778530041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (XsdByte_t1120972221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (XsdNonNegativeInteger_t3587933853), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (XsdUnsignedLong_t137890294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (XsdUnsignedInt_t2956447959), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (XsdUnsignedShort_t3693774826), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (XsdUnsignedByte_t3216355454), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (XsdPositiveInteger_t1896481288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (XsdNonPositiveInteger_t409343009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (XsdNegativeInteger_t399444136), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (XsdFloat_t386143221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (XsdDouble_t2510112208), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (XsdBase64Binary_t1094704629), -1, sizeof(XsdBase64Binary_t1094704629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1739[2] = 
{
	XsdBase64Binary_t1094704629_StaticFields::get_offset_of_ALPHABET_61(),
	XsdBase64Binary_t1094704629_StaticFields::get_offset_of_decodeTable_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (XsdHexBinary_t3496718151), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (XsdQName_t930779123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (XsdBoolean_t4126353587), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (XsdAnyURI_t2527983239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (XsdDuration_t1605638443), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (XdtDayTimeDuration_t2797717973), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (XdtYearMonthDuration_t1764328599), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (XsdDateTime_t1344468684), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (XsdDate_t919459387), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (XsdTime_t4165680512), -1, sizeof(XsdTime_t4165680512_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1749[1] = 
{
	XsdTime_t4165680512_StaticFields::get_offset_of_timeFormats_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (XsdGYearMonth_t1363357165), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (XsdGMonthDay_t3859978294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (XsdGYear_t3810382607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (XsdGMonth_t4076673358), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (XsdGDay_t1914244270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (XmlSchema_t880472818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1756[1] = 
{
	XmlSchema_t880472818::get_offset_of_id_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (XmlSchemaAnnotated_t2082486936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (XmlSchemaAttribute_t4015859774), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (XmlSchemaCompilationSettings_t2971213394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1759[1] = 
{
	XmlSchemaCompilationSettings_t2971213394::get_offset_of_enable_upa_check_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (XmlSchemaComplexType_t4086789226), -1, sizeof(XmlSchemaComplexType_t4086789226_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1760[1] = 
{
	XmlSchemaComplexType_t4086789226_StaticFields::get_offset_of_AnyTypeName_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (XmlSchemaDatatype_t1195946242), -1, sizeof(XmlSchemaDatatype_t1195946242_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1761[55] = 
{
	XmlSchemaDatatype_t1195946242::get_offset_of_WhitespaceValue_0(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_wsChars_1(),
	XmlSchemaDatatype_t1195946242::get_offset_of_sb_2(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeAnySimpleType_3(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeString_4(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNormalizedString_5(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeToken_6(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeLanguage_7(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNMToken_8(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNMTokens_9(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeName_10(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNCName_11(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeID_12(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeIDRef_13(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeIDRefs_14(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeEntity_15(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeEntities_16(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNotation_17(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDecimal_18(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeInteger_19(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeLong_20(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeInt_21(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeShort_22(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeByte_23(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNonNegativeInteger_24(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypePositiveInteger_25(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedLong_26(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedInt_27(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedShort_28(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedByte_29(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNonPositiveInteger_30(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNegativeInteger_31(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeFloat_32(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDouble_33(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeBase64Binary_34(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeBoolean_35(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeAnyURI_36(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDuration_37(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDateTime_38(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDate_39(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeTime_40(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeHexBinary_41(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeQName_42(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGYearMonth_43(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGMonthDay_44(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGYear_45(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGMonth_46(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGDay_47(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeAnyAtomicType_48(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUntypedAtomic_49(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDayTimeDuration_50(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeYearMonthDuration_51(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_U3CU3Ef__switchU24map2A_52(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_U3CU3Ef__switchU24map2B_53(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_U3CU3Ef__switchU24map2C_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (XmlSchemaDerivationMethod_t3165007540)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1762[9] = 
{
	XmlSchemaDerivationMethod_t3165007540::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (XmlSchemaElement_t2433337156), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (XmlSchemaFacet_t614309579), -1, sizeof(XmlSchemaFacet_t614309579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1764[2] = 
{
	XmlSchemaFacet_t614309579_StaticFields::get_offset_of_AllFacets_3(),
	XmlSchemaFacet_t614309579::get_offset_of_val_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (Facet_t3019654938)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1765[14] = 
{
	Facet_t3019654938::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (XmlSchemaForm_t1143227640)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1766[4] = 
{
	XmlSchemaForm_t1143227640::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (XmlSchemaInfo_t2864028808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[7] = 
{
	XmlSchemaInfo_t2864028808::get_offset_of_isDefault_0(),
	XmlSchemaInfo_t2864028808::get_offset_of_isNil_1(),
	XmlSchemaInfo_t2864028808::get_offset_of_memberType_2(),
	XmlSchemaInfo_t2864028808::get_offset_of_attr_3(),
	XmlSchemaInfo_t2864028808::get_offset_of_elem_4(),
	XmlSchemaInfo_t2864028808::get_offset_of_type_5(),
	XmlSchemaInfo_t2864028808::get_offset_of_validity_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (XmlSchemaObject_t2050913741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1768[3] = 
{
	XmlSchemaObject_t2050913741::get_offset_of_namespaces_0(),
	XmlSchemaObject_t2050913741::get_offset_of_unhandledAttributeList_1(),
	XmlSchemaObject_t2050913741::get_offset_of_CompilationId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (XmlSchemaParticle_t3365045970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (XmlSchemaPatternFacet_t2024909611), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (XmlSchemaSet_t313318308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1771[5] = 
{
	XmlSchemaSet_t313318308::get_offset_of_nameTable_0(),
	XmlSchemaSet_t313318308::get_offset_of_xmlResolver_1(),
	XmlSchemaSet_t313318308::get_offset_of_schemas_2(),
	XmlSchemaSet_t313318308::get_offset_of_settings_3(),
	XmlSchemaSet_t313318308::get_offset_of_CompilationId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (XmlSchemaSimpleType_t248156492), -1, sizeof(XmlSchemaSimpleType_t248156492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1772[53] = 
{
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_schemaLocationType_9(),
	XmlSchemaSimpleType_t248156492::get_offset_of_content_10(),
	XmlSchemaSimpleType_t248156492::get_offset_of_islocal_11(),
	XmlSchemaSimpleType_t248156492::get_offset_of_variety_12(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsAnySimpleType_13(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsString_14(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsBoolean_15(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDecimal_16(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsFloat_17(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDouble_18(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDuration_19(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDateTime_20(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsTime_21(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDate_22(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGYearMonth_23(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGYear_24(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGMonthDay_25(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGDay_26(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGMonth_27(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsHexBinary_28(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsBase64Binary_29(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsAnyUri_30(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsQName_31(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNotation_32(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNormalizedString_33(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsToken_34(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsLanguage_35(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNMToken_36(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNMTokens_37(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsName_38(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNCName_39(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsID_40(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsIDRef_41(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsIDRefs_42(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsEntity_43(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsEntities_44(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsInteger_45(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNonPositiveInteger_46(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNegativeInteger_47(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsLong_48(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsInt_49(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsShort_50(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsByte_51(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNonNegativeInteger_52(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedLong_53(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedInt_54(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedShort_55(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedByte_56(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsPositiveInteger_57(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtUntypedAtomic_58(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtAnyAtomicType_59(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtYearMonthDuration_60(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtDayTimeDuration_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (XmlSchemaSimpleTypeContent_t1606103299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (XmlSchemaSimpleTypeList_t2170323082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[2] = 
{
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_itemType_3(),
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_itemTypeName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (XmlSchemaSimpleTypeRestriction_t1099506232), -1, sizeof(XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1775[2] = 
{
	XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields::get_offset_of_lengthStyle_3(),
	XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields::get_offset_of_listFacets_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (XmlSchemaSimpleTypeUnion_t91327365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (XmlSchemaType_t1795078578), -1, sizeof(XmlSchemaType_t1795078578_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1777[6] = 
{
	XmlSchemaType_t1795078578::get_offset_of_final_3(),
	XmlSchemaType_t1795078578::get_offset_of_BaseXmlSchemaTypeInternal_4(),
	XmlSchemaType_t1795078578::get_offset_of_DatatypeInternal_5(),
	XmlSchemaType_t1795078578::get_offset_of_QNameInternal_6(),
	XmlSchemaType_t1795078578_StaticFields::get_offset_of_U3CU3Ef__switchU24map2E_7(),
	XmlSchemaType_t1795078578_StaticFields::get_offset_of_U3CU3Ef__switchU24map2F_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (XmlSchemaUtil_t3576230726), -1, sizeof(XmlSchemaUtil_t3576230726_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1778[4] = 
{
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_FinalAllowed_0(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_ElementBlockAllowed_1(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_ComplexTypeBlockAllowed_2(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_StrictMsCompliant_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (XmlSchemaValidity_t995929432)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1779[4] = 
{
	XmlSchemaValidity_t995929432::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (CodeIdentifier_t3245527920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (KeyHelper_t2419695731), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (ReflectionHelper_t3833447559), -1, sizeof(ReflectionHelper_t3833447559_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1783[3] = 
{
	ReflectionHelper_t3833447559::get_offset_of__clrTypes_0(),
	ReflectionHelper_t3833447559::get_offset_of__schemaTypes_1(),
	ReflectionHelper_t3833447559_StaticFields::get_offset_of_empty_modifiers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (SchemaTypes_t3045759914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1784[9] = 
{
	SchemaTypes_t3045759914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (SerializationSource_t1619243689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1785[3] = 
{
	SerializationSource_t1619243689::get_offset_of_includedTypes_0(),
	SerializationSource_t1619243689::get_offset_of_namspace_1(),
	SerializationSource_t1619243689::get_offset_of_canBeGenerated_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (XmlTypeSerializationSource_t3069538444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1786[3] = 
{
	XmlTypeSerializationSource_t3069538444::get_offset_of_attributeOverridesHash_3(),
	XmlTypeSerializationSource_t3069538444::get_offset_of_type_4(),
	XmlTypeSerializationSource_t3069538444::get_offset_of_rootHash_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (TypeData_t3979356678), -1, sizeof(TypeData_t3979356678_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1787[12] = 
{
	TypeData_t3979356678::get_offset_of_type_0(),
	TypeData_t3979356678::get_offset_of_elementName_1(),
	TypeData_t3979356678::get_offset_of_sType_2(),
	TypeData_t3979356678::get_offset_of_listItemType_3(),
	TypeData_t3979356678::get_offset_of_typeName_4(),
	TypeData_t3979356678::get_offset_of_fullTypeName_5(),
	TypeData_t3979356678::get_offset_of_listItemTypeData_6(),
	TypeData_t3979356678::get_offset_of_mappedType_7(),
	TypeData_t3979356678::get_offset_of_facet_8(),
	TypeData_t3979356678::get_offset_of_hasPublicConstructor_9(),
	TypeData_t3979356678::get_offset_of_nullableOverride_10(),
	TypeData_t3979356678_StaticFields::get_offset_of_keywords_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (TypeMember_t1615994940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1788[2] = 
{
	TypeMember_t1615994940::get_offset_of_type_0(),
	TypeMember_t1615994940::get_offset_of_member_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (TypeTranslator_t1077722680), -1, sizeof(TypeTranslator_t1077722680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1789[4] = 
{
	TypeTranslator_t1077722680_StaticFields::get_offset_of_nameCache_0(),
	TypeTranslator_t1077722680_StaticFields::get_offset_of_primitiveTypes_1(),
	TypeTranslator_t1077722680_StaticFields::get_offset_of_primitiveArrayTypes_2(),
	TypeTranslator_t1077722680_StaticFields::get_offset_of_nullableTypes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (UnreferencedObjectEventArgs_t983274210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1790[2] = 
{
	UnreferencedObjectEventArgs_t983274210::get_offset_of_unreferencedObject_1(),
	UnreferencedObjectEventArgs_t983274210::get_offset_of_unreferencedId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (XmlAnyAttributeAttribute_t713947513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (XmlAnyElementAttribute_t2502375235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1792[2] = 
{
	XmlAnyElementAttribute_t2502375235::get_offset_of_elementName_0(),
	XmlAnyElementAttribute_t2502375235::get_offset_of_ns_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (XmlAnyElementAttributes_t4274036600), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (XmlArrayAttribute_t4163692084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1794[4] = 
{
	XmlArrayAttribute_t4163692084::get_offset_of_elementName_0(),
	XmlArrayAttribute_t4163692084::get_offset_of_form_1(),
	XmlArrayAttribute_t4163692084::get_offset_of_isNullable_2(),
	XmlArrayAttribute_t4163692084::get_offset_of_ns_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (XmlArrayItemAttribute_t2615142271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1795[7] = 
{
	XmlArrayItemAttribute_t2615142271::get_offset_of_dataType_0(),
	XmlArrayItemAttribute_t2615142271::get_offset_of_elementName_1(),
	XmlArrayItemAttribute_t2615142271::get_offset_of_form_2(),
	XmlArrayItemAttribute_t2615142271::get_offset_of_ns_3(),
	XmlArrayItemAttribute_t2615142271::get_offset_of_isNullable_4(),
	XmlArrayItemAttribute_t2615142271::get_offset_of_nestingLevel_5(),
	XmlArrayItemAttribute_t2615142271::get_offset_of_type_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (XmlArrayItemAttributes_t1923979634), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (XmlAttributeAttribute_t850813783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1797[5] = 
{
	XmlAttributeAttribute_t850813783::get_offset_of_attributeName_0(),
	XmlAttributeAttribute_t850813783::get_offset_of_dataType_1(),
	XmlAttributeAttribute_t850813783::get_offset_of_type_2(),
	XmlAttributeAttribute_t850813783::get_offset_of_form_3(),
	XmlAttributeAttribute_t850813783::get_offset_of_ns_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (XmlAttributeEventArgs_t3859077982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1798[5] = 
{
	XmlAttributeEventArgs_t3859077982::get_offset_of_attr_1(),
	XmlAttributeEventArgs_t3859077982::get_offset_of_lineNumber_2(),
	XmlAttributeEventArgs_t3859077982::get_offset_of_linePosition_3(),
	XmlAttributeEventArgs_t3859077982::get_offset_of_obj_4(),
	XmlAttributeEventArgs_t3859077982::get_offset_of_expectedAttributes_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (XmlAttributeOverrides_t3994942922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1799[1] = 
{
	XmlAttributeOverrides_t3994942922::get_offset_of_overrides_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
