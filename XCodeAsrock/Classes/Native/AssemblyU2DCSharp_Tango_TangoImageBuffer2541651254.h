﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoImageForma3932021136.h"
#include "mscorlib_System_IntPtr2504060609.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoImageBuffer
struct  TangoImageBuffer_t2541651254 
{
public:
	// System.UInt32 Tango.TangoImageBuffer::width
	uint32_t ___width_0;
	// System.UInt32 Tango.TangoImageBuffer::height
	uint32_t ___height_1;
	// System.UInt32 Tango.TangoImageBuffer::stride
	uint32_t ___stride_2;
	// System.Double Tango.TangoImageBuffer::timestamp
	double ___timestamp_3;
	// System.Int64 Tango.TangoImageBuffer::frame_number
	int64_t ___frame_number_4;
	// Tango.TangoEnums/TangoImageFormatType Tango.TangoImageBuffer::format
	int32_t ___format_5;
	// System.IntPtr Tango.TangoImageBuffer::data
	IntPtr_t ___data_6;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(TangoImageBuffer_t2541651254, ___width_0)); }
	inline uint32_t get_width_0() const { return ___width_0; }
	inline uint32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(uint32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(TangoImageBuffer_t2541651254, ___height_1)); }
	inline uint32_t get_height_1() const { return ___height_1; }
	inline uint32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(uint32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_stride_2() { return static_cast<int32_t>(offsetof(TangoImageBuffer_t2541651254, ___stride_2)); }
	inline uint32_t get_stride_2() const { return ___stride_2; }
	inline uint32_t* get_address_of_stride_2() { return &___stride_2; }
	inline void set_stride_2(uint32_t value)
	{
		___stride_2 = value;
	}

	inline static int32_t get_offset_of_timestamp_3() { return static_cast<int32_t>(offsetof(TangoImageBuffer_t2541651254, ___timestamp_3)); }
	inline double get_timestamp_3() const { return ___timestamp_3; }
	inline double* get_address_of_timestamp_3() { return &___timestamp_3; }
	inline void set_timestamp_3(double value)
	{
		___timestamp_3 = value;
	}

	inline static int32_t get_offset_of_frame_number_4() { return static_cast<int32_t>(offsetof(TangoImageBuffer_t2541651254, ___frame_number_4)); }
	inline int64_t get_frame_number_4() const { return ___frame_number_4; }
	inline int64_t* get_address_of_frame_number_4() { return &___frame_number_4; }
	inline void set_frame_number_4(int64_t value)
	{
		___frame_number_4 = value;
	}

	inline static int32_t get_offset_of_format_5() { return static_cast<int32_t>(offsetof(TangoImageBuffer_t2541651254, ___format_5)); }
	inline int32_t get_format_5() const { return ___format_5; }
	inline int32_t* get_address_of_format_5() { return &___format_5; }
	inline void set_format_5(int32_t value)
	{
		___format_5 = value;
	}

	inline static int32_t get_offset_of_data_6() { return static_cast<int32_t>(offsetof(TangoImageBuffer_t2541651254, ___data_6)); }
	inline IntPtr_t get_data_6() const { return ___data_6; }
	inline IntPtr_t* get_address_of_data_6() { return &___data_6; }
	inline void set_data_6(IntPtr_t value)
	{
		___data_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
