﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// TangoPoseController
struct TangoPoseController_t4427816;
// TangoPointCloud
struct TangoPointCloud_t374155286;
// Tango.TangoApplication
struct TangoApplication_t278578959;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PointCloudGUIController
struct  PointCloudGUIController_t2325220810  : public MonoBehaviour_t1158329972
{
public:
	// TangoPoseController PointCloudGUIController::m_tangoPoseController
	TangoPoseController_t4427816 * ___m_tangoPoseController_28;
	// TangoPointCloud PointCloudGUIController::m_pointcloud
	TangoPointCloud_t374155286 * ___m_pointcloud_29;
	// System.String PointCloudGUIController::m_fpsText
	String_t* ___m_fpsText_31;
	// System.Int32 PointCloudGUIController::m_currentFPS
	int32_t ___m_currentFPS_32;
	// System.Int32 PointCloudGUIController::m_framesSinceUpdate
	int32_t ___m_framesSinceUpdate_33;
	// System.Single PointCloudGUIController::m_accumulation
	float ___m_accumulation_34;
	// System.Single PointCloudGUIController::m_currentTime
	float ___m_currentTime_35;
	// Tango.TangoApplication PointCloudGUIController::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_36;

public:
	inline static int32_t get_offset_of_m_tangoPoseController_28() { return static_cast<int32_t>(offsetof(PointCloudGUIController_t2325220810, ___m_tangoPoseController_28)); }
	inline TangoPoseController_t4427816 * get_m_tangoPoseController_28() const { return ___m_tangoPoseController_28; }
	inline TangoPoseController_t4427816 ** get_address_of_m_tangoPoseController_28() { return &___m_tangoPoseController_28; }
	inline void set_m_tangoPoseController_28(TangoPoseController_t4427816 * value)
	{
		___m_tangoPoseController_28 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoPoseController_28, value);
	}

	inline static int32_t get_offset_of_m_pointcloud_29() { return static_cast<int32_t>(offsetof(PointCloudGUIController_t2325220810, ___m_pointcloud_29)); }
	inline TangoPointCloud_t374155286 * get_m_pointcloud_29() const { return ___m_pointcloud_29; }
	inline TangoPointCloud_t374155286 ** get_address_of_m_pointcloud_29() { return &___m_pointcloud_29; }
	inline void set_m_pointcloud_29(TangoPointCloud_t374155286 * value)
	{
		___m_pointcloud_29 = value;
		Il2CppCodeGenWriteBarrier(&___m_pointcloud_29, value);
	}

	inline static int32_t get_offset_of_m_fpsText_31() { return static_cast<int32_t>(offsetof(PointCloudGUIController_t2325220810, ___m_fpsText_31)); }
	inline String_t* get_m_fpsText_31() const { return ___m_fpsText_31; }
	inline String_t** get_address_of_m_fpsText_31() { return &___m_fpsText_31; }
	inline void set_m_fpsText_31(String_t* value)
	{
		___m_fpsText_31 = value;
		Il2CppCodeGenWriteBarrier(&___m_fpsText_31, value);
	}

	inline static int32_t get_offset_of_m_currentFPS_32() { return static_cast<int32_t>(offsetof(PointCloudGUIController_t2325220810, ___m_currentFPS_32)); }
	inline int32_t get_m_currentFPS_32() const { return ___m_currentFPS_32; }
	inline int32_t* get_address_of_m_currentFPS_32() { return &___m_currentFPS_32; }
	inline void set_m_currentFPS_32(int32_t value)
	{
		___m_currentFPS_32 = value;
	}

	inline static int32_t get_offset_of_m_framesSinceUpdate_33() { return static_cast<int32_t>(offsetof(PointCloudGUIController_t2325220810, ___m_framesSinceUpdate_33)); }
	inline int32_t get_m_framesSinceUpdate_33() const { return ___m_framesSinceUpdate_33; }
	inline int32_t* get_address_of_m_framesSinceUpdate_33() { return &___m_framesSinceUpdate_33; }
	inline void set_m_framesSinceUpdate_33(int32_t value)
	{
		___m_framesSinceUpdate_33 = value;
	}

	inline static int32_t get_offset_of_m_accumulation_34() { return static_cast<int32_t>(offsetof(PointCloudGUIController_t2325220810, ___m_accumulation_34)); }
	inline float get_m_accumulation_34() const { return ___m_accumulation_34; }
	inline float* get_address_of_m_accumulation_34() { return &___m_accumulation_34; }
	inline void set_m_accumulation_34(float value)
	{
		___m_accumulation_34 = value;
	}

	inline static int32_t get_offset_of_m_currentTime_35() { return static_cast<int32_t>(offsetof(PointCloudGUIController_t2325220810, ___m_currentTime_35)); }
	inline float get_m_currentTime_35() const { return ___m_currentTime_35; }
	inline float* get_address_of_m_currentTime_35() { return &___m_currentTime_35; }
	inline void set_m_currentTime_35(float value)
	{
		___m_currentTime_35 = value;
	}

	inline static int32_t get_offset_of_m_tangoApplication_36() { return static_cast<int32_t>(offsetof(PointCloudGUIController_t2325220810, ___m_tangoApplication_36)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_36() const { return ___m_tangoApplication_36; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_36() { return &___m_tangoApplication_36; }
	inline void set_m_tangoApplication_36(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_36 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_36, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
