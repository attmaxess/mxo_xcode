﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Texture
struct Texture_t2243626319;
// TangoEnvironmentalLighting/SphericalHarmonicSample[]
struct SphericalHarmonicSampleU5BU5D_t113172983;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TangoEnvironmentalLighting
struct  TangoEnvironmentalLighting_t3401121675  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TangoEnvironmentalLighting::m_enableDebugUI
	bool ___m_enableDebugUI_2;
	// System.Boolean TangoEnvironmentalLighting::m_enableEnvironmentalLighting
	bool ___m_enableEnvironmentalLighting_3;
	// UnityEngine.Texture TangoEnvironmentalLighting::m_environmentMap
	Texture_t2243626319 * ___m_environmentMap_13;
	// TangoEnvironmentalLighting/SphericalHarmonicSample[] TangoEnvironmentalLighting::m_samples
	SphericalHarmonicSampleU5BU5D_t113172983* ___m_samples_14;
	// UnityEngine.Vector3[] TangoEnvironmentalLighting::m_coefficients
	Vector3U5BU5D_t1172311765* ___m_coefficients_15;

public:
	inline static int32_t get_offset_of_m_enableDebugUI_2() { return static_cast<int32_t>(offsetof(TangoEnvironmentalLighting_t3401121675, ___m_enableDebugUI_2)); }
	inline bool get_m_enableDebugUI_2() const { return ___m_enableDebugUI_2; }
	inline bool* get_address_of_m_enableDebugUI_2() { return &___m_enableDebugUI_2; }
	inline void set_m_enableDebugUI_2(bool value)
	{
		___m_enableDebugUI_2 = value;
	}

	inline static int32_t get_offset_of_m_enableEnvironmentalLighting_3() { return static_cast<int32_t>(offsetof(TangoEnvironmentalLighting_t3401121675, ___m_enableEnvironmentalLighting_3)); }
	inline bool get_m_enableEnvironmentalLighting_3() const { return ___m_enableEnvironmentalLighting_3; }
	inline bool* get_address_of_m_enableEnvironmentalLighting_3() { return &___m_enableEnvironmentalLighting_3; }
	inline void set_m_enableEnvironmentalLighting_3(bool value)
	{
		___m_enableEnvironmentalLighting_3 = value;
	}

	inline static int32_t get_offset_of_m_environmentMap_13() { return static_cast<int32_t>(offsetof(TangoEnvironmentalLighting_t3401121675, ___m_environmentMap_13)); }
	inline Texture_t2243626319 * get_m_environmentMap_13() const { return ___m_environmentMap_13; }
	inline Texture_t2243626319 ** get_address_of_m_environmentMap_13() { return &___m_environmentMap_13; }
	inline void set_m_environmentMap_13(Texture_t2243626319 * value)
	{
		___m_environmentMap_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_environmentMap_13, value);
	}

	inline static int32_t get_offset_of_m_samples_14() { return static_cast<int32_t>(offsetof(TangoEnvironmentalLighting_t3401121675, ___m_samples_14)); }
	inline SphericalHarmonicSampleU5BU5D_t113172983* get_m_samples_14() const { return ___m_samples_14; }
	inline SphericalHarmonicSampleU5BU5D_t113172983** get_address_of_m_samples_14() { return &___m_samples_14; }
	inline void set_m_samples_14(SphericalHarmonicSampleU5BU5D_t113172983* value)
	{
		___m_samples_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_samples_14, value);
	}

	inline static int32_t get_offset_of_m_coefficients_15() { return static_cast<int32_t>(offsetof(TangoEnvironmentalLighting_t3401121675, ___m_coefficients_15)); }
	inline Vector3U5BU5D_t1172311765* get_m_coefficients_15() const { return ___m_coefficients_15; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_coefficients_15() { return &___m_coefficients_15; }
	inline void set_m_coefficients_15(Vector3U5BU5D_t1172311765* value)
	{
		___m_coefficients_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_coefficients_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
