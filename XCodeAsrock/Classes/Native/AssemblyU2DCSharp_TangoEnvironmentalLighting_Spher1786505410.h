﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Single[]
struct SingleU5BU5D_t577127397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TangoEnvironmentalLighting/SphericalHarmonicSample
struct  SphericalHarmonicSample_t1786505410 
{
public:
	// UnityEngine.Vector2 TangoEnvironmentalLighting/SphericalHarmonicSample::sph
	Vector2_t2243707579  ___sph_0;
	// UnityEngine.Vector3 TangoEnvironmentalLighting/SphericalHarmonicSample::vec
	Vector3_t2243707580  ___vec_1;
	// System.Single[] TangoEnvironmentalLighting/SphericalHarmonicSample::coeff
	SingleU5BU5D_t577127397* ___coeff_2;

public:
	inline static int32_t get_offset_of_sph_0() { return static_cast<int32_t>(offsetof(SphericalHarmonicSample_t1786505410, ___sph_0)); }
	inline Vector2_t2243707579  get_sph_0() const { return ___sph_0; }
	inline Vector2_t2243707579 * get_address_of_sph_0() { return &___sph_0; }
	inline void set_sph_0(Vector2_t2243707579  value)
	{
		___sph_0 = value;
	}

	inline static int32_t get_offset_of_vec_1() { return static_cast<int32_t>(offsetof(SphericalHarmonicSample_t1786505410, ___vec_1)); }
	inline Vector3_t2243707580  get_vec_1() const { return ___vec_1; }
	inline Vector3_t2243707580 * get_address_of_vec_1() { return &___vec_1; }
	inline void set_vec_1(Vector3_t2243707580  value)
	{
		___vec_1 = value;
	}

	inline static int32_t get_offset_of_coeff_2() { return static_cast<int32_t>(offsetof(SphericalHarmonicSample_t1786505410, ___coeff_2)); }
	inline SingleU5BU5D_t577127397* get_coeff_2() const { return ___coeff_2; }
	inline SingleU5BU5D_t577127397** get_address_of_coeff_2() { return &___coeff_2; }
	inline void set_coeff_2(SingleU5BU5D_t577127397* value)
	{
		___coeff_2 = value;
		Il2CppCodeGenWriteBarrier(&___coeff_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TangoEnvironmentalLighting/SphericalHarmonicSample
struct SphericalHarmonicSample_t1786505410_marshaled_pinvoke
{
	Vector2_t2243707579  ___sph_0;
	Vector3_t2243707580  ___vec_1;
	float* ___coeff_2;
};
// Native definition for COM marshalling of TangoEnvironmentalLighting/SphericalHarmonicSample
struct SphericalHarmonicSample_t1786505410_marshaled_com
{
	Vector2_t2243707579  ___sph_0;
	Vector3_t2243707580  ___vec_1;
	float* ___coeff_2;
};
