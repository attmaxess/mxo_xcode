﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle3409268066.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.Tango3DReconstruction/APIMeshGCHandles
struct  APIMeshGCHandles_t2481716188 
{
public:
	// System.Runtime.InteropServices.GCHandle Tango.Tango3DReconstruction/APIMeshGCHandles::m_verticesGCHandle
	GCHandle_t3409268066  ___m_verticesGCHandle_0;
	// System.Runtime.InteropServices.GCHandle Tango.Tango3DReconstruction/APIMeshGCHandles::m_facesGCHandle
	GCHandle_t3409268066  ___m_facesGCHandle_1;
	// System.Runtime.InteropServices.GCHandle Tango.Tango3DReconstruction/APIMeshGCHandles::m_normalsGCHandle
	GCHandle_t3409268066  ___m_normalsGCHandle_2;
	// System.Runtime.InteropServices.GCHandle Tango.Tango3DReconstruction/APIMeshGCHandles::m_colorsGCHandle
	GCHandle_t3409268066  ___m_colorsGCHandle_3;

public:
	inline static int32_t get_offset_of_m_verticesGCHandle_0() { return static_cast<int32_t>(offsetof(APIMeshGCHandles_t2481716188, ___m_verticesGCHandle_0)); }
	inline GCHandle_t3409268066  get_m_verticesGCHandle_0() const { return ___m_verticesGCHandle_0; }
	inline GCHandle_t3409268066 * get_address_of_m_verticesGCHandle_0() { return &___m_verticesGCHandle_0; }
	inline void set_m_verticesGCHandle_0(GCHandle_t3409268066  value)
	{
		___m_verticesGCHandle_0 = value;
	}

	inline static int32_t get_offset_of_m_facesGCHandle_1() { return static_cast<int32_t>(offsetof(APIMeshGCHandles_t2481716188, ___m_facesGCHandle_1)); }
	inline GCHandle_t3409268066  get_m_facesGCHandle_1() const { return ___m_facesGCHandle_1; }
	inline GCHandle_t3409268066 * get_address_of_m_facesGCHandle_1() { return &___m_facesGCHandle_1; }
	inline void set_m_facesGCHandle_1(GCHandle_t3409268066  value)
	{
		___m_facesGCHandle_1 = value;
	}

	inline static int32_t get_offset_of_m_normalsGCHandle_2() { return static_cast<int32_t>(offsetof(APIMeshGCHandles_t2481716188, ___m_normalsGCHandle_2)); }
	inline GCHandle_t3409268066  get_m_normalsGCHandle_2() const { return ___m_normalsGCHandle_2; }
	inline GCHandle_t3409268066 * get_address_of_m_normalsGCHandle_2() { return &___m_normalsGCHandle_2; }
	inline void set_m_normalsGCHandle_2(GCHandle_t3409268066  value)
	{
		___m_normalsGCHandle_2 = value;
	}

	inline static int32_t get_offset_of_m_colorsGCHandle_3() { return static_cast<int32_t>(offsetof(APIMeshGCHandles_t2481716188, ___m_colorsGCHandle_3)); }
	inline GCHandle_t3409268066  get_m_colorsGCHandle_3() const { return ___m_colorsGCHandle_3; }
	inline GCHandle_t3409268066 * get_address_of_m_colorsGCHandle_3() { return &___m_colorsGCHandle_3; }
	inline void set_m_colorsGCHandle_3(GCHandle_t3409268066  value)
	{
		___m_colorsGCHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
