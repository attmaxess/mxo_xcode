﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// Tango.TangoApplication
struct TangoApplication_t278578959;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RelocalizingOverlay
struct  RelocalizingOverlay_t2716187845  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject RelocalizingOverlay::m_relocalizationOverlay
	GameObject_t1756533147 * ___m_relocalizationOverlay_2;
	// Tango.TangoApplication RelocalizingOverlay::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_3;

public:
	inline static int32_t get_offset_of_m_relocalizationOverlay_2() { return static_cast<int32_t>(offsetof(RelocalizingOverlay_t2716187845, ___m_relocalizationOverlay_2)); }
	inline GameObject_t1756533147 * get_m_relocalizationOverlay_2() const { return ___m_relocalizationOverlay_2; }
	inline GameObject_t1756533147 ** get_address_of_m_relocalizationOverlay_2() { return &___m_relocalizationOverlay_2; }
	inline void set_m_relocalizationOverlay_2(GameObject_t1756533147 * value)
	{
		___m_relocalizationOverlay_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_relocalizationOverlay_2, value);
	}

	inline static int32_t get_offset_of_m_tangoApplication_3() { return static_cast<int32_t>(offsetof(RelocalizingOverlay_t2716187845, ___m_tangoApplication_3)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_3() const { return ___m_tangoApplication_3; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_3() { return &___m_tangoApplication_3; }
	inline void set_m_tangoApplication_3(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
