﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// Tango.TangoApplication
struct TangoApplication_t278578959;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPSCounter
struct  FPSCounter_t921841495  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 FPSCounter::m_FPSLabelOffsetX
	int32_t ___m_FPSLabelOffsetX_2;
	// System.Int32 FPSCounter::m_FPSLabelOffsetY
	int32_t ___m_FPSLabelOffsetY_3;
	// System.Single FPSCounter::m_updateFrequency
	float ___m_updateFrequency_7;
	// System.String FPSCounter::m_fpsText
	String_t* ___m_fpsText_8;
	// System.Int32 FPSCounter::m_currentFPS
	int32_t ___m_currentFPS_9;
	// System.Int32 FPSCounter::m_framesSinceUpdate
	int32_t ___m_framesSinceUpdate_10;
	// System.Single FPSCounter::m_accumulation
	float ___m_accumulation_11;
	// System.Single FPSCounter::m_currentTime
	float ___m_currentTime_12;
	// Tango.TangoApplication FPSCounter::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_13;

public:
	inline static int32_t get_offset_of_m_FPSLabelOffsetX_2() { return static_cast<int32_t>(offsetof(FPSCounter_t921841495, ___m_FPSLabelOffsetX_2)); }
	inline int32_t get_m_FPSLabelOffsetX_2() const { return ___m_FPSLabelOffsetX_2; }
	inline int32_t* get_address_of_m_FPSLabelOffsetX_2() { return &___m_FPSLabelOffsetX_2; }
	inline void set_m_FPSLabelOffsetX_2(int32_t value)
	{
		___m_FPSLabelOffsetX_2 = value;
	}

	inline static int32_t get_offset_of_m_FPSLabelOffsetY_3() { return static_cast<int32_t>(offsetof(FPSCounter_t921841495, ___m_FPSLabelOffsetY_3)); }
	inline int32_t get_m_FPSLabelOffsetY_3() const { return ___m_FPSLabelOffsetY_3; }
	inline int32_t* get_address_of_m_FPSLabelOffsetY_3() { return &___m_FPSLabelOffsetY_3; }
	inline void set_m_FPSLabelOffsetY_3(int32_t value)
	{
		___m_FPSLabelOffsetY_3 = value;
	}

	inline static int32_t get_offset_of_m_updateFrequency_7() { return static_cast<int32_t>(offsetof(FPSCounter_t921841495, ___m_updateFrequency_7)); }
	inline float get_m_updateFrequency_7() const { return ___m_updateFrequency_7; }
	inline float* get_address_of_m_updateFrequency_7() { return &___m_updateFrequency_7; }
	inline void set_m_updateFrequency_7(float value)
	{
		___m_updateFrequency_7 = value;
	}

	inline static int32_t get_offset_of_m_fpsText_8() { return static_cast<int32_t>(offsetof(FPSCounter_t921841495, ___m_fpsText_8)); }
	inline String_t* get_m_fpsText_8() const { return ___m_fpsText_8; }
	inline String_t** get_address_of_m_fpsText_8() { return &___m_fpsText_8; }
	inline void set_m_fpsText_8(String_t* value)
	{
		___m_fpsText_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_fpsText_8, value);
	}

	inline static int32_t get_offset_of_m_currentFPS_9() { return static_cast<int32_t>(offsetof(FPSCounter_t921841495, ___m_currentFPS_9)); }
	inline int32_t get_m_currentFPS_9() const { return ___m_currentFPS_9; }
	inline int32_t* get_address_of_m_currentFPS_9() { return &___m_currentFPS_9; }
	inline void set_m_currentFPS_9(int32_t value)
	{
		___m_currentFPS_9 = value;
	}

	inline static int32_t get_offset_of_m_framesSinceUpdate_10() { return static_cast<int32_t>(offsetof(FPSCounter_t921841495, ___m_framesSinceUpdate_10)); }
	inline int32_t get_m_framesSinceUpdate_10() const { return ___m_framesSinceUpdate_10; }
	inline int32_t* get_address_of_m_framesSinceUpdate_10() { return &___m_framesSinceUpdate_10; }
	inline void set_m_framesSinceUpdate_10(int32_t value)
	{
		___m_framesSinceUpdate_10 = value;
	}

	inline static int32_t get_offset_of_m_accumulation_11() { return static_cast<int32_t>(offsetof(FPSCounter_t921841495, ___m_accumulation_11)); }
	inline float get_m_accumulation_11() const { return ___m_accumulation_11; }
	inline float* get_address_of_m_accumulation_11() { return &___m_accumulation_11; }
	inline void set_m_accumulation_11(float value)
	{
		___m_accumulation_11 = value;
	}

	inline static int32_t get_offset_of_m_currentTime_12() { return static_cast<int32_t>(offsetof(FPSCounter_t921841495, ___m_currentTime_12)); }
	inline float get_m_currentTime_12() const { return ___m_currentTime_12; }
	inline float* get_address_of_m_currentTime_12() { return &___m_currentTime_12; }
	inline void set_m_currentTime_12(float value)
	{
		___m_currentTime_12 = value;
	}

	inline static int32_t get_offset_of_m_tangoApplication_13() { return static_cast<int32_t>(offsetof(FPSCounter_t921841495, ___m_tangoApplication_13)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_13() const { return ___m_tangoApplication_13; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_13() { return &___m_tangoApplication_13; }
	inline void set_m_tangoApplication_13(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
