﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// AreaDescriptionListElement
struct AreaDescriptionListElement_t2319994151;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// TangoDeltaPoseController
struct TangoDeltaPoseController_t1881166960;
// Tango.TangoApplication
struct TangoApplication_t278578959;
// Tango.AreaDescription
struct AreaDescription_t1434786909;
// Tango.AreaDescription/Metadata
struct Metadata_t134815774;
// System.Threading.Thread
struct Thread_t241561612;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ADMGUIController
struct  ADMGUIController_t1723854221  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ADMGUIController::m_managementRoot
	GameObject_t1756533147 * ___m_managementRoot_2;
	// UnityEngine.GameObject ADMGUIController::m_qualityRoot
	GameObject_t1756533147 * ___m_qualityRoot_3;
	// UnityEngine.RectTransform ADMGUIController::m_listParent
	RectTransform_t3349966182 * ___m_listParent_4;
	// AreaDescriptionListElement ADMGUIController::m_listElement
	AreaDescriptionListElement_t2319994151 * ___m_listElement_5;
	// UnityEngine.RectTransform ADMGUIController::m_listEmptyText
	RectTransform_t3349966182 * ___m_listEmptyText_6;
	// UnityEngine.RectTransform ADMGUIController::m_detailsParent
	RectTransform_t3349966182 * ___m_detailsParent_7;
	// UnityEngine.UI.Text ADMGUIController::m_detailsDate
	Text_t356221433 * ___m_detailsDate_8;
	// UnityEngine.UI.InputField ADMGUIController::m_detailsEditableName
	InputField_t1631627530 * ___m_detailsEditableName_9;
	// UnityEngine.UI.InputField ADMGUIController::m_detailsEditablePosX
	InputField_t1631627530 * ___m_detailsEditablePosX_10;
	// UnityEngine.UI.InputField ADMGUIController::m_detailsEditablePosY
	InputField_t1631627530 * ___m_detailsEditablePosY_11;
	// UnityEngine.UI.InputField ADMGUIController::m_detailsEditablePosZ
	InputField_t1631627530 * ___m_detailsEditablePosZ_12;
	// UnityEngine.UI.InputField ADMGUIController::m_detailsEditableRotQX
	InputField_t1631627530 * ___m_detailsEditableRotQX_13;
	// UnityEngine.UI.InputField ADMGUIController::m_detailsEditableRotQY
	InputField_t1631627530 * ___m_detailsEditableRotQY_14;
	// UnityEngine.UI.InputField ADMGUIController::m_detailsEditableRotQZ
	InputField_t1631627530 * ___m_detailsEditableRotQZ_15;
	// UnityEngine.UI.InputField ADMGUIController::m_detailsEditableRotQW
	InputField_t1631627530 * ___m_detailsEditableRotQW_16;
	// TangoDeltaPoseController ADMGUIController::m_deltaPoseController
	TangoDeltaPoseController_t1881166960 * ___m_deltaPoseController_17;
	// UnityEngine.RectTransform ADMGUIController::m_savingTextParent
	RectTransform_t3349966182 * ___m_savingTextParent_18;
	// UnityEngine.UI.Text ADMGUIController::m_savingText
	Text_t356221433 * ___m_savingText_19;
	// Tango.TangoApplication ADMGUIController::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_20;
	// Tango.AreaDescription ADMGUIController::m_selectedAreaDescription
	AreaDescription_t1434786909 * ___m_selectedAreaDescription_21;
	// Tango.AreaDescription/Metadata ADMGUIController::m_selectedMetadata
	Metadata_t134815774 * ___m_selectedMetadata_22;
	// System.Threading.Thread ADMGUIController::m_saveThread
	Thread_t241561612 * ___m_saveThread_23;

public:
	inline static int32_t get_offset_of_m_managementRoot_2() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_managementRoot_2)); }
	inline GameObject_t1756533147 * get_m_managementRoot_2() const { return ___m_managementRoot_2; }
	inline GameObject_t1756533147 ** get_address_of_m_managementRoot_2() { return &___m_managementRoot_2; }
	inline void set_m_managementRoot_2(GameObject_t1756533147 * value)
	{
		___m_managementRoot_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_managementRoot_2, value);
	}

	inline static int32_t get_offset_of_m_qualityRoot_3() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_qualityRoot_3)); }
	inline GameObject_t1756533147 * get_m_qualityRoot_3() const { return ___m_qualityRoot_3; }
	inline GameObject_t1756533147 ** get_address_of_m_qualityRoot_3() { return &___m_qualityRoot_3; }
	inline void set_m_qualityRoot_3(GameObject_t1756533147 * value)
	{
		___m_qualityRoot_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_qualityRoot_3, value);
	}

	inline static int32_t get_offset_of_m_listParent_4() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_listParent_4)); }
	inline RectTransform_t3349966182 * get_m_listParent_4() const { return ___m_listParent_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_listParent_4() { return &___m_listParent_4; }
	inline void set_m_listParent_4(RectTransform_t3349966182 * value)
	{
		___m_listParent_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_listParent_4, value);
	}

	inline static int32_t get_offset_of_m_listElement_5() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_listElement_5)); }
	inline AreaDescriptionListElement_t2319994151 * get_m_listElement_5() const { return ___m_listElement_5; }
	inline AreaDescriptionListElement_t2319994151 ** get_address_of_m_listElement_5() { return &___m_listElement_5; }
	inline void set_m_listElement_5(AreaDescriptionListElement_t2319994151 * value)
	{
		___m_listElement_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_listElement_5, value);
	}

	inline static int32_t get_offset_of_m_listEmptyText_6() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_listEmptyText_6)); }
	inline RectTransform_t3349966182 * get_m_listEmptyText_6() const { return ___m_listEmptyText_6; }
	inline RectTransform_t3349966182 ** get_address_of_m_listEmptyText_6() { return &___m_listEmptyText_6; }
	inline void set_m_listEmptyText_6(RectTransform_t3349966182 * value)
	{
		___m_listEmptyText_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_listEmptyText_6, value);
	}

	inline static int32_t get_offset_of_m_detailsParent_7() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_detailsParent_7)); }
	inline RectTransform_t3349966182 * get_m_detailsParent_7() const { return ___m_detailsParent_7; }
	inline RectTransform_t3349966182 ** get_address_of_m_detailsParent_7() { return &___m_detailsParent_7; }
	inline void set_m_detailsParent_7(RectTransform_t3349966182 * value)
	{
		___m_detailsParent_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_detailsParent_7, value);
	}

	inline static int32_t get_offset_of_m_detailsDate_8() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_detailsDate_8)); }
	inline Text_t356221433 * get_m_detailsDate_8() const { return ___m_detailsDate_8; }
	inline Text_t356221433 ** get_address_of_m_detailsDate_8() { return &___m_detailsDate_8; }
	inline void set_m_detailsDate_8(Text_t356221433 * value)
	{
		___m_detailsDate_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_detailsDate_8, value);
	}

	inline static int32_t get_offset_of_m_detailsEditableName_9() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_detailsEditableName_9)); }
	inline InputField_t1631627530 * get_m_detailsEditableName_9() const { return ___m_detailsEditableName_9; }
	inline InputField_t1631627530 ** get_address_of_m_detailsEditableName_9() { return &___m_detailsEditableName_9; }
	inline void set_m_detailsEditableName_9(InputField_t1631627530 * value)
	{
		___m_detailsEditableName_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_detailsEditableName_9, value);
	}

	inline static int32_t get_offset_of_m_detailsEditablePosX_10() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_detailsEditablePosX_10)); }
	inline InputField_t1631627530 * get_m_detailsEditablePosX_10() const { return ___m_detailsEditablePosX_10; }
	inline InputField_t1631627530 ** get_address_of_m_detailsEditablePosX_10() { return &___m_detailsEditablePosX_10; }
	inline void set_m_detailsEditablePosX_10(InputField_t1631627530 * value)
	{
		___m_detailsEditablePosX_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_detailsEditablePosX_10, value);
	}

	inline static int32_t get_offset_of_m_detailsEditablePosY_11() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_detailsEditablePosY_11)); }
	inline InputField_t1631627530 * get_m_detailsEditablePosY_11() const { return ___m_detailsEditablePosY_11; }
	inline InputField_t1631627530 ** get_address_of_m_detailsEditablePosY_11() { return &___m_detailsEditablePosY_11; }
	inline void set_m_detailsEditablePosY_11(InputField_t1631627530 * value)
	{
		___m_detailsEditablePosY_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_detailsEditablePosY_11, value);
	}

	inline static int32_t get_offset_of_m_detailsEditablePosZ_12() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_detailsEditablePosZ_12)); }
	inline InputField_t1631627530 * get_m_detailsEditablePosZ_12() const { return ___m_detailsEditablePosZ_12; }
	inline InputField_t1631627530 ** get_address_of_m_detailsEditablePosZ_12() { return &___m_detailsEditablePosZ_12; }
	inline void set_m_detailsEditablePosZ_12(InputField_t1631627530 * value)
	{
		___m_detailsEditablePosZ_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_detailsEditablePosZ_12, value);
	}

	inline static int32_t get_offset_of_m_detailsEditableRotQX_13() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_detailsEditableRotQX_13)); }
	inline InputField_t1631627530 * get_m_detailsEditableRotQX_13() const { return ___m_detailsEditableRotQX_13; }
	inline InputField_t1631627530 ** get_address_of_m_detailsEditableRotQX_13() { return &___m_detailsEditableRotQX_13; }
	inline void set_m_detailsEditableRotQX_13(InputField_t1631627530 * value)
	{
		___m_detailsEditableRotQX_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_detailsEditableRotQX_13, value);
	}

	inline static int32_t get_offset_of_m_detailsEditableRotQY_14() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_detailsEditableRotQY_14)); }
	inline InputField_t1631627530 * get_m_detailsEditableRotQY_14() const { return ___m_detailsEditableRotQY_14; }
	inline InputField_t1631627530 ** get_address_of_m_detailsEditableRotQY_14() { return &___m_detailsEditableRotQY_14; }
	inline void set_m_detailsEditableRotQY_14(InputField_t1631627530 * value)
	{
		___m_detailsEditableRotQY_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_detailsEditableRotQY_14, value);
	}

	inline static int32_t get_offset_of_m_detailsEditableRotQZ_15() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_detailsEditableRotQZ_15)); }
	inline InputField_t1631627530 * get_m_detailsEditableRotQZ_15() const { return ___m_detailsEditableRotQZ_15; }
	inline InputField_t1631627530 ** get_address_of_m_detailsEditableRotQZ_15() { return &___m_detailsEditableRotQZ_15; }
	inline void set_m_detailsEditableRotQZ_15(InputField_t1631627530 * value)
	{
		___m_detailsEditableRotQZ_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_detailsEditableRotQZ_15, value);
	}

	inline static int32_t get_offset_of_m_detailsEditableRotQW_16() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_detailsEditableRotQW_16)); }
	inline InputField_t1631627530 * get_m_detailsEditableRotQW_16() const { return ___m_detailsEditableRotQW_16; }
	inline InputField_t1631627530 ** get_address_of_m_detailsEditableRotQW_16() { return &___m_detailsEditableRotQW_16; }
	inline void set_m_detailsEditableRotQW_16(InputField_t1631627530 * value)
	{
		___m_detailsEditableRotQW_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_detailsEditableRotQW_16, value);
	}

	inline static int32_t get_offset_of_m_deltaPoseController_17() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_deltaPoseController_17)); }
	inline TangoDeltaPoseController_t1881166960 * get_m_deltaPoseController_17() const { return ___m_deltaPoseController_17; }
	inline TangoDeltaPoseController_t1881166960 ** get_address_of_m_deltaPoseController_17() { return &___m_deltaPoseController_17; }
	inline void set_m_deltaPoseController_17(TangoDeltaPoseController_t1881166960 * value)
	{
		___m_deltaPoseController_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_deltaPoseController_17, value);
	}

	inline static int32_t get_offset_of_m_savingTextParent_18() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_savingTextParent_18)); }
	inline RectTransform_t3349966182 * get_m_savingTextParent_18() const { return ___m_savingTextParent_18; }
	inline RectTransform_t3349966182 ** get_address_of_m_savingTextParent_18() { return &___m_savingTextParent_18; }
	inline void set_m_savingTextParent_18(RectTransform_t3349966182 * value)
	{
		___m_savingTextParent_18 = value;
		Il2CppCodeGenWriteBarrier(&___m_savingTextParent_18, value);
	}

	inline static int32_t get_offset_of_m_savingText_19() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_savingText_19)); }
	inline Text_t356221433 * get_m_savingText_19() const { return ___m_savingText_19; }
	inline Text_t356221433 ** get_address_of_m_savingText_19() { return &___m_savingText_19; }
	inline void set_m_savingText_19(Text_t356221433 * value)
	{
		___m_savingText_19 = value;
		Il2CppCodeGenWriteBarrier(&___m_savingText_19, value);
	}

	inline static int32_t get_offset_of_m_tangoApplication_20() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_tangoApplication_20)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_20() const { return ___m_tangoApplication_20; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_20() { return &___m_tangoApplication_20; }
	inline void set_m_tangoApplication_20(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_20, value);
	}

	inline static int32_t get_offset_of_m_selectedAreaDescription_21() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_selectedAreaDescription_21)); }
	inline AreaDescription_t1434786909 * get_m_selectedAreaDescription_21() const { return ___m_selectedAreaDescription_21; }
	inline AreaDescription_t1434786909 ** get_address_of_m_selectedAreaDescription_21() { return &___m_selectedAreaDescription_21; }
	inline void set_m_selectedAreaDescription_21(AreaDescription_t1434786909 * value)
	{
		___m_selectedAreaDescription_21 = value;
		Il2CppCodeGenWriteBarrier(&___m_selectedAreaDescription_21, value);
	}

	inline static int32_t get_offset_of_m_selectedMetadata_22() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_selectedMetadata_22)); }
	inline Metadata_t134815774 * get_m_selectedMetadata_22() const { return ___m_selectedMetadata_22; }
	inline Metadata_t134815774 ** get_address_of_m_selectedMetadata_22() { return &___m_selectedMetadata_22; }
	inline void set_m_selectedMetadata_22(Metadata_t134815774 * value)
	{
		___m_selectedMetadata_22 = value;
		Il2CppCodeGenWriteBarrier(&___m_selectedMetadata_22, value);
	}

	inline static int32_t get_offset_of_m_saveThread_23() { return static_cast<int32_t>(offsetof(ADMGUIController_t1723854221, ___m_saveThread_23)); }
	inline Thread_t241561612 * get_m_saveThread_23() const { return ___m_saveThread_23; }
	inline Thread_t241561612 ** get_address_of_m_saveThread_23() { return &___m_saveThread_23; }
	inline void set_m_saveThread_23(Thread_t241561612 * value)
	{
		___m_saveThread_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_saveThread_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
