﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

// System.String
struct String_t;
// System.Double[]
struct DoubleU5BU5D_t1889952540;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.AreaDescription/Metadata
struct  Metadata_t134815774  : public Il2CppObject
{
public:
	// System.String Tango.AreaDescription/Metadata::m_name
	String_t* ___m_name_0;
	// System.DateTime Tango.AreaDescription/Metadata::m_dateTime
	DateTime_t693205669  ___m_dateTime_1;
	// System.Double[] Tango.AreaDescription/Metadata::m_transformationPosition
	DoubleU5BU5D_t1889952540* ___m_transformationPosition_2;
	// System.Double[] Tango.AreaDescription/Metadata::m_transformationRotation
	DoubleU5BU5D_t1889952540* ___m_transformationRotation_3;

public:
	inline static int32_t get_offset_of_m_name_0() { return static_cast<int32_t>(offsetof(Metadata_t134815774, ___m_name_0)); }
	inline String_t* get_m_name_0() const { return ___m_name_0; }
	inline String_t** get_address_of_m_name_0() { return &___m_name_0; }
	inline void set_m_name_0(String_t* value)
	{
		___m_name_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_name_0, value);
	}

	inline static int32_t get_offset_of_m_dateTime_1() { return static_cast<int32_t>(offsetof(Metadata_t134815774, ___m_dateTime_1)); }
	inline DateTime_t693205669  get_m_dateTime_1() const { return ___m_dateTime_1; }
	inline DateTime_t693205669 * get_address_of_m_dateTime_1() { return &___m_dateTime_1; }
	inline void set_m_dateTime_1(DateTime_t693205669  value)
	{
		___m_dateTime_1 = value;
	}

	inline static int32_t get_offset_of_m_transformationPosition_2() { return static_cast<int32_t>(offsetof(Metadata_t134815774, ___m_transformationPosition_2)); }
	inline DoubleU5BU5D_t1889952540* get_m_transformationPosition_2() const { return ___m_transformationPosition_2; }
	inline DoubleU5BU5D_t1889952540** get_address_of_m_transformationPosition_2() { return &___m_transformationPosition_2; }
	inline void set_m_transformationPosition_2(DoubleU5BU5D_t1889952540* value)
	{
		___m_transformationPosition_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_transformationPosition_2, value);
	}

	inline static int32_t get_offset_of_m_transformationRotation_3() { return static_cast<int32_t>(offsetof(Metadata_t134815774, ___m_transformationRotation_3)); }
	inline DoubleU5BU5D_t1889952540* get_m_transformationRotation_3() const { return ___m_transformationRotation_3; }
	inline DoubleU5BU5D_t1889952540** get_address_of_m_transformationRotation_3() { return &___m_transformationRotation_3; }
	inline void set_m_transformationRotation_3(DoubleU5BU5D_t1889952540* value)
	{
		___m_transformationRotation_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_transformationRotation_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
