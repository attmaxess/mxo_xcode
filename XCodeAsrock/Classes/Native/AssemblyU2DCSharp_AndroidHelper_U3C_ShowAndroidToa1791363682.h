﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t2973420583;
// AndroidHelper/<_ShowAndroidToastMessage>c__AnonStorey1
struct U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t225279741;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidHelper/<_ShowAndroidToastMessage>c__AnonStorey0
struct  U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t1791363682  : public Il2CppObject
{
public:
	// UnityEngine.AndroidJavaClass AndroidHelper/<_ShowAndroidToastMessage>c__AnonStorey0::toastClass
	AndroidJavaClass_t2973420583 * ___toastClass_0;
	// AndroidHelper/<_ShowAndroidToastMessage>c__AnonStorey1 AndroidHelper/<_ShowAndroidToastMessage>c__AnonStorey0::<>f__ref$1
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t225279741 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_toastClass_0() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t1791363682, ___toastClass_0)); }
	inline AndroidJavaClass_t2973420583 * get_toastClass_0() const { return ___toastClass_0; }
	inline AndroidJavaClass_t2973420583 ** get_address_of_toastClass_0() { return &___toastClass_0; }
	inline void set_toastClass_0(AndroidJavaClass_t2973420583 * value)
	{
		___toastClass_0 = value;
		Il2CppCodeGenWriteBarrier(&___toastClass_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t1791363682, ___U3CU3Ef__refU241_1)); }
	inline U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t225279741 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t225279741 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t225279741 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
