﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U2646854145.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U3886071158.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_Un142665927.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U4216008690.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U1104644293.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U1656212632.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_Un872580813.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U3370800699.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U1333006279.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U3296518558.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U1622117597.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U3705772742.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U3189755211.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U3999066834.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U1661963345.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U4166385952.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U1558153491.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PointCloudParticleExa986756623.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityARCameraManager2138856896.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U3368998101.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_Un146867607.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U1698990409.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U2351297253.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityPointCloudExamp3196264220.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U1086564192.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U4039665643.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_XR_iOS_U3608388148.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1486305137.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementat762068664.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_AndroidHelper1537643947.h"
#include "AssemblyU2DCSharp_AndroidHelper_ToastLength3780156981.h"
#include "AssemblyU2DCSharp_AndroidHelper_OnTangoServiceConn4169241012.h"
#include "AssemblyU2DCSharp_AndroidHelper_OnTangoServiceDisc1901329298.h"
#include "AssemblyU2DCSharp_AndroidHelper_TangoServiceLifecyc431297296.h"
#include "AssemblyU2DCSharp_AndroidHelper_U3C_ShowAndroidToas225279741.h"
#include "AssemblyU2DCSharp_AndroidHelper_U3C_ShowAndroidToa1791363682.h"
#include "AssemblyU2DCSharp_AndroidHelperWrapper864207182.h"
#include "AssemblyU2DCSharp_OnStartEventHandler4250010451.h"
#include "AssemblyU2DCSharp_OnStopEventHandler1144032567.h"
#include "AssemblyU2DCSharp_OnPauseEventHandler2696015029.h"
#include "AssemblyU2DCSharp_OnResumeEventHandler3883967602.h"
#include "AssemblyU2DCSharp_OnActivityResultEventHandler2943688375.h"
#include "AssemblyU2DCSharp_OnRequestPermissionsResultHandler856552163.h"
#include "AssemblyU2DCSharp_OnDisplayChangedEventHandler4254774819.h"
#include "AssemblyU2DCSharp_AndroidScreenRotation3969760129.h"
#include "AssemblyU2DCSharp_AndroidPermissionGrantResult3892987557.h"
#include "AssemblyU2DCSharp_AndroidLifecycleCallbacks1531251569.h"
#include "AssemblyU2DCSharp_Cube2937860983.h"
#include "AssemblyU2DCSharp_Managers3758969020.h"
#include "AssemblyU2DCSharp_OnFoundPlaneVector1442898976.h"
#include "AssemblyU2DCSharp_OnFoundPlane92057343.h"
#include "AssemblyU2DCSharp_QRCodeReader3711862773.h"
#include "AssemblyU2DCSharp_QRCodeReaderTango2920665506.h"
#include "AssemblyU2DCSharp_SharingBroadcast3699410277.h"
#include "AssemblyU2DCSharp_SharingReceive1015268309.h"
#include "AssemblyU2DCSharp_TestCube1935904321.h"
#include "AssemblyU2DCSharp_TangoARPoseController3131628351.h"
#include "AssemblyU2DCSharp_TangoDeltaPoseController1881166960.h"
#include "AssemblyU2DCSharp_ARCameraPostProcess3075820819.h"
#include "AssemblyU2DCSharp_TangoARScreen2366305298.h"
#include "AssemblyU2DCSharp_TangoEnvironmentalLighting3401121675.h"
#include "AssemblyU2DCSharp_TangoEnvironmentalLighting_API4256039756.h"
#include "AssemblyU2DCSharp_TangoEnvironmentalLighting_Spher1786505410.h"
#include "AssemblyU2DCSharp_TangoGestureCamera2762882179.h"
#include "AssemblyU2DCSharp_TangoGestureCamera_CameraType4020249893.h"
#include "AssemblyU2DCSharp_TangoMultiCamera3288171709.h"
#include "AssemblyU2DCSharp_TangoMultiCamera_CameraType2660248723.h"
#include "AssemblyU2DCSharp_TangoPointCloud374155286.h"
#include "AssemblyU2DCSharp_TangoPointCloudFloor3393423680.h"
#include "AssemblyU2DCSharp_TangoPoseController4427816.h"
#include "AssemblyU2DCSharp_TangoPoseController_BaseFrameSele171456315.h"
#include "AssemblyU2DCSharp_Tango_Common442314523.h"
#include "AssemblyU2DCSharp_Tango_Common_AndroidResult1670778695.h"
#include "AssemblyU2DCSharp_Tango_Common_ErrorType477358569.h"
#include "AssemblyU2DCSharp_Tango_Common_MetaDataKeyType4202728003.h"
#include "AssemblyU2DCSharp_Tango_DMatrix4x443123165.h"
#include "AssemblyU2DCSharp_Tango_DVector33583584176.h"
#include "AssemblyU2DCSharp_Tango_DVector42017500235.h"
#include "AssemblyU2DCSharp_Tango_MarshallingHelper1923493080.h"
#include "AssemblyU2DCSharp_Tango_OrientationManager1904847119.h"
#include "AssemblyU2DCSharp_Tango_OrientationManager_Rotatio1451031165.h"
#include "AssemblyU2DCSharp_Tango_PublicForTesting3773363386.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums690178291.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoPoseStatus1112875089.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoCoordinate3800473787.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoCameraId106182192.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoEventType2750756310.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoConfigType2597920516.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoCalibratio1064312954.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoImageForma3932021136.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoDepthCamer3368133418.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoPoseType3471088457.h"
#include "AssemblyU2DCSharp_TangoExtensions_TangoExtensions457904463.h"
#include "AssemblyU2DCSharp_Tango_TangoCoordinateFramePair3148559008.h"
#include "AssemblyU2DCSharp_Tango_TangoPointCloudIntPtr1795497939.h"
#include "AssemblyU2DCSharp_Tango_TangoImage3319528210.h"
#include "AssemblyU2DCSharp_Tango_TangoCameraMetadata1412457087.h"
#include "AssemblyU2DCSharp_Tango_TangoImageBuffer2541651254.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (ARAnchorAdded_t2646854145), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (ARAnchorUpdated_t3886071158), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (ARAnchorRemoved_t142665927), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (ARUserAnchorAdded_t4216008690), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (ARUserAnchorUpdated_t1104644293), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (ARUserAnchorRemoved_t1656212632), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (ARSessionFailed_t872580813), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (ARSessionCallback_t3370800699), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (ARSessionTrackingChanged_t1333006279), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (internal_ARFrameUpdate_t3296518558), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (internal_ARAnchorAdded_t1622117597), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (internal_ARAnchorUpdated_t3705772742), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (internal_ARAnchorRemoved_t3189755211), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (internal_ARUserAnchorAdded_t3999066834), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (internal_ARUserAnchorUpdated_t1661963345), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (internal_ARUserAnchorRemoved_t4166385952), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (internal_ARSessionTrackingChanged_t1558153491), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (PointCloudParticleExample_t986756623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[7] = 
{
	PointCloudParticleExample_t986756623::get_offset_of_pointCloudParticlePrefab_2(),
	PointCloudParticleExample_t986756623::get_offset_of_maxPointsToShow_3(),
	PointCloudParticleExample_t986756623::get_offset_of_particleSize_4(),
	PointCloudParticleExample_t986756623::get_offset_of_m_PointCloudData_5(),
	PointCloudParticleExample_t986756623::get_offset_of_frameUpdated_6(),
	PointCloudParticleExample_t986756623::get_offset_of_currentPS_7(),
	PointCloudParticleExample_t986756623::get_offset_of_particles_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (UnityARCameraManager_t2138856896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[7] = 
{
	UnityARCameraManager_t2138856896::get_offset_of_m_camera_2(),
	UnityARCameraManager_t2138856896::get_offset_of_m_session_3(),
	UnityARCameraManager_t2138856896::get_offset_of_savedClearMaterial_4(),
	UnityARCameraManager_t2138856896::get_offset_of_startAlignment_5(),
	UnityARCameraManager_t2138856896::get_offset_of_planeDetection_6(),
	UnityARCameraManager_t2138856896::get_offset_of_getPointCloud_7(),
	UnityARCameraManager_t2138856896::get_offset_of_enableLightEstimation_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (UnityARGeneratePlane_t3368998101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[2] = 
{
	UnityARGeneratePlane_t3368998101::get_offset_of_planePrefab_2(),
	UnityARGeneratePlane_t3368998101::get_offset_of_unityARAnchorManager_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (UnityARHitTestExample_t146867607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[1] = 
{
	UnityARHitTestExample_t146867607::get_offset_of_m_HitTransform_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (UnityARKitControl_t1698990409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2221[6] = 
{
	UnityARKitControl_t1698990409::get_offset_of_runOptions_2(),
	UnityARKitControl_t1698990409::get_offset_of_alignmentOptions_3(),
	UnityARKitControl_t1698990409::get_offset_of_planeOptions_4(),
	UnityARKitControl_t1698990409::get_offset_of_currentOptionIndex_5(),
	UnityARKitControl_t1698990409::get_offset_of_currentAlignmentIndex_6(),
	UnityARKitControl_t1698990409::get_offset_of_currentPlaneIndex_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (UnityARVideo_t2351297253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2222[6] = 
{
	UnityARVideo_t2351297253::get_offset_of_m_ClearMaterial_2(),
	UnityARVideo_t2351297253::get_offset_of_m_VideoCommandBuffer_3(),
	UnityARVideo_t2351297253::get_offset_of__videoTextureY_4(),
	UnityARVideo_t2351297253::get_offset_of__videoTextureCbCr_5(),
	UnityARVideo_t2351297253::get_offset_of__displayTransform_6(),
	UnityARVideo_t2351297253::get_offset_of_bCommandBufferInitialized_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (UnityPointCloudExample_t3196264220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[4] = 
{
	UnityPointCloudExample_t3196264220::get_offset_of_numPointsToShow_2(),
	UnityPointCloudExample_t3196264220::get_offset_of_PointCloudPrefab_3(),
	UnityPointCloudExample_t3196264220::get_offset_of_pointCloudObjects_4(),
	UnityPointCloudExample_t3196264220::get_offset_of_m_PointCloudData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (UnityARAnchorManager_t1086564192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[1] = 
{
	UnityARAnchorManager_t1086564192::get_offset_of_planeAnchorMap_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (UnityARMatrixOps_t4039665643), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (UnityARUtility_t3608388148), -1, sizeof(UnityARUtility_t3608388148_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2226[3] = 
{
	UnityARUtility_t3608388148::get_offset_of_meshCollider_0(),
	UnityARUtility_t3608388148::get_offset_of_meshFilter_1(),
	UnityARUtility_t3608388148_StaticFields::get_offset_of_planePrefab_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2227[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (U24ArrayTypeU3D24_t762068664)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D24_t762068664 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (AndroidHelper_t1537643947), -1, sizeof(AndroidHelper_t1537643947_StaticFields), sizeof(AndroidHelper_t1537643947_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable2231[10] = 
{
	AndroidHelper_t1537643947_StaticFields::get_offset_of_m_unityActivity_2(),
	AndroidHelper_t1537643947_StaticFields::get_offset_of_m_callbacks_3(),
	0,
	AndroidHelper_t1537643947_StaticFields::get_offset_of_m_tangoServiceLifecycle_5(),
	AndroidHelper_t1537643947_StaticFields::get_offset_of_m_tangoUxHelper_6(),
	AndroidHelper_t1537643947_StaticFields::get_offset_of_m_tangoUxHelper_class_7(),
	AndroidHelper_t1537643947_StaticFields::get_offset_of_m_tangoUxHelper_obj_8(),
	AndroidHelper_t1537643947_StaticFields::get_offset_of_m_tangoUxHelper_processPoseDataStatus_9(),
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (ToastLength_t3780156981)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2232[3] = 
{
	ToastLength_t3780156981::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (OnTangoServiceConnected_t4169241012), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (OnTangoServiceDisconnected_t1901329298), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (TangoServiceLifecycleListener_t431297296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[2] = 
{
	TangoServiceLifecycleListener_t431297296::get_offset_of_m_onTangoServiceConnected_1(),
	TangoServiceLifecycleListener_t431297296::get_offset_of_m_onTangoServiceDisconnected_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t225279741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[3] = 
{
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t225279741::get_offset_of_unityActivity_0(),
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t225279741::get_offset_of_message_1(),
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t225279741::get_offset_of_length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t1791363682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2237[2] = 
{
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t1791363682::get_offset_of_toastClass_0(),
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t1791363682::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (AndroidHelperWrapper_t864207182), -1, sizeof(AndroidHelperWrapper_t864207182_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2238[1] = 
{
	AndroidHelperWrapper_t864207182_StaticFields::get_offset_of_m_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (OnStartEventHandler_t4250010451), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (OnStopEventHandler_t1144032567), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (OnPauseEventHandler_t2696015029), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (OnResumeEventHandler_t3883967602), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (OnActivityResultEventHandler_t2943688375), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (OnRequestPermissionsResultHandler_t856552163), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (OnDisplayChangedEventHandler_t4254774819), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (AndroidScreenRotation_t3969760129)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2246[5] = 
{
	AndroidScreenRotation_t3969760129::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (AndroidPermissionGrantResult_t3892987557)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2247[3] = 
{
	AndroidPermissionGrantResult_t3892987557::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (AndroidLifecycleCallbacks_t1531251569), -1, sizeof(AndroidLifecycleCallbacks_t1531251569_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2248[7] = 
{
	AndroidLifecycleCallbacks_t1531251569_StaticFields::get_offset_of_m_onStartEvent_1(),
	AndroidLifecycleCallbacks_t1531251569_StaticFields::get_offset_of_m_onStopEvent_2(),
	AndroidLifecycleCallbacks_t1531251569_StaticFields::get_offset_of_m_onPauseEvent_3(),
	AndroidLifecycleCallbacks_t1531251569_StaticFields::get_offset_of_m_onResumeEvent_4(),
	AndroidLifecycleCallbacks_t1531251569_StaticFields::get_offset_of_m_onActivityResultEvent_5(),
	AndroidLifecycleCallbacks_t1531251569_StaticFields::get_offset_of_m_onDisplayChangedEvent_6(),
	AndroidLifecycleCallbacks_t1531251569_StaticFields::get_offset_of_m_onRequestPermissionsResultEvent_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (Cube_t2937860983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[1] = 
{
	Cube_t2937860983::get_offset_of_qrcodePlane_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (Managers_t3758969020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2250[7] = 
{
	Managers_t3758969020::get_offset_of_QRCodeReaderObj_2(),
	Managers_t3758969020::get_offset_of_SpawPointObj_3(),
	Managers_t3758969020::get_offset_of_ButtonScan_4(),
	Managers_t3758969020::get_offset_of_ButtonTestPosition_5(),
	Managers_t3758969020::get_offset_of_Cube_6(),
	Managers_t3758969020::get_offset_of_isReady_7(),
	Managers_t3758969020::get_offset_of_isScan_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (OnFoundPlaneVector_t1442898976), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (OnFoundPlane_t92057343), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (QRCodeReader_t3711862773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[6] = 
{
	QRCodeReader_t3711862773::get_offset_of_CallBackOnFoundPlaneVector_2(),
	QRCodeReader_t3711862773::get_offset_of_CallBackOnFoundPlane_3(),
	QRCodeReader_t3711862773::get_offset_of_doneScanQRCode_4(),
	QRCodeReader_t3711862773::get_offset_of_arSession_5(),
	QRCodeReader_t3711862773::get_offset_of_qrcodePlane_6(),
	QRCodeReader_t3711862773::get_offset_of_plane_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (QRCodeReaderTango_t2920665506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[6] = 
{
	0,
	QRCodeReaderTango_t2920665506::get_offset_of_tangoApp_3(),
	QRCodeReaderTango_t2920665506::get_offset_of_done_4(),
	QRCodeReaderTango_t2920665506::get_offset_of_qrcodePlane_5(),
	QRCodeReaderTango_t2920665506::get_offset_of_plane_6(),
	QRCodeReaderTango_t2920665506::get_offset_of_markerList_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (SharingBroadcast_t3699410277), -1, sizeof(SharingBroadcast_t3699410277_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2255[3] = 
{
	SharingBroadcast_t3699410277::get_offset_of_qrcodePlane_2(),
	SharingBroadcast_t3699410277::get_offset_of_udpBroadcast_3(),
	SharingBroadcast_t3699410277_StaticFields::get_offset_of_udpEndPoint_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (SharingReceive_t1015268309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[4] = 
{
	SharingReceive_t1015268309::get_offset_of_qrcodePlane_2(),
	SharingReceive_t1015268309::get_offset_of_avatarPrefab_3(),
	SharingReceive_t1015268309::get_offset_of_devices_4(),
	SharingReceive_t1015268309::get_offset_of_avatars_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (TestCube_t1935904321), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (TangoARPoseController_t3131628351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2258[10] = 
{
	TangoARPoseController_t3131628351::get_offset_of_m_useAreaDescriptionPose_2(),
	TangoARPoseController_t3131628351::get_offset_of_m_syncToARScreen_3(),
	TangoARPoseController_t3131628351::get_offset_of_m_poseCount_4(),
	TangoARPoseController_t3131628351::get_offset_of_m_poseStatus_5(),
	TangoARPoseController_t3131628351::get_offset_of_m_poseTimestamp_6(),
	TangoARPoseController_t3131628351::get_offset_of_m_tangoPosition_7(),
	TangoARPoseController_t3131628351::get_offset_of_m_tangoRotation_8(),
	TangoARPoseController_t3131628351::get_offset_of_m_dTuc_9(),
	TangoARPoseController_t3131628351::get_offset_of_m_tangoARScreen_10(),
	TangoARPoseController_t3131628351::get_offset_of_m_tangoApplication_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (TangoDeltaPoseController_t1881166960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[16] = 
{
	TangoDeltaPoseController_t1881166960::get_offset_of_m_poseDeltaTime_2(),
	TangoDeltaPoseController_t1881166960::get_offset_of_m_poseCount_3(),
	TangoDeltaPoseController_t1881166960::get_offset_of_m_poseStatus_4(),
	TangoDeltaPoseController_t1881166960::get_offset_of_m_poseTimestamp_5(),
	TangoDeltaPoseController_t1881166960::get_offset_of_m_tangoPosition_6(),
	TangoDeltaPoseController_t1881166960::get_offset_of_m_tangoRotation_7(),
	TangoDeltaPoseController_t1881166960::get_offset_of_m_characterMotion_8(),
	TangoDeltaPoseController_t1881166960::get_offset_of_m_enableClutchUI_9(),
	TangoDeltaPoseController_t1881166960::get_offset_of_m_useAreaDescriptionPose_10(),
	TangoDeltaPoseController_t1881166960::get_offset_of_m_tangoApplication_11(),
	TangoDeltaPoseController_t1881166960::get_offset_of_m_prevTangoPosition_12(),
	TangoDeltaPoseController_t1881166960::get_offset_of_m_prevTangoRotation_13(),
	TangoDeltaPoseController_t1881166960::get_offset_of_m_clutchActive_14(),
	TangoDeltaPoseController_t1881166960::get_offset_of_m_characterController_15(),
	TangoDeltaPoseController_t1881166960::get_offset_of_m_uwTuc_16(),
	TangoDeltaPoseController_t1881166960::get_offset_of_m_uwOffsetTuw_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (ARCameraPostProcess_t3075820819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[1] = 
{
	ARCameraPostProcess_t3075820819::get_offset_of_m_postProcessMaterial_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (TangoARScreen_t2366305298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2261[9] = 
{
	TangoARScreen_t2366305298::get_offset_of_m_enableOcclusion_2(),
	TangoARScreen_t2366305298::get_offset_of_m_occlusionShader_3(),
	TangoARScreen_t2366305298::get_offset_of_m_screenUpdateTime_4(),
	TangoARScreen_t2366305298::get_offset_of_m_tangoApplication_5(),
	TangoARScreen_t2366305298::get_offset_of_m_arCameraPostProcess_6(),
	TangoARScreen_t2366305298::get_offset_of_m_camera_7(),
	TangoARScreen_t2366305298::get_offset_of_m_uOffset_8(),
	TangoARScreen_t2366305298::get_offset_of_m_vOffset_9(),
	TangoARScreen_t2366305298::get_offset_of_U3CIsRenderingU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (TangoEnvironmentalLighting_t3401121675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2262[14] = 
{
	TangoEnvironmentalLighting_t3401121675::get_offset_of_m_enableDebugUI_2(),
	TangoEnvironmentalLighting_t3401121675::get_offset_of_m_enableEnvironmentalLighting_3(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	TangoEnvironmentalLighting_t3401121675::get_offset_of_m_environmentMap_13(),
	TangoEnvironmentalLighting_t3401121675::get_offset_of_m_samples_14(),
	TangoEnvironmentalLighting_t3401121675::get_offset_of_m_coefficients_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (API_t4256039756)+ sizeof (Il2CppObject), sizeof(API_t4256039756 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (SphericalHarmonicSample_t1786505410)+ sizeof (Il2CppObject), sizeof(SphericalHarmonicSample_t1786505410_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2264[3] = 
{
	SphericalHarmonicSample_t1786505410::get_offset_of_sph_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicSample_t1786505410::get_offset_of_vec_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicSample_t1786505410::get_offset_of_coeff_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (TangoGestureCamera_t2762882179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[22] = 
{
	TangoGestureCamera_t2762882179::get_offset_of_m_targetFollowingObject_2(),
	TangoGestureCamera_t2762882179::get_offset_of_m_enableCameraModeUI_3(),
	TangoGestureCamera_t2762882179::get_offset_of_m_defaultCameraMode_4(),
	0,
	0,
	0,
	0,
	TangoGestureCamera_t2762882179::get_offset_of_m_curOffset_9(),
	TangoGestureCamera_t2762882179::get_offset_of_m_thirdPersonCamOffset_10(),
	TangoGestureCamera_t2762882179::get_offset_of_m_topDownCamOffset_11(),
	TangoGestureCamera_t2762882179::get_offset_of_m_currentCamera_12(),
	TangoGestureCamera_t2762882179::get_offset_of_curThirdPersonRotationX_13(),
	TangoGestureCamera_t2762882179::get_offset_of_curThirdPersonRotationY_14(),
	TangoGestureCamera_t2762882179::get_offset_of_startThirdPersonRotationX_15(),
	TangoGestureCamera_t2762882179::get_offset_of_startThirdPersonRotationY_16(),
	TangoGestureCamera_t2762882179::get_offset_of_startThirdPersonCameraCircleR_17(),
	TangoGestureCamera_t2762882179::get_offset_of_curThirdPersonCameraCircleR_18(),
	TangoGestureCamera_t2762882179::get_offset_of_touchStartPoint_19(),
	TangoGestureCamera_t2762882179::get_offset_of_topDownStartY_20(),
	TangoGestureCamera_t2762882179::get_offset_of_touchStartDist_21(),
	TangoGestureCamera_t2762882179::get_offset_of_topDownStartPos_22(),
	TangoGestureCamera_t2762882179::get_offset_of_thirdPersonCamStartOffset_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (CameraType_t4020249893)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2266[4] = 
{
	CameraType_t4020249893::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (TangoMultiCamera_t3288171709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[22] = 
{
	TangoMultiCamera_t3288171709::get_offset_of_m_targetFollowingObject_2(),
	TangoMultiCamera_t3288171709::get_offset_of_m_enableCameraTypeUI_3(),
	TangoMultiCamera_t3288171709::get_offset_of_m_defaultCameraType_4(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	TangoMultiCamera_t3288171709::get_offset_of_m_currentCamera_15(),
	TangoMultiCamera_t3288171709::get_offset_of_m_touchStartPosition_16(),
	TangoMultiCamera_t3288171709::get_offset_of_m_touchStartDistance_17(),
	TangoMultiCamera_t3288171709::get_offset_of_m_thirdPersonRotationEuler_18(),
	TangoMultiCamera_t3288171709::get_offset_of_m_thirdPersonRotationEulerStart_19(),
	TangoMultiCamera_t3288171709::get_offset_of_m_thirdPersonDistance_20(),
	TangoMultiCamera_t3288171709::get_offset_of_m_thirdPersonDistanceStart_21(),
	TangoMultiCamera_t3288171709::get_offset_of_m_topDownOffset_22(),
	TangoMultiCamera_t3288171709::get_offset_of_m_topDownOffsetStart_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (CameraType_t2660248723)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2268[4] = 
{
	CameraType_t2660248723::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (TangoPointCloud_t374155286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2269[27] = 
{
	TangoPointCloud_t374155286::get_offset_of_m_useAreaDescriptionPose_2(),
	TangoPointCloud_t374155286::get_offset_of_m_updatePointsMesh_3(),
	TangoPointCloud_t374155286::get_offset_of_m_points_4(),
	TangoPointCloud_t374155286::get_offset_of_m_pointsCount_5(),
	TangoPointCloud_t374155286::get_offset_of_m_depthTimestamp_6(),
	TangoPointCloud_t374155286::get_offset_of_m_overallZ_7(),
	TangoPointCloud_t374155286::get_offset_of_m_depthDeltaTime_8(),
	TangoPointCloud_t374155286::get_offset_of_m_floorPlaneY_9(),
	TangoPointCloud_t374155286::get_offset_of_m_floorFound_10(),
	0,
	0,
	0,
	0,
	TangoPointCloud_t374155286::get_offset_of_m_tangoApplication_15(),
	TangoPointCloud_t374155286::get_offset_of_m_imuTDevice_16(),
	TangoPointCloud_t374155286::get_offset_of_m_imuTDepthCamera_17(),
	TangoPointCloud_t374155286::get_offset_of_m_deviceTDepthCamera_18(),
	TangoPointCloud_t374155286::get_offset_of_m_colorCameraIntrinsics_19(),
	TangoPointCloud_t374155286::get_offset_of_m_cameraDataSetUp_20(),
	TangoPointCloud_t374155286::get_offset_of_m_mesh_21(),
	TangoPointCloud_t374155286::get_offset_of_m_renderer_22(),
	TangoPointCloud_t374155286::get_offset_of_m_tangoDeltaPoseController_23(),
	TangoPointCloud_t374155286::get_offset_of_m_findFloorWithDepth_24(),
	TangoPointCloud_t374155286::get_offset_of_m_numPointsAtY_25(),
	TangoPointCloud_t374155286::get_offset_of_m_nonNoiseBuckets_26(),
	TangoPointCloud_t374155286::get_offset_of_m_mostRecentPointCloud_27(),
	TangoPointCloud_t374155286::get_offset_of_m_mostRecentUnityWorldTDepthCamera_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (TangoPointCloudFloor_t3393423680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[5] = 
{
	TangoPointCloudFloor_t3393423680::get_offset_of_m_turnOffDepthCamera_2(),
	TangoPointCloudFloor_t3393423680::get_offset_of_m_floorFound_3(),
	TangoPointCloudFloor_t3393423680::get_offset_of_m_tangoApplication_4(),
	TangoPointCloudFloor_t3393423680::get_offset_of_m_pointCloud_5(),
	TangoPointCloudFloor_t3393423680::get_offset_of_m_depthTriggered_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (TangoPoseController_t4427816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[8] = 
{
	TangoPoseController_t4427816::get_offset_of_m_clutchEnabled_2(),
	TangoPoseController_t4427816::get_offset_of_m_baseFrameMode_3(),
	TangoPoseController_t4427816::get_offset_of_m_tangoARScreen_4(),
	TangoPoseController_t4427816::get_offset_of_m_characterController_5(),
	TangoPoseController_t4427816::get_offset_of_m_unityWorld_T_unityCamera_6(),
	TangoPoseController_t4427816::get_offset_of_m_unityWorldTransformOffset_T_unityWorld_7(),
	TangoPoseController_t4427816::get_offset_of_m_tangoApplication_8(),
	TangoPoseController_t4427816::get_offset_of_U3CLastPoseTimestampU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (BaseFrameSelectionModeEnum_t171456315)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2272[4] = 
{
	BaseFrameSelectionModeEnum_t171456315::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (Common_t442314523)+ sizeof (Il2CppObject), sizeof(Common_t442314523 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2273[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (AndroidResult_t1670778695)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2274[4] = 
{
	AndroidResult_t1670778695::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (ErrorType_t477358569)+ sizeof (Il2CppObject), sizeof(ErrorType_t477358569 ), sizeof(ErrorType_t477358569_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2275[6] = 
{
	ErrorType_t477358569_StaticFields::get_offset_of_TANGO_NO_CAMERA_PERMISSION_0(),
	ErrorType_t477358569_StaticFields::get_offset_of_TANGO_NO_ADF_PERMISSION_1(),
	ErrorType_t477358569_StaticFields::get_offset_of_TANGO_NO_MOTION_TRACKING_PERMISSION_2(),
	ErrorType_t477358569_StaticFields::get_offset_of_TANGO_INVALID_3(),
	ErrorType_t477358569_StaticFields::get_offset_of_TANGO_ERROR_4(),
	ErrorType_t477358569_StaticFields::get_offset_of_TANGO_SUCCESS_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (MetaDataKeyType_t4202728003)+ sizeof (Il2CppObject), sizeof(MetaDataKeyType_t4202728003 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2276[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (DMatrix4x4_t43123165)+ sizeof (Il2CppObject), sizeof(DMatrix4x4_t43123165 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2277[16] = 
{
	DMatrix4x4_t43123165::get_offset_of_m00_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DMatrix4x4_t43123165::get_offset_of_m10_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DMatrix4x4_t43123165::get_offset_of_m20_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DMatrix4x4_t43123165::get_offset_of_m30_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DMatrix4x4_t43123165::get_offset_of_m01_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DMatrix4x4_t43123165::get_offset_of_m11_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DMatrix4x4_t43123165::get_offset_of_m21_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DMatrix4x4_t43123165::get_offset_of_m31_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DMatrix4x4_t43123165::get_offset_of_m02_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DMatrix4x4_t43123165::get_offset_of_m12_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DMatrix4x4_t43123165::get_offset_of_m22_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DMatrix4x4_t43123165::get_offset_of_m32_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DMatrix4x4_t43123165::get_offset_of_m03_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DMatrix4x4_t43123165::get_offset_of_m13_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DMatrix4x4_t43123165::get_offset_of_m23_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DMatrix4x4_t43123165::get_offset_of_m33_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (DVector3_t3583584176)+ sizeof (Il2CppObject), sizeof(DVector3_t3583584176 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2278[3] = 
{
	DVector3_t3583584176::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DVector3_t3583584176::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DVector3_t3583584176::get_offset_of_z_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (DVector4_t2017500235)+ sizeof (Il2CppObject), sizeof(DVector4_t2017500235 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2279[4] = 
{
	DVector4_t2017500235::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DVector4_t2017500235::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DVector4_t2017500235::get_offset_of_z_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DVector4_t2017500235::get_offset_of_w_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (MarshallingHelper_t1923493080), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (OrientationManager_t1904847119), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (Rotation_t1451031165)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2282[6] = 
{
	Rotation_t1451031165::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (PublicForTesting_t3773363386), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (TangoEnums_t690178291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (TangoPoseStatusType_t1112875089)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2285[6] = 
{
	TangoPoseStatusType_t1112875089::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (TangoCoordinateFrameType_t3800473787)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2286[13] = 
{
	TangoCoordinateFrameType_t3800473787::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (TangoCameraId_t106182192)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2287[6] = 
{
	TangoCameraId_t106182192::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (TangoEventType_t2750756310)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2288[8] = 
{
	TangoEventType_t2750756310::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (TangoConfigType_t2597920516)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2289[7] = 
{
	TangoConfigType_t2597920516::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (TangoCalibrationType_t1064312954)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2290[6] = 
{
	TangoCalibrationType_t1064312954::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (TangoImageFormatType_t3932021136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2291[2] = 
{
	TangoImageFormatType_t3932021136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (TangoDepthCameraRate_t3368133418)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2292[3] = 
{
	TangoDepthCameraRate_t3368133418::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (TangoPoseType_t3471088457)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2293[4] = 
{
	TangoPoseType_t3471088457::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (TangoExtensions_t457904463), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (TangoCoordinateFramePair_t3148559008)+ sizeof (Il2CppObject), sizeof(TangoCoordinateFramePair_t3148559008 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2295[2] = 
{
	TangoCoordinateFramePair_t3148559008::get_offset_of_baseFrame_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoCoordinateFramePair_t3148559008::get_offset_of_targetFrame_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (TangoPointCloudIntPtr_t1795497939)+ sizeof (Il2CppObject), sizeof(TangoPointCloudIntPtr_t1795497939 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2296[4] = 
{
	TangoPointCloudIntPtr_t1795497939::get_offset_of_m_version_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoPointCloudIntPtr_t1795497939::get_offset_of_m_timestamp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoPointCloudIntPtr_t1795497939::get_offset_of_m_numPoints_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoPointCloudIntPtr_t1795497939::get_offset_of_m_points_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (TangoImage_t3319528210)+ sizeof (Il2CppObject), sizeof(TangoImage_t3319528210 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2297[21] = 
{
	TangoImage_t3319528210::get_offset_of_m_width_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_format_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_timestampNs_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_numPlanes_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_planeData0_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_planeData1_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_planeData2_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_planeData3_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_planeSize0_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_planeSize1_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_planeSize2_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_planeSize3_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_planeRowStride0_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_planeRowStride1_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_planeRowStride2_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_planeRowStride3_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_planePpixelStride0_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_planePixelStride1_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_planePixelStride2_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImage_t3319528210::get_offset_of_m_planePixelStride3_20() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (TangoCameraMetadata_t1412457087)+ sizeof (Il2CppObject), sizeof(TangoCameraMetadata_t1412457087 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2298[3] = 
{
	TangoCameraMetadata_t1412457087::get_offset_of_m_timestampNs_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoCameraMetadata_t1412457087::get_offset_of_m_frameNumber_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoCameraMetadata_t1412457087::get_offset_of_m_exposureDurationNs_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (TangoImageBuffer_t2541651254)+ sizeof (Il2CppObject), sizeof(TangoImageBuffer_t2541651254 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2299[7] = 
{
	TangoImageBuffer_t2541651254::get_offset_of_width_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImageBuffer_t2541651254::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImageBuffer_t2541651254::get_offset_of_stride_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImageBuffer_t2541651254::get_offset_of_timestamp_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImageBuffer_t2541651254::get_offset_of_frame_number_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImageBuffer_t2541651254::get_offset_of_format_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TangoImageBuffer_t2541651254::get_offset_of_data_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
