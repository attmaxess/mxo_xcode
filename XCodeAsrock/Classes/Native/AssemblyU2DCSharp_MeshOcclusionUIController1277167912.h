﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t1030026315;
// UnityEngine.Material
struct Material_t193706927;
// TangoDynamicMesh
struct TangoDynamicMesh_t714838395;
// Tango.AreaDescription
struct AreaDescription_t1434786909;
// TangoPoseController
struct TangoPoseController_t4427816;
// Tango.TangoApplication
struct TangoApplication_t278578959;
// System.Threading.Thread
struct Thread_t241561612;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeshOcclusionUIController
struct  MeshOcclusionUIController_t1277167912  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MeshOcclusionUIController::m_markerObject
	GameObject_t1756533147 * ___m_markerObject_2;
	// UnityEngine.GameObject MeshOcclusionUIController::m_meshBuildPanel
	GameObject_t1756533147 * ___m_meshBuildPanel_3;
	// UnityEngine.GameObject MeshOcclusionUIController::m_meshInteractionPanel
	GameObject_t1756533147 * ___m_meshInteractionPanel_4;
	// UnityEngine.GameObject MeshOcclusionUIController::m_viewMeshButton
	GameObject_t1756533147 * ___m_viewMeshButton_5;
	// UnityEngine.GameObject MeshOcclusionUIController::m_hideMeshButton
	GameObject_t1756533147 * ___m_hideMeshButton_6;
	// UnityEngine.UI.Image MeshOcclusionUIController::m_relocalizeImage
	Image_t2042527209 * ___m_relocalizeImage_7;
	// UnityEngine.UI.Text MeshOcclusionUIController::m_savingText
	Text_t356221433 * ___m_savingText_8;
	// UnityEngine.UI.Button MeshOcclusionUIController::m_createSelectedButton
	Button_t2872111280 * ___m_createSelectedButton_9;
	// UnityEngine.UI.Button MeshOcclusionUIController::m_startGameButton
	Button_t2872111280 * ___m_startGameButton_10;
	// UnityEngine.GameObject MeshOcclusionUIController::m_areaDescriptionLoaderPanel
	GameObject_t1756533147 * ___m_areaDescriptionLoaderPanel_11;
	// UnityEngine.GameObject MeshOcclusionUIController::m_listElement
	GameObject_t1756533147 * ___m_listElement_12;
	// UnityEngine.RectTransform MeshOcclusionUIController::m_listContentParent
	RectTransform_t3349966182 * ___m_listContentParent_13;
	// UnityEngine.UI.ToggleGroup MeshOcclusionUIController::m_toggleGroup
	ToggleGroup_t1030026315 * ___m_toggleGroup_14;
	// UnityEngine.Material MeshOcclusionUIController::m_depthMaskMat
	Material_t193706927 * ___m_depthMaskMat_15;
	// UnityEngine.Material MeshOcclusionUIController::m_visibleMat
	Material_t193706927 * ___m_visibleMat_16;
	// TangoDynamicMesh MeshOcclusionUIController::m_tangoDynamicMesh
	TangoDynamicMesh_t714838395 * ___m_tangoDynamicMesh_17;
	// UnityEngine.GameObject MeshOcclusionUIController::m_meshFromFile
	GameObject_t1756533147 * ___m_meshFromFile_18;
	// Tango.AreaDescription MeshOcclusionUIController::m_curAreaDescription
	AreaDescription_t1434786909 * ___m_curAreaDescription_19;
	// TangoPoseController MeshOcclusionUIController::m_poseController
	TangoPoseController_t4427816 * ___m_poseController_20;
	// Tango.TangoApplication MeshOcclusionUIController::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_21;
	// System.Threading.Thread MeshOcclusionUIController::m_saveThread
	Thread_t241561612 * ___m_saveThread_22;
	// System.Boolean MeshOcclusionUIController::m_initialized
	bool ___m_initialized_23;
	// System.Boolean MeshOcclusionUIController::m_3dReconstruction
	bool ___m_3dReconstruction_24;
	// System.Boolean MeshOcclusionUIController::m_menuOpen
	bool ___m_menuOpen_25;
	// System.String MeshOcclusionUIController::m_savedUUID
	String_t* ___m_savedUUID_26;
	// System.String MeshOcclusionUIController::m_meshSavePath
	String_t* ___m_meshSavePath_27;

public:
	inline static int32_t get_offset_of_m_markerObject_2() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_markerObject_2)); }
	inline GameObject_t1756533147 * get_m_markerObject_2() const { return ___m_markerObject_2; }
	inline GameObject_t1756533147 ** get_address_of_m_markerObject_2() { return &___m_markerObject_2; }
	inline void set_m_markerObject_2(GameObject_t1756533147 * value)
	{
		___m_markerObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_markerObject_2, value);
	}

	inline static int32_t get_offset_of_m_meshBuildPanel_3() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_meshBuildPanel_3)); }
	inline GameObject_t1756533147 * get_m_meshBuildPanel_3() const { return ___m_meshBuildPanel_3; }
	inline GameObject_t1756533147 ** get_address_of_m_meshBuildPanel_3() { return &___m_meshBuildPanel_3; }
	inline void set_m_meshBuildPanel_3(GameObject_t1756533147 * value)
	{
		___m_meshBuildPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_meshBuildPanel_3, value);
	}

	inline static int32_t get_offset_of_m_meshInteractionPanel_4() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_meshInteractionPanel_4)); }
	inline GameObject_t1756533147 * get_m_meshInteractionPanel_4() const { return ___m_meshInteractionPanel_4; }
	inline GameObject_t1756533147 ** get_address_of_m_meshInteractionPanel_4() { return &___m_meshInteractionPanel_4; }
	inline void set_m_meshInteractionPanel_4(GameObject_t1756533147 * value)
	{
		___m_meshInteractionPanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_meshInteractionPanel_4, value);
	}

	inline static int32_t get_offset_of_m_viewMeshButton_5() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_viewMeshButton_5)); }
	inline GameObject_t1756533147 * get_m_viewMeshButton_5() const { return ___m_viewMeshButton_5; }
	inline GameObject_t1756533147 ** get_address_of_m_viewMeshButton_5() { return &___m_viewMeshButton_5; }
	inline void set_m_viewMeshButton_5(GameObject_t1756533147 * value)
	{
		___m_viewMeshButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_viewMeshButton_5, value);
	}

	inline static int32_t get_offset_of_m_hideMeshButton_6() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_hideMeshButton_6)); }
	inline GameObject_t1756533147 * get_m_hideMeshButton_6() const { return ___m_hideMeshButton_6; }
	inline GameObject_t1756533147 ** get_address_of_m_hideMeshButton_6() { return &___m_hideMeshButton_6; }
	inline void set_m_hideMeshButton_6(GameObject_t1756533147 * value)
	{
		___m_hideMeshButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_hideMeshButton_6, value);
	}

	inline static int32_t get_offset_of_m_relocalizeImage_7() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_relocalizeImage_7)); }
	inline Image_t2042527209 * get_m_relocalizeImage_7() const { return ___m_relocalizeImage_7; }
	inline Image_t2042527209 ** get_address_of_m_relocalizeImage_7() { return &___m_relocalizeImage_7; }
	inline void set_m_relocalizeImage_7(Image_t2042527209 * value)
	{
		___m_relocalizeImage_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_relocalizeImage_7, value);
	}

	inline static int32_t get_offset_of_m_savingText_8() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_savingText_8)); }
	inline Text_t356221433 * get_m_savingText_8() const { return ___m_savingText_8; }
	inline Text_t356221433 ** get_address_of_m_savingText_8() { return &___m_savingText_8; }
	inline void set_m_savingText_8(Text_t356221433 * value)
	{
		___m_savingText_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_savingText_8, value);
	}

	inline static int32_t get_offset_of_m_createSelectedButton_9() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_createSelectedButton_9)); }
	inline Button_t2872111280 * get_m_createSelectedButton_9() const { return ___m_createSelectedButton_9; }
	inline Button_t2872111280 ** get_address_of_m_createSelectedButton_9() { return &___m_createSelectedButton_9; }
	inline void set_m_createSelectedButton_9(Button_t2872111280 * value)
	{
		___m_createSelectedButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_createSelectedButton_9, value);
	}

	inline static int32_t get_offset_of_m_startGameButton_10() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_startGameButton_10)); }
	inline Button_t2872111280 * get_m_startGameButton_10() const { return ___m_startGameButton_10; }
	inline Button_t2872111280 ** get_address_of_m_startGameButton_10() { return &___m_startGameButton_10; }
	inline void set_m_startGameButton_10(Button_t2872111280 * value)
	{
		___m_startGameButton_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_startGameButton_10, value);
	}

	inline static int32_t get_offset_of_m_areaDescriptionLoaderPanel_11() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_areaDescriptionLoaderPanel_11)); }
	inline GameObject_t1756533147 * get_m_areaDescriptionLoaderPanel_11() const { return ___m_areaDescriptionLoaderPanel_11; }
	inline GameObject_t1756533147 ** get_address_of_m_areaDescriptionLoaderPanel_11() { return &___m_areaDescriptionLoaderPanel_11; }
	inline void set_m_areaDescriptionLoaderPanel_11(GameObject_t1756533147 * value)
	{
		___m_areaDescriptionLoaderPanel_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_areaDescriptionLoaderPanel_11, value);
	}

	inline static int32_t get_offset_of_m_listElement_12() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_listElement_12)); }
	inline GameObject_t1756533147 * get_m_listElement_12() const { return ___m_listElement_12; }
	inline GameObject_t1756533147 ** get_address_of_m_listElement_12() { return &___m_listElement_12; }
	inline void set_m_listElement_12(GameObject_t1756533147 * value)
	{
		___m_listElement_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_listElement_12, value);
	}

	inline static int32_t get_offset_of_m_listContentParent_13() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_listContentParent_13)); }
	inline RectTransform_t3349966182 * get_m_listContentParent_13() const { return ___m_listContentParent_13; }
	inline RectTransform_t3349966182 ** get_address_of_m_listContentParent_13() { return &___m_listContentParent_13; }
	inline void set_m_listContentParent_13(RectTransform_t3349966182 * value)
	{
		___m_listContentParent_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_listContentParent_13, value);
	}

	inline static int32_t get_offset_of_m_toggleGroup_14() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_toggleGroup_14)); }
	inline ToggleGroup_t1030026315 * get_m_toggleGroup_14() const { return ___m_toggleGroup_14; }
	inline ToggleGroup_t1030026315 ** get_address_of_m_toggleGroup_14() { return &___m_toggleGroup_14; }
	inline void set_m_toggleGroup_14(ToggleGroup_t1030026315 * value)
	{
		___m_toggleGroup_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_toggleGroup_14, value);
	}

	inline static int32_t get_offset_of_m_depthMaskMat_15() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_depthMaskMat_15)); }
	inline Material_t193706927 * get_m_depthMaskMat_15() const { return ___m_depthMaskMat_15; }
	inline Material_t193706927 ** get_address_of_m_depthMaskMat_15() { return &___m_depthMaskMat_15; }
	inline void set_m_depthMaskMat_15(Material_t193706927 * value)
	{
		___m_depthMaskMat_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_depthMaskMat_15, value);
	}

	inline static int32_t get_offset_of_m_visibleMat_16() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_visibleMat_16)); }
	inline Material_t193706927 * get_m_visibleMat_16() const { return ___m_visibleMat_16; }
	inline Material_t193706927 ** get_address_of_m_visibleMat_16() { return &___m_visibleMat_16; }
	inline void set_m_visibleMat_16(Material_t193706927 * value)
	{
		___m_visibleMat_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_visibleMat_16, value);
	}

	inline static int32_t get_offset_of_m_tangoDynamicMesh_17() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_tangoDynamicMesh_17)); }
	inline TangoDynamicMesh_t714838395 * get_m_tangoDynamicMesh_17() const { return ___m_tangoDynamicMesh_17; }
	inline TangoDynamicMesh_t714838395 ** get_address_of_m_tangoDynamicMesh_17() { return &___m_tangoDynamicMesh_17; }
	inline void set_m_tangoDynamicMesh_17(TangoDynamicMesh_t714838395 * value)
	{
		___m_tangoDynamicMesh_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoDynamicMesh_17, value);
	}

	inline static int32_t get_offset_of_m_meshFromFile_18() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_meshFromFile_18)); }
	inline GameObject_t1756533147 * get_m_meshFromFile_18() const { return ___m_meshFromFile_18; }
	inline GameObject_t1756533147 ** get_address_of_m_meshFromFile_18() { return &___m_meshFromFile_18; }
	inline void set_m_meshFromFile_18(GameObject_t1756533147 * value)
	{
		___m_meshFromFile_18 = value;
		Il2CppCodeGenWriteBarrier(&___m_meshFromFile_18, value);
	}

	inline static int32_t get_offset_of_m_curAreaDescription_19() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_curAreaDescription_19)); }
	inline AreaDescription_t1434786909 * get_m_curAreaDescription_19() const { return ___m_curAreaDescription_19; }
	inline AreaDescription_t1434786909 ** get_address_of_m_curAreaDescription_19() { return &___m_curAreaDescription_19; }
	inline void set_m_curAreaDescription_19(AreaDescription_t1434786909 * value)
	{
		___m_curAreaDescription_19 = value;
		Il2CppCodeGenWriteBarrier(&___m_curAreaDescription_19, value);
	}

	inline static int32_t get_offset_of_m_poseController_20() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_poseController_20)); }
	inline TangoPoseController_t4427816 * get_m_poseController_20() const { return ___m_poseController_20; }
	inline TangoPoseController_t4427816 ** get_address_of_m_poseController_20() { return &___m_poseController_20; }
	inline void set_m_poseController_20(TangoPoseController_t4427816 * value)
	{
		___m_poseController_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_poseController_20, value);
	}

	inline static int32_t get_offset_of_m_tangoApplication_21() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_tangoApplication_21)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_21() const { return ___m_tangoApplication_21; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_21() { return &___m_tangoApplication_21; }
	inline void set_m_tangoApplication_21(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_21 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_21, value);
	}

	inline static int32_t get_offset_of_m_saveThread_22() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_saveThread_22)); }
	inline Thread_t241561612 * get_m_saveThread_22() const { return ___m_saveThread_22; }
	inline Thread_t241561612 ** get_address_of_m_saveThread_22() { return &___m_saveThread_22; }
	inline void set_m_saveThread_22(Thread_t241561612 * value)
	{
		___m_saveThread_22 = value;
		Il2CppCodeGenWriteBarrier(&___m_saveThread_22, value);
	}

	inline static int32_t get_offset_of_m_initialized_23() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_initialized_23)); }
	inline bool get_m_initialized_23() const { return ___m_initialized_23; }
	inline bool* get_address_of_m_initialized_23() { return &___m_initialized_23; }
	inline void set_m_initialized_23(bool value)
	{
		___m_initialized_23 = value;
	}

	inline static int32_t get_offset_of_m_3dReconstruction_24() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_3dReconstruction_24)); }
	inline bool get_m_3dReconstruction_24() const { return ___m_3dReconstruction_24; }
	inline bool* get_address_of_m_3dReconstruction_24() { return &___m_3dReconstruction_24; }
	inline void set_m_3dReconstruction_24(bool value)
	{
		___m_3dReconstruction_24 = value;
	}

	inline static int32_t get_offset_of_m_menuOpen_25() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_menuOpen_25)); }
	inline bool get_m_menuOpen_25() const { return ___m_menuOpen_25; }
	inline bool* get_address_of_m_menuOpen_25() { return &___m_menuOpen_25; }
	inline void set_m_menuOpen_25(bool value)
	{
		___m_menuOpen_25 = value;
	}

	inline static int32_t get_offset_of_m_savedUUID_26() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_savedUUID_26)); }
	inline String_t* get_m_savedUUID_26() const { return ___m_savedUUID_26; }
	inline String_t** get_address_of_m_savedUUID_26() { return &___m_savedUUID_26; }
	inline void set_m_savedUUID_26(String_t* value)
	{
		___m_savedUUID_26 = value;
		Il2CppCodeGenWriteBarrier(&___m_savedUUID_26, value);
	}

	inline static int32_t get_offset_of_m_meshSavePath_27() { return static_cast<int32_t>(offsetof(MeshOcclusionUIController_t1277167912, ___m_meshSavePath_27)); }
	inline String_t* get_m_meshSavePath_27() const { return ___m_meshSavePath_27; }
	inline String_t** get_address_of_m_meshSavePath_27() { return &___m_meshSavePath_27; }
	inline void set_m_meshSavePath_27(String_t* value)
	{
		___m_meshSavePath_27 = value;
		Il2CppCodeGenWriteBarrier(&___m_meshSavePath_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
