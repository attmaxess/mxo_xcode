﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// Tango.PoseProvider/APIOnPoseAvailable
struct APIOnPoseAvailable_t896491865;
// Tango.TangoPoseData
struct TangoPoseData_t192582348;
// Tango.OnTangoPoseAvailableEventHandler
struct OnTangoPoseAvailableEventHandler_t1363242156;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.PoseListener
struct  PoseListener_t4247666351  : public Il2CppObject
{
public:

public:
};

struct PoseListener_t4247666351_StaticFields
{
public:
	// System.Object Tango.PoseListener::m_lockObject
	Il2CppObject * ___m_lockObject_0;
	// Tango.PoseProvider/APIOnPoseAvailable Tango.PoseListener::m_poseAvailableCallback
	APIOnPoseAvailable_t896491865 * ___m_poseAvailableCallback_1;
	// Tango.TangoPoseData Tango.PoseListener::m_motionTrackingData
	TangoPoseData_t192582348 * ___m_motionTrackingData_2;
	// Tango.TangoPoseData Tango.PoseListener::m_areaLearningData
	TangoPoseData_t192582348 * ___m_areaLearningData_3;
	// Tango.TangoPoseData Tango.PoseListener::m_relocalizationData
	TangoPoseData_t192582348 * ___m_relocalizationData_4;
	// Tango.TangoPoseData Tango.PoseListener::m_cloudPoseData
	TangoPoseData_t192582348 * ___m_cloudPoseData_5;
	// Tango.OnTangoPoseAvailableEventHandler Tango.PoseListener::m_onTangoPoseAvailable
	OnTangoPoseAvailableEventHandler_t1363242156 * ___m_onTangoPoseAvailable_6;
	// System.Boolean Tango.PoseListener::m_isMotionTrackingPoseAvailable
	bool ___m_isMotionTrackingPoseAvailable_7;
	// System.Boolean Tango.PoseListener::m_isAreaLearningPoseAvailable
	bool ___m_isAreaLearningPoseAvailable_8;
	// System.Boolean Tango.PoseListener::m_isRelocalizationPoseAvailable
	bool ___m_isRelocalizationPoseAvailable_9;
	// System.Boolean Tango.PoseListener::m_isCloudPoseAvailable
	bool ___m_isCloudPoseAvailable_10;

public:
	inline static int32_t get_offset_of_m_lockObject_0() { return static_cast<int32_t>(offsetof(PoseListener_t4247666351_StaticFields, ___m_lockObject_0)); }
	inline Il2CppObject * get_m_lockObject_0() const { return ___m_lockObject_0; }
	inline Il2CppObject ** get_address_of_m_lockObject_0() { return &___m_lockObject_0; }
	inline void set_m_lockObject_0(Il2CppObject * value)
	{
		___m_lockObject_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_lockObject_0, value);
	}

	inline static int32_t get_offset_of_m_poseAvailableCallback_1() { return static_cast<int32_t>(offsetof(PoseListener_t4247666351_StaticFields, ___m_poseAvailableCallback_1)); }
	inline APIOnPoseAvailable_t896491865 * get_m_poseAvailableCallback_1() const { return ___m_poseAvailableCallback_1; }
	inline APIOnPoseAvailable_t896491865 ** get_address_of_m_poseAvailableCallback_1() { return &___m_poseAvailableCallback_1; }
	inline void set_m_poseAvailableCallback_1(APIOnPoseAvailable_t896491865 * value)
	{
		___m_poseAvailableCallback_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_poseAvailableCallback_1, value);
	}

	inline static int32_t get_offset_of_m_motionTrackingData_2() { return static_cast<int32_t>(offsetof(PoseListener_t4247666351_StaticFields, ___m_motionTrackingData_2)); }
	inline TangoPoseData_t192582348 * get_m_motionTrackingData_2() const { return ___m_motionTrackingData_2; }
	inline TangoPoseData_t192582348 ** get_address_of_m_motionTrackingData_2() { return &___m_motionTrackingData_2; }
	inline void set_m_motionTrackingData_2(TangoPoseData_t192582348 * value)
	{
		___m_motionTrackingData_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_motionTrackingData_2, value);
	}

	inline static int32_t get_offset_of_m_areaLearningData_3() { return static_cast<int32_t>(offsetof(PoseListener_t4247666351_StaticFields, ___m_areaLearningData_3)); }
	inline TangoPoseData_t192582348 * get_m_areaLearningData_3() const { return ___m_areaLearningData_3; }
	inline TangoPoseData_t192582348 ** get_address_of_m_areaLearningData_3() { return &___m_areaLearningData_3; }
	inline void set_m_areaLearningData_3(TangoPoseData_t192582348 * value)
	{
		___m_areaLearningData_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_areaLearningData_3, value);
	}

	inline static int32_t get_offset_of_m_relocalizationData_4() { return static_cast<int32_t>(offsetof(PoseListener_t4247666351_StaticFields, ___m_relocalizationData_4)); }
	inline TangoPoseData_t192582348 * get_m_relocalizationData_4() const { return ___m_relocalizationData_4; }
	inline TangoPoseData_t192582348 ** get_address_of_m_relocalizationData_4() { return &___m_relocalizationData_4; }
	inline void set_m_relocalizationData_4(TangoPoseData_t192582348 * value)
	{
		___m_relocalizationData_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_relocalizationData_4, value);
	}

	inline static int32_t get_offset_of_m_cloudPoseData_5() { return static_cast<int32_t>(offsetof(PoseListener_t4247666351_StaticFields, ___m_cloudPoseData_5)); }
	inline TangoPoseData_t192582348 * get_m_cloudPoseData_5() const { return ___m_cloudPoseData_5; }
	inline TangoPoseData_t192582348 ** get_address_of_m_cloudPoseData_5() { return &___m_cloudPoseData_5; }
	inline void set_m_cloudPoseData_5(TangoPoseData_t192582348 * value)
	{
		___m_cloudPoseData_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_cloudPoseData_5, value);
	}

	inline static int32_t get_offset_of_m_onTangoPoseAvailable_6() { return static_cast<int32_t>(offsetof(PoseListener_t4247666351_StaticFields, ___m_onTangoPoseAvailable_6)); }
	inline OnTangoPoseAvailableEventHandler_t1363242156 * get_m_onTangoPoseAvailable_6() const { return ___m_onTangoPoseAvailable_6; }
	inline OnTangoPoseAvailableEventHandler_t1363242156 ** get_address_of_m_onTangoPoseAvailable_6() { return &___m_onTangoPoseAvailable_6; }
	inline void set_m_onTangoPoseAvailable_6(OnTangoPoseAvailableEventHandler_t1363242156 * value)
	{
		___m_onTangoPoseAvailable_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_onTangoPoseAvailable_6, value);
	}

	inline static int32_t get_offset_of_m_isMotionTrackingPoseAvailable_7() { return static_cast<int32_t>(offsetof(PoseListener_t4247666351_StaticFields, ___m_isMotionTrackingPoseAvailable_7)); }
	inline bool get_m_isMotionTrackingPoseAvailable_7() const { return ___m_isMotionTrackingPoseAvailable_7; }
	inline bool* get_address_of_m_isMotionTrackingPoseAvailable_7() { return &___m_isMotionTrackingPoseAvailable_7; }
	inline void set_m_isMotionTrackingPoseAvailable_7(bool value)
	{
		___m_isMotionTrackingPoseAvailable_7 = value;
	}

	inline static int32_t get_offset_of_m_isAreaLearningPoseAvailable_8() { return static_cast<int32_t>(offsetof(PoseListener_t4247666351_StaticFields, ___m_isAreaLearningPoseAvailable_8)); }
	inline bool get_m_isAreaLearningPoseAvailable_8() const { return ___m_isAreaLearningPoseAvailable_8; }
	inline bool* get_address_of_m_isAreaLearningPoseAvailable_8() { return &___m_isAreaLearningPoseAvailable_8; }
	inline void set_m_isAreaLearningPoseAvailable_8(bool value)
	{
		___m_isAreaLearningPoseAvailable_8 = value;
	}

	inline static int32_t get_offset_of_m_isRelocalizationPoseAvailable_9() { return static_cast<int32_t>(offsetof(PoseListener_t4247666351_StaticFields, ___m_isRelocalizationPoseAvailable_9)); }
	inline bool get_m_isRelocalizationPoseAvailable_9() const { return ___m_isRelocalizationPoseAvailable_9; }
	inline bool* get_address_of_m_isRelocalizationPoseAvailable_9() { return &___m_isRelocalizationPoseAvailable_9; }
	inline void set_m_isRelocalizationPoseAvailable_9(bool value)
	{
		___m_isRelocalizationPoseAvailable_9 = value;
	}

	inline static int32_t get_offset_of_m_isCloudPoseAvailable_10() { return static_cast<int32_t>(offsetof(PoseListener_t4247666351_StaticFields, ___m_isCloudPoseAvailable_10)); }
	inline bool get_m_isCloudPoseAvailable_10() const { return ___m_isCloudPoseAvailable_10; }
	inline bool* get_address_of_m_isCloudPoseAvailable_10() { return &___m_isCloudPoseAvailable_10; }
	inline void set_m_isCloudPoseAvailable_10(bool value)
	{
		___m_isCloudPoseAvailable_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
