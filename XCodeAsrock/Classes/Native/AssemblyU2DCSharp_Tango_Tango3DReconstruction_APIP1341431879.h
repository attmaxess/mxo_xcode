﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_IntPtr2504060609.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.Tango3DReconstruction/APIPointCloud
struct  APIPointCloud_t1341431879 
{
public:
	// System.Double Tango.Tango3DReconstruction/APIPointCloud::timestamp
	double ___timestamp_0;
	// System.Int32 Tango.Tango3DReconstruction/APIPointCloud::numPoints
	int32_t ___numPoints_1;
	// System.IntPtr Tango.Tango3DReconstruction/APIPointCloud::points
	IntPtr_t ___points_2;

public:
	inline static int32_t get_offset_of_timestamp_0() { return static_cast<int32_t>(offsetof(APIPointCloud_t1341431879, ___timestamp_0)); }
	inline double get_timestamp_0() const { return ___timestamp_0; }
	inline double* get_address_of_timestamp_0() { return &___timestamp_0; }
	inline void set_timestamp_0(double value)
	{
		___timestamp_0 = value;
	}

	inline static int32_t get_offset_of_numPoints_1() { return static_cast<int32_t>(offsetof(APIPointCloud_t1341431879, ___numPoints_1)); }
	inline int32_t get_numPoints_1() const { return ___numPoints_1; }
	inline int32_t* get_address_of_numPoints_1() { return &___numPoints_1; }
	inline void set_numPoints_1(int32_t value)
	{
		___numPoints_1 = value;
	}

	inline static int32_t get_offset_of_points_2() { return static_cast<int32_t>(offsetof(APIPointCloud_t1341431879, ___points_2)); }
	inline IntPtr_t get_points_2() const { return ___points_2; }
	inline IntPtr_t* get_address_of_points_2() { return &___points_2; }
	inline void set_points_2(IntPtr_t value)
	{
		___points_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
