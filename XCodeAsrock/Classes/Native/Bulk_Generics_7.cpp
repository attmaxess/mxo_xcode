﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_488203048.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp3138797698.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_678743357.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23186312782.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_Grid1635028266.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3237672747.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3702943073.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_ObjectDisposedException2695136451.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat975728254.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3292975645.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3758245971.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat402048720.h"
#include "mscorlib_System_Collections_Generic_List_1_gen867319046.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat980360738.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1445631064.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat538879072.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1004149398.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2052409814.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2517680140.h"
#include "AssemblyU2DCSharp_Tango_TangoCoordinateFramePair3148559008.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3173112550.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3638382876.h"
#include "AssemblyU2DCSharp_Tango_TangoSupport_APIMarker4269261744.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerato66402098.h"
#include "mscorlib_System_Collections_Generic_List_1_gen531672424.h"
#include "AssemblyU2DCSharp_Tango_TangoSupport_Marker1162551292.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2809602155.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3274872481.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3905751349.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4073335620.h"
#include "mscorlib_System_Collections_Generic_List_1_gen243638650.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3220004478.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3685274804.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct List_1_t3702943073;
// System.InvalidOperationException
struct InvalidOperationException_t721527559;
// System.Type
struct Type_t;
// System.ObjectDisposedException
struct ObjectDisposedException_t2695136451;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t3758245971;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t867319046;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;
// System.Collections.Generic.List`1<Tango.Tango3DReconstruction/GridIndex>
struct List_1_t1004149398;
// System.Collections.Generic.List`1<Tango.TangoCoordinateFramePair>
struct List_1_t2517680140;
// System.Collections.Generic.List`1<Tango.TangoSupport/APIMarker>
struct List_1_t3638382876;
// System.Collections.Generic.List`1<Tango.TangoSupport/Marker>
struct List_1_t531672424;
// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo>
struct List_1_t3274872481;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t243638650;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m1391611625_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m1739958171_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m1394661909_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m2613351884_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m4238196864_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m3720025750_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m3859556280_MetadataUsageId;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1324042990_MetadataUsageId;
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m1266371732_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m2167629240_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m825848279_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m739025304_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m355114893_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m435841047_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m952445890_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m2678209574_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2376729941_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m619337795_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2761640348_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m3430113218_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3637545155_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m1010973337_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2888093155_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m674863933_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2408426667_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m1828382917_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3979461448_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m1677639504_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3079057684_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m2948867230_MetadataUsageId;

// System.String[]
struct StringU5BU5D_t1642385972  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2854920344  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t38854645  m_Items[1];

public:
	inline KeyValuePair_2_t38854645  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t38854645 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t38854645  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t38854645  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t38854645 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t38854645  value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t3030399641  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3304067486  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CustomAttributeNamedArgument_t94157543  m_Items[1];

public:
	inline CustomAttributeNamedArgument_t94157543  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CustomAttributeNamedArgument_t94157543 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeNamedArgument_t94157543  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline CustomAttributeNamedArgument_t94157543  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CustomAttributeNamedArgument_t94157543 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CustomAttributeNamedArgument_t94157543  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1075686591  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CustomAttributeTypedArgument_t1498197914  m_Items[1];

public:
	inline CustomAttributeTypedArgument_t1498197914  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CustomAttributeTypedArgument_t1498197914 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeTypedArgument_t1498197914  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline CustomAttributeTypedArgument_t1498197914  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CustomAttributeTypedArgument_t1498197914 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CustomAttributeTypedArgument_t1498197914  value)
	{
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_t577127397  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// Tango.Tango3DReconstruction/GridIndex[]
struct GridIndexU5BU5D_t2467124975  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GridIndex_t1635028266  m_Items[1];

public:
	inline GridIndex_t1635028266  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GridIndex_t1635028266 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GridIndex_t1635028266  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline GridIndex_t1635028266  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GridIndex_t1635028266 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GridIndex_t1635028266  value)
	{
		m_Items[index] = value;
	}
};
// Tango.TangoCoordinateFramePair[]
struct TangoCoordinateFramePairU5BU5D_t3518861025  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TangoCoordinateFramePair_t3148559008  m_Items[1];

public:
	inline TangoCoordinateFramePair_t3148559008  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TangoCoordinateFramePair_t3148559008 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TangoCoordinateFramePair_t3148559008  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline TangoCoordinateFramePair_t3148559008  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TangoCoordinateFramePair_t3148559008 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TangoCoordinateFramePair_t3148559008  value)
	{
		m_Items[index] = value;
	}
};
// Tango.TangoSupport/APIMarker[]
struct APIMarkerU5BU5D_t3559507857  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) APIMarker_t4269261744  m_Items[1];

public:
	inline APIMarker_t4269261744  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline APIMarker_t4269261744 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, APIMarker_t4269261744  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline APIMarker_t4269261744  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline APIMarker_t4269261744 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, APIMarker_t4269261744  value)
	{
		m_Items[index] = value;
	}
};
// Tango.TangoSupport/Marker[]
struct MarkerU5BU5D_t2644104341  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Marker_t1162551292  m_Items[1];

public:
	inline Marker_t1162551292  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Marker_t1162551292 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Marker_t1162551292  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Marker_t1162551292  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Marker_t1162551292 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Marker_t1162551292  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.AnimatorClipInfo[]
struct AnimatorClipInfoU5BU5D_t2969332312  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AnimatorClipInfo_t3905751349  m_Items[1];

public:
	inline AnimatorClipInfo_t3905751349  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline AnimatorClipInfo_t3905751349 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, AnimatorClipInfo_t3905751349  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline AnimatorClipInfo_t3905751349  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline AnimatorClipInfo_t3905751349 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, AnimatorClipInfo_t3905751349  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Color32_t874517518  m_Items[1];

public:
	inline Color32_t874517518  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color32_t874517518 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color32_t874517518  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color32_t874517518  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color32_t874517518 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color32_t874517518  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t603556505  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RaycastResult_t21186376  m_Items[1];

public:
	inline RaycastResult_t21186376  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RaycastResult_t21186376 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RaycastResult_t21186376  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline RaycastResult_t21186376  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RaycastResult_t21186376 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RaycastResult_t21186376  value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1350990071_gshared (KeyValuePair_2_t3749587448 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2726037047_gshared (KeyValuePair_2_t3749587448 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3201181706_gshared (KeyValuePair_2_t3749587448 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1537018582_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2897691047_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1391611625_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1222844869_gshared (KeyValuePair_2_t1174980068 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m965533293_gshared (KeyValuePair_2_t1174980068 * __this, bool p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4040336782_gshared (KeyValuePair_2_t1174980068 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2113318928_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m1916631176_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1739958171_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1307112735_gshared (KeyValuePair_2_t3716250094 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1921288671_gshared (KeyValuePair_2_t3716250094 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1877755778_gshared (KeyValuePair_2_t3716250094 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1454531804_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3699669100_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1394661909_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m744486900_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1416408204_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3464331946_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3385717033_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1251901674_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2613351884_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2339804284_gshared (KeyValuePair_2_t488203048 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2019724268_gshared (KeyValuePair_2_t488203048 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3870834457_gshared (KeyValuePair_2_t488203048 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m573362703_gshared (KeyValuePair_2_t488203048 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1644876463_gshared (KeyValuePair_2_t488203048 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4238196864_gshared (KeyValuePair_2_t488203048 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m410520028_gshared (KeyValuePair_2_t678743357 * __this, float p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2201718756_gshared (KeyValuePair_2_t678743357 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m437938725_gshared (KeyValuePair_2_t678743357 * __this, float ___key0, int32_t ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::get_Key()
extern "C"  float KeyValuePair_2_get_Key_m3135027831_gshared (KeyValuePair_2_t678743357 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1470891151_gshared (KeyValuePair_2_t678743357 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3720025750_gshared (KeyValuePair_2_t678743357 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2391876160_gshared (KeyValuePair_2_t3186312782 * __this, GridIndex_t1635028266  p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3735160320_gshared (KeyValuePair_2_t3186312782 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3533299533_gshared (KeyValuePair_2_t3186312782 * __this, GridIndex_t1635028266  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::get_Key()
extern "C"  GridIndex_t1635028266  KeyValuePair_2_get_Key_m869726205_gshared (KeyValuePair_2_t3186312782 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1039056622_gshared (KeyValuePair_2_t3186312782 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3859556280_gshared (KeyValuePair_2_t3186312782 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1246468418_gshared (Enumerator_t3237672747 * __this, List_1_t3702943073 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1266371732_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m372155760_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1324042990_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m2341522715_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3337940480_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m1406088962_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1614742070_gshared (Enumerator_t975728254 * __this, List_1_t1440998580 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2167629240_gshared (Enumerator_t975728254 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1016756388_gshared (Enumerator_t975728254 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_gshared (Enumerator_t975728254 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1274756239_gshared (Enumerator_t975728254 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3214913740_gshared (Enumerator_t975728254 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1694110554_gshared (Enumerator_t975728254 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3769601633_gshared (Enumerator_t1593300101 * __this, List_1_t2058570427 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m825848279_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3736175406_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m44995089_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2577424081_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3021143890_gshared (Enumerator_t3292975645 * __this, List_1_t3758245971 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
extern "C"  void Enumerator_VerifyState_m739025304_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m610822832_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m3704913451_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m598197344_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C"  CustomAttributeNamedArgument_t94157543  Enumerator_get_Current_m3860473239_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3421311553_gshared (Enumerator_t402048720 * __this, List_1_t867319046 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::VerifyState()
extern "C"  void Enumerator_VerifyState_m435841047_gshared (Enumerator_t402048720 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1436660297_gshared (Enumerator_t402048720 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m355114893_gshared (Enumerator_t402048720 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m3434518394_gshared (Enumerator_t402048720 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1792725673_gshared (Enumerator_t402048720 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C"  CustomAttributeTypedArgument_t1498197914  Enumerator_get_Current_m1371324410_gshared (Enumerator_t402048720 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2390830956_gshared (Enumerator_t980360738 * __this, List_1_t1445631064 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2678209574_gshared (Enumerator_t980360738 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1924386654_gshared (Enumerator_t980360738 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m952445890_gshared (Enumerator_t980360738 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m3373976255_gshared (Enumerator_t980360738 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1015452742_gshared (Enumerator_t980360738 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<System.Single>::get_Current()
extern "C"  float Enumerator_get_Current_m2934617767_gshared (Enumerator_t980360738 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1164203525_gshared (Enumerator_t538879072 * __this, List_1_t1004149398 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::VerifyState()
extern "C"  void Enumerator_VerifyState_m619337795_gshared (Enumerator_t538879072 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1420967061_gshared (Enumerator_t538879072 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2376729941_gshared (Enumerator_t538879072 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::Dispose()
extern "C"  void Enumerator_Dispose_m298132802_gshared (Enumerator_t538879072 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3536619405_gshared (Enumerator_t538879072 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::get_Current()
extern "C"  GridIndex_t1635028266  Enumerator_get_Current_m2994439408_gshared (Enumerator_t538879072 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3079668608_gshared (Enumerator_t2052409814 * __this, List_1_t2517680140 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3430113218_gshared (Enumerator_t2052409814 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4197296922_gshared (Enumerator_t2052409814 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2761640348_gshared (Enumerator_t2052409814 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::Dispose()
extern "C"  void Enumerator_Dispose_m2717126681_gshared (Enumerator_t2052409814 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m995044398_gshared (Enumerator_t2052409814 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::get_Current()
extern "C"  TangoCoordinateFramePair_t3148559008  Enumerator_get_Current_m4146353537_gshared (Enumerator_t2052409814 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1681979951_gshared (Enumerator_t3173112550 * __this, List_1_t3638382876 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1010973337_gshared (Enumerator_t3173112550 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1013115955_gshared (Enumerator_t3173112550 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3637545155_gshared (Enumerator_t3173112550 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::Dispose()
extern "C"  void Enumerator_Dispose_m3031870456_gshared (Enumerator_t3173112550 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4039552551_gshared (Enumerator_t3173112550 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::get_Current()
extern "C"  APIMarker_t4269261744  Enumerator_get_Current_m2705929228_gshared (Enumerator_t3173112550 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m91689835_gshared (Enumerator_t66402098 * __this, List_1_t531672424 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::VerifyState()
extern "C"  void Enumerator_VerifyState_m674863933_gshared (Enumerator_t66402098 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m809410215_gshared (Enumerator_t66402098 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2888093155_gshared (Enumerator_t66402098 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::Dispose()
extern "C"  void Enumerator_Dispose_m713108712_gshared (Enumerator_t66402098 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3343834211_gshared (Enumerator_t66402098 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::get_Current()
extern "C"  Marker_t1162551292  Enumerator_get_Current_m256872862_gshared (Enumerator_t66402098 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1380589695_gshared (Enumerator_t2809602155 * __this, List_1_t3274872481 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1828382917_gshared (Enumerator_t2809602155 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m886102771_gshared (Enumerator_t2809602155 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2408426667_gshared (Enumerator_t2809602155 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m15511624_gshared (Enumerator_t2809602155 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2893037143_gshared (Enumerator_t2809602155 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::get_Current()
extern "C"  AnimatorClipInfo_t3905751349  Enumerator_get_Current_m1830000836_gshared (Enumerator_t2809602155 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2054046066_gshared (Enumerator_t4073335620 * __this, List_1_t243638650 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1677639504_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1344379320_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3979461448_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::Dispose()
extern "C"  void Enumerator_Dispose_m1300762389_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2625246500_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::get_Current()
extern "C"  Color32_t874517518  Enumerator_get_Current_m1482710541_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3979168432_gshared (Enumerator_t3220004478 * __this, List_1_t3685274804 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2948867230_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m336811426_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3079057684_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C"  void Enumerator_Dispose_m3455280711_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2628556578_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C"  RaycastResult_t21186376  Enumerator_get_Current_m2728219003_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method);

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1350990071(__this, p0, method) ((  void (*) (KeyValuePair_2_t3749587448 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1350990071_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2726037047(__this, p0, method) ((  void (*) (KeyValuePair_2_t3749587448 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m2726037047_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3201181706(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3749587448 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m3201181706_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m1537018582(__this, method) ((  int32_t (*) (KeyValuePair_2_t3749587448 *, const MethodInfo*))KeyValuePair_2_get_Key_m1537018582_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m2897691047(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3749587448 *, const MethodInfo*))KeyValuePair_2_get_Value_m2897691047_gshared)(__this, method)
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m2960866144 (int32_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String[])
extern "C"  String_t* String_Concat_m626692867 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
#define KeyValuePair_2_ToString_m1391611625(__this, method) ((  String_t* (*) (KeyValuePair_2_t3749587448 *, const MethodInfo*))KeyValuePair_2_ToString_m1391611625_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1222844869(__this, p0, method) ((  void (*) (KeyValuePair_2_t1174980068 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m1222844869_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m965533293(__this, p0, method) ((  void (*) (KeyValuePair_2_t1174980068 *, bool, const MethodInfo*))KeyValuePair_2_set_Value_m965533293_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m4040336782(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1174980068 *, Il2CppObject *, bool, const MethodInfo*))KeyValuePair_2__ctor_m4040336782_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
#define KeyValuePair_2_get_Key_m2113318928(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1174980068 *, const MethodInfo*))KeyValuePair_2_get_Key_m2113318928_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m1916631176(__this, method) ((  bool (*) (KeyValuePair_2_t1174980068 *, const MethodInfo*))KeyValuePair_2_get_Value_m1916631176_gshared)(__this, method)
// System.String System.Boolean::ToString()
extern "C"  String_t* Boolean_ToString_m1253164328 (bool* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
#define KeyValuePair_2_ToString_m1739958171(__this, method) ((  String_t* (*) (KeyValuePair_2_t1174980068 *, const MethodInfo*))KeyValuePair_2_ToString_m1739958171_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1307112735(__this, p0, method) ((  void (*) (KeyValuePair_2_t3716250094 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m1307112735_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1921288671(__this, p0, method) ((  void (*) (KeyValuePair_2_t3716250094 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m1921288671_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1877755778(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3716250094 *, Il2CppObject *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m1877755778_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m1454531804(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3716250094 *, const MethodInfo*))KeyValuePair_2_get_Key_m1454531804_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m3699669100(__this, method) ((  int32_t (*) (KeyValuePair_2_t3716250094 *, const MethodInfo*))KeyValuePair_2_get_Value_m3699669100_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m1394661909(__this, method) ((  String_t* (*) (KeyValuePair_2_t3716250094 *, const MethodInfo*))KeyValuePair_2_ToString_m1394661909_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m744486900(__this, p0, method) ((  void (*) (KeyValuePair_2_t38854645 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1416408204(__this, p0, method) ((  void (*) (KeyValuePair_2_t38854645 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3464331946(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t38854645 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m3464331946_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m3385717033(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t38854645 *, const MethodInfo*))KeyValuePair_2_get_Key_m3385717033_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m1251901674(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t38854645 *, const MethodInfo*))KeyValuePair_2_get_Value_m1251901674_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
#define KeyValuePair_2_ToString_m2613351884(__this, method) ((  String_t* (*) (KeyValuePair_2_t38854645 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2339804284(__this, p0, method) ((  void (*) (KeyValuePair_2_t488203048 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m2339804284_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2019724268(__this, p0, method) ((  void (*) (KeyValuePair_2_t488203048 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m2019724268_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3870834457(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t488203048 *, Il2CppObject *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m3870834457_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
#define KeyValuePair_2_get_Key_m573362703(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t488203048 *, const MethodInfo*))KeyValuePair_2_get_Key_m573362703_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
#define KeyValuePair_2_get_Value_m1644876463(__this, method) ((  int32_t (*) (KeyValuePair_2_t488203048 *, const MethodInfo*))KeyValuePair_2_get_Value_m1644876463_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToString()
#define KeyValuePair_2_ToString_m4238196864(__this, method) ((  String_t* (*) (KeyValuePair_2_t488203048 *, const MethodInfo*))KeyValuePair_2_ToString_m4238196864_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m410520028(__this, p0, method) ((  void (*) (KeyValuePair_2_t678743357 *, float, const MethodInfo*))KeyValuePair_2_set_Key_m410520028_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2201718756(__this, p0, method) ((  void (*) (KeyValuePair_2_t678743357 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m2201718756_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m437938725(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t678743357 *, float, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m437938725_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m3135027831(__this, method) ((  float (*) (KeyValuePair_2_t678743357 *, const MethodInfo*))KeyValuePair_2_get_Key_m3135027831_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m1470891151(__this, method) ((  int32_t (*) (KeyValuePair_2_t678743357 *, const MethodInfo*))KeyValuePair_2_get_Value_m1470891151_gshared)(__this, method)
// System.String System.Single::ToString()
extern "C"  String_t* Single_ToString_m1813392066 (float* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m3720025750(__this, method) ((  String_t* (*) (KeyValuePair_2_t678743357 *, const MethodInfo*))KeyValuePair_2_ToString_m3720025750_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2391876160(__this, p0, method) ((  void (*) (KeyValuePair_2_t3186312782 *, GridIndex_t1635028266 , const MethodInfo*))KeyValuePair_2_set_Key_m2391876160_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3735160320(__this, p0, method) ((  void (*) (KeyValuePair_2_t3186312782 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m3735160320_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3533299533(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3186312782 *, GridIndex_t1635028266 , Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m3533299533_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m869726205(__this, method) ((  GridIndex_t1635028266  (*) (KeyValuePair_2_t3186312782 *, const MethodInfo*))KeyValuePair_2_get_Key_m869726205_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m1039056622(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3186312782 *, const MethodInfo*))KeyValuePair_2_get_Value_m1039056622_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::ToString()
#define KeyValuePair_2_ToString_m3859556280(__this, method) ((  String_t* (*) (KeyValuePair_2_t3186312782 *, const MethodInfo*))KeyValuePair_2_ToString_m3859556280_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1246468418(__this, ___l0, method) ((  void (*) (Enumerator_t3237672747 *, List_1_t3702943073 *, const MethodInfo*))Enumerator__ctor_m1246468418_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyState()
#define Enumerator_VerifyState_m1266371732(__this, method) ((  void (*) (Enumerator_t3237672747 *, const MethodInfo*))Enumerator_VerifyState_m1266371732_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m372155760(__this, method) ((  void (*) (Enumerator_t3237672747 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m372155760_gshared)(__this, method)
// System.Void System.InvalidOperationException::.ctor()
extern "C"  void InvalidOperationException__ctor_m102359810 (InvalidOperationException_t721527559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1324042990(__this, method) ((  Il2CppObject * (*) (Enumerator_t3237672747 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1324042990_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
#define Enumerator_Dispose_m2341522715(__this, method) ((  void (*) (Enumerator_t3237672747 *, const MethodInfo*))Enumerator_Dispose_m2341522715_gshared)(__this, method)
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m191970594 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObjectDisposedException::.ctor(System.String)
extern "C"  void ObjectDisposedException__ctor_m3156784572 (ObjectDisposedException_t2695136451 * __this, String_t* ___objectName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m2801133788 (InvalidOperationException_t721527559 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
#define Enumerator_MoveNext_m3337940480(__this, method) ((  bool (*) (Enumerator_t3237672747 *, const MethodInfo*))Enumerator_MoveNext_m3337940480_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
#define Enumerator_get_Current_m1406088962(__this, method) ((  KeyValuePair_2_t38854645  (*) (Enumerator_t3237672747 *, const MethodInfo*))Enumerator_get_Current_m1406088962_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1614742070(__this, ___l0, method) ((  void (*) (Enumerator_t975728254 *, List_1_t1440998580 *, const MethodInfo*))Enumerator__ctor_m1614742070_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
#define Enumerator_VerifyState_m2167629240(__this, method) ((  void (*) (Enumerator_t975728254 *, const MethodInfo*))Enumerator_VerifyState_m2167629240_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1016756388(__this, method) ((  void (*) (Enumerator_t975728254 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1016756388_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2154261170(__this, method) ((  Il2CppObject * (*) (Enumerator_t975728254 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
#define Enumerator_Dispose_m1274756239(__this, method) ((  void (*) (Enumerator_t975728254 *, const MethodInfo*))Enumerator_Dispose_m1274756239_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
#define Enumerator_MoveNext_m3214913740(__this, method) ((  bool (*) (Enumerator_t975728254 *, const MethodInfo*))Enumerator_MoveNext_m3214913740_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
#define Enumerator_get_Current_m1694110554(__this, method) ((  int32_t (*) (Enumerator_t975728254 *, const MethodInfo*))Enumerator_get_Current_m1694110554_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3769601633(__this, ___l0, method) ((  void (*) (Enumerator_t1593300101 *, List_1_t2058570427 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
#define Enumerator_VerifyState_m825848279(__this, method) ((  void (*) (Enumerator_t1593300101 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3440386353(__this, method) ((  void (*) (Enumerator_t1593300101 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2853089017(__this, method) ((  Il2CppObject * (*) (Enumerator_t1593300101 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
#define Enumerator_Dispose_m3736175406(__this, method) ((  void (*) (Enumerator_t1593300101 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
#define Enumerator_MoveNext_m44995089(__this, method) ((  bool (*) (Enumerator_t1593300101 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
#define Enumerator_get_Current_m2577424081(__this, method) ((  Il2CppObject * (*) (Enumerator_t1593300101 *, const MethodInfo*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3021143890(__this, ___l0, method) ((  void (*) (Enumerator_t3292975645 *, List_1_t3758245971 *, const MethodInfo*))Enumerator__ctor_m3021143890_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
#define Enumerator_VerifyState_m739025304(__this, method) ((  void (*) (Enumerator_t3292975645 *, const MethodInfo*))Enumerator_VerifyState_m739025304_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m610822832(__this, method) ((  void (*) (Enumerator_t3292975645 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m610822832_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1278092846(__this, method) ((  Il2CppObject * (*) (Enumerator_t3292975645 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
#define Enumerator_Dispose_m3704913451(__this, method) ((  void (*) (Enumerator_t3292975645 *, const MethodInfo*))Enumerator_Dispose_m3704913451_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
#define Enumerator_MoveNext_m598197344(__this, method) ((  bool (*) (Enumerator_t3292975645 *, const MethodInfo*))Enumerator_MoveNext_m598197344_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
#define Enumerator_get_Current_m3860473239(__this, method) ((  CustomAttributeNamedArgument_t94157543  (*) (Enumerator_t3292975645 *, const MethodInfo*))Enumerator_get_Current_m3860473239_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3421311553(__this, ___l0, method) ((  void (*) (Enumerator_t402048720 *, List_1_t867319046 *, const MethodInfo*))Enumerator__ctor_m3421311553_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::VerifyState()
#define Enumerator_VerifyState_m435841047(__this, method) ((  void (*) (Enumerator_t402048720 *, const MethodInfo*))Enumerator_VerifyState_m435841047_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1436660297(__this, method) ((  void (*) (Enumerator_t402048720 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1436660297_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m355114893(__this, method) ((  Il2CppObject * (*) (Enumerator_t402048720 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m355114893_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::Dispose()
#define Enumerator_Dispose_m3434518394(__this, method) ((  void (*) (Enumerator_t402048720 *, const MethodInfo*))Enumerator_Dispose_m3434518394_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
#define Enumerator_MoveNext_m1792725673(__this, method) ((  bool (*) (Enumerator_t402048720 *, const MethodInfo*))Enumerator_MoveNext_m1792725673_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::get_Current()
#define Enumerator_get_Current_m1371324410(__this, method) ((  CustomAttributeTypedArgument_t1498197914  (*) (Enumerator_t402048720 *, const MethodInfo*))Enumerator_get_Current_m1371324410_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2390830956(__this, ___l0, method) ((  void (*) (Enumerator_t980360738 *, List_1_t1445631064 *, const MethodInfo*))Enumerator__ctor_m2390830956_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::VerifyState()
#define Enumerator_VerifyState_m2678209574(__this, method) ((  void (*) (Enumerator_t980360738 *, const MethodInfo*))Enumerator_VerifyState_m2678209574_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1924386654(__this, method) ((  void (*) (Enumerator_t980360738 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1924386654_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Single>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m952445890(__this, method) ((  Il2CppObject * (*) (Enumerator_t980360738 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m952445890_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::Dispose()
#define Enumerator_Dispose_m3373976255(__this, method) ((  void (*) (Enumerator_t980360738 *, const MethodInfo*))Enumerator_Dispose_m3373976255_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Single>::MoveNext()
#define Enumerator_MoveNext_m1015452742(__this, method) ((  bool (*) (Enumerator_t980360738 *, const MethodInfo*))Enumerator_MoveNext_m1015452742_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Single>::get_Current()
#define Enumerator_get_Current_m2934617767(__this, method) ((  float (*) (Enumerator_t980360738 *, const MethodInfo*))Enumerator_get_Current_m2934617767_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1164203525(__this, ___l0, method) ((  void (*) (Enumerator_t538879072 *, List_1_t1004149398 *, const MethodInfo*))Enumerator__ctor_m1164203525_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::VerifyState()
#define Enumerator_VerifyState_m619337795(__this, method) ((  void (*) (Enumerator_t538879072 *, const MethodInfo*))Enumerator_VerifyState_m619337795_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1420967061(__this, method) ((  void (*) (Enumerator_t538879072 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1420967061_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2376729941(__this, method) ((  Il2CppObject * (*) (Enumerator_t538879072 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2376729941_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::Dispose()
#define Enumerator_Dispose_m298132802(__this, method) ((  void (*) (Enumerator_t538879072 *, const MethodInfo*))Enumerator_Dispose_m298132802_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::MoveNext()
#define Enumerator_MoveNext_m3536619405(__this, method) ((  bool (*) (Enumerator_t538879072 *, const MethodInfo*))Enumerator_MoveNext_m3536619405_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::get_Current()
#define Enumerator_get_Current_m2994439408(__this, method) ((  GridIndex_t1635028266  (*) (Enumerator_t538879072 *, const MethodInfo*))Enumerator_get_Current_m2994439408_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3079668608(__this, ___l0, method) ((  void (*) (Enumerator_t2052409814 *, List_1_t2517680140 *, const MethodInfo*))Enumerator__ctor_m3079668608_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::VerifyState()
#define Enumerator_VerifyState_m3430113218(__this, method) ((  void (*) (Enumerator_t2052409814 *, const MethodInfo*))Enumerator_VerifyState_m3430113218_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4197296922(__this, method) ((  void (*) (Enumerator_t2052409814 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4197296922_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2761640348(__this, method) ((  Il2CppObject * (*) (Enumerator_t2052409814 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2761640348_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::Dispose()
#define Enumerator_Dispose_m2717126681(__this, method) ((  void (*) (Enumerator_t2052409814 *, const MethodInfo*))Enumerator_Dispose_m2717126681_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::MoveNext()
#define Enumerator_MoveNext_m995044398(__this, method) ((  bool (*) (Enumerator_t2052409814 *, const MethodInfo*))Enumerator_MoveNext_m995044398_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::get_Current()
#define Enumerator_get_Current_m4146353537(__this, method) ((  TangoCoordinateFramePair_t3148559008  (*) (Enumerator_t2052409814 *, const MethodInfo*))Enumerator_get_Current_m4146353537_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1681979951(__this, ___l0, method) ((  void (*) (Enumerator_t3173112550 *, List_1_t3638382876 *, const MethodInfo*))Enumerator__ctor_m1681979951_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::VerifyState()
#define Enumerator_VerifyState_m1010973337(__this, method) ((  void (*) (Enumerator_t3173112550 *, const MethodInfo*))Enumerator_VerifyState_m1010973337_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1013115955(__this, method) ((  void (*) (Enumerator_t3173112550 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1013115955_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3637545155(__this, method) ((  Il2CppObject * (*) (Enumerator_t3173112550 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3637545155_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::Dispose()
#define Enumerator_Dispose_m3031870456(__this, method) ((  void (*) (Enumerator_t3173112550 *, const MethodInfo*))Enumerator_Dispose_m3031870456_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::MoveNext()
#define Enumerator_MoveNext_m4039552551(__this, method) ((  bool (*) (Enumerator_t3173112550 *, const MethodInfo*))Enumerator_MoveNext_m4039552551_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::get_Current()
#define Enumerator_get_Current_m2705929228(__this, method) ((  APIMarker_t4269261744  (*) (Enumerator_t3173112550 *, const MethodInfo*))Enumerator_get_Current_m2705929228_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m91689835(__this, ___l0, method) ((  void (*) (Enumerator_t66402098 *, List_1_t531672424 *, const MethodInfo*))Enumerator__ctor_m91689835_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::VerifyState()
#define Enumerator_VerifyState_m674863933(__this, method) ((  void (*) (Enumerator_t66402098 *, const MethodInfo*))Enumerator_VerifyState_m674863933_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m809410215(__this, method) ((  void (*) (Enumerator_t66402098 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m809410215_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2888093155(__this, method) ((  Il2CppObject * (*) (Enumerator_t66402098 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2888093155_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::Dispose()
#define Enumerator_Dispose_m713108712(__this, method) ((  void (*) (Enumerator_t66402098 *, const MethodInfo*))Enumerator_Dispose_m713108712_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::MoveNext()
#define Enumerator_MoveNext_m3343834211(__this, method) ((  bool (*) (Enumerator_t66402098 *, const MethodInfo*))Enumerator_MoveNext_m3343834211_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::get_Current()
#define Enumerator_get_Current_m256872862(__this, method) ((  Marker_t1162551292  (*) (Enumerator_t66402098 *, const MethodInfo*))Enumerator_get_Current_m256872862_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1380589695(__this, ___l0, method) ((  void (*) (Enumerator_t2809602155 *, List_1_t3274872481 *, const MethodInfo*))Enumerator__ctor_m1380589695_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::VerifyState()
#define Enumerator_VerifyState_m1828382917(__this, method) ((  void (*) (Enumerator_t2809602155 *, const MethodInfo*))Enumerator_VerifyState_m1828382917_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m886102771(__this, method) ((  void (*) (Enumerator_t2809602155 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m886102771_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2408426667(__this, method) ((  Il2CppObject * (*) (Enumerator_t2809602155 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2408426667_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::Dispose()
#define Enumerator_Dispose_m15511624(__this, method) ((  void (*) (Enumerator_t2809602155 *, const MethodInfo*))Enumerator_Dispose_m15511624_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::MoveNext()
#define Enumerator_MoveNext_m2893037143(__this, method) ((  bool (*) (Enumerator_t2809602155 *, const MethodInfo*))Enumerator_MoveNext_m2893037143_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::get_Current()
#define Enumerator_get_Current_m1830000836(__this, method) ((  AnimatorClipInfo_t3905751349  (*) (Enumerator_t2809602155 *, const MethodInfo*))Enumerator_get_Current_m1830000836_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2054046066(__this, ___l0, method) ((  void (*) (Enumerator_t4073335620 *, List_1_t243638650 *, const MethodInfo*))Enumerator__ctor_m2054046066_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::VerifyState()
#define Enumerator_VerifyState_m1677639504(__this, method) ((  void (*) (Enumerator_t4073335620 *, const MethodInfo*))Enumerator_VerifyState_m1677639504_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1344379320(__this, method) ((  void (*) (Enumerator_t4073335620 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1344379320_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3979461448(__this, method) ((  Il2CppObject * (*) (Enumerator_t4073335620 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3979461448_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::Dispose()
#define Enumerator_Dispose_m1300762389(__this, method) ((  void (*) (Enumerator_t4073335620 *, const MethodInfo*))Enumerator_Dispose_m1300762389_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::MoveNext()
#define Enumerator_MoveNext_m2625246500(__this, method) ((  bool (*) (Enumerator_t4073335620 *, const MethodInfo*))Enumerator_MoveNext_m2625246500_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::get_Current()
#define Enumerator_get_Current_m1482710541(__this, method) ((  Color32_t874517518  (*) (Enumerator_t4073335620 *, const MethodInfo*))Enumerator_get_Current_m1482710541_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3979168432(__this, ___l0, method) ((  void (*) (Enumerator_t3220004478 *, List_1_t3685274804 *, const MethodInfo*))Enumerator__ctor_m3979168432_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::VerifyState()
#define Enumerator_VerifyState_m2948867230(__this, method) ((  void (*) (Enumerator_t3220004478 *, const MethodInfo*))Enumerator_VerifyState_m2948867230_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m336811426(__this, method) ((  void (*) (Enumerator_t3220004478 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m336811426_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3079057684(__this, method) ((  Il2CppObject * (*) (Enumerator_t3220004478 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3079057684_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
#define Enumerator_Dispose_m3455280711(__this, method) ((  void (*) (Enumerator_t3220004478 *, const MethodInfo*))Enumerator_Dispose_m3455280711_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
#define Enumerator_MoveNext_m2628556578(__this, method) ((  bool (*) (Enumerator_t3220004478 *, const MethodInfo*))Enumerator_MoveNext_m2628556578_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
#define Enumerator_get_Current_m2728219003(__this, method) ((  RaycastResult_t21186376  (*) (Enumerator_t3220004478 *, const MethodInfo*))Enumerator_get_Current_m2728219003_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3201181706_gshared (KeyValuePair_2_t3749587448 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m1350990071((KeyValuePair_2_t3749587448 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m2726037047((KeyValuePair_2_t3749587448 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3201181706_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	KeyValuePair_2__ctor_m3201181706(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1537018582_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1537018582_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1537018582(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1350990071_gshared (KeyValuePair_2_t3749587448 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1350990071_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	KeyValuePair_2_set_Key_m1350990071(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2897691047_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2897691047_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2897691047(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2726037047_gshared (KeyValuePair_2_t3749587448 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2726037047_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	KeyValuePair_2_set_Value_m2726037047(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1391611625_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1391611625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m1537018582((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m1537018582((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m2960866144((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m2897691047((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m2897691047((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1391611625_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	return KeyValuePair_2_ToString_m1391611625(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4040336782_gshared (KeyValuePair_2_t1174980068 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1222844869((KeyValuePair_2_t1174980068 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = ___value1;
		KeyValuePair_2_set_Value_m965533293((KeyValuePair_2_t1174980068 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m4040336782_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	KeyValuePair_2__ctor_m4040336782(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2113318928_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2113318928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2113318928(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1222844869_gshared (KeyValuePair_2_t1174980068 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1222844869_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	KeyValuePair_2_set_Key_m1222844869(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m1916631176_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_value_1();
		return L_0;
	}
}
extern "C"  bool KeyValuePair_2_get_Value_m1916631176_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1916631176(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m965533293_gshared (KeyValuePair_2_t1174980068 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m965533293_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	KeyValuePair_2_set_Value_m965533293(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1739958171_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1739958171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		bool L_8 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		bool L_9 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (bool)L_9;
		String_t* L_10 = Boolean_ToString_m1253164328((bool*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1739958171_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	return KeyValuePair_2_ToString_m1739958171(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1877755778_gshared (KeyValuePair_2_t3716250094 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1307112735((KeyValuePair_2_t3716250094 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m1921288671((KeyValuePair_2_t3716250094 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1877755778_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	KeyValuePair_2__ctor_m1877755778(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1454531804_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1454531804_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1454531804(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1307112735_gshared (KeyValuePair_2_t3716250094 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1307112735_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	KeyValuePair_2_set_Key_m1307112735(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3699669100_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m3699669100_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3699669100(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1921288671_gshared (KeyValuePair_2_t3716250094 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1921288671_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	KeyValuePair_2_set_Value_m1921288671(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1394661909_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1394661909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m2960866144((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1394661909_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	return KeyValuePair_2_ToString_m1394661909(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3464331946_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m744486900((KeyValuePair_2_t38854645 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m1416408204((KeyValuePair_2_t38854645 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3464331946_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	KeyValuePair_2__ctor_m3464331946(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3385717033_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3385717033_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3385717033(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m744486900_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m744486900_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	KeyValuePair_2_set_Key_m744486900(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1251901674_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1251901674_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1251901674(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1416408204_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1416408204_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	KeyValuePair_2_set_Value_m1416408204(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2613351884_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2613351884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m3385717033((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m3385717033((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m1251901674((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m1251901674((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2613351884_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	return KeyValuePair_2_ToString_m2613351884(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3870834457_gshared (KeyValuePair_2_t488203048 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m2339804284((KeyValuePair_2_t488203048 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m2019724268((KeyValuePair_2_t488203048 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3870834457_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t488203048 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t488203048 *>(__this + 1);
	KeyValuePair_2__ctor_m3870834457(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m573362703_gshared (KeyValuePair_2_t488203048 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m573362703_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t488203048 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t488203048 *>(__this + 1);
	return KeyValuePair_2_get_Key_m573362703(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2339804284_gshared (KeyValuePair_2_t488203048 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2339804284_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t488203048 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t488203048 *>(__this + 1);
	KeyValuePair_2_set_Key_m2339804284(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1644876463_gshared (KeyValuePair_2_t488203048 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m1644876463_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t488203048 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t488203048 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1644876463(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2019724268_gshared (KeyValuePair_2_t488203048 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2019724268_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t488203048 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t488203048 *>(__this + 1);
	KeyValuePair_2_set_Value_m2019724268(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4238196864_gshared (KeyValuePair_2_t488203048 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m4238196864_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m573362703((KeyValuePair_2_t488203048 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m573362703((KeyValuePair_2_t488203048 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m1644876463((KeyValuePair_2_t488203048 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m1644876463((KeyValuePair_2_t488203048 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m4238196864_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t488203048 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t488203048 *>(__this + 1);
	return KeyValuePair_2_ToString_m4238196864(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m437938725_gshared (KeyValuePair_2_t678743357 * __this, float ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		float L_0 = ___key0;
		KeyValuePair_2_set_Key_m410520028((KeyValuePair_2_t678743357 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m2201718756((KeyValuePair_2_t678743357 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m437938725_AdjustorThunk (Il2CppObject * __this, float ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t678743357 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t678743357 *>(__this + 1);
	KeyValuePair_2__ctor_m437938725(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::get_Key()
extern "C"  float KeyValuePair_2_get_Key_m3135027831_gshared (KeyValuePair_2_t678743357 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_key_0();
		return L_0;
	}
}
extern "C"  float KeyValuePair_2_get_Key_m3135027831_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t678743357 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t678743357 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3135027831(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m410520028_gshared (KeyValuePair_2_t678743357 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m410520028_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t678743357 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t678743357 *>(__this + 1);
	KeyValuePair_2_set_Key_m410520028(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1470891151_gshared (KeyValuePair_2_t678743357 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m1470891151_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t678743357 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t678743357 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1470891151(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2201718756_gshared (KeyValuePair_2_t678743357 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2201718756_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t678743357 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t678743357 *>(__this + 1);
	KeyValuePair_2_set_Value_m2201718756(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3720025750_gshared (KeyValuePair_2_t678743357 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3720025750_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		float L_2 = KeyValuePair_2_get_Key_m3135027831((KeyValuePair_2_t678743357 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		float L_3 = KeyValuePair_2_get_Key_m3135027831((KeyValuePair_2_t678743357 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (float)L_3;
		String_t* L_4 = Single_ToString_m1813392066((float*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m1470891151((KeyValuePair_2_t678743357 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m1470891151((KeyValuePair_2_t678743357 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m2960866144((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3720025750_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t678743357 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t678743357 *>(__this + 1);
	return KeyValuePair_2_ToString_m3720025750(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3533299533_gshared (KeyValuePair_2_t3186312782 * __this, GridIndex_t1635028266  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		GridIndex_t1635028266  L_0 = ___key0;
		KeyValuePair_2_set_Key_m2391876160((KeyValuePair_2_t3186312782 *)__this, (GridIndex_t1635028266 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m3735160320((KeyValuePair_2_t3186312782 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3533299533_AdjustorThunk (Il2CppObject * __this, GridIndex_t1635028266  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3186312782 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3186312782 *>(__this + 1);
	KeyValuePair_2__ctor_m3533299533(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::get_Key()
extern "C"  GridIndex_t1635028266  KeyValuePair_2_get_Key_m869726205_gshared (KeyValuePair_2_t3186312782 * __this, const MethodInfo* method)
{
	{
		GridIndex_t1635028266  L_0 = (GridIndex_t1635028266 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  GridIndex_t1635028266  KeyValuePair_2_get_Key_m869726205_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3186312782 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3186312782 *>(__this + 1);
	return KeyValuePair_2_get_Key_m869726205(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2391876160_gshared (KeyValuePair_2_t3186312782 * __this, GridIndex_t1635028266  ___value0, const MethodInfo* method)
{
	{
		GridIndex_t1635028266  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2391876160_AdjustorThunk (Il2CppObject * __this, GridIndex_t1635028266  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3186312782 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3186312782 *>(__this + 1);
	KeyValuePair_2_set_Key_m2391876160(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1039056622_gshared (KeyValuePair_2_t3186312782 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1039056622_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3186312782 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3186312782 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1039056622(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3735160320_gshared (KeyValuePair_2_t3186312782 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3735160320_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3186312782 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3186312782 *>(__this + 1);
	KeyValuePair_2_set_Value_m3735160320(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Tango.Tango3DReconstruction/GridIndex,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3859556280_gshared (KeyValuePair_2_t3186312782 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3859556280_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GridIndex_t1635028266  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		GridIndex_t1635028266  L_2 = KeyValuePair_2_get_Key_m869726205((KeyValuePair_2_t3186312782 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		GridIndex_t1635028266  L_3 = KeyValuePair_2_get_Key_m869726205((KeyValuePair_2_t3186312782 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (GridIndex_t1635028266 )L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral811305474);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_8 = (StringU5BU5D_t1642385972*)L_7;
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m1039056622((KeyValuePair_2_t3186312782 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = KeyValuePair_2_get_Value_m1039056622((KeyValuePair_2_t3186312782 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3859556280_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3186312782 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3186312782 *>(__this + 1);
	return KeyValuePair_2_ToString_m3859556280(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1246468418_gshared (Enumerator_t3237672747 * __this, List_1_t3702943073 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3702943073 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3702943073 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1246468418_AdjustorThunk (Il2CppObject * __this, List_1_t3702943073 * ___l0, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	Enumerator__ctor_m1246468418(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m372155760_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1266371732((Enumerator_t3237672747 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m372155760_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m372155760(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1324042990_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1324042990_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1266371732((Enumerator_t3237672747 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		KeyValuePair_2_t38854645  L_2 = (KeyValuePair_2_t38854645 )__this->get_current_3();
		KeyValuePair_2_t38854645  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1324042990_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1324042990(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m2341522715_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3702943073 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2341522715_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	Enumerator_Dispose_m2341522715(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1266371732_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1266371732_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3702943073 * L_0 = (List_1_t3702943073 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3237672747  L_1 = (*(Enumerator_t3237672747 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3702943073 * L_7 = (List_1_t3702943073 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1266371732_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	Enumerator_VerifyState_m1266371732(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3337940480_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1266371732((Enumerator_t3237672747 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3702943073 * L_2 = (List_1_t3702943073 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3702943073 * L_4 = (List_1_t3702943073 *)__this->get_l_0();
		NullCheck(L_4);
		KeyValuePair_2U5BU5D_t2854920344* L_5 = (KeyValuePair_2U5BU5D_t2854920344*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		KeyValuePair_2_t38854645  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3337940480_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	return Enumerator_MoveNext_m3337940480(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m1406088962_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m1406088962_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	return Enumerator_get_Current_m1406088962(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1614742070_gshared (Enumerator_t975728254 * __this, List_1_t1440998580 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1440998580 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1440998580 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1614742070_AdjustorThunk (Il2CppObject * __this, List_1_t1440998580 * ___l0, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	Enumerator__ctor_m1614742070(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1016756388_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2167629240((Enumerator_t975728254 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1016756388_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1016756388(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2167629240((Enumerator_t975728254 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2154261170(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1274756239_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1440998580 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1274756239_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	Enumerator_Dispose_m1274756239(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2167629240_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2167629240_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t975728254  L_1 = (*(Enumerator_t975728254 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1440998580 * L_7 = (List_1_t1440998580 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2167629240_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	Enumerator_VerifyState_m2167629240(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3214913740_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2167629240((Enumerator_t975728254 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1440998580 * L_2 = (List_1_t1440998580 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1440998580 * L_4 = (List_1_t1440998580 *)__this->get_l_0();
		NullCheck(L_4);
		Int32U5BU5D_t3030399641* L_5 = (Int32U5BU5D_t3030399641*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3214913740_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	return Enumerator_MoveNext_m3214913740(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1694110554_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m1694110554_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	return Enumerator_get_Current_m1694110554(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3769601633_gshared (Enumerator_t1593300101 * __this, List_1_t2058570427 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2058570427 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3769601633_AdjustorThunk (Il2CppObject * __this, List_1_t2058570427 * ___l0, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	Enumerator__ctor_m3769601633(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m825848279((Enumerator_t1593300101 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3440386353_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3440386353(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m825848279((Enumerator_t1593300101 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_current_3();
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2853089017(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3736175406_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t2058570427 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3736175406_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	Enumerator_Dispose_m3736175406(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m825848279_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m825848279_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1593300101  L_1 = (*(Enumerator_t1593300101 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2058570427 * L_7 = (List_1_t2058570427 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m825848279_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	Enumerator_VerifyState_m825848279(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m44995089_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m825848279((Enumerator_t1593300101 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2058570427 * L_2 = (List_1_t2058570427 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2058570427 * L_4 = (List_1_t2058570427 *)__this->get_l_0();
		NullCheck(L_4);
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m44995089_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	return Enumerator_MoveNext_m44995089(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2577424081_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_current_3();
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m2577424081_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	return Enumerator_get_Current_m2577424081(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3021143890_gshared (Enumerator_t3292975645 * __this, List_1_t3758245971 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3758245971 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3758245971 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3021143890_AdjustorThunk (Il2CppObject * __this, List_1_t3758245971 * ___l0, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	Enumerator__ctor_m3021143890(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m610822832_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m739025304((Enumerator_t3292975645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m610822832_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m610822832(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m739025304((Enumerator_t3292975645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeNamedArgument_t94157543  L_2 = (CustomAttributeNamedArgument_t94157543 )__this->get_current_3();
		CustomAttributeNamedArgument_t94157543  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1278092846(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m3704913451_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3758245971 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3704913451_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	Enumerator_Dispose_m3704913451(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
extern "C"  void Enumerator_VerifyState_m739025304_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m739025304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3758245971 * L_0 = (List_1_t3758245971 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3292975645  L_1 = (*(Enumerator_t3292975645 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3758245971 * L_7 = (List_1_t3758245971 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m739025304_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	Enumerator_VerifyState_m739025304(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m598197344_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m739025304((Enumerator_t3292975645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3758245971 * L_2 = (List_1_t3758245971 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3758245971 * L_4 = (List_1_t3758245971 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_5 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		CustomAttributeNamedArgument_t94157543  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m598197344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	return Enumerator_MoveNext_m598197344(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C"  CustomAttributeNamedArgument_t94157543  Enumerator_get_Current_m3860473239_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeNamedArgument_t94157543  Enumerator_get_Current_m3860473239_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	return Enumerator_get_Current_m3860473239(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3421311553_gshared (Enumerator_t402048720 * __this, List_1_t867319046 * ___l0, const MethodInfo* method)
{
	{
		List_1_t867319046 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t867319046 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3421311553_AdjustorThunk (Il2CppObject * __this, List_1_t867319046 * ___l0, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	Enumerator__ctor_m3421311553(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1436660297_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m435841047((Enumerator_t402048720 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1436660297_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1436660297(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m355114893_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m355114893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m435841047((Enumerator_t402048720 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeTypedArgument_t1498197914  L_2 = (CustomAttributeTypedArgument_t1498197914 )__this->get_current_3();
		CustomAttributeTypedArgument_t1498197914  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m355114893_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m355114893(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m3434518394_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t867319046 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3434518394_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	Enumerator_Dispose_m3434518394(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::VerifyState()
extern "C"  void Enumerator_VerifyState_m435841047_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m435841047_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t867319046 * L_0 = (List_1_t867319046 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t402048720  L_1 = (*(Enumerator_t402048720 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t867319046 * L_7 = (List_1_t867319046 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m435841047_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	Enumerator_VerifyState_m435841047(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1792725673_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m435841047((Enumerator_t402048720 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t867319046 * L_2 = (List_1_t867319046 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t867319046 * L_4 = (List_1_t867319046 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_5 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		CustomAttributeTypedArgument_t1498197914  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1792725673_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	return Enumerator_MoveNext_m1792725673(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C"  CustomAttributeTypedArgument_t1498197914  Enumerator_get_Current_m1371324410_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeTypedArgument_t1498197914  Enumerator_get_Current_m1371324410_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	return Enumerator_get_Current_m1371324410(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2390830956_gshared (Enumerator_t980360738 * __this, List_1_t1445631064 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1445631064 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1445631064 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2390830956_AdjustorThunk (Il2CppObject * __this, List_1_t1445631064 * ___l0, const MethodInfo* method)
{
	Enumerator_t980360738 * _thisAdjusted = reinterpret_cast<Enumerator_t980360738 *>(__this + 1);
	Enumerator__ctor_m2390830956(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1924386654_gshared (Enumerator_t980360738 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2678209574((Enumerator_t980360738 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1924386654_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t980360738 * _thisAdjusted = reinterpret_cast<Enumerator_t980360738 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1924386654(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m952445890_gshared (Enumerator_t980360738 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m952445890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2678209574((Enumerator_t980360738 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		float L_2 = (float)__this->get_current_3();
		float L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m952445890_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t980360738 * _thisAdjusted = reinterpret_cast<Enumerator_t980360738 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m952445890(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m3373976255_gshared (Enumerator_t980360738 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1445631064 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3373976255_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t980360738 * _thisAdjusted = reinterpret_cast<Enumerator_t980360738 *>(__this + 1);
	Enumerator_Dispose_m3373976255(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2678209574_gshared (Enumerator_t980360738 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2678209574_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t980360738  L_1 = (*(Enumerator_t980360738 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1445631064 * L_7 = (List_1_t1445631064 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2678209574_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t980360738 * _thisAdjusted = reinterpret_cast<Enumerator_t980360738 *>(__this + 1);
	Enumerator_VerifyState_m2678209574(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1015452742_gshared (Enumerator_t980360738 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2678209574((Enumerator_t980360738 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1445631064 * L_2 = (List_1_t1445631064 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1445631064 * L_4 = (List_1_t1445631064 *)__this->get_l_0();
		NullCheck(L_4);
		SingleU5BU5D_t577127397* L_5 = (SingleU5BU5D_t577127397*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		float L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1015452742_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t980360738 * _thisAdjusted = reinterpret_cast<Enumerator_t980360738 *>(__this + 1);
	return Enumerator_MoveNext_m1015452742(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Single>::get_Current()
extern "C"  float Enumerator_get_Current_m2934617767_gshared (Enumerator_t980360738 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_current_3();
		return L_0;
	}
}
extern "C"  float Enumerator_get_Current_m2934617767_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t980360738 * _thisAdjusted = reinterpret_cast<Enumerator_t980360738 *>(__this + 1);
	return Enumerator_get_Current_m2934617767(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1164203525_gshared (Enumerator_t538879072 * __this, List_1_t1004149398 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1004149398 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1004149398 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1164203525_AdjustorThunk (Il2CppObject * __this, List_1_t1004149398 * ___l0, const MethodInfo* method)
{
	Enumerator_t538879072 * _thisAdjusted = reinterpret_cast<Enumerator_t538879072 *>(__this + 1);
	Enumerator__ctor_m1164203525(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1420967061_gshared (Enumerator_t538879072 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m619337795((Enumerator_t538879072 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1420967061_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t538879072 * _thisAdjusted = reinterpret_cast<Enumerator_t538879072 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1420967061(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2376729941_gshared (Enumerator_t538879072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2376729941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m619337795((Enumerator_t538879072 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		GridIndex_t1635028266  L_2 = (GridIndex_t1635028266 )__this->get_current_3();
		GridIndex_t1635028266  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2376729941_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t538879072 * _thisAdjusted = reinterpret_cast<Enumerator_t538879072 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2376729941(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::Dispose()
extern "C"  void Enumerator_Dispose_m298132802_gshared (Enumerator_t538879072 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1004149398 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m298132802_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t538879072 * _thisAdjusted = reinterpret_cast<Enumerator_t538879072 *>(__this + 1);
	Enumerator_Dispose_m298132802(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::VerifyState()
extern "C"  void Enumerator_VerifyState_m619337795_gshared (Enumerator_t538879072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m619337795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1004149398 * L_0 = (List_1_t1004149398 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t538879072  L_1 = (*(Enumerator_t538879072 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1004149398 * L_7 = (List_1_t1004149398 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m619337795_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t538879072 * _thisAdjusted = reinterpret_cast<Enumerator_t538879072 *>(__this + 1);
	Enumerator_VerifyState_m619337795(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3536619405_gshared (Enumerator_t538879072 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m619337795((Enumerator_t538879072 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1004149398 * L_2 = (List_1_t1004149398 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1004149398 * L_4 = (List_1_t1004149398 *)__this->get_l_0();
		NullCheck(L_4);
		GridIndexU5BU5D_t2467124975* L_5 = (GridIndexU5BU5D_t2467124975*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		GridIndex_t1635028266  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3536619405_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t538879072 * _thisAdjusted = reinterpret_cast<Enumerator_t538879072 *>(__this + 1);
	return Enumerator_MoveNext_m3536619405(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Tango.Tango3DReconstruction/GridIndex>::get_Current()
extern "C"  GridIndex_t1635028266  Enumerator_get_Current_m2994439408_gshared (Enumerator_t538879072 * __this, const MethodInfo* method)
{
	{
		GridIndex_t1635028266  L_0 = (GridIndex_t1635028266 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  GridIndex_t1635028266  Enumerator_get_Current_m2994439408_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t538879072 * _thisAdjusted = reinterpret_cast<Enumerator_t538879072 *>(__this + 1);
	return Enumerator_get_Current_m2994439408(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3079668608_gshared (Enumerator_t2052409814 * __this, List_1_t2517680140 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2517680140 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2517680140 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3079668608_AdjustorThunk (Il2CppObject * __this, List_1_t2517680140 * ___l0, const MethodInfo* method)
{
	Enumerator_t2052409814 * _thisAdjusted = reinterpret_cast<Enumerator_t2052409814 *>(__this + 1);
	Enumerator__ctor_m3079668608(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4197296922_gshared (Enumerator_t2052409814 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3430113218((Enumerator_t2052409814 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4197296922_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2052409814 * _thisAdjusted = reinterpret_cast<Enumerator_t2052409814 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4197296922(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2761640348_gshared (Enumerator_t2052409814 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2761640348_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m3430113218((Enumerator_t2052409814 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		TangoCoordinateFramePair_t3148559008  L_2 = (TangoCoordinateFramePair_t3148559008 )__this->get_current_3();
		TangoCoordinateFramePair_t3148559008  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2761640348_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2052409814 * _thisAdjusted = reinterpret_cast<Enumerator_t2052409814 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2761640348(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::Dispose()
extern "C"  void Enumerator_Dispose_m2717126681_gshared (Enumerator_t2052409814 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t2517680140 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2717126681_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2052409814 * _thisAdjusted = reinterpret_cast<Enumerator_t2052409814 *>(__this + 1);
	Enumerator_Dispose_m2717126681(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3430113218_gshared (Enumerator_t2052409814 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3430113218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2517680140 * L_0 = (List_1_t2517680140 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2052409814  L_1 = (*(Enumerator_t2052409814 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2517680140 * L_7 = (List_1_t2517680140 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3430113218_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2052409814 * _thisAdjusted = reinterpret_cast<Enumerator_t2052409814 *>(__this + 1);
	Enumerator_VerifyState_m3430113218(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m995044398_gshared (Enumerator_t2052409814 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m3430113218((Enumerator_t2052409814 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2517680140 * L_2 = (List_1_t2517680140 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2517680140 * L_4 = (List_1_t2517680140 *)__this->get_l_0();
		NullCheck(L_4);
		TangoCoordinateFramePairU5BU5D_t3518861025* L_5 = (TangoCoordinateFramePairU5BU5D_t3518861025*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		TangoCoordinateFramePair_t3148559008  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m995044398_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2052409814 * _thisAdjusted = reinterpret_cast<Enumerator_t2052409814 *>(__this + 1);
	return Enumerator_MoveNext_m995044398(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Tango.TangoCoordinateFramePair>::get_Current()
extern "C"  TangoCoordinateFramePair_t3148559008  Enumerator_get_Current_m4146353537_gshared (Enumerator_t2052409814 * __this, const MethodInfo* method)
{
	{
		TangoCoordinateFramePair_t3148559008  L_0 = (TangoCoordinateFramePair_t3148559008 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  TangoCoordinateFramePair_t3148559008  Enumerator_get_Current_m4146353537_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2052409814 * _thisAdjusted = reinterpret_cast<Enumerator_t2052409814 *>(__this + 1);
	return Enumerator_get_Current_m4146353537(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1681979951_gshared (Enumerator_t3173112550 * __this, List_1_t3638382876 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3638382876 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3638382876 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1681979951_AdjustorThunk (Il2CppObject * __this, List_1_t3638382876 * ___l0, const MethodInfo* method)
{
	Enumerator_t3173112550 * _thisAdjusted = reinterpret_cast<Enumerator_t3173112550 *>(__this + 1);
	Enumerator__ctor_m1681979951(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1013115955_gshared (Enumerator_t3173112550 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1010973337((Enumerator_t3173112550 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1013115955_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3173112550 * _thisAdjusted = reinterpret_cast<Enumerator_t3173112550 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1013115955(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3637545155_gshared (Enumerator_t3173112550 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3637545155_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1010973337((Enumerator_t3173112550 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		APIMarker_t4269261744  L_2 = (APIMarker_t4269261744 )__this->get_current_3();
		APIMarker_t4269261744  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3637545155_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3173112550 * _thisAdjusted = reinterpret_cast<Enumerator_t3173112550 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3637545155(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::Dispose()
extern "C"  void Enumerator_Dispose_m3031870456_gshared (Enumerator_t3173112550 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3638382876 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3031870456_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3173112550 * _thisAdjusted = reinterpret_cast<Enumerator_t3173112550 *>(__this + 1);
	Enumerator_Dispose_m3031870456(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1010973337_gshared (Enumerator_t3173112550 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1010973337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3638382876 * L_0 = (List_1_t3638382876 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3173112550  L_1 = (*(Enumerator_t3173112550 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3638382876 * L_7 = (List_1_t3638382876 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1010973337_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3173112550 * _thisAdjusted = reinterpret_cast<Enumerator_t3173112550 *>(__this + 1);
	Enumerator_VerifyState_m1010973337(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4039552551_gshared (Enumerator_t3173112550 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1010973337((Enumerator_t3173112550 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3638382876 * L_2 = (List_1_t3638382876 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3638382876 * L_4 = (List_1_t3638382876 *)__this->get_l_0();
		NullCheck(L_4);
		APIMarkerU5BU5D_t3559507857* L_5 = (APIMarkerU5BU5D_t3559507857*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		APIMarker_t4269261744  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m4039552551_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3173112550 * _thisAdjusted = reinterpret_cast<Enumerator_t3173112550 *>(__this + 1);
	return Enumerator_MoveNext_m4039552551(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/APIMarker>::get_Current()
extern "C"  APIMarker_t4269261744  Enumerator_get_Current_m2705929228_gshared (Enumerator_t3173112550 * __this, const MethodInfo* method)
{
	{
		APIMarker_t4269261744  L_0 = (APIMarker_t4269261744 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  APIMarker_t4269261744  Enumerator_get_Current_m2705929228_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3173112550 * _thisAdjusted = reinterpret_cast<Enumerator_t3173112550 *>(__this + 1);
	return Enumerator_get_Current_m2705929228(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m91689835_gshared (Enumerator_t66402098 * __this, List_1_t531672424 * ___l0, const MethodInfo* method)
{
	{
		List_1_t531672424 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t531672424 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m91689835_AdjustorThunk (Il2CppObject * __this, List_1_t531672424 * ___l0, const MethodInfo* method)
{
	Enumerator_t66402098 * _thisAdjusted = reinterpret_cast<Enumerator_t66402098 *>(__this + 1);
	Enumerator__ctor_m91689835(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m809410215_gshared (Enumerator_t66402098 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m674863933((Enumerator_t66402098 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m809410215_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t66402098 * _thisAdjusted = reinterpret_cast<Enumerator_t66402098 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m809410215(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2888093155_gshared (Enumerator_t66402098 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2888093155_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m674863933((Enumerator_t66402098 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Marker_t1162551292  L_2 = (Marker_t1162551292 )__this->get_current_3();
		Marker_t1162551292  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2888093155_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t66402098 * _thisAdjusted = reinterpret_cast<Enumerator_t66402098 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2888093155(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::Dispose()
extern "C"  void Enumerator_Dispose_m713108712_gshared (Enumerator_t66402098 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t531672424 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m713108712_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t66402098 * _thisAdjusted = reinterpret_cast<Enumerator_t66402098 *>(__this + 1);
	Enumerator_Dispose_m713108712(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::VerifyState()
extern "C"  void Enumerator_VerifyState_m674863933_gshared (Enumerator_t66402098 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m674863933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t531672424 * L_0 = (List_1_t531672424 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t66402098  L_1 = (*(Enumerator_t66402098 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t531672424 * L_7 = (List_1_t531672424 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m674863933_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t66402098 * _thisAdjusted = reinterpret_cast<Enumerator_t66402098 *>(__this + 1);
	Enumerator_VerifyState_m674863933(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3343834211_gshared (Enumerator_t66402098 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m674863933((Enumerator_t66402098 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t531672424 * L_2 = (List_1_t531672424 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t531672424 * L_4 = (List_1_t531672424 *)__this->get_l_0();
		NullCheck(L_4);
		MarkerU5BU5D_t2644104341* L_5 = (MarkerU5BU5D_t2644104341*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		Marker_t1162551292  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3343834211_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t66402098 * _thisAdjusted = reinterpret_cast<Enumerator_t66402098 *>(__this + 1);
	return Enumerator_MoveNext_m3343834211(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Tango.TangoSupport/Marker>::get_Current()
extern "C"  Marker_t1162551292  Enumerator_get_Current_m256872862_gshared (Enumerator_t66402098 * __this, const MethodInfo* method)
{
	{
		Marker_t1162551292  L_0 = (Marker_t1162551292 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Marker_t1162551292  Enumerator_get_Current_m256872862_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t66402098 * _thisAdjusted = reinterpret_cast<Enumerator_t66402098 *>(__this + 1);
	return Enumerator_get_Current_m256872862(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1380589695_gshared (Enumerator_t2809602155 * __this, List_1_t3274872481 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3274872481 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3274872481 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1380589695_AdjustorThunk (Il2CppObject * __this, List_1_t3274872481 * ___l0, const MethodInfo* method)
{
	Enumerator_t2809602155 * _thisAdjusted = reinterpret_cast<Enumerator_t2809602155 *>(__this + 1);
	Enumerator__ctor_m1380589695(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m886102771_gshared (Enumerator_t2809602155 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1828382917((Enumerator_t2809602155 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m886102771_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2809602155 * _thisAdjusted = reinterpret_cast<Enumerator_t2809602155 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m886102771(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2408426667_gshared (Enumerator_t2809602155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2408426667_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1828382917((Enumerator_t2809602155 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		AnimatorClipInfo_t3905751349  L_2 = (AnimatorClipInfo_t3905751349 )__this->get_current_3();
		AnimatorClipInfo_t3905751349  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2408426667_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2809602155 * _thisAdjusted = reinterpret_cast<Enumerator_t2809602155 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2408426667(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m15511624_gshared (Enumerator_t2809602155 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3274872481 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m15511624_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2809602155 * _thisAdjusted = reinterpret_cast<Enumerator_t2809602155 *>(__this + 1);
	Enumerator_Dispose_m15511624(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1828382917_gshared (Enumerator_t2809602155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1828382917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3274872481 * L_0 = (List_1_t3274872481 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2809602155  L_1 = (*(Enumerator_t2809602155 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3274872481 * L_7 = (List_1_t3274872481 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1828382917_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2809602155 * _thisAdjusted = reinterpret_cast<Enumerator_t2809602155 *>(__this + 1);
	Enumerator_VerifyState_m1828382917(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2893037143_gshared (Enumerator_t2809602155 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1828382917((Enumerator_t2809602155 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3274872481 * L_2 = (List_1_t3274872481 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3274872481 * L_4 = (List_1_t3274872481 *)__this->get_l_0();
		NullCheck(L_4);
		AnimatorClipInfoU5BU5D_t2969332312* L_5 = (AnimatorClipInfoU5BU5D_t2969332312*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		AnimatorClipInfo_t3905751349  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2893037143_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2809602155 * _thisAdjusted = reinterpret_cast<Enumerator_t2809602155 *>(__this + 1);
	return Enumerator_MoveNext_m2893037143(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::get_Current()
extern "C"  AnimatorClipInfo_t3905751349  Enumerator_get_Current_m1830000836_gshared (Enumerator_t2809602155 * __this, const MethodInfo* method)
{
	{
		AnimatorClipInfo_t3905751349  L_0 = (AnimatorClipInfo_t3905751349 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  AnimatorClipInfo_t3905751349  Enumerator_get_Current_m1830000836_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2809602155 * _thisAdjusted = reinterpret_cast<Enumerator_t2809602155 *>(__this + 1);
	return Enumerator_get_Current_m1830000836(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2054046066_gshared (Enumerator_t4073335620 * __this, List_1_t243638650 * ___l0, const MethodInfo* method)
{
	{
		List_1_t243638650 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t243638650 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2054046066_AdjustorThunk (Il2CppObject * __this, List_1_t243638650 * ___l0, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	Enumerator__ctor_m2054046066(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1344379320_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1677639504((Enumerator_t4073335620 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1344379320_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1344379320(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3979461448_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3979461448_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1677639504((Enumerator_t4073335620 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Color32_t874517518  L_2 = (Color32_t874517518 )__this->get_current_3();
		Color32_t874517518  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3979461448_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3979461448(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::Dispose()
extern "C"  void Enumerator_Dispose_m1300762389_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t243638650 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1300762389_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	Enumerator_Dispose_m1300762389(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1677639504_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1677639504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t243638650 * L_0 = (List_1_t243638650 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t4073335620  L_1 = (*(Enumerator_t4073335620 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t243638650 * L_7 = (List_1_t243638650 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1677639504_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	Enumerator_VerifyState_m1677639504(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2625246500_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1677639504((Enumerator_t4073335620 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t243638650 * L_2 = (List_1_t243638650 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t243638650 * L_4 = (List_1_t243638650 *)__this->get_l_0();
		NullCheck(L_4);
		Color32U5BU5D_t30278651* L_5 = (Color32U5BU5D_t30278651*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		Color32_t874517518  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2625246500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	return Enumerator_MoveNext_m2625246500(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::get_Current()
extern "C"  Color32_t874517518  Enumerator_get_Current_m1482710541_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	{
		Color32_t874517518  L_0 = (Color32_t874517518 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Color32_t874517518  Enumerator_get_Current_m1482710541_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	return Enumerator_get_Current_m1482710541(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3979168432_gshared (Enumerator_t3220004478 * __this, List_1_t3685274804 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3685274804 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3685274804 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3979168432_AdjustorThunk (Il2CppObject * __this, List_1_t3685274804 * ___l0, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	Enumerator__ctor_m3979168432(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m336811426_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2948867230((Enumerator_t3220004478 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m336811426_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m336811426(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3079057684_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3079057684_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2948867230((Enumerator_t3220004478 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		RaycastResult_t21186376  L_2 = (RaycastResult_t21186376 )__this->get_current_3();
		RaycastResult_t21186376  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3079057684_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3079057684(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C"  void Enumerator_Dispose_m3455280711_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3685274804 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3455280711_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	Enumerator_Dispose_m3455280711(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2948867230_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2948867230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3685274804 * L_0 = (List_1_t3685274804 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3220004478  L_1 = (*(Enumerator_t3220004478 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3685274804 * L_7 = (List_1_t3685274804 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2948867230_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	Enumerator_VerifyState_m2948867230(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2628556578_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2948867230((Enumerator_t3220004478 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3685274804 * L_2 = (List_1_t3685274804 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3685274804 * L_4 = (List_1_t3685274804 *)__this->get_l_0();
		NullCheck(L_4);
		RaycastResultU5BU5D_t603556505* L_5 = (RaycastResultU5BU5D_t603556505*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		RaycastResult_t21186376  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2628556578_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	return Enumerator_MoveNext_m2628556578(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C"  RaycastResult_t21186376  Enumerator_get_Current_m2728219003_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	{
		RaycastResult_t21186376  L_0 = (RaycastResult_t21186376 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  RaycastResult_t21186376  Enumerator_get_Current_m2728219003_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	return Enumerator_get_Current_m2728219003(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
