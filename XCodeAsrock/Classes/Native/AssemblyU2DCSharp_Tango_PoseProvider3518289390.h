﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.PoseProvider
struct  PoseProvider_t3518289390  : public Il2CppObject
{
public:

public:
};

struct PoseProvider_t3518289390_StaticFields
{
public:
	// System.String Tango.PoseProvider::CLASS_NAME
	String_t* ___CLASS_NAME_2;

public:
	inline static int32_t get_offset_of_CLASS_NAME_2() { return static_cast<int32_t>(offsetof(PoseProvider_t3518289390_StaticFields, ___CLASS_NAME_2)); }
	inline String_t* get_CLASS_NAME_2() const { return ___CLASS_NAME_2; }
	inline String_t** get_address_of_CLASS_NAME_2() { return &___CLASS_NAME_2; }
	inline void set_CLASS_NAME_2(String_t* value)
	{
		___CLASS_NAME_2 = value;
		Il2CppCodeGenWriteBarrier(&___CLASS_NAME_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
