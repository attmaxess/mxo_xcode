﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Camera
struct Camera_t189460977;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeshOcclusionCameraDepthTexture
struct  MeshOcclusionCameraDepthTexture_t40066095  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera MeshOcclusionCameraDepthTexture::m_parentCamera
	Camera_t189460977 * ___m_parentCamera_2;
	// UnityEngine.Camera MeshOcclusionCameraDepthTexture::m_camera
	Camera_t189460977 * ___m_camera_3;

public:
	inline static int32_t get_offset_of_m_parentCamera_2() { return static_cast<int32_t>(offsetof(MeshOcclusionCameraDepthTexture_t40066095, ___m_parentCamera_2)); }
	inline Camera_t189460977 * get_m_parentCamera_2() const { return ___m_parentCamera_2; }
	inline Camera_t189460977 ** get_address_of_m_parentCamera_2() { return &___m_parentCamera_2; }
	inline void set_m_parentCamera_2(Camera_t189460977 * value)
	{
		___m_parentCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_parentCamera_2, value);
	}

	inline static int32_t get_offset_of_m_camera_3() { return static_cast<int32_t>(offsetof(MeshOcclusionCameraDepthTexture_t40066095, ___m_camera_3)); }
	inline Camera_t189460977 * get_m_camera_3() const { return ___m_camera_3; }
	inline Camera_t189460977 ** get_address_of_m_camera_3() { return &___m_camera_3; }
	inline void set_m_camera_3(Camera_t189460977 * value)
	{
		___m_camera_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_camera_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
