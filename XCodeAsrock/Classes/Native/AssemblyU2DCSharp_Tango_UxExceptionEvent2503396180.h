﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_Tango_TangoUxEnums_UxExceptionEv2661651178.h"
#include "AssemblyU2DCSharp_Tango_TangoUxEnums_UxExceptionEv1464923600.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.UxExceptionEvent
struct  UxExceptionEvent_t2503396180 
{
public:
	// Tango.TangoUxEnums/UxExceptionEventType Tango.UxExceptionEvent::type
	int32_t ___type_0;
	// System.Single Tango.UxExceptionEvent::value
	float ___value_1;
	// Tango.TangoUxEnums/UxExceptionEventStatus Tango.UxExceptionEvent::status
	int32_t ___status_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(UxExceptionEvent_t2503396180, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(UxExceptionEvent_t2503396180, ___value_1)); }
	inline float get_value_1() const { return ___value_1; }
	inline float* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(float value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(UxExceptionEvent_t2503396180, ___status_2)); }
	inline int32_t get_status_2() const { return ___status_2; }
	inline int32_t* get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(int32_t value)
	{
		___status_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
