﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_APIP1341431879.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_APIPo580197307.h"

// System.Collections.Generic.List`1<Tango.Tango3DReconstruction/GridIndex>
struct List_1_t1004149398;
// System.Object
struct Il2CppObject;
// Tango.OnTango3DReconstructionGridIndiciesDirtyEventHandler
struct OnTango3DReconstructionGridIndiciesDirtyEventHandler_t4194210309;
// System.Single[]
struct SingleU5BU5D_t577127397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.Tango3DReconstruction
struct  Tango3DReconstruction_t1839722436  : public Il2CppObject
{
public:
	// System.Boolean Tango.Tango3DReconstruction::m_useAreaDescriptionPose
	bool ___m_useAreaDescriptionPose_0;
	// System.Boolean Tango.Tango3DReconstruction::m_sendColorToUpdate
	bool ___m_sendColorToUpdate_1;
	// System.IntPtr Tango.Tango3DReconstruction::m_context
	IntPtr_t ___m_context_2;
	// System.Collections.Generic.List`1<Tango.Tango3DReconstruction/GridIndex> Tango.Tango3DReconstruction::m_updatedIndices
	List_1_t1004149398 * ___m_updatedIndices_3;
	// System.Object Tango.Tango3DReconstruction::m_lockObject
	Il2CppObject * ___m_lockObject_4;
	// Tango.OnTango3DReconstructionGridIndiciesDirtyEventHandler Tango.Tango3DReconstruction::m_onGridIndicesDirty
	OnTango3DReconstructionGridIndiciesDirtyEventHandler_t4194210309 * ___m_onGridIndicesDirty_5;
	// UnityEngine.Matrix4x4 Tango.Tango3DReconstruction::m_device_T_depthCamera
	Matrix4x4_t2933234003  ___m_device_T_depthCamera_6;
	// UnityEngine.Matrix4x4 Tango.Tango3DReconstruction::m_device_T_colorCamera
	Matrix4x4_t2933234003  ___m_device_T_colorCamera_7;
	// Tango.Tango3DReconstruction/APIPointCloud Tango.Tango3DReconstruction::m_mostRecentDepth
	APIPointCloud_t1341431879  ___m_mostRecentDepth_8;
	// System.Single[] Tango.Tango3DReconstruction::m_mostRecentDepthPoints
	SingleU5BU5D_t577127397* ___m_mostRecentDepthPoints_9;
	// Tango.Tango3DReconstruction/APIPose Tango.Tango3DReconstruction::m_mostRecentDepthPose
	APIPose_t580197307  ___m_mostRecentDepthPose_10;
	// System.Boolean Tango.Tango3DReconstruction::m_mostRecentDepthIsValid
	bool ___m_mostRecentDepthIsValid_11;
	// System.Boolean Tango.Tango3DReconstruction::m_enabled
	bool ___m_enabled_12;

public:
	inline static int32_t get_offset_of_m_useAreaDescriptionPose_0() { return static_cast<int32_t>(offsetof(Tango3DReconstruction_t1839722436, ___m_useAreaDescriptionPose_0)); }
	inline bool get_m_useAreaDescriptionPose_0() const { return ___m_useAreaDescriptionPose_0; }
	inline bool* get_address_of_m_useAreaDescriptionPose_0() { return &___m_useAreaDescriptionPose_0; }
	inline void set_m_useAreaDescriptionPose_0(bool value)
	{
		___m_useAreaDescriptionPose_0 = value;
	}

	inline static int32_t get_offset_of_m_sendColorToUpdate_1() { return static_cast<int32_t>(offsetof(Tango3DReconstruction_t1839722436, ___m_sendColorToUpdate_1)); }
	inline bool get_m_sendColorToUpdate_1() const { return ___m_sendColorToUpdate_1; }
	inline bool* get_address_of_m_sendColorToUpdate_1() { return &___m_sendColorToUpdate_1; }
	inline void set_m_sendColorToUpdate_1(bool value)
	{
		___m_sendColorToUpdate_1 = value;
	}

	inline static int32_t get_offset_of_m_context_2() { return static_cast<int32_t>(offsetof(Tango3DReconstruction_t1839722436, ___m_context_2)); }
	inline IntPtr_t get_m_context_2() const { return ___m_context_2; }
	inline IntPtr_t* get_address_of_m_context_2() { return &___m_context_2; }
	inline void set_m_context_2(IntPtr_t value)
	{
		___m_context_2 = value;
	}

	inline static int32_t get_offset_of_m_updatedIndices_3() { return static_cast<int32_t>(offsetof(Tango3DReconstruction_t1839722436, ___m_updatedIndices_3)); }
	inline List_1_t1004149398 * get_m_updatedIndices_3() const { return ___m_updatedIndices_3; }
	inline List_1_t1004149398 ** get_address_of_m_updatedIndices_3() { return &___m_updatedIndices_3; }
	inline void set_m_updatedIndices_3(List_1_t1004149398 * value)
	{
		___m_updatedIndices_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_updatedIndices_3, value);
	}

	inline static int32_t get_offset_of_m_lockObject_4() { return static_cast<int32_t>(offsetof(Tango3DReconstruction_t1839722436, ___m_lockObject_4)); }
	inline Il2CppObject * get_m_lockObject_4() const { return ___m_lockObject_4; }
	inline Il2CppObject ** get_address_of_m_lockObject_4() { return &___m_lockObject_4; }
	inline void set_m_lockObject_4(Il2CppObject * value)
	{
		___m_lockObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_lockObject_4, value);
	}

	inline static int32_t get_offset_of_m_onGridIndicesDirty_5() { return static_cast<int32_t>(offsetof(Tango3DReconstruction_t1839722436, ___m_onGridIndicesDirty_5)); }
	inline OnTango3DReconstructionGridIndiciesDirtyEventHandler_t4194210309 * get_m_onGridIndicesDirty_5() const { return ___m_onGridIndicesDirty_5; }
	inline OnTango3DReconstructionGridIndiciesDirtyEventHandler_t4194210309 ** get_address_of_m_onGridIndicesDirty_5() { return &___m_onGridIndicesDirty_5; }
	inline void set_m_onGridIndicesDirty_5(OnTango3DReconstructionGridIndiciesDirtyEventHandler_t4194210309 * value)
	{
		___m_onGridIndicesDirty_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_onGridIndicesDirty_5, value);
	}

	inline static int32_t get_offset_of_m_device_T_depthCamera_6() { return static_cast<int32_t>(offsetof(Tango3DReconstruction_t1839722436, ___m_device_T_depthCamera_6)); }
	inline Matrix4x4_t2933234003  get_m_device_T_depthCamera_6() const { return ___m_device_T_depthCamera_6; }
	inline Matrix4x4_t2933234003 * get_address_of_m_device_T_depthCamera_6() { return &___m_device_T_depthCamera_6; }
	inline void set_m_device_T_depthCamera_6(Matrix4x4_t2933234003  value)
	{
		___m_device_T_depthCamera_6 = value;
	}

	inline static int32_t get_offset_of_m_device_T_colorCamera_7() { return static_cast<int32_t>(offsetof(Tango3DReconstruction_t1839722436, ___m_device_T_colorCamera_7)); }
	inline Matrix4x4_t2933234003  get_m_device_T_colorCamera_7() const { return ___m_device_T_colorCamera_7; }
	inline Matrix4x4_t2933234003 * get_address_of_m_device_T_colorCamera_7() { return &___m_device_T_colorCamera_7; }
	inline void set_m_device_T_colorCamera_7(Matrix4x4_t2933234003  value)
	{
		___m_device_T_colorCamera_7 = value;
	}

	inline static int32_t get_offset_of_m_mostRecentDepth_8() { return static_cast<int32_t>(offsetof(Tango3DReconstruction_t1839722436, ___m_mostRecentDepth_8)); }
	inline APIPointCloud_t1341431879  get_m_mostRecentDepth_8() const { return ___m_mostRecentDepth_8; }
	inline APIPointCloud_t1341431879 * get_address_of_m_mostRecentDepth_8() { return &___m_mostRecentDepth_8; }
	inline void set_m_mostRecentDepth_8(APIPointCloud_t1341431879  value)
	{
		___m_mostRecentDepth_8 = value;
	}

	inline static int32_t get_offset_of_m_mostRecentDepthPoints_9() { return static_cast<int32_t>(offsetof(Tango3DReconstruction_t1839722436, ___m_mostRecentDepthPoints_9)); }
	inline SingleU5BU5D_t577127397* get_m_mostRecentDepthPoints_9() const { return ___m_mostRecentDepthPoints_9; }
	inline SingleU5BU5D_t577127397** get_address_of_m_mostRecentDepthPoints_9() { return &___m_mostRecentDepthPoints_9; }
	inline void set_m_mostRecentDepthPoints_9(SingleU5BU5D_t577127397* value)
	{
		___m_mostRecentDepthPoints_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_mostRecentDepthPoints_9, value);
	}

	inline static int32_t get_offset_of_m_mostRecentDepthPose_10() { return static_cast<int32_t>(offsetof(Tango3DReconstruction_t1839722436, ___m_mostRecentDepthPose_10)); }
	inline APIPose_t580197307  get_m_mostRecentDepthPose_10() const { return ___m_mostRecentDepthPose_10; }
	inline APIPose_t580197307 * get_address_of_m_mostRecentDepthPose_10() { return &___m_mostRecentDepthPose_10; }
	inline void set_m_mostRecentDepthPose_10(APIPose_t580197307  value)
	{
		___m_mostRecentDepthPose_10 = value;
	}

	inline static int32_t get_offset_of_m_mostRecentDepthIsValid_11() { return static_cast<int32_t>(offsetof(Tango3DReconstruction_t1839722436, ___m_mostRecentDepthIsValid_11)); }
	inline bool get_m_mostRecentDepthIsValid_11() const { return ___m_mostRecentDepthIsValid_11; }
	inline bool* get_address_of_m_mostRecentDepthIsValid_11() { return &___m_mostRecentDepthIsValid_11; }
	inline void set_m_mostRecentDepthIsValid_11(bool value)
	{
		___m_mostRecentDepthIsValid_11 = value;
	}

	inline static int32_t get_offset_of_m_enabled_12() { return static_cast<int32_t>(offsetof(Tango3DReconstruction_t1839722436, ___m_enabled_12)); }
	inline bool get_m_enabled_12() const { return ___m_enabled_12; }
	inline bool* get_address_of_m_enabled_12() { return &___m_enabled_12; }
	inline void set_m_enabled_12(bool value)
	{
		___m_enabled_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
