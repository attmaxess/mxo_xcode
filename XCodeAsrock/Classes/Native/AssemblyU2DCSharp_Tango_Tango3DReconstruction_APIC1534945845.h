﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.Tango3DReconstruction/APICameraCalibration
struct  APICameraCalibration_t1534945845 
{
public:
	// System.Int32 Tango.Tango3DReconstruction/APICameraCalibration::calibration_type
	int32_t ___calibration_type_0;
	// System.UInt32 Tango.Tango3DReconstruction/APICameraCalibration::width
	uint32_t ___width_1;
	// System.UInt32 Tango.Tango3DReconstruction/APICameraCalibration::height
	uint32_t ___height_2;
	// System.Double Tango.Tango3DReconstruction/APICameraCalibration::fx
	double ___fx_3;
	// System.Double Tango.Tango3DReconstruction/APICameraCalibration::fy
	double ___fy_4;
	// System.Double Tango.Tango3DReconstruction/APICameraCalibration::cx
	double ___cx_5;
	// System.Double Tango.Tango3DReconstruction/APICameraCalibration::cy
	double ___cy_6;
	// System.Double Tango.Tango3DReconstruction/APICameraCalibration::distortion0
	double ___distortion0_7;
	// System.Double Tango.Tango3DReconstruction/APICameraCalibration::distortion1
	double ___distortion1_8;
	// System.Double Tango.Tango3DReconstruction/APICameraCalibration::distortion2
	double ___distortion2_9;
	// System.Double Tango.Tango3DReconstruction/APICameraCalibration::distortion3
	double ___distortion3_10;
	// System.Double Tango.Tango3DReconstruction/APICameraCalibration::distortion4
	double ___distortion4_11;

public:
	inline static int32_t get_offset_of_calibration_type_0() { return static_cast<int32_t>(offsetof(APICameraCalibration_t1534945845, ___calibration_type_0)); }
	inline int32_t get_calibration_type_0() const { return ___calibration_type_0; }
	inline int32_t* get_address_of_calibration_type_0() { return &___calibration_type_0; }
	inline void set_calibration_type_0(int32_t value)
	{
		___calibration_type_0 = value;
	}

	inline static int32_t get_offset_of_width_1() { return static_cast<int32_t>(offsetof(APICameraCalibration_t1534945845, ___width_1)); }
	inline uint32_t get_width_1() const { return ___width_1; }
	inline uint32_t* get_address_of_width_1() { return &___width_1; }
	inline void set_width_1(uint32_t value)
	{
		___width_1 = value;
	}

	inline static int32_t get_offset_of_height_2() { return static_cast<int32_t>(offsetof(APICameraCalibration_t1534945845, ___height_2)); }
	inline uint32_t get_height_2() const { return ___height_2; }
	inline uint32_t* get_address_of_height_2() { return &___height_2; }
	inline void set_height_2(uint32_t value)
	{
		___height_2 = value;
	}

	inline static int32_t get_offset_of_fx_3() { return static_cast<int32_t>(offsetof(APICameraCalibration_t1534945845, ___fx_3)); }
	inline double get_fx_3() const { return ___fx_3; }
	inline double* get_address_of_fx_3() { return &___fx_3; }
	inline void set_fx_3(double value)
	{
		___fx_3 = value;
	}

	inline static int32_t get_offset_of_fy_4() { return static_cast<int32_t>(offsetof(APICameraCalibration_t1534945845, ___fy_4)); }
	inline double get_fy_4() const { return ___fy_4; }
	inline double* get_address_of_fy_4() { return &___fy_4; }
	inline void set_fy_4(double value)
	{
		___fy_4 = value;
	}

	inline static int32_t get_offset_of_cx_5() { return static_cast<int32_t>(offsetof(APICameraCalibration_t1534945845, ___cx_5)); }
	inline double get_cx_5() const { return ___cx_5; }
	inline double* get_address_of_cx_5() { return &___cx_5; }
	inline void set_cx_5(double value)
	{
		___cx_5 = value;
	}

	inline static int32_t get_offset_of_cy_6() { return static_cast<int32_t>(offsetof(APICameraCalibration_t1534945845, ___cy_6)); }
	inline double get_cy_6() const { return ___cy_6; }
	inline double* get_address_of_cy_6() { return &___cy_6; }
	inline void set_cy_6(double value)
	{
		___cy_6 = value;
	}

	inline static int32_t get_offset_of_distortion0_7() { return static_cast<int32_t>(offsetof(APICameraCalibration_t1534945845, ___distortion0_7)); }
	inline double get_distortion0_7() const { return ___distortion0_7; }
	inline double* get_address_of_distortion0_7() { return &___distortion0_7; }
	inline void set_distortion0_7(double value)
	{
		___distortion0_7 = value;
	}

	inline static int32_t get_offset_of_distortion1_8() { return static_cast<int32_t>(offsetof(APICameraCalibration_t1534945845, ___distortion1_8)); }
	inline double get_distortion1_8() const { return ___distortion1_8; }
	inline double* get_address_of_distortion1_8() { return &___distortion1_8; }
	inline void set_distortion1_8(double value)
	{
		___distortion1_8 = value;
	}

	inline static int32_t get_offset_of_distortion2_9() { return static_cast<int32_t>(offsetof(APICameraCalibration_t1534945845, ___distortion2_9)); }
	inline double get_distortion2_9() const { return ___distortion2_9; }
	inline double* get_address_of_distortion2_9() { return &___distortion2_9; }
	inline void set_distortion2_9(double value)
	{
		___distortion2_9 = value;
	}

	inline static int32_t get_offset_of_distortion3_10() { return static_cast<int32_t>(offsetof(APICameraCalibration_t1534945845, ___distortion3_10)); }
	inline double get_distortion3_10() const { return ___distortion3_10; }
	inline double* get_address_of_distortion3_10() { return &___distortion3_10; }
	inline void set_distortion3_10(double value)
	{
		___distortion3_10 = value;
	}

	inline static int32_t get_offset_of_distortion4_11() { return static_cast<int32_t>(offsetof(APICameraCalibration_t1534945845, ___distortion4_11)); }
	inline double get_distortion4_11() const { return ___distortion4_11; }
	inline double* get_address_of_distortion4_11() { return &___distortion4_11; }
	inline void set_distortion4_11(double value)
	{
		___distortion4_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
