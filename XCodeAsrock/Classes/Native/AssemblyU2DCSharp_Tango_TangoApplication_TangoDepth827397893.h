﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Tango.TangoApplication/TangoApplicationState
struct TangoApplicationState_t848537215;
// Tango.IDepthListenerWrapper
struct IDepthListenerWrapper_t3650543163;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoApplication/TangoDepthCameraManager
struct  TangoDepthCameraManager_t827397893  : public Il2CppObject
{
public:
	// Tango.TangoApplication/TangoApplicationState Tango.TangoApplication/TangoDepthCameraManager::m_tangoApplicationState
	TangoApplicationState_t848537215 * ___m_tangoApplicationState_0;
	// Tango.IDepthListenerWrapper Tango.TangoApplication/TangoDepthCameraManager::m_depthListener
	Il2CppObject * ___m_depthListener_1;
	// System.Int32 Tango.TangoApplication/TangoDepthCameraManager::<LastSetDepthCameraRate>k__BackingField
	int32_t ___U3CLastSetDepthCameraRateU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_m_tangoApplicationState_0() { return static_cast<int32_t>(offsetof(TangoDepthCameraManager_t827397893, ___m_tangoApplicationState_0)); }
	inline TangoApplicationState_t848537215 * get_m_tangoApplicationState_0() const { return ___m_tangoApplicationState_0; }
	inline TangoApplicationState_t848537215 ** get_address_of_m_tangoApplicationState_0() { return &___m_tangoApplicationState_0; }
	inline void set_m_tangoApplicationState_0(TangoApplicationState_t848537215 * value)
	{
		___m_tangoApplicationState_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplicationState_0, value);
	}

	inline static int32_t get_offset_of_m_depthListener_1() { return static_cast<int32_t>(offsetof(TangoDepthCameraManager_t827397893, ___m_depthListener_1)); }
	inline Il2CppObject * get_m_depthListener_1() const { return ___m_depthListener_1; }
	inline Il2CppObject ** get_address_of_m_depthListener_1() { return &___m_depthListener_1; }
	inline void set_m_depthListener_1(Il2CppObject * value)
	{
		___m_depthListener_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_depthListener_1, value);
	}

	inline static int32_t get_offset_of_U3CLastSetDepthCameraRateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TangoDepthCameraManager_t827397893, ___U3CLastSetDepthCameraRateU3Ek__BackingField_2)); }
	inline int32_t get_U3CLastSetDepthCameraRateU3Ek__BackingField_2() const { return ___U3CLastSetDepthCameraRateU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CLastSetDepthCameraRateU3Ek__BackingField_2() { return &___U3CLastSetDepthCameraRateU3Ek__BackingField_2; }
	inline void set_U3CLastSetDepthCameraRateU3Ek__BackingField_2(int32_t value)
	{
		___U3CLastSetDepthCameraRateU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
