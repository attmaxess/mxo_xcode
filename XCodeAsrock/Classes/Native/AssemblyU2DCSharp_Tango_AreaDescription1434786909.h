﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.AreaDescription
struct  AreaDescription_t1434786909  : public Il2CppObject
{
public:
	// System.String Tango.AreaDescription::m_uuid
	String_t* ___m_uuid_0;

public:
	inline static int32_t get_offset_of_m_uuid_0() { return static_cast<int32_t>(offsetof(AreaDescription_t1434786909, ___m_uuid_0)); }
	inline String_t* get_m_uuid_0() const { return ___m_uuid_0; }
	inline String_t** get_address_of_m_uuid_0() { return &___m_uuid_0; }
	inline void set_m_uuid_0(String_t* value)
	{
		___m_uuid_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_uuid_0, value);
	}
};

struct AreaDescription_t1434786909_StaticFields
{
public:
	// System.DateTime Tango.AreaDescription::METADATA_EPOCH
	DateTime_t693205669  ___METADATA_EPOCH_4;

public:
	inline static int32_t get_offset_of_METADATA_EPOCH_4() { return static_cast<int32_t>(offsetof(AreaDescription_t1434786909_StaticFields, ___METADATA_EPOCH_4)); }
	inline DateTime_t693205669  get_METADATA_EPOCH_4() const { return ___METADATA_EPOCH_4; }
	inline DateTime_t693205669 * get_address_of_METADATA_EPOCH_4() { return &___METADATA_EPOCH_4; }
	inline void set_METADATA_EPOCH_4(DateTime_t693205669  value)
	{
		___METADATA_EPOCH_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
