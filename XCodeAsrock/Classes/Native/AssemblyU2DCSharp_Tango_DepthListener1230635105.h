﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// Tango.DepthProvider/APIOnPointCloudAvailable
struct APIOnPointCloudAvailable_t1407154221;
// Tango.TangoPointCloudData
struct TangoPointCloudData_t511956092;
// System.Single[]
struct SingleU5BU5D_t577127397;
// Tango.OnTangoDepthAvailableEventHandler
struct OnTangoDepthAvailableEventHandler_t2179186224;
// Tango.OnTangoDepthMulithreadedAvailableEventHandler
struct OnTangoDepthMulithreadedAvailableEventHandler_t82925278;
// Tango.OnPointCloudAvailableEventHandler
struct OnPointCloudAvailableEventHandler_t934212769;
// Tango.OnPointCloudMultithreadedAvailableEventHandler
struct OnPointCloudMultithreadedAvailableEventHandler_t3985220823;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.DepthListener
struct  DepthListener_t1230635105  : public Il2CppObject
{
public:

public:
};

struct DepthListener_t1230635105_StaticFields
{
public:
	// System.Object Tango.DepthListener::m_lockObject
	Il2CppObject * ___m_lockObject_0;
	// Tango.DepthProvider/APIOnPointCloudAvailable Tango.DepthListener::m_onPointCloudAvailableCallback
	APIOnPointCloudAvailable_t1407154221 * ___m_onPointCloudAvailableCallback_1;
	// System.Boolean Tango.DepthListener::m_isDirty
	bool ___m_isDirty_2;
	// Tango.TangoPointCloudData Tango.DepthListener::m_pointCloud
	TangoPointCloudData_t511956092 * ___m_pointCloud_3;
	// System.Single[] Tango.DepthListener::m_xyzPoints
	SingleU5BU5D_t577127397* ___m_xyzPoints_4;
	// System.Int32 Tango.DepthListener::m_maxNumReducedDepthPoints
	int32_t ___m_maxNumReducedDepthPoints_5;
	// Tango.OnTangoDepthAvailableEventHandler Tango.DepthListener::m_onTangoDepthAvailable
	OnTangoDepthAvailableEventHandler_t2179186224 * ___m_onTangoDepthAvailable_6;
	// Tango.OnTangoDepthMulithreadedAvailableEventHandler Tango.DepthListener::m_onTangoDepthMultithreadedAvailable
	OnTangoDepthMulithreadedAvailableEventHandler_t82925278 * ___m_onTangoDepthMultithreadedAvailable_7;
	// Tango.OnPointCloudAvailableEventHandler Tango.DepthListener::m_onPointCloudAvailable
	OnPointCloudAvailableEventHandler_t934212769 * ___m_onPointCloudAvailable_8;
	// Tango.OnPointCloudMultithreadedAvailableEventHandler Tango.DepthListener::m_onPointCloudMultithreadedAvailable
	OnPointCloudMultithreadedAvailableEventHandler_t3985220823 * ___m_onPointCloudMultithreadedAvailable_9;

public:
	inline static int32_t get_offset_of_m_lockObject_0() { return static_cast<int32_t>(offsetof(DepthListener_t1230635105_StaticFields, ___m_lockObject_0)); }
	inline Il2CppObject * get_m_lockObject_0() const { return ___m_lockObject_0; }
	inline Il2CppObject ** get_address_of_m_lockObject_0() { return &___m_lockObject_0; }
	inline void set_m_lockObject_0(Il2CppObject * value)
	{
		___m_lockObject_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_lockObject_0, value);
	}

	inline static int32_t get_offset_of_m_onPointCloudAvailableCallback_1() { return static_cast<int32_t>(offsetof(DepthListener_t1230635105_StaticFields, ___m_onPointCloudAvailableCallback_1)); }
	inline APIOnPointCloudAvailable_t1407154221 * get_m_onPointCloudAvailableCallback_1() const { return ___m_onPointCloudAvailableCallback_1; }
	inline APIOnPointCloudAvailable_t1407154221 ** get_address_of_m_onPointCloudAvailableCallback_1() { return &___m_onPointCloudAvailableCallback_1; }
	inline void set_m_onPointCloudAvailableCallback_1(APIOnPointCloudAvailable_t1407154221 * value)
	{
		___m_onPointCloudAvailableCallback_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_onPointCloudAvailableCallback_1, value);
	}

	inline static int32_t get_offset_of_m_isDirty_2() { return static_cast<int32_t>(offsetof(DepthListener_t1230635105_StaticFields, ___m_isDirty_2)); }
	inline bool get_m_isDirty_2() const { return ___m_isDirty_2; }
	inline bool* get_address_of_m_isDirty_2() { return &___m_isDirty_2; }
	inline void set_m_isDirty_2(bool value)
	{
		___m_isDirty_2 = value;
	}

	inline static int32_t get_offset_of_m_pointCloud_3() { return static_cast<int32_t>(offsetof(DepthListener_t1230635105_StaticFields, ___m_pointCloud_3)); }
	inline TangoPointCloudData_t511956092 * get_m_pointCloud_3() const { return ___m_pointCloud_3; }
	inline TangoPointCloudData_t511956092 ** get_address_of_m_pointCloud_3() { return &___m_pointCloud_3; }
	inline void set_m_pointCloud_3(TangoPointCloudData_t511956092 * value)
	{
		___m_pointCloud_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_pointCloud_3, value);
	}

	inline static int32_t get_offset_of_m_xyzPoints_4() { return static_cast<int32_t>(offsetof(DepthListener_t1230635105_StaticFields, ___m_xyzPoints_4)); }
	inline SingleU5BU5D_t577127397* get_m_xyzPoints_4() const { return ___m_xyzPoints_4; }
	inline SingleU5BU5D_t577127397** get_address_of_m_xyzPoints_4() { return &___m_xyzPoints_4; }
	inline void set_m_xyzPoints_4(SingleU5BU5D_t577127397* value)
	{
		___m_xyzPoints_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_xyzPoints_4, value);
	}

	inline static int32_t get_offset_of_m_maxNumReducedDepthPoints_5() { return static_cast<int32_t>(offsetof(DepthListener_t1230635105_StaticFields, ___m_maxNumReducedDepthPoints_5)); }
	inline int32_t get_m_maxNumReducedDepthPoints_5() const { return ___m_maxNumReducedDepthPoints_5; }
	inline int32_t* get_address_of_m_maxNumReducedDepthPoints_5() { return &___m_maxNumReducedDepthPoints_5; }
	inline void set_m_maxNumReducedDepthPoints_5(int32_t value)
	{
		___m_maxNumReducedDepthPoints_5 = value;
	}

	inline static int32_t get_offset_of_m_onTangoDepthAvailable_6() { return static_cast<int32_t>(offsetof(DepthListener_t1230635105_StaticFields, ___m_onTangoDepthAvailable_6)); }
	inline OnTangoDepthAvailableEventHandler_t2179186224 * get_m_onTangoDepthAvailable_6() const { return ___m_onTangoDepthAvailable_6; }
	inline OnTangoDepthAvailableEventHandler_t2179186224 ** get_address_of_m_onTangoDepthAvailable_6() { return &___m_onTangoDepthAvailable_6; }
	inline void set_m_onTangoDepthAvailable_6(OnTangoDepthAvailableEventHandler_t2179186224 * value)
	{
		___m_onTangoDepthAvailable_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_onTangoDepthAvailable_6, value);
	}

	inline static int32_t get_offset_of_m_onTangoDepthMultithreadedAvailable_7() { return static_cast<int32_t>(offsetof(DepthListener_t1230635105_StaticFields, ___m_onTangoDepthMultithreadedAvailable_7)); }
	inline OnTangoDepthMulithreadedAvailableEventHandler_t82925278 * get_m_onTangoDepthMultithreadedAvailable_7() const { return ___m_onTangoDepthMultithreadedAvailable_7; }
	inline OnTangoDepthMulithreadedAvailableEventHandler_t82925278 ** get_address_of_m_onTangoDepthMultithreadedAvailable_7() { return &___m_onTangoDepthMultithreadedAvailable_7; }
	inline void set_m_onTangoDepthMultithreadedAvailable_7(OnTangoDepthMulithreadedAvailableEventHandler_t82925278 * value)
	{
		___m_onTangoDepthMultithreadedAvailable_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_onTangoDepthMultithreadedAvailable_7, value);
	}

	inline static int32_t get_offset_of_m_onPointCloudAvailable_8() { return static_cast<int32_t>(offsetof(DepthListener_t1230635105_StaticFields, ___m_onPointCloudAvailable_8)); }
	inline OnPointCloudAvailableEventHandler_t934212769 * get_m_onPointCloudAvailable_8() const { return ___m_onPointCloudAvailable_8; }
	inline OnPointCloudAvailableEventHandler_t934212769 ** get_address_of_m_onPointCloudAvailable_8() { return &___m_onPointCloudAvailable_8; }
	inline void set_m_onPointCloudAvailable_8(OnPointCloudAvailableEventHandler_t934212769 * value)
	{
		___m_onPointCloudAvailable_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_onPointCloudAvailable_8, value);
	}

	inline static int32_t get_offset_of_m_onPointCloudMultithreadedAvailable_9() { return static_cast<int32_t>(offsetof(DepthListener_t1230635105_StaticFields, ___m_onPointCloudMultithreadedAvailable_9)); }
	inline OnPointCloudMultithreadedAvailableEventHandler_t3985220823 * get_m_onPointCloudMultithreadedAvailable_9() const { return ___m_onPointCloudMultithreadedAvailable_9; }
	inline OnPointCloudMultithreadedAvailableEventHandler_t3985220823 ** get_address_of_m_onPointCloudMultithreadedAvailable_9() { return &___m_onPointCloudMultithreadedAvailable_9; }
	inline void set_m_onPointCloudMultithreadedAvailable_9(OnPointCloudMultithreadedAvailableEventHandler_t3985220823 * value)
	{
		___m_onPointCloudMultithreadedAvailable_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_onPointCloudMultithreadedAvailable_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
