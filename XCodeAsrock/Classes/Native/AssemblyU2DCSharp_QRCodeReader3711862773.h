﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// OnFoundPlaneVector
struct OnFoundPlaneVector_t1442898976;
// OnFoundPlane
struct OnFoundPlane_t92057343;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct UnityARSessionNativeInterface_t1130867170;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QRCodeReader
struct  QRCodeReader_t3711862773  : public MonoBehaviour_t1158329972
{
public:
	// OnFoundPlaneVector QRCodeReader::CallBackOnFoundPlaneVector
	OnFoundPlaneVector_t1442898976 * ___CallBackOnFoundPlaneVector_2;
	// OnFoundPlane QRCodeReader::CallBackOnFoundPlane
	OnFoundPlane_t92057343 * ___CallBackOnFoundPlane_3;
	// System.Boolean QRCodeReader::doneScanQRCode
	bool ___doneScanQRCode_4;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface QRCodeReader::arSession
	UnityARSessionNativeInterface_t1130867170 * ___arSession_5;
	// UnityEngine.GameObject QRCodeReader::qrcodePlane
	GameObject_t1756533147 * ___qrcodePlane_6;
	// UnityEngine.GameObject QRCodeReader::plane
	GameObject_t1756533147 * ___plane_7;

public:
	inline static int32_t get_offset_of_CallBackOnFoundPlaneVector_2() { return static_cast<int32_t>(offsetof(QRCodeReader_t3711862773, ___CallBackOnFoundPlaneVector_2)); }
	inline OnFoundPlaneVector_t1442898976 * get_CallBackOnFoundPlaneVector_2() const { return ___CallBackOnFoundPlaneVector_2; }
	inline OnFoundPlaneVector_t1442898976 ** get_address_of_CallBackOnFoundPlaneVector_2() { return &___CallBackOnFoundPlaneVector_2; }
	inline void set_CallBackOnFoundPlaneVector_2(OnFoundPlaneVector_t1442898976 * value)
	{
		___CallBackOnFoundPlaneVector_2 = value;
		Il2CppCodeGenWriteBarrier(&___CallBackOnFoundPlaneVector_2, value);
	}

	inline static int32_t get_offset_of_CallBackOnFoundPlane_3() { return static_cast<int32_t>(offsetof(QRCodeReader_t3711862773, ___CallBackOnFoundPlane_3)); }
	inline OnFoundPlane_t92057343 * get_CallBackOnFoundPlane_3() const { return ___CallBackOnFoundPlane_3; }
	inline OnFoundPlane_t92057343 ** get_address_of_CallBackOnFoundPlane_3() { return &___CallBackOnFoundPlane_3; }
	inline void set_CallBackOnFoundPlane_3(OnFoundPlane_t92057343 * value)
	{
		___CallBackOnFoundPlane_3 = value;
		Il2CppCodeGenWriteBarrier(&___CallBackOnFoundPlane_3, value);
	}

	inline static int32_t get_offset_of_doneScanQRCode_4() { return static_cast<int32_t>(offsetof(QRCodeReader_t3711862773, ___doneScanQRCode_4)); }
	inline bool get_doneScanQRCode_4() const { return ___doneScanQRCode_4; }
	inline bool* get_address_of_doneScanQRCode_4() { return &___doneScanQRCode_4; }
	inline void set_doneScanQRCode_4(bool value)
	{
		___doneScanQRCode_4 = value;
	}

	inline static int32_t get_offset_of_arSession_5() { return static_cast<int32_t>(offsetof(QRCodeReader_t3711862773, ___arSession_5)); }
	inline UnityARSessionNativeInterface_t1130867170 * get_arSession_5() const { return ___arSession_5; }
	inline UnityARSessionNativeInterface_t1130867170 ** get_address_of_arSession_5() { return &___arSession_5; }
	inline void set_arSession_5(UnityARSessionNativeInterface_t1130867170 * value)
	{
		___arSession_5 = value;
		Il2CppCodeGenWriteBarrier(&___arSession_5, value);
	}

	inline static int32_t get_offset_of_qrcodePlane_6() { return static_cast<int32_t>(offsetof(QRCodeReader_t3711862773, ___qrcodePlane_6)); }
	inline GameObject_t1756533147 * get_qrcodePlane_6() const { return ___qrcodePlane_6; }
	inline GameObject_t1756533147 ** get_address_of_qrcodePlane_6() { return &___qrcodePlane_6; }
	inline void set_qrcodePlane_6(GameObject_t1756533147 * value)
	{
		___qrcodePlane_6 = value;
		Il2CppCodeGenWriteBarrier(&___qrcodePlane_6, value);
	}

	inline static int32_t get_offset_of_plane_7() { return static_cast<int32_t>(offsetof(QRCodeReader_t3711862773, ___plane_7)); }
	inline GameObject_t1756533147 * get_plane_7() const { return ___plane_7; }
	inline GameObject_t1756533147 ** get_address_of_plane_7() { return &___plane_7; }
	inline void set_plane_7(GameObject_t1756533147 * value)
	{
		___plane_7 = value;
		Il2CppCodeGenWriteBarrier(&___plane_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
