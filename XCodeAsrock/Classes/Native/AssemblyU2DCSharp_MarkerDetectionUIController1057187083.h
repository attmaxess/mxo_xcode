﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t3671312409;
// System.Collections.Generic.List`1<Tango.TangoSupport/Marker>
struct List_1_t531672424;
// Tango.TangoApplication
struct TangoApplication_t278578959;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerDetectionUIController
struct  MarkerDetectionUIController_t1057187083  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MarkerDetectionUIController::m_markerPrefab
	GameObject_t1756533147 * ___m_markerPrefab_2;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> MarkerDetectionUIController::m_markerObjects
	Dictionary_2_t3671312409 * ___m_markerObjects_4;
	// System.Collections.Generic.List`1<Tango.TangoSupport/Marker> MarkerDetectionUIController::m_markerList
	List_1_t531672424 * ___m_markerList_5;
	// Tango.TangoApplication MarkerDetectionUIController::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_6;

public:
	inline static int32_t get_offset_of_m_markerPrefab_2() { return static_cast<int32_t>(offsetof(MarkerDetectionUIController_t1057187083, ___m_markerPrefab_2)); }
	inline GameObject_t1756533147 * get_m_markerPrefab_2() const { return ___m_markerPrefab_2; }
	inline GameObject_t1756533147 ** get_address_of_m_markerPrefab_2() { return &___m_markerPrefab_2; }
	inline void set_m_markerPrefab_2(GameObject_t1756533147 * value)
	{
		___m_markerPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_markerPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_markerObjects_4() { return static_cast<int32_t>(offsetof(MarkerDetectionUIController_t1057187083, ___m_markerObjects_4)); }
	inline Dictionary_2_t3671312409 * get_m_markerObjects_4() const { return ___m_markerObjects_4; }
	inline Dictionary_2_t3671312409 ** get_address_of_m_markerObjects_4() { return &___m_markerObjects_4; }
	inline void set_m_markerObjects_4(Dictionary_2_t3671312409 * value)
	{
		___m_markerObjects_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_markerObjects_4, value);
	}

	inline static int32_t get_offset_of_m_markerList_5() { return static_cast<int32_t>(offsetof(MarkerDetectionUIController_t1057187083, ___m_markerList_5)); }
	inline List_1_t531672424 * get_m_markerList_5() const { return ___m_markerList_5; }
	inline List_1_t531672424 ** get_address_of_m_markerList_5() { return &___m_markerList_5; }
	inline void set_m_markerList_5(List_1_t531672424 * value)
	{
		___m_markerList_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_markerList_5, value);
	}

	inline static int32_t get_offset_of_m_tangoApplication_6() { return static_cast<int32_t>(offsetof(MarkerDetectionUIController_t1057187083, ___m_tangoApplication_6)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_6() const { return ___m_tangoApplication_6; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_6() { return &___m_tangoApplication_6; }
	inline void set_m_tangoApplication_6(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
