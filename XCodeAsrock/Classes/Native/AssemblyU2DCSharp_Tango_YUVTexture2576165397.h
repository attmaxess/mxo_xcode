﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Texture
struct Texture_t2243626319;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.YUVTexture
struct  YUVTexture_t2576165397  : public Il2CppObject
{
public:
	// UnityEngine.Texture Tango.YUVTexture::m_videoOverlayTextureY
	Texture_t2243626319 * ___m_videoOverlayTextureY_0;
	// UnityEngine.Texture Tango.YUVTexture::m_videoOverlayTextureCb
	Texture_t2243626319 * ___m_videoOverlayTextureCb_1;
	// UnityEngine.Texture Tango.YUVTexture::m_videoOverlayTextureCr
	Texture_t2243626319 * ___m_videoOverlayTextureCr_2;

public:
	inline static int32_t get_offset_of_m_videoOverlayTextureY_0() { return static_cast<int32_t>(offsetof(YUVTexture_t2576165397, ___m_videoOverlayTextureY_0)); }
	inline Texture_t2243626319 * get_m_videoOverlayTextureY_0() const { return ___m_videoOverlayTextureY_0; }
	inline Texture_t2243626319 ** get_address_of_m_videoOverlayTextureY_0() { return &___m_videoOverlayTextureY_0; }
	inline void set_m_videoOverlayTextureY_0(Texture_t2243626319 * value)
	{
		___m_videoOverlayTextureY_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_videoOverlayTextureY_0, value);
	}

	inline static int32_t get_offset_of_m_videoOverlayTextureCb_1() { return static_cast<int32_t>(offsetof(YUVTexture_t2576165397, ___m_videoOverlayTextureCb_1)); }
	inline Texture_t2243626319 * get_m_videoOverlayTextureCb_1() const { return ___m_videoOverlayTextureCb_1; }
	inline Texture_t2243626319 ** get_address_of_m_videoOverlayTextureCb_1() { return &___m_videoOverlayTextureCb_1; }
	inline void set_m_videoOverlayTextureCb_1(Texture_t2243626319 * value)
	{
		___m_videoOverlayTextureCb_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_videoOverlayTextureCb_1, value);
	}

	inline static int32_t get_offset_of_m_videoOverlayTextureCr_2() { return static_cast<int32_t>(offsetof(YUVTexture_t2576165397, ___m_videoOverlayTextureCr_2)); }
	inline Texture_t2243626319 * get_m_videoOverlayTextureCr_2() const { return ___m_videoOverlayTextureCr_2; }
	inline Texture_t2243626319 ** get_address_of_m_videoOverlayTextureCr_2() { return &___m_videoOverlayTextureCr_2; }
	inline void set_m_videoOverlayTextureCr_2(Texture_t2243626319 * value)
	{
		___m_videoOverlayTextureCr_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_videoOverlayTextureCr_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
