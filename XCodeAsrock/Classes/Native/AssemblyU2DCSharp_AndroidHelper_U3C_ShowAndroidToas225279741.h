﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_AndroidHelper_ToastLength3780156981.h"

// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4251328308;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidHelper/<_ShowAndroidToastMessage>c__AnonStorey1
struct  U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t225279741  : public Il2CppObject
{
public:
	// UnityEngine.AndroidJavaObject AndroidHelper/<_ShowAndroidToastMessage>c__AnonStorey1::unityActivity
	AndroidJavaObject_t4251328308 * ___unityActivity_0;
	// System.String AndroidHelper/<_ShowAndroidToastMessage>c__AnonStorey1::message
	String_t* ___message_1;
	// AndroidHelper/ToastLength AndroidHelper/<_ShowAndroidToastMessage>c__AnonStorey1::length
	int32_t ___length_2;

public:
	inline static int32_t get_offset_of_unityActivity_0() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t225279741, ___unityActivity_0)); }
	inline AndroidJavaObject_t4251328308 * get_unityActivity_0() const { return ___unityActivity_0; }
	inline AndroidJavaObject_t4251328308 ** get_address_of_unityActivity_0() { return &___unityActivity_0; }
	inline void set_unityActivity_0(AndroidJavaObject_t4251328308 * value)
	{
		___unityActivity_0 = value;
		Il2CppCodeGenWriteBarrier(&___unityActivity_0, value);
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t225279741, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier(&___message_1, value);
	}

	inline static int32_t get_offset_of_length_2() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t225279741, ___length_2)); }
	inline int32_t get_length_2() const { return ___length_2; }
	inline int32_t* get_address_of_length_2() { return &___length_2; }
	inline void set_length_2(int32_t value)
	{
		___length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
