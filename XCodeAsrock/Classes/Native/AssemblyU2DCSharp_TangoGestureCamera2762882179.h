﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_TangoGestureCamera_CameraType4020249893.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TangoGestureCamera
struct  TangoGestureCamera_t2762882179  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject TangoGestureCamera::m_targetFollowingObject
	GameObject_t1756533147 * ___m_targetFollowingObject_2;
	// System.Boolean TangoGestureCamera::m_enableCameraModeUI
	bool ___m_enableCameraModeUI_3;
	// TangoGestureCamera/CameraType TangoGestureCamera::m_defaultCameraMode
	int32_t ___m_defaultCameraMode_4;
	// UnityEngine.Vector3 TangoGestureCamera::m_curOffset
	Vector3_t2243707580  ___m_curOffset_9;
	// UnityEngine.Vector3 TangoGestureCamera::m_thirdPersonCamOffset
	Vector3_t2243707580  ___m_thirdPersonCamOffset_10;
	// UnityEngine.Vector3 TangoGestureCamera::m_topDownCamOffset
	Vector3_t2243707580  ___m_topDownCamOffset_11;
	// TangoGestureCamera/CameraType TangoGestureCamera::m_currentCamera
	int32_t ___m_currentCamera_12;
	// System.Single TangoGestureCamera::curThirdPersonRotationX
	float ___curThirdPersonRotationX_13;
	// System.Single TangoGestureCamera::curThirdPersonRotationY
	float ___curThirdPersonRotationY_14;
	// System.Single TangoGestureCamera::startThirdPersonRotationX
	float ___startThirdPersonRotationX_15;
	// System.Single TangoGestureCamera::startThirdPersonRotationY
	float ___startThirdPersonRotationY_16;
	// System.Single TangoGestureCamera::startThirdPersonCameraCircleR
	float ___startThirdPersonCameraCircleR_17;
	// System.Single TangoGestureCamera::curThirdPersonCameraCircleR
	float ___curThirdPersonCameraCircleR_18;
	// UnityEngine.Vector2 TangoGestureCamera::touchStartPoint
	Vector2_t2243707579  ___touchStartPoint_19;
	// System.Single TangoGestureCamera::topDownStartY
	float ___topDownStartY_20;
	// System.Single TangoGestureCamera::touchStartDist
	float ___touchStartDist_21;
	// UnityEngine.Vector2 TangoGestureCamera::topDownStartPos
	Vector2_t2243707579  ___topDownStartPos_22;
	// UnityEngine.Vector3 TangoGestureCamera::thirdPersonCamStartOffset
	Vector3_t2243707580  ___thirdPersonCamStartOffset_23;

public:
	inline static int32_t get_offset_of_m_targetFollowingObject_2() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___m_targetFollowingObject_2)); }
	inline GameObject_t1756533147 * get_m_targetFollowingObject_2() const { return ___m_targetFollowingObject_2; }
	inline GameObject_t1756533147 ** get_address_of_m_targetFollowingObject_2() { return &___m_targetFollowingObject_2; }
	inline void set_m_targetFollowingObject_2(GameObject_t1756533147 * value)
	{
		___m_targetFollowingObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_targetFollowingObject_2, value);
	}

	inline static int32_t get_offset_of_m_enableCameraModeUI_3() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___m_enableCameraModeUI_3)); }
	inline bool get_m_enableCameraModeUI_3() const { return ___m_enableCameraModeUI_3; }
	inline bool* get_address_of_m_enableCameraModeUI_3() { return &___m_enableCameraModeUI_3; }
	inline void set_m_enableCameraModeUI_3(bool value)
	{
		___m_enableCameraModeUI_3 = value;
	}

	inline static int32_t get_offset_of_m_defaultCameraMode_4() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___m_defaultCameraMode_4)); }
	inline int32_t get_m_defaultCameraMode_4() const { return ___m_defaultCameraMode_4; }
	inline int32_t* get_address_of_m_defaultCameraMode_4() { return &___m_defaultCameraMode_4; }
	inline void set_m_defaultCameraMode_4(int32_t value)
	{
		___m_defaultCameraMode_4 = value;
	}

	inline static int32_t get_offset_of_m_curOffset_9() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___m_curOffset_9)); }
	inline Vector3_t2243707580  get_m_curOffset_9() const { return ___m_curOffset_9; }
	inline Vector3_t2243707580 * get_address_of_m_curOffset_9() { return &___m_curOffset_9; }
	inline void set_m_curOffset_9(Vector3_t2243707580  value)
	{
		___m_curOffset_9 = value;
	}

	inline static int32_t get_offset_of_m_thirdPersonCamOffset_10() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___m_thirdPersonCamOffset_10)); }
	inline Vector3_t2243707580  get_m_thirdPersonCamOffset_10() const { return ___m_thirdPersonCamOffset_10; }
	inline Vector3_t2243707580 * get_address_of_m_thirdPersonCamOffset_10() { return &___m_thirdPersonCamOffset_10; }
	inline void set_m_thirdPersonCamOffset_10(Vector3_t2243707580  value)
	{
		___m_thirdPersonCamOffset_10 = value;
	}

	inline static int32_t get_offset_of_m_topDownCamOffset_11() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___m_topDownCamOffset_11)); }
	inline Vector3_t2243707580  get_m_topDownCamOffset_11() const { return ___m_topDownCamOffset_11; }
	inline Vector3_t2243707580 * get_address_of_m_topDownCamOffset_11() { return &___m_topDownCamOffset_11; }
	inline void set_m_topDownCamOffset_11(Vector3_t2243707580  value)
	{
		___m_topDownCamOffset_11 = value;
	}

	inline static int32_t get_offset_of_m_currentCamera_12() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___m_currentCamera_12)); }
	inline int32_t get_m_currentCamera_12() const { return ___m_currentCamera_12; }
	inline int32_t* get_address_of_m_currentCamera_12() { return &___m_currentCamera_12; }
	inline void set_m_currentCamera_12(int32_t value)
	{
		___m_currentCamera_12 = value;
	}

	inline static int32_t get_offset_of_curThirdPersonRotationX_13() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___curThirdPersonRotationX_13)); }
	inline float get_curThirdPersonRotationX_13() const { return ___curThirdPersonRotationX_13; }
	inline float* get_address_of_curThirdPersonRotationX_13() { return &___curThirdPersonRotationX_13; }
	inline void set_curThirdPersonRotationX_13(float value)
	{
		___curThirdPersonRotationX_13 = value;
	}

	inline static int32_t get_offset_of_curThirdPersonRotationY_14() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___curThirdPersonRotationY_14)); }
	inline float get_curThirdPersonRotationY_14() const { return ___curThirdPersonRotationY_14; }
	inline float* get_address_of_curThirdPersonRotationY_14() { return &___curThirdPersonRotationY_14; }
	inline void set_curThirdPersonRotationY_14(float value)
	{
		___curThirdPersonRotationY_14 = value;
	}

	inline static int32_t get_offset_of_startThirdPersonRotationX_15() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___startThirdPersonRotationX_15)); }
	inline float get_startThirdPersonRotationX_15() const { return ___startThirdPersonRotationX_15; }
	inline float* get_address_of_startThirdPersonRotationX_15() { return &___startThirdPersonRotationX_15; }
	inline void set_startThirdPersonRotationX_15(float value)
	{
		___startThirdPersonRotationX_15 = value;
	}

	inline static int32_t get_offset_of_startThirdPersonRotationY_16() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___startThirdPersonRotationY_16)); }
	inline float get_startThirdPersonRotationY_16() const { return ___startThirdPersonRotationY_16; }
	inline float* get_address_of_startThirdPersonRotationY_16() { return &___startThirdPersonRotationY_16; }
	inline void set_startThirdPersonRotationY_16(float value)
	{
		___startThirdPersonRotationY_16 = value;
	}

	inline static int32_t get_offset_of_startThirdPersonCameraCircleR_17() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___startThirdPersonCameraCircleR_17)); }
	inline float get_startThirdPersonCameraCircleR_17() const { return ___startThirdPersonCameraCircleR_17; }
	inline float* get_address_of_startThirdPersonCameraCircleR_17() { return &___startThirdPersonCameraCircleR_17; }
	inline void set_startThirdPersonCameraCircleR_17(float value)
	{
		___startThirdPersonCameraCircleR_17 = value;
	}

	inline static int32_t get_offset_of_curThirdPersonCameraCircleR_18() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___curThirdPersonCameraCircleR_18)); }
	inline float get_curThirdPersonCameraCircleR_18() const { return ___curThirdPersonCameraCircleR_18; }
	inline float* get_address_of_curThirdPersonCameraCircleR_18() { return &___curThirdPersonCameraCircleR_18; }
	inline void set_curThirdPersonCameraCircleR_18(float value)
	{
		___curThirdPersonCameraCircleR_18 = value;
	}

	inline static int32_t get_offset_of_touchStartPoint_19() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___touchStartPoint_19)); }
	inline Vector2_t2243707579  get_touchStartPoint_19() const { return ___touchStartPoint_19; }
	inline Vector2_t2243707579 * get_address_of_touchStartPoint_19() { return &___touchStartPoint_19; }
	inline void set_touchStartPoint_19(Vector2_t2243707579  value)
	{
		___touchStartPoint_19 = value;
	}

	inline static int32_t get_offset_of_topDownStartY_20() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___topDownStartY_20)); }
	inline float get_topDownStartY_20() const { return ___topDownStartY_20; }
	inline float* get_address_of_topDownStartY_20() { return &___topDownStartY_20; }
	inline void set_topDownStartY_20(float value)
	{
		___topDownStartY_20 = value;
	}

	inline static int32_t get_offset_of_touchStartDist_21() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___touchStartDist_21)); }
	inline float get_touchStartDist_21() const { return ___touchStartDist_21; }
	inline float* get_address_of_touchStartDist_21() { return &___touchStartDist_21; }
	inline void set_touchStartDist_21(float value)
	{
		___touchStartDist_21 = value;
	}

	inline static int32_t get_offset_of_topDownStartPos_22() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___topDownStartPos_22)); }
	inline Vector2_t2243707579  get_topDownStartPos_22() const { return ___topDownStartPos_22; }
	inline Vector2_t2243707579 * get_address_of_topDownStartPos_22() { return &___topDownStartPos_22; }
	inline void set_topDownStartPos_22(Vector2_t2243707579  value)
	{
		___topDownStartPos_22 = value;
	}

	inline static int32_t get_offset_of_thirdPersonCamStartOffset_23() { return static_cast<int32_t>(offsetof(TangoGestureCamera_t2762882179, ___thirdPersonCamStartOffset_23)); }
	inline Vector3_t2243707580  get_thirdPersonCamStartOffset_23() const { return ___thirdPersonCamStartOffset_23; }
	inline Vector3_t2243707580 * get_address_of_thirdPersonCamStartOffset_23() { return &___thirdPersonCamStartOffset_23; }
	inline void set_thirdPersonCamStartOffset_23(Vector3_t2243707580  value)
	{
		___thirdPersonCamStartOffset_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
