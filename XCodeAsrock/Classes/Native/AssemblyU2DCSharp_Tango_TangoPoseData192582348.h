﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Tango_DVector42017500235.h"
#include "AssemblyU2DCSharp_Tango_DVector33583584176.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoPoseStatus1112875089.h"
#include "AssemblyU2DCSharp_Tango_TangoCoordinateFramePair3148559008.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoPoseData
struct  TangoPoseData_t192582348  : public Il2CppObject
{
public:
	// System.Int32 Tango.TangoPoseData::version
	int32_t ___version_0;
	// System.Double Tango.TangoPoseData::timestamp
	double ___timestamp_1;
	// Tango.DVector4 Tango.TangoPoseData::orientation
	DVector4_t2017500235  ___orientation_2;
	// Tango.DVector3 Tango.TangoPoseData::translation
	DVector3_t3583584176  ___translation_3;
	// Tango.TangoEnums/TangoPoseStatusType Tango.TangoPoseData::status_code
	int32_t ___status_code_4;
	// Tango.TangoCoordinateFramePair Tango.TangoPoseData::framePair
	TangoCoordinateFramePair_t3148559008  ___framePair_5;
	// System.Int32 Tango.TangoPoseData::confidence
	int32_t ___confidence_6;
	// System.Single Tango.TangoPoseData::accuracy
	float ___accuracy_7;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(TangoPoseData_t192582348, ___version_0)); }
	inline int32_t get_version_0() const { return ___version_0; }
	inline int32_t* get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(int32_t value)
	{
		___version_0 = value;
	}

	inline static int32_t get_offset_of_timestamp_1() { return static_cast<int32_t>(offsetof(TangoPoseData_t192582348, ___timestamp_1)); }
	inline double get_timestamp_1() const { return ___timestamp_1; }
	inline double* get_address_of_timestamp_1() { return &___timestamp_1; }
	inline void set_timestamp_1(double value)
	{
		___timestamp_1 = value;
	}

	inline static int32_t get_offset_of_orientation_2() { return static_cast<int32_t>(offsetof(TangoPoseData_t192582348, ___orientation_2)); }
	inline DVector4_t2017500235  get_orientation_2() const { return ___orientation_2; }
	inline DVector4_t2017500235 * get_address_of_orientation_2() { return &___orientation_2; }
	inline void set_orientation_2(DVector4_t2017500235  value)
	{
		___orientation_2 = value;
	}

	inline static int32_t get_offset_of_translation_3() { return static_cast<int32_t>(offsetof(TangoPoseData_t192582348, ___translation_3)); }
	inline DVector3_t3583584176  get_translation_3() const { return ___translation_3; }
	inline DVector3_t3583584176 * get_address_of_translation_3() { return &___translation_3; }
	inline void set_translation_3(DVector3_t3583584176  value)
	{
		___translation_3 = value;
	}

	inline static int32_t get_offset_of_status_code_4() { return static_cast<int32_t>(offsetof(TangoPoseData_t192582348, ___status_code_4)); }
	inline int32_t get_status_code_4() const { return ___status_code_4; }
	inline int32_t* get_address_of_status_code_4() { return &___status_code_4; }
	inline void set_status_code_4(int32_t value)
	{
		___status_code_4 = value;
	}

	inline static int32_t get_offset_of_framePair_5() { return static_cast<int32_t>(offsetof(TangoPoseData_t192582348, ___framePair_5)); }
	inline TangoCoordinateFramePair_t3148559008  get_framePair_5() const { return ___framePair_5; }
	inline TangoCoordinateFramePair_t3148559008 * get_address_of_framePair_5() { return &___framePair_5; }
	inline void set_framePair_5(TangoCoordinateFramePair_t3148559008  value)
	{
		___framePair_5 = value;
	}

	inline static int32_t get_offset_of_confidence_6() { return static_cast<int32_t>(offsetof(TangoPoseData_t192582348, ___confidence_6)); }
	inline int32_t get_confidence_6() const { return ___confidence_6; }
	inline int32_t* get_address_of_confidence_6() { return &___confidence_6; }
	inline void set_confidence_6(int32_t value)
	{
		___confidence_6 = value;
	}

	inline static int32_t get_offset_of_accuracy_7() { return static_cast<int32_t>(offsetof(TangoPoseData_t192582348, ___accuracy_7)); }
	inline float get_accuracy_7() const { return ___accuracy_7; }
	inline float* get_address_of_accuracy_7() { return &___accuracy_7; }
	inline void set_accuracy_7(float value)
	{
		___accuracy_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Tango.TangoPoseData
struct TangoPoseData_t192582348_marshaled_pinvoke
{
	int32_t ___version_0;
	double ___timestamp_1;
	DVector4_t2017500235  ___orientation_2;
	DVector3_t3583584176  ___translation_3;
	int32_t ___status_code_4;
	TangoCoordinateFramePair_t3148559008  ___framePair_5;
	int32_t ___confidence_6;
	float ___accuracy_7;
};
// Native definition for COM marshalling of Tango.TangoPoseData
struct TangoPoseData_t192582348_marshaled_com
{
	int32_t ___version_0;
	double ___timestamp_1;
	DVector4_t2017500235  ___orientation_2;
	DVector3_t3583584176  ___translation_3;
	int32_t ___status_code_4;
	TangoCoordinateFramePair_t3148559008  ___framePair_5;
	int32_t ___confidence_6;
	float ___accuracy_7;
};
