﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// System.Collections.Generic.Queue`1<Tango.TangoApplication/AndroidMessage>
struct Queue_1_t4247336416;
// Tango.TangoApplication/ITangoDepthCameraManager
struct ITangoDepthCameraManager_t2550061576;
// Tango.TangoApplication/TangoApplicationState
struct TangoApplicationState_t848537215;
// Tango.TangoApplication/ITangoPermissionsManager
struct ITangoPermissionsManager_t1593860194;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// Tango.OnDisplayChangedEventHandler
struct OnDisplayChangedEventHandler_t1075383569;
// IAndroidHelperWrapper
struct IAndroidHelperWrapper_t128471629;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoApplication/TangoAndroidMessageManager
struct  TangoAndroidMessageManager_t2861932349  : public Il2CppObject
{
public:
	// System.Object Tango.TangoApplication/TangoAndroidMessageManager::m_messageQueueLock
	Il2CppObject * ___m_messageQueueLock_0;
	// System.Collections.Generic.Queue`1<Tango.TangoApplication/AndroidMessage> Tango.TangoApplication/TangoAndroidMessageManager::m_androidMessageQueue
	Queue_1_t4247336416 * ___m_androidMessageQueue_1;
	// Tango.TangoApplication/ITangoDepthCameraManager Tango.TangoApplication/TangoAndroidMessageManager::m_depthCameraManager
	Il2CppObject * ___m_depthCameraManager_2;
	// Tango.TangoApplication/TangoApplicationState Tango.TangoApplication/TangoAndroidMessageManager::m_applicationState
	TangoApplicationState_t848537215 * ___m_applicationState_3;
	// Tango.TangoApplication/ITangoPermissionsManager Tango.TangoApplication/TangoAndroidMessageManager::m_permissionsManager
	Il2CppObject * ___m_permissionsManager_4;
	// System.Action`1<System.Boolean> Tango.TangoApplication/TangoAndroidMessageManager::m_onAndroidPauseResumeAsync
	Action_1_t3627374100 * ___m_onAndroidPauseResumeAsync_5;
	// Tango.OnDisplayChangedEventHandler Tango.TangoApplication/TangoAndroidMessageManager::m_onDisplayChanged
	OnDisplayChangedEventHandler_t1075383569 * ___m_onDisplayChanged_6;
	// IAndroidHelperWrapper Tango.TangoApplication/TangoAndroidMessageManager::m_androidHelper
	Il2CppObject * ___m_androidHelper_7;
	// System.Boolean Tango.TangoApplication/TangoAndroidMessageManager::m_isPaused
	bool ___m_isPaused_8;
	// System.Int32 Tango.TangoApplication/TangoAndroidMessageManager::savedDepthCameraRate
	int32_t ___savedDepthCameraRate_9;

public:
	inline static int32_t get_offset_of_m_messageQueueLock_0() { return static_cast<int32_t>(offsetof(TangoAndroidMessageManager_t2861932349, ___m_messageQueueLock_0)); }
	inline Il2CppObject * get_m_messageQueueLock_0() const { return ___m_messageQueueLock_0; }
	inline Il2CppObject ** get_address_of_m_messageQueueLock_0() { return &___m_messageQueueLock_0; }
	inline void set_m_messageQueueLock_0(Il2CppObject * value)
	{
		___m_messageQueueLock_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_messageQueueLock_0, value);
	}

	inline static int32_t get_offset_of_m_androidMessageQueue_1() { return static_cast<int32_t>(offsetof(TangoAndroidMessageManager_t2861932349, ___m_androidMessageQueue_1)); }
	inline Queue_1_t4247336416 * get_m_androidMessageQueue_1() const { return ___m_androidMessageQueue_1; }
	inline Queue_1_t4247336416 ** get_address_of_m_androidMessageQueue_1() { return &___m_androidMessageQueue_1; }
	inline void set_m_androidMessageQueue_1(Queue_1_t4247336416 * value)
	{
		___m_androidMessageQueue_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_androidMessageQueue_1, value);
	}

	inline static int32_t get_offset_of_m_depthCameraManager_2() { return static_cast<int32_t>(offsetof(TangoAndroidMessageManager_t2861932349, ___m_depthCameraManager_2)); }
	inline Il2CppObject * get_m_depthCameraManager_2() const { return ___m_depthCameraManager_2; }
	inline Il2CppObject ** get_address_of_m_depthCameraManager_2() { return &___m_depthCameraManager_2; }
	inline void set_m_depthCameraManager_2(Il2CppObject * value)
	{
		___m_depthCameraManager_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_depthCameraManager_2, value);
	}

	inline static int32_t get_offset_of_m_applicationState_3() { return static_cast<int32_t>(offsetof(TangoAndroidMessageManager_t2861932349, ___m_applicationState_3)); }
	inline TangoApplicationState_t848537215 * get_m_applicationState_3() const { return ___m_applicationState_3; }
	inline TangoApplicationState_t848537215 ** get_address_of_m_applicationState_3() { return &___m_applicationState_3; }
	inline void set_m_applicationState_3(TangoApplicationState_t848537215 * value)
	{
		___m_applicationState_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_applicationState_3, value);
	}

	inline static int32_t get_offset_of_m_permissionsManager_4() { return static_cast<int32_t>(offsetof(TangoAndroidMessageManager_t2861932349, ___m_permissionsManager_4)); }
	inline Il2CppObject * get_m_permissionsManager_4() const { return ___m_permissionsManager_4; }
	inline Il2CppObject ** get_address_of_m_permissionsManager_4() { return &___m_permissionsManager_4; }
	inline void set_m_permissionsManager_4(Il2CppObject * value)
	{
		___m_permissionsManager_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_permissionsManager_4, value);
	}

	inline static int32_t get_offset_of_m_onAndroidPauseResumeAsync_5() { return static_cast<int32_t>(offsetof(TangoAndroidMessageManager_t2861932349, ___m_onAndroidPauseResumeAsync_5)); }
	inline Action_1_t3627374100 * get_m_onAndroidPauseResumeAsync_5() const { return ___m_onAndroidPauseResumeAsync_5; }
	inline Action_1_t3627374100 ** get_address_of_m_onAndroidPauseResumeAsync_5() { return &___m_onAndroidPauseResumeAsync_5; }
	inline void set_m_onAndroidPauseResumeAsync_5(Action_1_t3627374100 * value)
	{
		___m_onAndroidPauseResumeAsync_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_onAndroidPauseResumeAsync_5, value);
	}

	inline static int32_t get_offset_of_m_onDisplayChanged_6() { return static_cast<int32_t>(offsetof(TangoAndroidMessageManager_t2861932349, ___m_onDisplayChanged_6)); }
	inline OnDisplayChangedEventHandler_t1075383569 * get_m_onDisplayChanged_6() const { return ___m_onDisplayChanged_6; }
	inline OnDisplayChangedEventHandler_t1075383569 ** get_address_of_m_onDisplayChanged_6() { return &___m_onDisplayChanged_6; }
	inline void set_m_onDisplayChanged_6(OnDisplayChangedEventHandler_t1075383569 * value)
	{
		___m_onDisplayChanged_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_onDisplayChanged_6, value);
	}

	inline static int32_t get_offset_of_m_androidHelper_7() { return static_cast<int32_t>(offsetof(TangoAndroidMessageManager_t2861932349, ___m_androidHelper_7)); }
	inline Il2CppObject * get_m_androidHelper_7() const { return ___m_androidHelper_7; }
	inline Il2CppObject ** get_address_of_m_androidHelper_7() { return &___m_androidHelper_7; }
	inline void set_m_androidHelper_7(Il2CppObject * value)
	{
		___m_androidHelper_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_androidHelper_7, value);
	}

	inline static int32_t get_offset_of_m_isPaused_8() { return static_cast<int32_t>(offsetof(TangoAndroidMessageManager_t2861932349, ___m_isPaused_8)); }
	inline bool get_m_isPaused_8() const { return ___m_isPaused_8; }
	inline bool* get_address_of_m_isPaused_8() { return &___m_isPaused_8; }
	inline void set_m_isPaused_8(bool value)
	{
		___m_isPaused_8 = value;
	}

	inline static int32_t get_offset_of_savedDepthCameraRate_9() { return static_cast<int32_t>(offsetof(TangoAndroidMessageManager_t2861932349, ___savedDepthCameraRate_9)); }
	inline int32_t get_savedDepthCameraRate_9() const { return ___savedDepthCameraRate_9; }
	inline int32_t* get_address_of_savedDepthCameraRate_9() { return &___savedDepthCameraRate_9; }
	inline void set_savedDepthCameraRate_9(int32_t value)
	{
		___savedDepthCameraRate_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
