﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"

// UnityEngine.Animation
struct Animation_t2068071072;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARMarker
struct  ARMarker_t1554260723  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ARMarker::m_type
	int32_t ___m_type_2;
	// System.Single ARMarker::m_timestamp
	float ___m_timestamp_3;
	// UnityEngine.Matrix4x4 ARMarker::m_deviceTMarker
	Matrix4x4_t2933234003  ___m_deviceTMarker_4;
	// UnityEngine.Animation ARMarker::m_anim
	Animation_t2068071072 * ___m_anim_5;

public:
	inline static int32_t get_offset_of_m_type_2() { return static_cast<int32_t>(offsetof(ARMarker_t1554260723, ___m_type_2)); }
	inline int32_t get_m_type_2() const { return ___m_type_2; }
	inline int32_t* get_address_of_m_type_2() { return &___m_type_2; }
	inline void set_m_type_2(int32_t value)
	{
		___m_type_2 = value;
	}

	inline static int32_t get_offset_of_m_timestamp_3() { return static_cast<int32_t>(offsetof(ARMarker_t1554260723, ___m_timestamp_3)); }
	inline float get_m_timestamp_3() const { return ___m_timestamp_3; }
	inline float* get_address_of_m_timestamp_3() { return &___m_timestamp_3; }
	inline void set_m_timestamp_3(float value)
	{
		___m_timestamp_3 = value;
	}

	inline static int32_t get_offset_of_m_deviceTMarker_4() { return static_cast<int32_t>(offsetof(ARMarker_t1554260723, ___m_deviceTMarker_4)); }
	inline Matrix4x4_t2933234003  get_m_deviceTMarker_4() const { return ___m_deviceTMarker_4; }
	inline Matrix4x4_t2933234003 * get_address_of_m_deviceTMarker_4() { return &___m_deviceTMarker_4; }
	inline void set_m_deviceTMarker_4(Matrix4x4_t2933234003  value)
	{
		___m_deviceTMarker_4 = value;
	}

	inline static int32_t get_offset_of_m_anim_5() { return static_cast<int32_t>(offsetof(ARMarker_t1554260723, ___m_anim_5)); }
	inline Animation_t2068071072 * get_m_anim_5() const { return ___m_anim_5; }
	inline Animation_t2068071072 ** get_address_of_m_anim_5() { return &___m_anim_5; }
	inline void set_m_anim_5(Animation_t2068071072 * value)
	{
		___m_anim_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_anim_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
