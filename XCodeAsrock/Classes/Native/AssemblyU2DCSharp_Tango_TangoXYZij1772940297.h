﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoXYZij
struct  TangoXYZij_t1772940297  : public Il2CppObject
{
public:
	// System.Int32 Tango.TangoXYZij::version
	int32_t ___version_0;
	// System.Double Tango.TangoXYZij::timestamp
	double ___timestamp_1;
	// System.Int32 Tango.TangoXYZij::xyz_count
	int32_t ___xyz_count_2;
	// System.IntPtr Tango.TangoXYZij::xyz
	IntPtr_t ___xyz_3;
	// System.Int32 Tango.TangoXYZij::ij_rows
	int32_t ___ij_rows_4;
	// System.Int32 Tango.TangoXYZij::ij_cols
	int32_t ___ij_cols_5;
	// System.IntPtr Tango.TangoXYZij::ij
	IntPtr_t ___ij_6;
	// System.IntPtr Tango.TangoXYZij::color_image
	IntPtr_t ___color_image_7;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(TangoXYZij_t1772940297, ___version_0)); }
	inline int32_t get_version_0() const { return ___version_0; }
	inline int32_t* get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(int32_t value)
	{
		___version_0 = value;
	}

	inline static int32_t get_offset_of_timestamp_1() { return static_cast<int32_t>(offsetof(TangoXYZij_t1772940297, ___timestamp_1)); }
	inline double get_timestamp_1() const { return ___timestamp_1; }
	inline double* get_address_of_timestamp_1() { return &___timestamp_1; }
	inline void set_timestamp_1(double value)
	{
		___timestamp_1 = value;
	}

	inline static int32_t get_offset_of_xyz_count_2() { return static_cast<int32_t>(offsetof(TangoXYZij_t1772940297, ___xyz_count_2)); }
	inline int32_t get_xyz_count_2() const { return ___xyz_count_2; }
	inline int32_t* get_address_of_xyz_count_2() { return &___xyz_count_2; }
	inline void set_xyz_count_2(int32_t value)
	{
		___xyz_count_2 = value;
	}

	inline static int32_t get_offset_of_xyz_3() { return static_cast<int32_t>(offsetof(TangoXYZij_t1772940297, ___xyz_3)); }
	inline IntPtr_t get_xyz_3() const { return ___xyz_3; }
	inline IntPtr_t* get_address_of_xyz_3() { return &___xyz_3; }
	inline void set_xyz_3(IntPtr_t value)
	{
		___xyz_3 = value;
	}

	inline static int32_t get_offset_of_ij_rows_4() { return static_cast<int32_t>(offsetof(TangoXYZij_t1772940297, ___ij_rows_4)); }
	inline int32_t get_ij_rows_4() const { return ___ij_rows_4; }
	inline int32_t* get_address_of_ij_rows_4() { return &___ij_rows_4; }
	inline void set_ij_rows_4(int32_t value)
	{
		___ij_rows_4 = value;
	}

	inline static int32_t get_offset_of_ij_cols_5() { return static_cast<int32_t>(offsetof(TangoXYZij_t1772940297, ___ij_cols_5)); }
	inline int32_t get_ij_cols_5() const { return ___ij_cols_5; }
	inline int32_t* get_address_of_ij_cols_5() { return &___ij_cols_5; }
	inline void set_ij_cols_5(int32_t value)
	{
		___ij_cols_5 = value;
	}

	inline static int32_t get_offset_of_ij_6() { return static_cast<int32_t>(offsetof(TangoXYZij_t1772940297, ___ij_6)); }
	inline IntPtr_t get_ij_6() const { return ___ij_6; }
	inline IntPtr_t* get_address_of_ij_6() { return &___ij_6; }
	inline void set_ij_6(IntPtr_t value)
	{
		___ij_6 = value;
	}

	inline static int32_t get_offset_of_color_image_7() { return static_cast<int32_t>(offsetof(TangoXYZij_t1772940297, ___color_image_7)); }
	inline IntPtr_t get_color_image_7() const { return ___color_image_7; }
	inline IntPtr_t* get_address_of_color_image_7() { return &___color_image_7; }
	inline void set_color_image_7(IntPtr_t value)
	{
		___color_image_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Tango.TangoXYZij
struct TangoXYZij_t1772940297_marshaled_pinvoke
{
	int32_t ___version_0;
	double ___timestamp_1;
	int32_t ___xyz_count_2;
	intptr_t ___xyz_3;
	int32_t ___ij_rows_4;
	int32_t ___ij_cols_5;
	intptr_t ___ij_6;
	intptr_t ___color_image_7;
};
// Native definition for COM marshalling of Tango.TangoXYZij
struct TangoXYZij_t1772940297_marshaled_com
{
	int32_t ___version_0;
	double ___timestamp_1;
	int32_t ___xyz_count_2;
	intptr_t ___xyz_3;
	int32_t ___ij_rows_4;
	int32_t ___ij_cols_5;
	intptr_t ___ij_6;
	intptr_t ___color_image_7;
};
