﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoUnityDepth
struct  TangoUnityDepth_t2545959683  : public Il2CppObject
{
public:
	// System.Int32 Tango.TangoUnityDepth::m_version
	int32_t ___m_version_2;
	// System.Int32 Tango.TangoUnityDepth::m_pointCount
	int32_t ___m_pointCount_3;
	// System.Single[] Tango.TangoUnityDepth::m_points
	SingleU5BU5D_t577127397* ___m_points_4;
	// System.Double Tango.TangoUnityDepth::m_timestamp
	double ___m_timestamp_5;
	// System.Int32 Tango.TangoUnityDepth::m_ijRows
	int32_t ___m_ijRows_6;
	// System.Int32 Tango.TangoUnityDepth::m_ijColumns
	int32_t ___m_ijColumns_7;
	// System.Int32[] Tango.TangoUnityDepth::m_ij
	Int32U5BU5D_t3030399641* ___m_ij_8;

public:
	inline static int32_t get_offset_of_m_version_2() { return static_cast<int32_t>(offsetof(TangoUnityDepth_t2545959683, ___m_version_2)); }
	inline int32_t get_m_version_2() const { return ___m_version_2; }
	inline int32_t* get_address_of_m_version_2() { return &___m_version_2; }
	inline void set_m_version_2(int32_t value)
	{
		___m_version_2 = value;
	}

	inline static int32_t get_offset_of_m_pointCount_3() { return static_cast<int32_t>(offsetof(TangoUnityDepth_t2545959683, ___m_pointCount_3)); }
	inline int32_t get_m_pointCount_3() const { return ___m_pointCount_3; }
	inline int32_t* get_address_of_m_pointCount_3() { return &___m_pointCount_3; }
	inline void set_m_pointCount_3(int32_t value)
	{
		___m_pointCount_3 = value;
	}

	inline static int32_t get_offset_of_m_points_4() { return static_cast<int32_t>(offsetof(TangoUnityDepth_t2545959683, ___m_points_4)); }
	inline SingleU5BU5D_t577127397* get_m_points_4() const { return ___m_points_4; }
	inline SingleU5BU5D_t577127397** get_address_of_m_points_4() { return &___m_points_4; }
	inline void set_m_points_4(SingleU5BU5D_t577127397* value)
	{
		___m_points_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_points_4, value);
	}

	inline static int32_t get_offset_of_m_timestamp_5() { return static_cast<int32_t>(offsetof(TangoUnityDepth_t2545959683, ___m_timestamp_5)); }
	inline double get_m_timestamp_5() const { return ___m_timestamp_5; }
	inline double* get_address_of_m_timestamp_5() { return &___m_timestamp_5; }
	inline void set_m_timestamp_5(double value)
	{
		___m_timestamp_5 = value;
	}

	inline static int32_t get_offset_of_m_ijRows_6() { return static_cast<int32_t>(offsetof(TangoUnityDepth_t2545959683, ___m_ijRows_6)); }
	inline int32_t get_m_ijRows_6() const { return ___m_ijRows_6; }
	inline int32_t* get_address_of_m_ijRows_6() { return &___m_ijRows_6; }
	inline void set_m_ijRows_6(int32_t value)
	{
		___m_ijRows_6 = value;
	}

	inline static int32_t get_offset_of_m_ijColumns_7() { return static_cast<int32_t>(offsetof(TangoUnityDepth_t2545959683, ___m_ijColumns_7)); }
	inline int32_t get_m_ijColumns_7() const { return ___m_ijColumns_7; }
	inline int32_t* get_address_of_m_ijColumns_7() { return &___m_ijColumns_7; }
	inline void set_m_ijColumns_7(int32_t value)
	{
		___m_ijColumns_7 = value;
	}

	inline static int32_t get_offset_of_m_ij_8() { return static_cast<int32_t>(offsetof(TangoUnityDepth_t2545959683, ___m_ij_8)); }
	inline Int32U5BU5D_t3030399641* get_m_ij_8() const { return ___m_ij_8; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_ij_8() { return &___m_ij_8; }
	inline void set_m_ij_8(Int32U5BU5D_t3030399641* value)
	{
		___m_ij_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_ij_8, value);
	}
};

struct TangoUnityDepth_t2545959683_StaticFields
{
public:
	// System.Int32 Tango.TangoUnityDepth::MAX_POINTS_ARRAY_SIZE
	int32_t ___MAX_POINTS_ARRAY_SIZE_0;
	// System.Int32 Tango.TangoUnityDepth::MAX_IJ_ARRAY_SIZE
	int32_t ___MAX_IJ_ARRAY_SIZE_1;

public:
	inline static int32_t get_offset_of_MAX_POINTS_ARRAY_SIZE_0() { return static_cast<int32_t>(offsetof(TangoUnityDepth_t2545959683_StaticFields, ___MAX_POINTS_ARRAY_SIZE_0)); }
	inline int32_t get_MAX_POINTS_ARRAY_SIZE_0() const { return ___MAX_POINTS_ARRAY_SIZE_0; }
	inline int32_t* get_address_of_MAX_POINTS_ARRAY_SIZE_0() { return &___MAX_POINTS_ARRAY_SIZE_0; }
	inline void set_MAX_POINTS_ARRAY_SIZE_0(int32_t value)
	{
		___MAX_POINTS_ARRAY_SIZE_0 = value;
	}

	inline static int32_t get_offset_of_MAX_IJ_ARRAY_SIZE_1() { return static_cast<int32_t>(offsetof(TangoUnityDepth_t2545959683_StaticFields, ___MAX_IJ_ARRAY_SIZE_1)); }
	inline int32_t get_MAX_IJ_ARRAY_SIZE_1() const { return ___MAX_IJ_ARRAY_SIZE_1; }
	inline int32_t* get_address_of_MAX_IJ_ARRAY_SIZE_1() { return &___MAX_IJ_ARRAY_SIZE_1; }
	inline void set_MAX_IJ_ARRAY_SIZE_1(int32_t value)
	{
		___MAX_IJ_ARRAY_SIZE_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
