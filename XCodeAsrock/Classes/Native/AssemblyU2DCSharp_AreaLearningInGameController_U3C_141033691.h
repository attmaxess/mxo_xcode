﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Plane3727654732.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"

// UnityEngine.Camera
struct Camera_t189460977;
// ARMarker
struct ARMarker_t1554260723;
// AreaLearningInGameController
struct AreaLearningInGameController_t4091715694;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AreaLearningInGameController/<_WaitForDepthAndFindPlane>c__Iterator1
struct  U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691  : public Il2CppObject
{
public:
	// UnityEngine.Camera AreaLearningInGameController/<_WaitForDepthAndFindPlane>c__Iterator1::<cam>__0
	Camera_t189460977 * ___U3CcamU3E__0_0;
	// UnityEngine.Vector2 AreaLearningInGameController/<_WaitForDepthAndFindPlane>c__Iterator1::touchPosition
	Vector2_t2243707579  ___touchPosition_1;
	// UnityEngine.Vector3 AreaLearningInGameController/<_WaitForDepthAndFindPlane>c__Iterator1::<planeCenter>__0
	Vector3_t2243707580  ___U3CplaneCenterU3E__0_2;
	// UnityEngine.Plane AreaLearningInGameController/<_WaitForDepthAndFindPlane>c__Iterator1::<plane>__0
	Plane_t3727654732  ___U3CplaneU3E__0_3;
	// UnityEngine.Vector3 AreaLearningInGameController/<_WaitForDepthAndFindPlane>c__Iterator1::<up>__0
	Vector3_t2243707580  ___U3CupU3E__0_4;
	// UnityEngine.Vector3 AreaLearningInGameController/<_WaitForDepthAndFindPlane>c__Iterator1::<forward>__1
	Vector3_t2243707580  ___U3CforwardU3E__1_5;
	// ARMarker AreaLearningInGameController/<_WaitForDepthAndFindPlane>c__Iterator1::<markerScript>__0
	ARMarker_t1554260723 * ___U3CmarkerScriptU3E__0_6;
	// UnityEngine.Matrix4x4 AreaLearningInGameController/<_WaitForDepthAndFindPlane>c__Iterator1::<uwTDevice>__0
	Matrix4x4_t2933234003  ___U3CuwTDeviceU3E__0_7;
	// UnityEngine.Matrix4x4 AreaLearningInGameController/<_WaitForDepthAndFindPlane>c__Iterator1::<uwTMarker>__0
	Matrix4x4_t2933234003  ___U3CuwTMarkerU3E__0_8;
	// AreaLearningInGameController AreaLearningInGameController/<_WaitForDepthAndFindPlane>c__Iterator1::$this
	AreaLearningInGameController_t4091715694 * ___U24this_9;
	// System.Object AreaLearningInGameController/<_WaitForDepthAndFindPlane>c__Iterator1::$current
	Il2CppObject * ___U24current_10;
	// System.Boolean AreaLearningInGameController/<_WaitForDepthAndFindPlane>c__Iterator1::$disposing
	bool ___U24disposing_11;
	// System.Int32 AreaLearningInGameController/<_WaitForDepthAndFindPlane>c__Iterator1::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_U3CcamU3E__0_0() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691, ___U3CcamU3E__0_0)); }
	inline Camera_t189460977 * get_U3CcamU3E__0_0() const { return ___U3CcamU3E__0_0; }
	inline Camera_t189460977 ** get_address_of_U3CcamU3E__0_0() { return &___U3CcamU3E__0_0; }
	inline void set_U3CcamU3E__0_0(Camera_t189460977 * value)
	{
		___U3CcamU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcamU3E__0_0, value);
	}

	inline static int32_t get_offset_of_touchPosition_1() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691, ___touchPosition_1)); }
	inline Vector2_t2243707579  get_touchPosition_1() const { return ___touchPosition_1; }
	inline Vector2_t2243707579 * get_address_of_touchPosition_1() { return &___touchPosition_1; }
	inline void set_touchPosition_1(Vector2_t2243707579  value)
	{
		___touchPosition_1 = value;
	}

	inline static int32_t get_offset_of_U3CplaneCenterU3E__0_2() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691, ___U3CplaneCenterU3E__0_2)); }
	inline Vector3_t2243707580  get_U3CplaneCenterU3E__0_2() const { return ___U3CplaneCenterU3E__0_2; }
	inline Vector3_t2243707580 * get_address_of_U3CplaneCenterU3E__0_2() { return &___U3CplaneCenterU3E__0_2; }
	inline void set_U3CplaneCenterU3E__0_2(Vector3_t2243707580  value)
	{
		___U3CplaneCenterU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CplaneU3E__0_3() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691, ___U3CplaneU3E__0_3)); }
	inline Plane_t3727654732  get_U3CplaneU3E__0_3() const { return ___U3CplaneU3E__0_3; }
	inline Plane_t3727654732 * get_address_of_U3CplaneU3E__0_3() { return &___U3CplaneU3E__0_3; }
	inline void set_U3CplaneU3E__0_3(Plane_t3727654732  value)
	{
		___U3CplaneU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CupU3E__0_4() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691, ___U3CupU3E__0_4)); }
	inline Vector3_t2243707580  get_U3CupU3E__0_4() const { return ___U3CupU3E__0_4; }
	inline Vector3_t2243707580 * get_address_of_U3CupU3E__0_4() { return &___U3CupU3E__0_4; }
	inline void set_U3CupU3E__0_4(Vector3_t2243707580  value)
	{
		___U3CupU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CforwardU3E__1_5() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691, ___U3CforwardU3E__1_5)); }
	inline Vector3_t2243707580  get_U3CforwardU3E__1_5() const { return ___U3CforwardU3E__1_5; }
	inline Vector3_t2243707580 * get_address_of_U3CforwardU3E__1_5() { return &___U3CforwardU3E__1_5; }
	inline void set_U3CforwardU3E__1_5(Vector3_t2243707580  value)
	{
		___U3CforwardU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CmarkerScriptU3E__0_6() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691, ___U3CmarkerScriptU3E__0_6)); }
	inline ARMarker_t1554260723 * get_U3CmarkerScriptU3E__0_6() const { return ___U3CmarkerScriptU3E__0_6; }
	inline ARMarker_t1554260723 ** get_address_of_U3CmarkerScriptU3E__0_6() { return &___U3CmarkerScriptU3E__0_6; }
	inline void set_U3CmarkerScriptU3E__0_6(ARMarker_t1554260723 * value)
	{
		___U3CmarkerScriptU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmarkerScriptU3E__0_6, value);
	}

	inline static int32_t get_offset_of_U3CuwTDeviceU3E__0_7() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691, ___U3CuwTDeviceU3E__0_7)); }
	inline Matrix4x4_t2933234003  get_U3CuwTDeviceU3E__0_7() const { return ___U3CuwTDeviceU3E__0_7; }
	inline Matrix4x4_t2933234003 * get_address_of_U3CuwTDeviceU3E__0_7() { return &___U3CuwTDeviceU3E__0_7; }
	inline void set_U3CuwTDeviceU3E__0_7(Matrix4x4_t2933234003  value)
	{
		___U3CuwTDeviceU3E__0_7 = value;
	}

	inline static int32_t get_offset_of_U3CuwTMarkerU3E__0_8() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691, ___U3CuwTMarkerU3E__0_8)); }
	inline Matrix4x4_t2933234003  get_U3CuwTMarkerU3E__0_8() const { return ___U3CuwTMarkerU3E__0_8; }
	inline Matrix4x4_t2933234003 * get_address_of_U3CuwTMarkerU3E__0_8() { return &___U3CuwTMarkerU3E__0_8; }
	inline void set_U3CuwTMarkerU3E__0_8(Matrix4x4_t2933234003  value)
	{
		___U3CuwTMarkerU3E__0_8 = value;
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691, ___U24this_9)); }
	inline AreaLearningInGameController_t4091715694 * get_U24this_9() const { return ___U24this_9; }
	inline AreaLearningInGameController_t4091715694 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(AreaLearningInGameController_t4091715694 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_9, value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
