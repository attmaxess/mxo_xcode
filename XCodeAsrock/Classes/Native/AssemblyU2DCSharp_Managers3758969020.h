﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// QRCodeReader
struct QRCodeReader_t3711862773;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Button
struct Button_t2872111280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Managers
struct  Managers_t3758969020  : public MonoBehaviour_t1158329972
{
public:
	// QRCodeReader Managers::QRCodeReaderObj
	QRCodeReader_t3711862773 * ___QRCodeReaderObj_2;
	// UnityEngine.GameObject Managers::SpawPointObj
	GameObject_t1756533147 * ___SpawPointObj_3;
	// UnityEngine.UI.Button Managers::ButtonScan
	Button_t2872111280 * ___ButtonScan_4;
	// UnityEngine.UI.Button Managers::ButtonTestPosition
	Button_t2872111280 * ___ButtonTestPosition_5;
	// UnityEngine.GameObject Managers::Cube
	GameObject_t1756533147 * ___Cube_6;
	// System.Boolean Managers::isReady
	bool ___isReady_7;
	// System.Boolean Managers::isScan
	bool ___isScan_8;

public:
	inline static int32_t get_offset_of_QRCodeReaderObj_2() { return static_cast<int32_t>(offsetof(Managers_t3758969020, ___QRCodeReaderObj_2)); }
	inline QRCodeReader_t3711862773 * get_QRCodeReaderObj_2() const { return ___QRCodeReaderObj_2; }
	inline QRCodeReader_t3711862773 ** get_address_of_QRCodeReaderObj_2() { return &___QRCodeReaderObj_2; }
	inline void set_QRCodeReaderObj_2(QRCodeReader_t3711862773 * value)
	{
		___QRCodeReaderObj_2 = value;
		Il2CppCodeGenWriteBarrier(&___QRCodeReaderObj_2, value);
	}

	inline static int32_t get_offset_of_SpawPointObj_3() { return static_cast<int32_t>(offsetof(Managers_t3758969020, ___SpawPointObj_3)); }
	inline GameObject_t1756533147 * get_SpawPointObj_3() const { return ___SpawPointObj_3; }
	inline GameObject_t1756533147 ** get_address_of_SpawPointObj_3() { return &___SpawPointObj_3; }
	inline void set_SpawPointObj_3(GameObject_t1756533147 * value)
	{
		___SpawPointObj_3 = value;
		Il2CppCodeGenWriteBarrier(&___SpawPointObj_3, value);
	}

	inline static int32_t get_offset_of_ButtonScan_4() { return static_cast<int32_t>(offsetof(Managers_t3758969020, ___ButtonScan_4)); }
	inline Button_t2872111280 * get_ButtonScan_4() const { return ___ButtonScan_4; }
	inline Button_t2872111280 ** get_address_of_ButtonScan_4() { return &___ButtonScan_4; }
	inline void set_ButtonScan_4(Button_t2872111280 * value)
	{
		___ButtonScan_4 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonScan_4, value);
	}

	inline static int32_t get_offset_of_ButtonTestPosition_5() { return static_cast<int32_t>(offsetof(Managers_t3758969020, ___ButtonTestPosition_5)); }
	inline Button_t2872111280 * get_ButtonTestPosition_5() const { return ___ButtonTestPosition_5; }
	inline Button_t2872111280 ** get_address_of_ButtonTestPosition_5() { return &___ButtonTestPosition_5; }
	inline void set_ButtonTestPosition_5(Button_t2872111280 * value)
	{
		___ButtonTestPosition_5 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonTestPosition_5, value);
	}

	inline static int32_t get_offset_of_Cube_6() { return static_cast<int32_t>(offsetof(Managers_t3758969020, ___Cube_6)); }
	inline GameObject_t1756533147 * get_Cube_6() const { return ___Cube_6; }
	inline GameObject_t1756533147 ** get_address_of_Cube_6() { return &___Cube_6; }
	inline void set_Cube_6(GameObject_t1756533147 * value)
	{
		___Cube_6 = value;
		Il2CppCodeGenWriteBarrier(&___Cube_6, value);
	}

	inline static int32_t get_offset_of_isReady_7() { return static_cast<int32_t>(offsetof(Managers_t3758969020, ___isReady_7)); }
	inline bool get_isReady_7() const { return ___isReady_7; }
	inline bool* get_address_of_isReady_7() { return &___isReady_7; }
	inline void set_isReady_7(bool value)
	{
		___isReady_7 = value;
	}

	inline static int32_t get_offset_of_isScan_8() { return static_cast<int32_t>(offsetof(Managers_t3758969020, ___isScan_8)); }
	inline bool get_isScan_8() const { return ___isScan_8; }
	inline bool* get_address_of_isScan_8() { return &___isScan_8; }
	inline void set_isScan_8(bool value)
	{
		___isScan_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
