﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoConfig
struct  TangoConfig_t3878449435  : public Il2CppObject
{
public:
	// System.IntPtr Tango.TangoConfig::m_configHandle
	IntPtr_t ___m_configHandle_3;

public:
	inline static int32_t get_offset_of_m_configHandle_3() { return static_cast<int32_t>(offsetof(TangoConfig_t3878449435, ___m_configHandle_3)); }
	inline IntPtr_t get_m_configHandle_3() const { return ___m_configHandle_3; }
	inline IntPtr_t* get_address_of_m_configHandle_3() { return &___m_configHandle_3; }
	inline void set_m_configHandle_3(IntPtr_t value)
	{
		___m_configHandle_3 = value;
	}
};

struct TangoConfig_t3878449435_StaticFields
{
public:
	// System.String Tango.TangoConfig::CLASS_NAME
	String_t* ___CLASS_NAME_1;
	// System.String Tango.TangoConfig::NO_CONFIG_FOUND
	String_t* ___NO_CONFIG_FOUND_2;

public:
	inline static int32_t get_offset_of_CLASS_NAME_1() { return static_cast<int32_t>(offsetof(TangoConfig_t3878449435_StaticFields, ___CLASS_NAME_1)); }
	inline String_t* get_CLASS_NAME_1() const { return ___CLASS_NAME_1; }
	inline String_t** get_address_of_CLASS_NAME_1() { return &___CLASS_NAME_1; }
	inline void set_CLASS_NAME_1(String_t* value)
	{
		___CLASS_NAME_1 = value;
		Il2CppCodeGenWriteBarrier(&___CLASS_NAME_1, value);
	}

	inline static int32_t get_offset_of_NO_CONFIG_FOUND_2() { return static_cast<int32_t>(offsetof(TangoConfig_t3878449435_StaticFields, ___NO_CONFIG_FOUND_2)); }
	inline String_t* get_NO_CONFIG_FOUND_2() const { return ___NO_CONFIG_FOUND_2; }
	inline String_t** get_address_of_NO_CONFIG_FOUND_2() { return &___NO_CONFIG_FOUND_2; }
	inline void set_NO_CONFIG_FOUND_2(String_t* value)
	{
		___NO_CONFIG_FOUND_2 = value;
		Il2CppCodeGenWriteBarrier(&___NO_CONFIG_FOUND_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
