﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// Tango.TangoEventProvider/APIOnEventAvailable
struct APIOnEventAvailable_t2464565050;
// Tango.OnTangoEventAvailableEventHandler
struct OnTangoEventAvailableEventHandler_t3985195121;
// Tango.TangoEvent
struct TangoEvent_t3674277405;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoEventListener
struct  TangoEventListener_t3965451773  : public Il2CppObject
{
public:

public:
};

struct TangoEventListener_t3965451773_StaticFields
{
public:
	// System.Object Tango.TangoEventListener::m_lockObject
	Il2CppObject * ___m_lockObject_0;
	// Tango.TangoEventProvider/APIOnEventAvailable Tango.TangoEventListener::m_onEventAvailableCallback
	APIOnEventAvailable_t2464565050 * ___m_onEventAvailableCallback_1;
	// Tango.OnTangoEventAvailableEventHandler Tango.TangoEventListener::m_onTangoEventAvailable
	OnTangoEventAvailableEventHandler_t3985195121 * ___m_onTangoEventAvailable_2;
	// Tango.OnTangoEventAvailableEventHandler Tango.TangoEventListener::m_onTangoEventMultithreadedAvailable
	OnTangoEventAvailableEventHandler_t3985195121 * ___m_onTangoEventMultithreadedAvailable_3;
	// Tango.TangoEvent Tango.TangoEventListener::m_tangoEvent
	TangoEvent_t3674277405 * ___m_tangoEvent_4;
	// System.Boolean Tango.TangoEventListener::m_isDirty
	bool ___m_isDirty_5;

public:
	inline static int32_t get_offset_of_m_lockObject_0() { return static_cast<int32_t>(offsetof(TangoEventListener_t3965451773_StaticFields, ___m_lockObject_0)); }
	inline Il2CppObject * get_m_lockObject_0() const { return ___m_lockObject_0; }
	inline Il2CppObject ** get_address_of_m_lockObject_0() { return &___m_lockObject_0; }
	inline void set_m_lockObject_0(Il2CppObject * value)
	{
		___m_lockObject_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_lockObject_0, value);
	}

	inline static int32_t get_offset_of_m_onEventAvailableCallback_1() { return static_cast<int32_t>(offsetof(TangoEventListener_t3965451773_StaticFields, ___m_onEventAvailableCallback_1)); }
	inline APIOnEventAvailable_t2464565050 * get_m_onEventAvailableCallback_1() const { return ___m_onEventAvailableCallback_1; }
	inline APIOnEventAvailable_t2464565050 ** get_address_of_m_onEventAvailableCallback_1() { return &___m_onEventAvailableCallback_1; }
	inline void set_m_onEventAvailableCallback_1(APIOnEventAvailable_t2464565050 * value)
	{
		___m_onEventAvailableCallback_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_onEventAvailableCallback_1, value);
	}

	inline static int32_t get_offset_of_m_onTangoEventAvailable_2() { return static_cast<int32_t>(offsetof(TangoEventListener_t3965451773_StaticFields, ___m_onTangoEventAvailable_2)); }
	inline OnTangoEventAvailableEventHandler_t3985195121 * get_m_onTangoEventAvailable_2() const { return ___m_onTangoEventAvailable_2; }
	inline OnTangoEventAvailableEventHandler_t3985195121 ** get_address_of_m_onTangoEventAvailable_2() { return &___m_onTangoEventAvailable_2; }
	inline void set_m_onTangoEventAvailable_2(OnTangoEventAvailableEventHandler_t3985195121 * value)
	{
		___m_onTangoEventAvailable_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_onTangoEventAvailable_2, value);
	}

	inline static int32_t get_offset_of_m_onTangoEventMultithreadedAvailable_3() { return static_cast<int32_t>(offsetof(TangoEventListener_t3965451773_StaticFields, ___m_onTangoEventMultithreadedAvailable_3)); }
	inline OnTangoEventAvailableEventHandler_t3985195121 * get_m_onTangoEventMultithreadedAvailable_3() const { return ___m_onTangoEventMultithreadedAvailable_3; }
	inline OnTangoEventAvailableEventHandler_t3985195121 ** get_address_of_m_onTangoEventMultithreadedAvailable_3() { return &___m_onTangoEventMultithreadedAvailable_3; }
	inline void set_m_onTangoEventMultithreadedAvailable_3(OnTangoEventAvailableEventHandler_t3985195121 * value)
	{
		___m_onTangoEventMultithreadedAvailable_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_onTangoEventMultithreadedAvailable_3, value);
	}

	inline static int32_t get_offset_of_m_tangoEvent_4() { return static_cast<int32_t>(offsetof(TangoEventListener_t3965451773_StaticFields, ___m_tangoEvent_4)); }
	inline TangoEvent_t3674277405 * get_m_tangoEvent_4() const { return ___m_tangoEvent_4; }
	inline TangoEvent_t3674277405 ** get_address_of_m_tangoEvent_4() { return &___m_tangoEvent_4; }
	inline void set_m_tangoEvent_4(TangoEvent_t3674277405 * value)
	{
		___m_tangoEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoEvent_4, value);
	}

	inline static int32_t get_offset_of_m_isDirty_5() { return static_cast<int32_t>(offsetof(TangoEventListener_t3965451773_StaticFields, ___m_isDirty_5)); }
	inline bool get_m_isDirty_5() const { return ___m_isDirty_5; }
	inline bool* get_address_of_m_isDirty_5() { return &___m_isDirty_5; }
	inline void set_m_isDirty_5(bool value)
	{
		___m_isDirty_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
