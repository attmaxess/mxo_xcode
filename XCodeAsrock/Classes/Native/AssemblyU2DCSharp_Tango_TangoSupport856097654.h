﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoSupport
struct  TangoSupport_t856097654  : public Il2CppObject
{
public:

public:
};

struct TangoSupport_t856097654_StaticFields
{
public:
	// UnityEngine.Matrix4x4 Tango.TangoSupport::UNITY_WORLD_T_START_SERVICE
	Matrix4x4_t2933234003  ___UNITY_WORLD_T_START_SERVICE_0;
	// UnityEngine.Matrix4x4 Tango.TangoSupport::DEVICE_T_UNITY_CAMERA
	Matrix4x4_t2933234003  ___DEVICE_T_UNITY_CAMERA_1;
	// UnityEngine.Matrix4x4 Tango.TangoSupport::COLOR_CAMERA_T_UNITY_CAMERA
	Matrix4x4_t2933234003  ___COLOR_CAMERA_T_UNITY_CAMERA_2;
	// UnityEngine.Matrix4x4 Tango.TangoSupport::m_colorCameraPoseRotation
	Matrix4x4_t2933234003  ___m_colorCameraPoseRotation_3;
	// UnityEngine.Matrix4x4 Tango.TangoSupport::m_devicePoseRotation
	Matrix4x4_t2933234003  ___m_devicePoseRotation_4;
	// UnityEngine.Matrix4x4 Tango.TangoSupport::ROTATION270_T_DEFAULT
	Matrix4x4_t2933234003  ___ROTATION270_T_DEFAULT_7;
	// UnityEngine.Matrix4x4 Tango.TangoSupport::ROTATION180_T_DEFAULT
	Matrix4x4_t2933234003  ___ROTATION180_T_DEFAULT_8;
	// UnityEngine.Matrix4x4 Tango.TangoSupport::ROTATION90_T_DEFAULT
	Matrix4x4_t2933234003  ___ROTATION90_T_DEFAULT_9;

public:
	inline static int32_t get_offset_of_UNITY_WORLD_T_START_SERVICE_0() { return static_cast<int32_t>(offsetof(TangoSupport_t856097654_StaticFields, ___UNITY_WORLD_T_START_SERVICE_0)); }
	inline Matrix4x4_t2933234003  get_UNITY_WORLD_T_START_SERVICE_0() const { return ___UNITY_WORLD_T_START_SERVICE_0; }
	inline Matrix4x4_t2933234003 * get_address_of_UNITY_WORLD_T_START_SERVICE_0() { return &___UNITY_WORLD_T_START_SERVICE_0; }
	inline void set_UNITY_WORLD_T_START_SERVICE_0(Matrix4x4_t2933234003  value)
	{
		___UNITY_WORLD_T_START_SERVICE_0 = value;
	}

	inline static int32_t get_offset_of_DEVICE_T_UNITY_CAMERA_1() { return static_cast<int32_t>(offsetof(TangoSupport_t856097654_StaticFields, ___DEVICE_T_UNITY_CAMERA_1)); }
	inline Matrix4x4_t2933234003  get_DEVICE_T_UNITY_CAMERA_1() const { return ___DEVICE_T_UNITY_CAMERA_1; }
	inline Matrix4x4_t2933234003 * get_address_of_DEVICE_T_UNITY_CAMERA_1() { return &___DEVICE_T_UNITY_CAMERA_1; }
	inline void set_DEVICE_T_UNITY_CAMERA_1(Matrix4x4_t2933234003  value)
	{
		___DEVICE_T_UNITY_CAMERA_1 = value;
	}

	inline static int32_t get_offset_of_COLOR_CAMERA_T_UNITY_CAMERA_2() { return static_cast<int32_t>(offsetof(TangoSupport_t856097654_StaticFields, ___COLOR_CAMERA_T_UNITY_CAMERA_2)); }
	inline Matrix4x4_t2933234003  get_COLOR_CAMERA_T_UNITY_CAMERA_2() const { return ___COLOR_CAMERA_T_UNITY_CAMERA_2; }
	inline Matrix4x4_t2933234003 * get_address_of_COLOR_CAMERA_T_UNITY_CAMERA_2() { return &___COLOR_CAMERA_T_UNITY_CAMERA_2; }
	inline void set_COLOR_CAMERA_T_UNITY_CAMERA_2(Matrix4x4_t2933234003  value)
	{
		___COLOR_CAMERA_T_UNITY_CAMERA_2 = value;
	}

	inline static int32_t get_offset_of_m_colorCameraPoseRotation_3() { return static_cast<int32_t>(offsetof(TangoSupport_t856097654_StaticFields, ___m_colorCameraPoseRotation_3)); }
	inline Matrix4x4_t2933234003  get_m_colorCameraPoseRotation_3() const { return ___m_colorCameraPoseRotation_3; }
	inline Matrix4x4_t2933234003 * get_address_of_m_colorCameraPoseRotation_3() { return &___m_colorCameraPoseRotation_3; }
	inline void set_m_colorCameraPoseRotation_3(Matrix4x4_t2933234003  value)
	{
		___m_colorCameraPoseRotation_3 = value;
	}

	inline static int32_t get_offset_of_m_devicePoseRotation_4() { return static_cast<int32_t>(offsetof(TangoSupport_t856097654_StaticFields, ___m_devicePoseRotation_4)); }
	inline Matrix4x4_t2933234003  get_m_devicePoseRotation_4() const { return ___m_devicePoseRotation_4; }
	inline Matrix4x4_t2933234003 * get_address_of_m_devicePoseRotation_4() { return &___m_devicePoseRotation_4; }
	inline void set_m_devicePoseRotation_4(Matrix4x4_t2933234003  value)
	{
		___m_devicePoseRotation_4 = value;
	}

	inline static int32_t get_offset_of_ROTATION270_T_DEFAULT_7() { return static_cast<int32_t>(offsetof(TangoSupport_t856097654_StaticFields, ___ROTATION270_T_DEFAULT_7)); }
	inline Matrix4x4_t2933234003  get_ROTATION270_T_DEFAULT_7() const { return ___ROTATION270_T_DEFAULT_7; }
	inline Matrix4x4_t2933234003 * get_address_of_ROTATION270_T_DEFAULT_7() { return &___ROTATION270_T_DEFAULT_7; }
	inline void set_ROTATION270_T_DEFAULT_7(Matrix4x4_t2933234003  value)
	{
		___ROTATION270_T_DEFAULT_7 = value;
	}

	inline static int32_t get_offset_of_ROTATION180_T_DEFAULT_8() { return static_cast<int32_t>(offsetof(TangoSupport_t856097654_StaticFields, ___ROTATION180_T_DEFAULT_8)); }
	inline Matrix4x4_t2933234003  get_ROTATION180_T_DEFAULT_8() const { return ___ROTATION180_T_DEFAULT_8; }
	inline Matrix4x4_t2933234003 * get_address_of_ROTATION180_T_DEFAULT_8() { return &___ROTATION180_T_DEFAULT_8; }
	inline void set_ROTATION180_T_DEFAULT_8(Matrix4x4_t2933234003  value)
	{
		___ROTATION180_T_DEFAULT_8 = value;
	}

	inline static int32_t get_offset_of_ROTATION90_T_DEFAULT_9() { return static_cast<int32_t>(offsetof(TangoSupport_t856097654_StaticFields, ___ROTATION90_T_DEFAULT_9)); }
	inline Matrix4x4_t2933234003  get_ROTATION90_T_DEFAULT_9() const { return ___ROTATION90_T_DEFAULT_9; }
	inline Matrix4x4_t2933234003 * get_address_of_ROTATION90_T_DEFAULT_9() { return &___ROTATION90_T_DEFAULT_9; }
	inline void set_ROTATION90_T_DEFAULT_9(Matrix4x4_t2933234003  value)
	{
		___ROTATION90_T_DEFAULT_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
