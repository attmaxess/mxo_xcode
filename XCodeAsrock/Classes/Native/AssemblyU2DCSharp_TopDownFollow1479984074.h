﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TopDownFollow
struct  TopDownFollow_t1479984074  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject TopDownFollow::followTarget
	GameObject_t1756533147 * ___followTarget_2;
	// System.Boolean TopDownFollow::followYaw
	bool ___followYaw_3;
	// UnityEngine.Vector3 TopDownFollow::pos
	Vector3_t2243707580  ___pos_4;
	// UnityEngine.Vector3 TopDownFollow::rotation
	Vector3_t2243707580  ___rotation_5;

public:
	inline static int32_t get_offset_of_followTarget_2() { return static_cast<int32_t>(offsetof(TopDownFollow_t1479984074, ___followTarget_2)); }
	inline GameObject_t1756533147 * get_followTarget_2() const { return ___followTarget_2; }
	inline GameObject_t1756533147 ** get_address_of_followTarget_2() { return &___followTarget_2; }
	inline void set_followTarget_2(GameObject_t1756533147 * value)
	{
		___followTarget_2 = value;
		Il2CppCodeGenWriteBarrier(&___followTarget_2, value);
	}

	inline static int32_t get_offset_of_followYaw_3() { return static_cast<int32_t>(offsetof(TopDownFollow_t1479984074, ___followYaw_3)); }
	inline bool get_followYaw_3() const { return ___followYaw_3; }
	inline bool* get_address_of_followYaw_3() { return &___followYaw_3; }
	inline void set_followYaw_3(bool value)
	{
		___followYaw_3 = value;
	}

	inline static int32_t get_offset_of_pos_4() { return static_cast<int32_t>(offsetof(TopDownFollow_t1479984074, ___pos_4)); }
	inline Vector3_t2243707580  get_pos_4() const { return ___pos_4; }
	inline Vector3_t2243707580 * get_address_of_pos_4() { return &___pos_4; }
	inline void set_pos_4(Vector3_t2243707580  value)
	{
		___pos_4 = value;
	}

	inline static int32_t get_offset_of_rotation_5() { return static_cast<int32_t>(offsetof(TopDownFollow_t1479984074, ___rotation_5)); }
	inline Vector3_t2243707580  get_rotation_5() const { return ___rotation_5; }
	inline Vector3_t2243707580 * get_address_of_rotation_5() { return &___rotation_5; }
	inline void set_rotation_5(Vector3_t2243707580  value)
	{
		___rotation_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
