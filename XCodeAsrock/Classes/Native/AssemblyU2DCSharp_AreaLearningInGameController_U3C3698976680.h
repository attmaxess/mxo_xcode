﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// AreaLearningInGameController/<_DoSaveCurrentAreaDescription>c__Iterator0
struct U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AreaLearningInGameController/<_DoSaveCurrentAreaDescription>c__Iterator0/<_DoSaveCurrentAreaDescription>c__AnonStorey2
struct  U3C_DoSaveCurrentAreaDescriptionU3Ec__AnonStorey2_t3698976680  : public Il2CppObject
{
public:
	// System.String AreaLearningInGameController/<_DoSaveCurrentAreaDescription>c__Iterator0/<_DoSaveCurrentAreaDescription>c__AnonStorey2::name
	String_t* ___name_0;
	// AreaLearningInGameController/<_DoSaveCurrentAreaDescription>c__Iterator0 AreaLearningInGameController/<_DoSaveCurrentAreaDescription>c__Iterator0/<_DoSaveCurrentAreaDescription>c__AnonStorey2::<>f__ref$0
	U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3C_DoSaveCurrentAreaDescriptionU3Ec__AnonStorey2_t3698976680, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3C_DoSaveCurrentAreaDescriptionU3Ec__AnonStorey2_t3698976680, ___U3CU3Ef__refU240_1)); }
	inline U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU240_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
