﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"

// TangoDynamicMesh/MeshSegmentDelegate
struct MeshSegmentDelegate_t389850259;
// Tango.ITangoApplication
struct ITangoApplication_t843339538;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// UnityEngine.MeshCollider
struct MeshCollider_t2718867283;
// System.Collections.Generic.Dictionary`2<Tango.Tango3DReconstruction/GridIndex,TangoDynamicMesh/TangoSingleDynamicMesh>
struct Dictionary_2_t1045046392;
// System.Collections.Generic.List`1<Tango.Tango3DReconstruction/GridIndex>
struct List_1_t1004149398;
// System.Collections.Generic.HashSet`1<Tango.Tango3DReconstruction/GridIndex>
struct HashSet_1_t4263456416;
// UnityEngine.Vector3[][]
struct Vector3U5BU5DU5BU5D_t1477540408;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TangoDynamicMesh
struct  TangoDynamicMesh_t714838395  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TangoDynamicMesh::m_enableDebugUI
	bool ___m_enableDebugUI_2;
	// System.Boolean TangoDynamicMesh::m_enableSelectiveMeshing
	bool ___m_enableSelectiveMeshing_3;
	// TangoDynamicMesh/MeshSegmentDelegate TangoDynamicMesh::m_meshSegmentCreated
	MeshSegmentDelegate_t389850259 * ___m_meshSegmentCreated_4;
	// TangoDynamicMesh/MeshSegmentDelegate TangoDynamicMesh::m_meshSegmentUpdated
	MeshSegmentDelegate_t389850259 * ___m_meshSegmentUpdated_5;
	// System.Single TangoDynamicMesh::m_minDirectionCheck
	float ___m_minDirectionCheck_13;
	// Tango.ITangoApplication TangoDynamicMesh::m_tangoApplication
	Il2CppObject * ___m_tangoApplication_14;
	// UnityEngine.MeshRenderer TangoDynamicMesh::m_meshRenderer
	MeshRenderer_t1268241104 * ___m_meshRenderer_15;
	// UnityEngine.MeshCollider TangoDynamicMesh::m_meshCollider
	MeshCollider_t2718867283 * ___m_meshCollider_16;
	// System.Collections.Generic.Dictionary`2<Tango.Tango3DReconstruction/GridIndex,TangoDynamicMesh/TangoSingleDynamicMesh> TangoDynamicMesh::m_meshes
	Dictionary_2_t1045046392 * ___m_meshes_17;
	// System.Collections.Generic.List`1<Tango.Tango3DReconstruction/GridIndex> TangoDynamicMesh::m_gridIndexToUpdate
	List_1_t1004149398 * ___m_gridIndexToUpdate_18;
	// System.Collections.Generic.HashSet`1<Tango.Tango3DReconstruction/GridIndex> TangoDynamicMesh::m_gridUpdateBacklog
	HashSet_1_t4263456416 * ___m_gridUpdateBacklog_19;
	// System.Int32 TangoDynamicMesh::m_debugTotalVertices
	int32_t ___m_debugTotalVertices_20;
	// System.Int32 TangoDynamicMesh::m_debugTotalTriangles
	int32_t ___m_debugTotalTriangles_21;
	// System.Single TangoDynamicMesh::m_debugRemeshingTime
	float ___m_debugRemeshingTime_22;
	// System.Int32 TangoDynamicMesh::m_debugRemeshingCount
	int32_t ___m_debugRemeshingCount_23;
	// UnityEngine.Vector3[][] TangoDynamicMesh::m_gridIndexConfigs
	Vector3U5BU5DU5BU5D_t1477540408* ___m_gridIndexConfigs_24;
	// UnityEngine.Bounds TangoDynamicMesh::m_bounds
	Bounds_t3033363703  ___m_bounds_25;

public:
	inline static int32_t get_offset_of_m_enableDebugUI_2() { return static_cast<int32_t>(offsetof(TangoDynamicMesh_t714838395, ___m_enableDebugUI_2)); }
	inline bool get_m_enableDebugUI_2() const { return ___m_enableDebugUI_2; }
	inline bool* get_address_of_m_enableDebugUI_2() { return &___m_enableDebugUI_2; }
	inline void set_m_enableDebugUI_2(bool value)
	{
		___m_enableDebugUI_2 = value;
	}

	inline static int32_t get_offset_of_m_enableSelectiveMeshing_3() { return static_cast<int32_t>(offsetof(TangoDynamicMesh_t714838395, ___m_enableSelectiveMeshing_3)); }
	inline bool get_m_enableSelectiveMeshing_3() const { return ___m_enableSelectiveMeshing_3; }
	inline bool* get_address_of_m_enableSelectiveMeshing_3() { return &___m_enableSelectiveMeshing_3; }
	inline void set_m_enableSelectiveMeshing_3(bool value)
	{
		___m_enableSelectiveMeshing_3 = value;
	}

	inline static int32_t get_offset_of_m_meshSegmentCreated_4() { return static_cast<int32_t>(offsetof(TangoDynamicMesh_t714838395, ___m_meshSegmentCreated_4)); }
	inline MeshSegmentDelegate_t389850259 * get_m_meshSegmentCreated_4() const { return ___m_meshSegmentCreated_4; }
	inline MeshSegmentDelegate_t389850259 ** get_address_of_m_meshSegmentCreated_4() { return &___m_meshSegmentCreated_4; }
	inline void set_m_meshSegmentCreated_4(MeshSegmentDelegate_t389850259 * value)
	{
		___m_meshSegmentCreated_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_meshSegmentCreated_4, value);
	}

	inline static int32_t get_offset_of_m_meshSegmentUpdated_5() { return static_cast<int32_t>(offsetof(TangoDynamicMesh_t714838395, ___m_meshSegmentUpdated_5)); }
	inline MeshSegmentDelegate_t389850259 * get_m_meshSegmentUpdated_5() const { return ___m_meshSegmentUpdated_5; }
	inline MeshSegmentDelegate_t389850259 ** get_address_of_m_meshSegmentUpdated_5() { return &___m_meshSegmentUpdated_5; }
	inline void set_m_meshSegmentUpdated_5(MeshSegmentDelegate_t389850259 * value)
	{
		___m_meshSegmentUpdated_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_meshSegmentUpdated_5, value);
	}

	inline static int32_t get_offset_of_m_minDirectionCheck_13() { return static_cast<int32_t>(offsetof(TangoDynamicMesh_t714838395, ___m_minDirectionCheck_13)); }
	inline float get_m_minDirectionCheck_13() const { return ___m_minDirectionCheck_13; }
	inline float* get_address_of_m_minDirectionCheck_13() { return &___m_minDirectionCheck_13; }
	inline void set_m_minDirectionCheck_13(float value)
	{
		___m_minDirectionCheck_13 = value;
	}

	inline static int32_t get_offset_of_m_tangoApplication_14() { return static_cast<int32_t>(offsetof(TangoDynamicMesh_t714838395, ___m_tangoApplication_14)); }
	inline Il2CppObject * get_m_tangoApplication_14() const { return ___m_tangoApplication_14; }
	inline Il2CppObject ** get_address_of_m_tangoApplication_14() { return &___m_tangoApplication_14; }
	inline void set_m_tangoApplication_14(Il2CppObject * value)
	{
		___m_tangoApplication_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_14, value);
	}

	inline static int32_t get_offset_of_m_meshRenderer_15() { return static_cast<int32_t>(offsetof(TangoDynamicMesh_t714838395, ___m_meshRenderer_15)); }
	inline MeshRenderer_t1268241104 * get_m_meshRenderer_15() const { return ___m_meshRenderer_15; }
	inline MeshRenderer_t1268241104 ** get_address_of_m_meshRenderer_15() { return &___m_meshRenderer_15; }
	inline void set_m_meshRenderer_15(MeshRenderer_t1268241104 * value)
	{
		___m_meshRenderer_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_meshRenderer_15, value);
	}

	inline static int32_t get_offset_of_m_meshCollider_16() { return static_cast<int32_t>(offsetof(TangoDynamicMesh_t714838395, ___m_meshCollider_16)); }
	inline MeshCollider_t2718867283 * get_m_meshCollider_16() const { return ___m_meshCollider_16; }
	inline MeshCollider_t2718867283 ** get_address_of_m_meshCollider_16() { return &___m_meshCollider_16; }
	inline void set_m_meshCollider_16(MeshCollider_t2718867283 * value)
	{
		___m_meshCollider_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_meshCollider_16, value);
	}

	inline static int32_t get_offset_of_m_meshes_17() { return static_cast<int32_t>(offsetof(TangoDynamicMesh_t714838395, ___m_meshes_17)); }
	inline Dictionary_2_t1045046392 * get_m_meshes_17() const { return ___m_meshes_17; }
	inline Dictionary_2_t1045046392 ** get_address_of_m_meshes_17() { return &___m_meshes_17; }
	inline void set_m_meshes_17(Dictionary_2_t1045046392 * value)
	{
		___m_meshes_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_meshes_17, value);
	}

	inline static int32_t get_offset_of_m_gridIndexToUpdate_18() { return static_cast<int32_t>(offsetof(TangoDynamicMesh_t714838395, ___m_gridIndexToUpdate_18)); }
	inline List_1_t1004149398 * get_m_gridIndexToUpdate_18() const { return ___m_gridIndexToUpdate_18; }
	inline List_1_t1004149398 ** get_address_of_m_gridIndexToUpdate_18() { return &___m_gridIndexToUpdate_18; }
	inline void set_m_gridIndexToUpdate_18(List_1_t1004149398 * value)
	{
		___m_gridIndexToUpdate_18 = value;
		Il2CppCodeGenWriteBarrier(&___m_gridIndexToUpdate_18, value);
	}

	inline static int32_t get_offset_of_m_gridUpdateBacklog_19() { return static_cast<int32_t>(offsetof(TangoDynamicMesh_t714838395, ___m_gridUpdateBacklog_19)); }
	inline HashSet_1_t4263456416 * get_m_gridUpdateBacklog_19() const { return ___m_gridUpdateBacklog_19; }
	inline HashSet_1_t4263456416 ** get_address_of_m_gridUpdateBacklog_19() { return &___m_gridUpdateBacklog_19; }
	inline void set_m_gridUpdateBacklog_19(HashSet_1_t4263456416 * value)
	{
		___m_gridUpdateBacklog_19 = value;
		Il2CppCodeGenWriteBarrier(&___m_gridUpdateBacklog_19, value);
	}

	inline static int32_t get_offset_of_m_debugTotalVertices_20() { return static_cast<int32_t>(offsetof(TangoDynamicMesh_t714838395, ___m_debugTotalVertices_20)); }
	inline int32_t get_m_debugTotalVertices_20() const { return ___m_debugTotalVertices_20; }
	inline int32_t* get_address_of_m_debugTotalVertices_20() { return &___m_debugTotalVertices_20; }
	inline void set_m_debugTotalVertices_20(int32_t value)
	{
		___m_debugTotalVertices_20 = value;
	}

	inline static int32_t get_offset_of_m_debugTotalTriangles_21() { return static_cast<int32_t>(offsetof(TangoDynamicMesh_t714838395, ___m_debugTotalTriangles_21)); }
	inline int32_t get_m_debugTotalTriangles_21() const { return ___m_debugTotalTriangles_21; }
	inline int32_t* get_address_of_m_debugTotalTriangles_21() { return &___m_debugTotalTriangles_21; }
	inline void set_m_debugTotalTriangles_21(int32_t value)
	{
		___m_debugTotalTriangles_21 = value;
	}

	inline static int32_t get_offset_of_m_debugRemeshingTime_22() { return static_cast<int32_t>(offsetof(TangoDynamicMesh_t714838395, ___m_debugRemeshingTime_22)); }
	inline float get_m_debugRemeshingTime_22() const { return ___m_debugRemeshingTime_22; }
	inline float* get_address_of_m_debugRemeshingTime_22() { return &___m_debugRemeshingTime_22; }
	inline void set_m_debugRemeshingTime_22(float value)
	{
		___m_debugRemeshingTime_22 = value;
	}

	inline static int32_t get_offset_of_m_debugRemeshingCount_23() { return static_cast<int32_t>(offsetof(TangoDynamicMesh_t714838395, ___m_debugRemeshingCount_23)); }
	inline int32_t get_m_debugRemeshingCount_23() const { return ___m_debugRemeshingCount_23; }
	inline int32_t* get_address_of_m_debugRemeshingCount_23() { return &___m_debugRemeshingCount_23; }
	inline void set_m_debugRemeshingCount_23(int32_t value)
	{
		___m_debugRemeshingCount_23 = value;
	}

	inline static int32_t get_offset_of_m_gridIndexConfigs_24() { return static_cast<int32_t>(offsetof(TangoDynamicMesh_t714838395, ___m_gridIndexConfigs_24)); }
	inline Vector3U5BU5DU5BU5D_t1477540408* get_m_gridIndexConfigs_24() const { return ___m_gridIndexConfigs_24; }
	inline Vector3U5BU5DU5BU5D_t1477540408** get_address_of_m_gridIndexConfigs_24() { return &___m_gridIndexConfigs_24; }
	inline void set_m_gridIndexConfigs_24(Vector3U5BU5DU5BU5D_t1477540408* value)
	{
		___m_gridIndexConfigs_24 = value;
		Il2CppCodeGenWriteBarrier(&___m_gridIndexConfigs_24, value);
	}

	inline static int32_t get_offset_of_m_bounds_25() { return static_cast<int32_t>(offsetof(TangoDynamicMesh_t714838395, ___m_bounds_25)); }
	inline Bounds_t3033363703  get_m_bounds_25() const { return ___m_bounds_25; }
	inline Bounds_t3033363703 * get_address_of_m_bounds_25() { return &___m_bounds_25; }
	inline void set_m_bounds_25(Bounds_t3033363703  value)
	{
		___m_bounds_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
