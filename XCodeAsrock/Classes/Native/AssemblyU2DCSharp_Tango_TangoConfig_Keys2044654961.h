﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoConfig/Keys
struct  Keys_t2044654961 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Keys_t2044654961__padding[1];
	};

public:
};

struct Keys_t2044654961_StaticFields
{
public:
	// System.String Tango.TangoConfig/Keys::ENABLE_DATASET_RECORDING
	String_t* ___ENABLE_DATASET_RECORDING_14;
	// System.String Tango.TangoConfig/Keys::GET_TANGO_SERVICE_VERSION_STRING
	String_t* ___GET_TANGO_SERVICE_VERSION_STRING_15;
	// System.String Tango.TangoConfig/Keys::RUNTIME_DEPTH_FRAMERATE
	String_t* ___RUNTIME_DEPTH_FRAMERATE_16;

public:
	inline static int32_t get_offset_of_ENABLE_DATASET_RECORDING_14() { return static_cast<int32_t>(offsetof(Keys_t2044654961_StaticFields, ___ENABLE_DATASET_RECORDING_14)); }
	inline String_t* get_ENABLE_DATASET_RECORDING_14() const { return ___ENABLE_DATASET_RECORDING_14; }
	inline String_t** get_address_of_ENABLE_DATASET_RECORDING_14() { return &___ENABLE_DATASET_RECORDING_14; }
	inline void set_ENABLE_DATASET_RECORDING_14(String_t* value)
	{
		___ENABLE_DATASET_RECORDING_14 = value;
		Il2CppCodeGenWriteBarrier(&___ENABLE_DATASET_RECORDING_14, value);
	}

	inline static int32_t get_offset_of_GET_TANGO_SERVICE_VERSION_STRING_15() { return static_cast<int32_t>(offsetof(Keys_t2044654961_StaticFields, ___GET_TANGO_SERVICE_VERSION_STRING_15)); }
	inline String_t* get_GET_TANGO_SERVICE_VERSION_STRING_15() const { return ___GET_TANGO_SERVICE_VERSION_STRING_15; }
	inline String_t** get_address_of_GET_TANGO_SERVICE_VERSION_STRING_15() { return &___GET_TANGO_SERVICE_VERSION_STRING_15; }
	inline void set_GET_TANGO_SERVICE_VERSION_STRING_15(String_t* value)
	{
		___GET_TANGO_SERVICE_VERSION_STRING_15 = value;
		Il2CppCodeGenWriteBarrier(&___GET_TANGO_SERVICE_VERSION_STRING_15, value);
	}

	inline static int32_t get_offset_of_RUNTIME_DEPTH_FRAMERATE_16() { return static_cast<int32_t>(offsetof(Keys_t2044654961_StaticFields, ___RUNTIME_DEPTH_FRAMERATE_16)); }
	inline String_t* get_RUNTIME_DEPTH_FRAMERATE_16() const { return ___RUNTIME_DEPTH_FRAMERATE_16; }
	inline String_t** get_address_of_RUNTIME_DEPTH_FRAMERATE_16() { return &___RUNTIME_DEPTH_FRAMERATE_16; }
	inline void set_RUNTIME_DEPTH_FRAMERATE_16(String_t* value)
	{
		___RUNTIME_DEPTH_FRAMERATE_16 = value;
		Il2CppCodeGenWriteBarrier(&___RUNTIME_DEPTH_FRAMERATE_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
