﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// ADMGUIController/<_DoSaveCurrentAreaDescription>c__Iterator2
struct U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator2_t2439469346;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ADMGUIController/<_DoSaveCurrentAreaDescription>c__Iterator2/<_DoSaveCurrentAreaDescription>c__AnonStorey4
struct  U3C_DoSaveCurrentAreaDescriptionU3Ec__AnonStorey4_t3411365485  : public Il2CppObject
{
public:
	// System.String ADMGUIController/<_DoSaveCurrentAreaDescription>c__Iterator2/<_DoSaveCurrentAreaDescription>c__AnonStorey4::fileNameFromKeyboard
	String_t* ___fileNameFromKeyboard_0;
	// ADMGUIController/<_DoSaveCurrentAreaDescription>c__Iterator2 ADMGUIController/<_DoSaveCurrentAreaDescription>c__Iterator2/<_DoSaveCurrentAreaDescription>c__AnonStorey4::<>f__ref$2
	U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator2_t2439469346 * ___U3CU3Ef__refU242_1;

public:
	inline static int32_t get_offset_of_fileNameFromKeyboard_0() { return static_cast<int32_t>(offsetof(U3C_DoSaveCurrentAreaDescriptionU3Ec__AnonStorey4_t3411365485, ___fileNameFromKeyboard_0)); }
	inline String_t* get_fileNameFromKeyboard_0() const { return ___fileNameFromKeyboard_0; }
	inline String_t** get_address_of_fileNameFromKeyboard_0() { return &___fileNameFromKeyboard_0; }
	inline void set_fileNameFromKeyboard_0(String_t* value)
	{
		___fileNameFromKeyboard_0 = value;
		Il2CppCodeGenWriteBarrier(&___fileNameFromKeyboard_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU242_1() { return static_cast<int32_t>(offsetof(U3C_DoSaveCurrentAreaDescriptionU3Ec__AnonStorey4_t3411365485, ___U3CU3Ef__refU242_1)); }
	inline U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator2_t2439469346 * get_U3CU3Ef__refU242_1() const { return ___U3CU3Ef__refU242_1; }
	inline U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator2_t2439469346 ** get_address_of_U3CU3Ef__refU242_1() { return &___U3CU3Ef__refU242_1; }
	inline void set_U3CU3Ef__refU242_1(U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator2_t2439469346 * value)
	{
		___U3CU3Ef__refU242_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU242_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
