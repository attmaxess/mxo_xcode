﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_TangoPresentController_TangoState749463897.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TangoPresentController
struct  TangoPresentController_t3947495060  : public MonoBehaviour_t1158329972
{
public:
	// TangoPresentController/TangoState TangoPresentController::m_editorPlayModeState
	int32_t ___m_editorPlayModeState_2;
	// UnityEngine.GameObject[] TangoPresentController::m_enableIfTangoPresent
	GameObjectU5BU5D_t3057952154* ___m_enableIfTangoPresent_3;
	// UnityEngine.GameObject[] TangoPresentController::m_enableIfTangoOutOfDate
	GameObjectU5BU5D_t3057952154* ___m_enableIfTangoOutOfDate_4;
	// UnityEngine.GameObject[] TangoPresentController::m_disableIfTangoPresent
	GameObjectU5BU5D_t3057952154* ___m_disableIfTangoPresent_5;

public:
	inline static int32_t get_offset_of_m_editorPlayModeState_2() { return static_cast<int32_t>(offsetof(TangoPresentController_t3947495060, ___m_editorPlayModeState_2)); }
	inline int32_t get_m_editorPlayModeState_2() const { return ___m_editorPlayModeState_2; }
	inline int32_t* get_address_of_m_editorPlayModeState_2() { return &___m_editorPlayModeState_2; }
	inline void set_m_editorPlayModeState_2(int32_t value)
	{
		___m_editorPlayModeState_2 = value;
	}

	inline static int32_t get_offset_of_m_enableIfTangoPresent_3() { return static_cast<int32_t>(offsetof(TangoPresentController_t3947495060, ___m_enableIfTangoPresent_3)); }
	inline GameObjectU5BU5D_t3057952154* get_m_enableIfTangoPresent_3() const { return ___m_enableIfTangoPresent_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_enableIfTangoPresent_3() { return &___m_enableIfTangoPresent_3; }
	inline void set_m_enableIfTangoPresent_3(GameObjectU5BU5D_t3057952154* value)
	{
		___m_enableIfTangoPresent_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_enableIfTangoPresent_3, value);
	}

	inline static int32_t get_offset_of_m_enableIfTangoOutOfDate_4() { return static_cast<int32_t>(offsetof(TangoPresentController_t3947495060, ___m_enableIfTangoOutOfDate_4)); }
	inline GameObjectU5BU5D_t3057952154* get_m_enableIfTangoOutOfDate_4() const { return ___m_enableIfTangoOutOfDate_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_enableIfTangoOutOfDate_4() { return &___m_enableIfTangoOutOfDate_4; }
	inline void set_m_enableIfTangoOutOfDate_4(GameObjectU5BU5D_t3057952154* value)
	{
		___m_enableIfTangoOutOfDate_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_enableIfTangoOutOfDate_4, value);
	}

	inline static int32_t get_offset_of_m_disableIfTangoPresent_5() { return static_cast<int32_t>(offsetof(TangoPresentController_t3947495060, ___m_disableIfTangoPresent_5)); }
	inline GameObjectU5BU5D_t3057952154* get_m_disableIfTangoPresent_5() const { return ___m_disableIfTangoPresent_5; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_disableIfTangoPresent_5() { return &___m_disableIfTangoPresent_5; }
	inline void set_m_disableIfTangoPresent_5(GameObjectU5BU5D_t3057952154* value)
	{
		___m_disableIfTangoPresent_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_disableIfTangoPresent_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
