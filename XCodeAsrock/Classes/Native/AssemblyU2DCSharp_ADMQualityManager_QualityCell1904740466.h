﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ADMQualityManager/QualityCell
struct  QualityCell_t1904740466  : public Il2CppObject
{
public:
	// System.Boolean[] ADMQualityManager/QualityCell::m_angleVisited
	BooleanU5BU5D_t3568034315* ___m_angleVisited_0;
	// UnityEngine.GameObject ADMQualityManager/QualityCell::m_visuals
	GameObject_t1756533147 * ___m_visuals_1;

public:
	inline static int32_t get_offset_of_m_angleVisited_0() { return static_cast<int32_t>(offsetof(QualityCell_t1904740466, ___m_angleVisited_0)); }
	inline BooleanU5BU5D_t3568034315* get_m_angleVisited_0() const { return ___m_angleVisited_0; }
	inline BooleanU5BU5D_t3568034315** get_address_of_m_angleVisited_0() { return &___m_angleVisited_0; }
	inline void set_m_angleVisited_0(BooleanU5BU5D_t3568034315* value)
	{
		___m_angleVisited_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_angleVisited_0, value);
	}

	inline static int32_t get_offset_of_m_visuals_1() { return static_cast<int32_t>(offsetof(QualityCell_t1904740466, ___m_visuals_1)); }
	inline GameObject_t1756533147 * get_m_visuals_1() const { return ___m_visuals_1; }
	inline GameObject_t1756533147 ** get_address_of_m_visuals_1() { return &___m_visuals_1; }
	inline void set_m_visuals_1(GameObject_t1756533147 * value)
	{
		___m_visuals_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_visuals_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
