﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<ADMQualityManager/QualityCell>>
struct List_1_t642982730;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ADMQualityManager
struct  ADMQualityManager_t1949231632  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform ADMQualityManager::m_poseController
	Transform_t3275118058 * ___m_poseController_2;
	// UnityEngine.GameObject ADMQualityManager::m_cellVisualsPrefab
	GameObject_t1756533147 * ___m_cellVisualsPrefab_3;
	// UnityEngine.RectTransform ADMQualityManager::m_badQualityTransformParent
	RectTransform_t3349966182 * ___m_badQualityTransformParent_4;
	// UnityEngine.UI.Text ADMQualityManager::m_qualityText
	Text_t356221433 * ___m_qualityText_5;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<ADMQualityManager/QualityCell>> ADMQualityManager::m_cellQualities
	List_1_t642982730 * ___m_cellQualities_10;
	// UnityEngine.Vector2 ADMQualityManager::m_cellsOrigin
	Vector2_t2243707579  ___m_cellsOrigin_11;
	// System.Single ADMQualityManager::m_badQualityTransformTime
	float ___m_badQualityTransformTime_12;

public:
	inline static int32_t get_offset_of_m_poseController_2() { return static_cast<int32_t>(offsetof(ADMQualityManager_t1949231632, ___m_poseController_2)); }
	inline Transform_t3275118058 * get_m_poseController_2() const { return ___m_poseController_2; }
	inline Transform_t3275118058 ** get_address_of_m_poseController_2() { return &___m_poseController_2; }
	inline void set_m_poseController_2(Transform_t3275118058 * value)
	{
		___m_poseController_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_poseController_2, value);
	}

	inline static int32_t get_offset_of_m_cellVisualsPrefab_3() { return static_cast<int32_t>(offsetof(ADMQualityManager_t1949231632, ___m_cellVisualsPrefab_3)); }
	inline GameObject_t1756533147 * get_m_cellVisualsPrefab_3() const { return ___m_cellVisualsPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_m_cellVisualsPrefab_3() { return &___m_cellVisualsPrefab_3; }
	inline void set_m_cellVisualsPrefab_3(GameObject_t1756533147 * value)
	{
		___m_cellVisualsPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellVisualsPrefab_3, value);
	}

	inline static int32_t get_offset_of_m_badQualityTransformParent_4() { return static_cast<int32_t>(offsetof(ADMQualityManager_t1949231632, ___m_badQualityTransformParent_4)); }
	inline RectTransform_t3349966182 * get_m_badQualityTransformParent_4() const { return ___m_badQualityTransformParent_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_badQualityTransformParent_4() { return &___m_badQualityTransformParent_4; }
	inline void set_m_badQualityTransformParent_4(RectTransform_t3349966182 * value)
	{
		___m_badQualityTransformParent_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_badQualityTransformParent_4, value);
	}

	inline static int32_t get_offset_of_m_qualityText_5() { return static_cast<int32_t>(offsetof(ADMQualityManager_t1949231632, ___m_qualityText_5)); }
	inline Text_t356221433 * get_m_qualityText_5() const { return ___m_qualityText_5; }
	inline Text_t356221433 ** get_address_of_m_qualityText_5() { return &___m_qualityText_5; }
	inline void set_m_qualityText_5(Text_t356221433 * value)
	{
		___m_qualityText_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_qualityText_5, value);
	}

	inline static int32_t get_offset_of_m_cellQualities_10() { return static_cast<int32_t>(offsetof(ADMQualityManager_t1949231632, ___m_cellQualities_10)); }
	inline List_1_t642982730 * get_m_cellQualities_10() const { return ___m_cellQualities_10; }
	inline List_1_t642982730 ** get_address_of_m_cellQualities_10() { return &___m_cellQualities_10; }
	inline void set_m_cellQualities_10(List_1_t642982730 * value)
	{
		___m_cellQualities_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellQualities_10, value);
	}

	inline static int32_t get_offset_of_m_cellsOrigin_11() { return static_cast<int32_t>(offsetof(ADMQualityManager_t1949231632, ___m_cellsOrigin_11)); }
	inline Vector2_t2243707579  get_m_cellsOrigin_11() const { return ___m_cellsOrigin_11; }
	inline Vector2_t2243707579 * get_address_of_m_cellsOrigin_11() { return &___m_cellsOrigin_11; }
	inline void set_m_cellsOrigin_11(Vector2_t2243707579  value)
	{
		___m_cellsOrigin_11 = value;
	}

	inline static int32_t get_offset_of_m_badQualityTransformTime_12() { return static_cast<int32_t>(offsetof(ADMQualityManager_t1949231632, ___m_badQualityTransformTime_12)); }
	inline float get_m_badQualityTransformTime_12() const { return ___m_badQualityTransformTime_12; }
	inline float* get_address_of_m_badQualityTransformTime_12() { return &___m_badQualityTransformTime_12; }
	inline void set_m_badQualityTransformTime_12(float value)
	{
		___m_badQualityTransformTime_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
