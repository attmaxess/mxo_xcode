﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Tango.TangoApplication
struct TangoApplication_t278578959;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<Tango.TangoSupport/Marker>
struct List_1_t531672424;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QRCodeReaderTango
struct  QRCodeReaderTango_t2920665506  : public MonoBehaviour_t1158329972
{
public:
	// Tango.TangoApplication QRCodeReaderTango::tangoApp
	TangoApplication_t278578959 * ___tangoApp_3;
	// System.Boolean QRCodeReaderTango::done
	bool ___done_4;
	// UnityEngine.GameObject QRCodeReaderTango::qrcodePlane
	GameObject_t1756533147 * ___qrcodePlane_5;
	// UnityEngine.GameObject QRCodeReaderTango::plane
	GameObject_t1756533147 * ___plane_6;
	// System.Collections.Generic.List`1<Tango.TangoSupport/Marker> QRCodeReaderTango::markerList
	List_1_t531672424 * ___markerList_7;

public:
	inline static int32_t get_offset_of_tangoApp_3() { return static_cast<int32_t>(offsetof(QRCodeReaderTango_t2920665506, ___tangoApp_3)); }
	inline TangoApplication_t278578959 * get_tangoApp_3() const { return ___tangoApp_3; }
	inline TangoApplication_t278578959 ** get_address_of_tangoApp_3() { return &___tangoApp_3; }
	inline void set_tangoApp_3(TangoApplication_t278578959 * value)
	{
		___tangoApp_3 = value;
		Il2CppCodeGenWriteBarrier(&___tangoApp_3, value);
	}

	inline static int32_t get_offset_of_done_4() { return static_cast<int32_t>(offsetof(QRCodeReaderTango_t2920665506, ___done_4)); }
	inline bool get_done_4() const { return ___done_4; }
	inline bool* get_address_of_done_4() { return &___done_4; }
	inline void set_done_4(bool value)
	{
		___done_4 = value;
	}

	inline static int32_t get_offset_of_qrcodePlane_5() { return static_cast<int32_t>(offsetof(QRCodeReaderTango_t2920665506, ___qrcodePlane_5)); }
	inline GameObject_t1756533147 * get_qrcodePlane_5() const { return ___qrcodePlane_5; }
	inline GameObject_t1756533147 ** get_address_of_qrcodePlane_5() { return &___qrcodePlane_5; }
	inline void set_qrcodePlane_5(GameObject_t1756533147 * value)
	{
		___qrcodePlane_5 = value;
		Il2CppCodeGenWriteBarrier(&___qrcodePlane_5, value);
	}

	inline static int32_t get_offset_of_plane_6() { return static_cast<int32_t>(offsetof(QRCodeReaderTango_t2920665506, ___plane_6)); }
	inline GameObject_t1756533147 * get_plane_6() const { return ___plane_6; }
	inline GameObject_t1756533147 ** get_address_of_plane_6() { return &___plane_6; }
	inline void set_plane_6(GameObject_t1756533147 * value)
	{
		___plane_6 = value;
		Il2CppCodeGenWriteBarrier(&___plane_6, value);
	}

	inline static int32_t get_offset_of_markerList_7() { return static_cast<int32_t>(offsetof(QRCodeReaderTango_t2920665506, ___markerList_7)); }
	inline List_1_t531672424 * get_markerList_7() const { return ___markerList_7; }
	inline List_1_t531672424 ** get_address_of_markerList_7() { return &___markerList_7; }
	inline void set_markerList_7(List_1_t531672424 * value)
	{
		___markerList_7 = value;
		Il2CppCodeGenWriteBarrier(&___markerList_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
