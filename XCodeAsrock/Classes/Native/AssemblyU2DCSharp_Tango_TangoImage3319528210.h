﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_Tango_TangoEnums_TangoImageForma3932021136.h"
#include "mscorlib_System_IntPtr2504060609.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoImage
struct  TangoImage_t3319528210 
{
public:
	// System.UInt32 Tango.TangoImage::m_width
	uint32_t ___m_width_0;
	// System.UInt32 Tango.TangoImage::m_height
	uint32_t ___m_height_1;
	// Tango.TangoEnums/TangoImageFormatType Tango.TangoImage::m_format
	int32_t ___m_format_2;
	// System.Int64 Tango.TangoImage::m_timestampNs
	int64_t ___m_timestampNs_3;
	// System.UInt32 Tango.TangoImage::m_numPlanes
	uint32_t ___m_numPlanes_4;
	// System.IntPtr Tango.TangoImage::m_planeData0
	IntPtr_t ___m_planeData0_5;
	// System.IntPtr Tango.TangoImage::m_planeData1
	IntPtr_t ___m_planeData1_6;
	// System.IntPtr Tango.TangoImage::m_planeData2
	IntPtr_t ___m_planeData2_7;
	// System.IntPtr Tango.TangoImage::m_planeData3
	IntPtr_t ___m_planeData3_8;
	// System.Int32 Tango.TangoImage::m_planeSize0
	int32_t ___m_planeSize0_9;
	// System.Int32 Tango.TangoImage::m_planeSize1
	int32_t ___m_planeSize1_10;
	// System.Int32 Tango.TangoImage::m_planeSize2
	int32_t ___m_planeSize2_11;
	// System.Int32 Tango.TangoImage::m_planeSize3
	int32_t ___m_planeSize3_12;
	// System.Int32 Tango.TangoImage::m_planeRowStride0
	int32_t ___m_planeRowStride0_13;
	// System.Int32 Tango.TangoImage::m_planeRowStride1
	int32_t ___m_planeRowStride1_14;
	// System.Int32 Tango.TangoImage::m_planeRowStride2
	int32_t ___m_planeRowStride2_15;
	// System.Int32 Tango.TangoImage::m_planeRowStride3
	int32_t ___m_planeRowStride3_16;
	// System.Int32 Tango.TangoImage::m_planePpixelStride0
	int32_t ___m_planePpixelStride0_17;
	// System.Int32 Tango.TangoImage::m_planePixelStride1
	int32_t ___m_planePixelStride1_18;
	// System.Int32 Tango.TangoImage::m_planePixelStride2
	int32_t ___m_planePixelStride2_19;
	// System.Int32 Tango.TangoImage::m_planePixelStride3
	int32_t ___m_planePixelStride3_20;

public:
	inline static int32_t get_offset_of_m_width_0() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_width_0)); }
	inline uint32_t get_m_width_0() const { return ___m_width_0; }
	inline uint32_t* get_address_of_m_width_0() { return &___m_width_0; }
	inline void set_m_width_0(uint32_t value)
	{
		___m_width_0 = value;
	}

	inline static int32_t get_offset_of_m_height_1() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_height_1)); }
	inline uint32_t get_m_height_1() const { return ___m_height_1; }
	inline uint32_t* get_address_of_m_height_1() { return &___m_height_1; }
	inline void set_m_height_1(uint32_t value)
	{
		___m_height_1 = value;
	}

	inline static int32_t get_offset_of_m_format_2() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_format_2)); }
	inline int32_t get_m_format_2() const { return ___m_format_2; }
	inline int32_t* get_address_of_m_format_2() { return &___m_format_2; }
	inline void set_m_format_2(int32_t value)
	{
		___m_format_2 = value;
	}

	inline static int32_t get_offset_of_m_timestampNs_3() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_timestampNs_3)); }
	inline int64_t get_m_timestampNs_3() const { return ___m_timestampNs_3; }
	inline int64_t* get_address_of_m_timestampNs_3() { return &___m_timestampNs_3; }
	inline void set_m_timestampNs_3(int64_t value)
	{
		___m_timestampNs_3 = value;
	}

	inline static int32_t get_offset_of_m_numPlanes_4() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_numPlanes_4)); }
	inline uint32_t get_m_numPlanes_4() const { return ___m_numPlanes_4; }
	inline uint32_t* get_address_of_m_numPlanes_4() { return &___m_numPlanes_4; }
	inline void set_m_numPlanes_4(uint32_t value)
	{
		___m_numPlanes_4 = value;
	}

	inline static int32_t get_offset_of_m_planeData0_5() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_planeData0_5)); }
	inline IntPtr_t get_m_planeData0_5() const { return ___m_planeData0_5; }
	inline IntPtr_t* get_address_of_m_planeData0_5() { return &___m_planeData0_5; }
	inline void set_m_planeData0_5(IntPtr_t value)
	{
		___m_planeData0_5 = value;
	}

	inline static int32_t get_offset_of_m_planeData1_6() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_planeData1_6)); }
	inline IntPtr_t get_m_planeData1_6() const { return ___m_planeData1_6; }
	inline IntPtr_t* get_address_of_m_planeData1_6() { return &___m_planeData1_6; }
	inline void set_m_planeData1_6(IntPtr_t value)
	{
		___m_planeData1_6 = value;
	}

	inline static int32_t get_offset_of_m_planeData2_7() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_planeData2_7)); }
	inline IntPtr_t get_m_planeData2_7() const { return ___m_planeData2_7; }
	inline IntPtr_t* get_address_of_m_planeData2_7() { return &___m_planeData2_7; }
	inline void set_m_planeData2_7(IntPtr_t value)
	{
		___m_planeData2_7 = value;
	}

	inline static int32_t get_offset_of_m_planeData3_8() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_planeData3_8)); }
	inline IntPtr_t get_m_planeData3_8() const { return ___m_planeData3_8; }
	inline IntPtr_t* get_address_of_m_planeData3_8() { return &___m_planeData3_8; }
	inline void set_m_planeData3_8(IntPtr_t value)
	{
		___m_planeData3_8 = value;
	}

	inline static int32_t get_offset_of_m_planeSize0_9() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_planeSize0_9)); }
	inline int32_t get_m_planeSize0_9() const { return ___m_planeSize0_9; }
	inline int32_t* get_address_of_m_planeSize0_9() { return &___m_planeSize0_9; }
	inline void set_m_planeSize0_9(int32_t value)
	{
		___m_planeSize0_9 = value;
	}

	inline static int32_t get_offset_of_m_planeSize1_10() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_planeSize1_10)); }
	inline int32_t get_m_planeSize1_10() const { return ___m_planeSize1_10; }
	inline int32_t* get_address_of_m_planeSize1_10() { return &___m_planeSize1_10; }
	inline void set_m_planeSize1_10(int32_t value)
	{
		___m_planeSize1_10 = value;
	}

	inline static int32_t get_offset_of_m_planeSize2_11() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_planeSize2_11)); }
	inline int32_t get_m_planeSize2_11() const { return ___m_planeSize2_11; }
	inline int32_t* get_address_of_m_planeSize2_11() { return &___m_planeSize2_11; }
	inline void set_m_planeSize2_11(int32_t value)
	{
		___m_planeSize2_11 = value;
	}

	inline static int32_t get_offset_of_m_planeSize3_12() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_planeSize3_12)); }
	inline int32_t get_m_planeSize3_12() const { return ___m_planeSize3_12; }
	inline int32_t* get_address_of_m_planeSize3_12() { return &___m_planeSize3_12; }
	inline void set_m_planeSize3_12(int32_t value)
	{
		___m_planeSize3_12 = value;
	}

	inline static int32_t get_offset_of_m_planeRowStride0_13() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_planeRowStride0_13)); }
	inline int32_t get_m_planeRowStride0_13() const { return ___m_planeRowStride0_13; }
	inline int32_t* get_address_of_m_planeRowStride0_13() { return &___m_planeRowStride0_13; }
	inline void set_m_planeRowStride0_13(int32_t value)
	{
		___m_planeRowStride0_13 = value;
	}

	inline static int32_t get_offset_of_m_planeRowStride1_14() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_planeRowStride1_14)); }
	inline int32_t get_m_planeRowStride1_14() const { return ___m_planeRowStride1_14; }
	inline int32_t* get_address_of_m_planeRowStride1_14() { return &___m_planeRowStride1_14; }
	inline void set_m_planeRowStride1_14(int32_t value)
	{
		___m_planeRowStride1_14 = value;
	}

	inline static int32_t get_offset_of_m_planeRowStride2_15() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_planeRowStride2_15)); }
	inline int32_t get_m_planeRowStride2_15() const { return ___m_planeRowStride2_15; }
	inline int32_t* get_address_of_m_planeRowStride2_15() { return &___m_planeRowStride2_15; }
	inline void set_m_planeRowStride2_15(int32_t value)
	{
		___m_planeRowStride2_15 = value;
	}

	inline static int32_t get_offset_of_m_planeRowStride3_16() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_planeRowStride3_16)); }
	inline int32_t get_m_planeRowStride3_16() const { return ___m_planeRowStride3_16; }
	inline int32_t* get_address_of_m_planeRowStride3_16() { return &___m_planeRowStride3_16; }
	inline void set_m_planeRowStride3_16(int32_t value)
	{
		___m_planeRowStride3_16 = value;
	}

	inline static int32_t get_offset_of_m_planePpixelStride0_17() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_planePpixelStride0_17)); }
	inline int32_t get_m_planePpixelStride0_17() const { return ___m_planePpixelStride0_17; }
	inline int32_t* get_address_of_m_planePpixelStride0_17() { return &___m_planePpixelStride0_17; }
	inline void set_m_planePpixelStride0_17(int32_t value)
	{
		___m_planePpixelStride0_17 = value;
	}

	inline static int32_t get_offset_of_m_planePixelStride1_18() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_planePixelStride1_18)); }
	inline int32_t get_m_planePixelStride1_18() const { return ___m_planePixelStride1_18; }
	inline int32_t* get_address_of_m_planePixelStride1_18() { return &___m_planePixelStride1_18; }
	inline void set_m_planePixelStride1_18(int32_t value)
	{
		___m_planePixelStride1_18 = value;
	}

	inline static int32_t get_offset_of_m_planePixelStride2_19() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_planePixelStride2_19)); }
	inline int32_t get_m_planePixelStride2_19() const { return ___m_planePixelStride2_19; }
	inline int32_t* get_address_of_m_planePixelStride2_19() { return &___m_planePixelStride2_19; }
	inline void set_m_planePixelStride2_19(int32_t value)
	{
		___m_planePixelStride2_19 = value;
	}

	inline static int32_t get_offset_of_m_planePixelStride3_20() { return static_cast<int32_t>(offsetof(TangoImage_t3319528210, ___m_planePixelStride3_20)); }
	inline int32_t get_m_planePixelStride3_20() const { return ___m_planePixelStride3_20; }
	inline int32_t* get_address_of_m_planePixelStride3_20() { return &___m_planePixelStride3_20; }
	inline void set_m_planePixelStride3_20(int32_t value)
	{
		___m_planePixelStride3_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
