﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Plane3727654732.h"

// UnityEngine.Camera
struct Camera_t189460977;
// ARGUIController
struct ARGUIController_t3357238990;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARGUIController/<_WaitForDepthAndFindPlane>c__Iterator0
struct  U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716  : public Il2CppObject
{
public:
	// UnityEngine.Camera ARGUIController/<_WaitForDepthAndFindPlane>c__Iterator0::<cam>__0
	Camera_t189460977 * ___U3CcamU3E__0_0;
	// UnityEngine.Vector2 ARGUIController/<_WaitForDepthAndFindPlane>c__Iterator0::touchPosition
	Vector2_t2243707579  ___touchPosition_1;
	// UnityEngine.Vector3 ARGUIController/<_WaitForDepthAndFindPlane>c__Iterator0::<planeCenter>__0
	Vector3_t2243707580  ___U3CplaneCenterU3E__0_2;
	// UnityEngine.Plane ARGUIController/<_WaitForDepthAndFindPlane>c__Iterator0::<plane>__0
	Plane_t3727654732  ___U3CplaneU3E__0_3;
	// UnityEngine.Vector3 ARGUIController/<_WaitForDepthAndFindPlane>c__Iterator0::<up>__0
	Vector3_t2243707580  ___U3CupU3E__0_4;
	// UnityEngine.Vector3 ARGUIController/<_WaitForDepthAndFindPlane>c__Iterator0::<forward>__1
	Vector3_t2243707580  ___U3CforwardU3E__1_5;
	// ARGUIController ARGUIController/<_WaitForDepthAndFindPlane>c__Iterator0::$this
	ARGUIController_t3357238990 * ___U24this_6;
	// System.Object ARGUIController/<_WaitForDepthAndFindPlane>c__Iterator0::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean ARGUIController/<_WaitForDepthAndFindPlane>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 ARGUIController/<_WaitForDepthAndFindPlane>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CcamU3E__0_0() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716, ___U3CcamU3E__0_0)); }
	inline Camera_t189460977 * get_U3CcamU3E__0_0() const { return ___U3CcamU3E__0_0; }
	inline Camera_t189460977 ** get_address_of_U3CcamU3E__0_0() { return &___U3CcamU3E__0_0; }
	inline void set_U3CcamU3E__0_0(Camera_t189460977 * value)
	{
		___U3CcamU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcamU3E__0_0, value);
	}

	inline static int32_t get_offset_of_touchPosition_1() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716, ___touchPosition_1)); }
	inline Vector2_t2243707579  get_touchPosition_1() const { return ___touchPosition_1; }
	inline Vector2_t2243707579 * get_address_of_touchPosition_1() { return &___touchPosition_1; }
	inline void set_touchPosition_1(Vector2_t2243707579  value)
	{
		___touchPosition_1 = value;
	}

	inline static int32_t get_offset_of_U3CplaneCenterU3E__0_2() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716, ___U3CplaneCenterU3E__0_2)); }
	inline Vector3_t2243707580  get_U3CplaneCenterU3E__0_2() const { return ___U3CplaneCenterU3E__0_2; }
	inline Vector3_t2243707580 * get_address_of_U3CplaneCenterU3E__0_2() { return &___U3CplaneCenterU3E__0_2; }
	inline void set_U3CplaneCenterU3E__0_2(Vector3_t2243707580  value)
	{
		___U3CplaneCenterU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CplaneU3E__0_3() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716, ___U3CplaneU3E__0_3)); }
	inline Plane_t3727654732  get_U3CplaneU3E__0_3() const { return ___U3CplaneU3E__0_3; }
	inline Plane_t3727654732 * get_address_of_U3CplaneU3E__0_3() { return &___U3CplaneU3E__0_3; }
	inline void set_U3CplaneU3E__0_3(Plane_t3727654732  value)
	{
		___U3CplaneU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CupU3E__0_4() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716, ___U3CupU3E__0_4)); }
	inline Vector3_t2243707580  get_U3CupU3E__0_4() const { return ___U3CupU3E__0_4; }
	inline Vector3_t2243707580 * get_address_of_U3CupU3E__0_4() { return &___U3CupU3E__0_4; }
	inline void set_U3CupU3E__0_4(Vector3_t2243707580  value)
	{
		___U3CupU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CforwardU3E__1_5() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716, ___U3CforwardU3E__1_5)); }
	inline Vector3_t2243707580  get_U3CforwardU3E__1_5() const { return ___U3CforwardU3E__1_5; }
	inline Vector3_t2243707580 * get_address_of_U3CforwardU3E__1_5() { return &___U3CforwardU3E__1_5; }
	inline void set_U3CforwardU3E__1_5(Vector3_t2243707580  value)
	{
		___U3CforwardU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716, ___U24this_6)); }
	inline ARGUIController_t3357238990 * get_U24this_6() const { return ___U24this_6; }
	inline ARGUIController_t3357238990 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(ARGUIController_t3357238990 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
