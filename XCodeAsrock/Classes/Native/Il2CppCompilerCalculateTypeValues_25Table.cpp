﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Utils_serializableUnityARPlaneAn2771464920.h"
#include "AssemblyU2DCSharp_Utils_serializablePointCloud1992421910.h"
#include "AssemblyU2DCSharp_Utils_serializableARSessionConfig908111522.h"
#include "AssemblyU2DCSharp_Utils_serializableARKitInit2850619308.h"
#include "AssemblyU2DCSharp_Utils_serializableFromEditorMess2894567809.h"
#include "AssemblyU2DCSharp_UnityEngine_XR_iOS_UnityRemoteVi1017728068.h"
#include "AssemblyU2DCSharp_UnityARUserAnchorExample3900755778.h"
#include "AssemblyU2DCSharp_FocusSquare3563802663.h"
#include "AssemblyU2DCSharp_FocusSquare_FocusState737906503.h"
#include "AssemblyU2DCSharp_BallMaker2085518213.h"
#include "AssemblyU2DCSharp_BallMover754704982.h"
#include "AssemblyU2DCSharp_Ballz4160380291.h"
#include "AssemblyU2DCSharp_ModeSwitcher223874984.h"
#include "AssemblyU2DCSharp_ColorValues3063098635.h"
#include "AssemblyU2DCSharp_ColorChangedEvent2990895397.h"
#include "AssemblyU2DCSharp_HSVChangedEvent1170297569.h"
#include "AssemblyU2DCSharp_ColorPickerTester1006114474.h"
#include "AssemblyU2DCSharp_TiltWindow1839185375.h"
#include "AssemblyU2DCSharp_ColorImage3157136356.h"
#include "AssemblyU2DCSharp_ColorLabel1884607337.h"
#include "AssemblyU2DCSharp_ColorPicker3035206225.h"
#include "AssemblyU2DCSharp_ColorPresets4120623669.h"
#include "AssemblyU2DCSharp_ColorSlider2729134766.h"
#include "AssemblyU2DCSharp_ColorSliderImage376502149.h"
#include "AssemblyU2DCSharp_HexColorField4192118964.h"
#include "AssemblyU2DCSharp_SVBoxSlider1173082351.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_BoxSlider1871650694.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_BoxSlider_Directi1632189177.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_BoxSlider_BoxSlid1774115848.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_BoxSlider_Axis3966514019.h"
#include "AssemblyU2DCSharp_HSVUtil3885028383.h"
#include "AssemblyU2DCSharp_HsvColor1057062332.h"
#include "AssemblyU2DCSharp_ParticlePainter1073897267.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (serializableUnityARPlaneAnchor_t2771464920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[5] = 
{
	serializableUnityARPlaneAnchor_t2771464920::get_offset_of_worldTransform_0(),
	serializableUnityARPlaneAnchor_t2771464920::get_offset_of_center_1(),
	serializableUnityARPlaneAnchor_t2771464920::get_offset_of_extent_2(),
	serializableUnityARPlaneAnchor_t2771464920::get_offset_of_planeAlignment_3(),
	serializableUnityARPlaneAnchor_t2771464920::get_offset_of_identifierStr_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (serializablePointCloud_t1992421910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[1] = 
{
	serializablePointCloud_t1992421910::get_offset_of_pointCloudData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (serializableARSessionConfiguration_t908111522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[4] = 
{
	serializableARSessionConfiguration_t908111522::get_offset_of_alignment_0(),
	serializableARSessionConfiguration_t908111522::get_offset_of_planeDetection_1(),
	serializableARSessionConfiguration_t908111522::get_offset_of_getPointCloudData_2(),
	serializableARSessionConfiguration_t908111522::get_offset_of_enableLightEstimation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (serializableARKitInit_t2850619308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[2] = 
{
	serializableARKitInit_t2850619308::get_offset_of_config_0(),
	serializableARKitInit_t2850619308::get_offset_of_runOption_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (serializableFromEditorMessage_t2894567809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[2] = 
{
	serializableFromEditorMessage_t2894567809::get_offset_of_subMessageId_0(),
	serializableFromEditorMessage_t2894567809::get_offset_of_arkitConfigMsg_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (UnityRemoteVideo_t1017728068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[10] = 
{
	UnityRemoteVideo_t1017728068::get_offset_of_connectToEditor_2(),
	UnityRemoteVideo_t1017728068::get_offset_of_m_Session_3(),
	UnityRemoteVideo_t1017728068::get_offset_of_bTexturesInitialized_4(),
	UnityRemoteVideo_t1017728068::get_offset_of_currentFrameIndex_5(),
	UnityRemoteVideo_t1017728068::get_offset_of_m_textureYBytes_6(),
	UnityRemoteVideo_t1017728068::get_offset_of_m_textureUVBytes_7(),
	UnityRemoteVideo_t1017728068::get_offset_of_m_textureYBytes2_8(),
	UnityRemoteVideo_t1017728068::get_offset_of_m_textureUVBytes2_9(),
	UnityRemoteVideo_t1017728068::get_offset_of_m_pinnedYArray_10(),
	UnityRemoteVideo_t1017728068::get_offset_of_m_pinnedUVArray_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (UnityARUserAnchorExample_t3900755778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[4] = 
{
	UnityARUserAnchorExample_t3900755778::get_offset_of_prefabObject_2(),
	UnityARUserAnchorExample_t3900755778::get_offset_of_distanceFromCamera_3(),
	UnityARUserAnchorExample_t3900755778::get_offset_of_m_Clones_4(),
	UnityARUserAnchorExample_t3900755778::get_offset_of_m_TimeUntilRemove_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (FocusSquare_t3563802663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2507[7] = 
{
	FocusSquare_t3563802663::get_offset_of_findingSquare_2(),
	FocusSquare_t3563802663::get_offset_of_foundSquare_3(),
	FocusSquare_t3563802663::get_offset_of_maxRayDistance_4(),
	FocusSquare_t3563802663::get_offset_of_collisionLayerMask_5(),
	FocusSquare_t3563802663::get_offset_of_findingSquareDist_6(),
	FocusSquare_t3563802663::get_offset_of_squareState_7(),
	FocusSquare_t3563802663::get_offset_of_trackingInitialized_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (FocusState_t737906503)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2508[4] = 
{
	FocusState_t737906503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (BallMaker_t2085518213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[3] = 
{
	BallMaker_t2085518213::get_offset_of_ballPrefab_2(),
	BallMaker_t2085518213::get_offset_of_createHeight_3(),
	BallMaker_t2085518213::get_offset_of_props_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (BallMover_t754704982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[2] = 
{
	BallMover_t754704982::get_offset_of_collBallPrefab_2(),
	BallMover_t754704982::get_offset_of_collBallGO_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (Ballz_t4160380291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[2] = 
{
	Ballz_t4160380291::get_offset_of_yDistanceThreshold_2(),
	Ballz_t4160380291::get_offset_of_startingY_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (ModeSwitcher_t223874984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2512[3] = 
{
	ModeSwitcher_t223874984::get_offset_of_ballMake_2(),
	ModeSwitcher_t223874984::get_offset_of_ballMove_3(),
	ModeSwitcher_t223874984::get_offset_of_appMode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (ColorValues_t3063098635)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2513[8] = 
{
	ColorValues_t3063098635::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (ColorChangedEvent_t2990895397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (HSVChangedEvent_t1170297569), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (ColorPickerTester_t1006114474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2516[2] = 
{
	ColorPickerTester_t1006114474::get_offset_of_renderer_2(),
	ColorPickerTester_t1006114474::get_offset_of_picker_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (TiltWindow_t1839185375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[4] = 
{
	TiltWindow_t1839185375::get_offset_of_range_2(),
	TiltWindow_t1839185375::get_offset_of_mTrans_3(),
	TiltWindow_t1839185375::get_offset_of_mStart_4(),
	TiltWindow_t1839185375::get_offset_of_mRot_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (ColorImage_t3157136356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2518[2] = 
{
	ColorImage_t3157136356::get_offset_of_picker_2(),
	ColorImage_t3157136356::get_offset_of_image_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (ColorLabel_t1884607337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[7] = 
{
	ColorLabel_t1884607337::get_offset_of_picker_2(),
	ColorLabel_t1884607337::get_offset_of_type_3(),
	ColorLabel_t1884607337::get_offset_of_prefix_4(),
	ColorLabel_t1884607337::get_offset_of_minValue_5(),
	ColorLabel_t1884607337::get_offset_of_maxValue_6(),
	ColorLabel_t1884607337::get_offset_of_precision_7(),
	ColorLabel_t1884607337::get_offset_of_label_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (ColorPicker_t3035206225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2520[9] = 
{
	ColorPicker_t3035206225::get_offset_of__hue_2(),
	ColorPicker_t3035206225::get_offset_of__saturation_3(),
	ColorPicker_t3035206225::get_offset_of__brightness_4(),
	ColorPicker_t3035206225::get_offset_of__red_5(),
	ColorPicker_t3035206225::get_offset_of__green_6(),
	ColorPicker_t3035206225::get_offset_of__blue_7(),
	ColorPicker_t3035206225::get_offset_of__alpha_8(),
	ColorPicker_t3035206225::get_offset_of_onValueChanged_9(),
	ColorPicker_t3035206225::get_offset_of_onHSVChanged_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (ColorPresets_t4120623669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2521[3] = 
{
	ColorPresets_t4120623669::get_offset_of_picker_2(),
	ColorPresets_t4120623669::get_offset_of_presets_3(),
	ColorPresets_t4120623669::get_offset_of_createPresetImage_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (ColorSlider_t2729134766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2522[4] = 
{
	ColorSlider_t2729134766::get_offset_of_hsvpicker_2(),
	ColorSlider_t2729134766::get_offset_of_type_3(),
	ColorSlider_t2729134766::get_offset_of_slider_4(),
	ColorSlider_t2729134766::get_offset_of_listen_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (ColorSliderImage_t376502149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2523[4] = 
{
	ColorSliderImage_t376502149::get_offset_of_picker_2(),
	ColorSliderImage_t376502149::get_offset_of_type_3(),
	ColorSliderImage_t376502149::get_offset_of_direction_4(),
	ColorSliderImage_t376502149::get_offset_of_image_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (HexColorField_t4192118964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[4] = 
{
	HexColorField_t4192118964::get_offset_of_hsvpicker_2(),
	HexColorField_t4192118964::get_offset_of_displayAlpha_3(),
	HexColorField_t4192118964::get_offset_of_hexInputField_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (SVBoxSlider_t1173082351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2525[5] = 
{
	SVBoxSlider_t1173082351::get_offset_of_picker_2(),
	SVBoxSlider_t1173082351::get_offset_of_slider_3(),
	SVBoxSlider_t1173082351::get_offset_of_image_4(),
	SVBoxSlider_t1173082351::get_offset_of_lastH_5(),
	SVBoxSlider_t1173082351::get_offset_of_listen_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (BoxSlider_t1871650694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2526[11] = 
{
	BoxSlider_t1871650694::get_offset_of_m_HandleRect_16(),
	BoxSlider_t1871650694::get_offset_of_m_MinValue_17(),
	BoxSlider_t1871650694::get_offset_of_m_MaxValue_18(),
	BoxSlider_t1871650694::get_offset_of_m_WholeNumbers_19(),
	BoxSlider_t1871650694::get_offset_of_m_Value_20(),
	BoxSlider_t1871650694::get_offset_of_m_ValueY_21(),
	BoxSlider_t1871650694::get_offset_of_m_OnValueChanged_22(),
	BoxSlider_t1871650694::get_offset_of_m_HandleTransform_23(),
	BoxSlider_t1871650694::get_offset_of_m_HandleContainerRect_24(),
	BoxSlider_t1871650694::get_offset_of_m_Offset_25(),
	BoxSlider_t1871650694::get_offset_of_m_Tracker_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (Direction_t1632189177)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2527[5] = 
{
	Direction_t1632189177::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (BoxSliderEvent_t1774115848), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (Axis_t3966514019)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2529[3] = 
{
	Axis_t3966514019::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (HSVUtil_t3885028383), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (HsvColor_t1057062332)+ sizeof (Il2CppObject), sizeof(HsvColor_t1057062332 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2531[3] = 
{
	HsvColor_t1057062332::get_offset_of_H_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HsvColor_t1057062332::get_offset_of_S_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HsvColor_t1057062332::get_offset_of_V_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (ParticlePainter_t1073897267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2532[14] = 
{
	ParticlePainter_t1073897267::get_offset_of_painterParticlePrefab_2(),
	ParticlePainter_t1073897267::get_offset_of_minDistanceThreshold_3(),
	ParticlePainter_t1073897267::get_offset_of_maxDistanceThreshold_4(),
	ParticlePainter_t1073897267::get_offset_of_frameUpdated_5(),
	ParticlePainter_t1073897267::get_offset_of_particleSize_6(),
	ParticlePainter_t1073897267::get_offset_of_penDistance_7(),
	ParticlePainter_t1073897267::get_offset_of_colorPicker_8(),
	ParticlePainter_t1073897267::get_offset_of_currentPS_9(),
	ParticlePainter_t1073897267::get_offset_of_particles_10(),
	ParticlePainter_t1073897267::get_offset_of_previousPosition_11(),
	ParticlePainter_t1073897267::get_offset_of_currentPaintVertices_12(),
	ParticlePainter_t1073897267::get_offset_of_currentColor_13(),
	ParticlePainter_t1073897267::get_offset_of_paintSystems_14(),
	ParticlePainter_t1073897267::get_offset_of_paintMode_15(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
