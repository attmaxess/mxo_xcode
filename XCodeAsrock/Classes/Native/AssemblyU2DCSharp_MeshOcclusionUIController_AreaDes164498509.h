﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeshOcclusionUIController/AreaDescriptionMesh
struct  AreaDescriptionMesh_t164498509  : public Il2CppObject
{
public:
	// System.String MeshOcclusionUIController/AreaDescriptionMesh::m_uuid
	String_t* ___m_uuid_0;
	// UnityEngine.Vector3[] MeshOcclusionUIController/AreaDescriptionMesh::m_vertices
	Vector3U5BU5D_t1172311765* ___m_vertices_1;
	// System.Int32[] MeshOcclusionUIController/AreaDescriptionMesh::m_triangles
	Int32U5BU5D_t3030399641* ___m_triangles_2;

public:
	inline static int32_t get_offset_of_m_uuid_0() { return static_cast<int32_t>(offsetof(AreaDescriptionMesh_t164498509, ___m_uuid_0)); }
	inline String_t* get_m_uuid_0() const { return ___m_uuid_0; }
	inline String_t** get_address_of_m_uuid_0() { return &___m_uuid_0; }
	inline void set_m_uuid_0(String_t* value)
	{
		___m_uuid_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_uuid_0, value);
	}

	inline static int32_t get_offset_of_m_vertices_1() { return static_cast<int32_t>(offsetof(AreaDescriptionMesh_t164498509, ___m_vertices_1)); }
	inline Vector3U5BU5D_t1172311765* get_m_vertices_1() const { return ___m_vertices_1; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_vertices_1() { return &___m_vertices_1; }
	inline void set_m_vertices_1(Vector3U5BU5D_t1172311765* value)
	{
		___m_vertices_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_vertices_1, value);
	}

	inline static int32_t get_offset_of_m_triangles_2() { return static_cast<int32_t>(offsetof(AreaDescriptionMesh_t164498509, ___m_triangles_2)); }
	inline Int32U5BU5D_t3030399641* get_m_triangles_2() const { return ___m_triangles_2; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_triangles_2() { return &___m_triangles_2; }
	inline void set_m_triangles_2(Int32U5BU5D_t3030399641* value)
	{
		___m_triangles_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_triangles_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
