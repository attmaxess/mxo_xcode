﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_IntPtr2504060609.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoPointCloudIntPtr
struct  TangoPointCloudIntPtr_t1795497939 
{
public:
	// System.Int32 Tango.TangoPointCloudIntPtr::m_version
	int32_t ___m_version_0;
	// System.Double Tango.TangoPointCloudIntPtr::m_timestamp
	double ___m_timestamp_1;
	// System.Int32 Tango.TangoPointCloudIntPtr::m_numPoints
	int32_t ___m_numPoints_2;
	// System.IntPtr Tango.TangoPointCloudIntPtr::m_points
	IntPtr_t ___m_points_3;

public:
	inline static int32_t get_offset_of_m_version_0() { return static_cast<int32_t>(offsetof(TangoPointCloudIntPtr_t1795497939, ___m_version_0)); }
	inline int32_t get_m_version_0() const { return ___m_version_0; }
	inline int32_t* get_address_of_m_version_0() { return &___m_version_0; }
	inline void set_m_version_0(int32_t value)
	{
		___m_version_0 = value;
	}

	inline static int32_t get_offset_of_m_timestamp_1() { return static_cast<int32_t>(offsetof(TangoPointCloudIntPtr_t1795497939, ___m_timestamp_1)); }
	inline double get_m_timestamp_1() const { return ___m_timestamp_1; }
	inline double* get_address_of_m_timestamp_1() { return &___m_timestamp_1; }
	inline void set_m_timestamp_1(double value)
	{
		___m_timestamp_1 = value;
	}

	inline static int32_t get_offset_of_m_numPoints_2() { return static_cast<int32_t>(offsetof(TangoPointCloudIntPtr_t1795497939, ___m_numPoints_2)); }
	inline int32_t get_m_numPoints_2() const { return ___m_numPoints_2; }
	inline int32_t* get_address_of_m_numPoints_2() { return &___m_numPoints_2; }
	inline void set_m_numPoints_2(int32_t value)
	{
		___m_numPoints_2 = value;
	}

	inline static int32_t get_offset_of_m_points_3() { return static_cast<int32_t>(offsetof(TangoPointCloudIntPtr_t1795497939, ___m_points_3)); }
	inline IntPtr_t get_m_points_3() const { return ___m_points_3; }
	inline IntPtr_t* get_address_of_m_points_3() { return &___m_points_3; }
	inline void set_m_points_3(IntPtr_t value)
	{
		___m_points_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
