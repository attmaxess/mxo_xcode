﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.Common
struct  Common_t442314523 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Common_t442314523__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
