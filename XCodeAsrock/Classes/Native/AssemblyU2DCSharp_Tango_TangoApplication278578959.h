﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Tango_Tango3DReconstruction_Updat407952626.h"
#include "mscorlib_System_Nullable_1_gen2601156776.h"

// Tango.OnDisplayChangedEventHandler
struct OnDisplayChangedEventHandler_t1075383569;
// System.String
struct String_t;
// System.Collections.Generic.HashSet`1<Tango.TangoUxEnums/UxExceptionEventType>
struct HashSet_1_t995112032;
// Tango.Tango3DReconstruction
struct Tango3DReconstruction_t1839722436;
// Tango.YUVTexture
struct YUVTexture_t2576165397;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// Tango.TangoApplication/OnTangoConnectDelegate
struct OnTangoConnectDelegate_t1579449014;
// Tango.TangoApplication/OnTangoDisconnectDelegate
struct OnTangoDisconnectDelegate_t1172501576;
// Tango.TangoApplication/ITangoAndroidMessageManager
struct ITangoAndroidMessageManager_t4144679962;
// Tango.TangoApplication/TangoApplicationState
struct TangoApplicationState_t848537215;
// Tango.TangoApplication/ITangoEventRegistrationManager
struct ITangoEventRegistrationManager_t702534989;
// Tango.TangoApplication/ITangoPermissionsManager
struct ITangoPermissionsManager_t1593860194;
// Tango.TangoApplication/ITangoSetupTeardownManager
struct ITangoSetupTeardownManager_t3283438619;
// Tango.TangoApplication/ITangoDepthCameraManager
struct ITangoDepthCameraManager_t2550061576;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoApplication
struct  TangoApplication_t278578959  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Tango.TangoApplication::m_autoConnectToService
	bool ___m_autoConnectToService_2;
	// System.Boolean Tango.TangoApplication::m_allowOutOfDateTangoAPI
	bool ___m_allowOutOfDateTangoAPI_3;
	// System.Boolean Tango.TangoApplication::m_enableMotionTracking
	bool ___m_enableMotionTracking_4;
	// System.Boolean Tango.TangoApplication::m_motionTrackingAutoReset
	bool ___m_motionTrackingAutoReset_5;
	// System.Boolean Tango.TangoApplication::m_enableAreaDescriptions
	bool ___m_enableAreaDescriptions_6;
	// System.Boolean Tango.TangoApplication::m_areaDescriptionLearningMode
	bool ___m_areaDescriptionLearningMode_7;
	// System.Boolean Tango.TangoApplication::m_enableDriftCorrection
	bool ___m_enableDriftCorrection_8;
	// System.Boolean Tango.TangoApplication::m_enableDepth
	bool ___m_enableDepth_9;
	// System.Boolean Tango.TangoApplication::m_enableAreaLearning
	bool ___m_enableAreaLearning_10;
	// System.Boolean Tango.TangoApplication::m_enableADFLoading
	bool ___m_enableADFLoading_11;
	// System.Boolean Tango.TangoApplication::m_enableCloudADF
	bool ___m_enableCloudADF_12;
	// System.Boolean Tango.TangoApplication::m_enable3DReconstruction
	bool ___m_enable3DReconstruction_13;
	// System.Single Tango.TangoApplication::m_3drResolutionMeters
	float ___m_3drResolutionMeters_14;
	// System.Boolean Tango.TangoApplication::m_3drGenerateColor
	bool ___m_3drGenerateColor_15;
	// System.Boolean Tango.TangoApplication::m_3drGenerateNormal
	bool ___m_3drGenerateNormal_16;
	// System.Boolean Tango.TangoApplication::m_3drGenerateTexCoord
	bool ___m_3drGenerateTexCoord_17;
	// System.Boolean Tango.TangoApplication::m_3drSpaceClearing
	bool ___m_3drSpaceClearing_18;
	// System.Boolean Tango.TangoApplication::m_3drUseAreaDescriptionPose
	bool ___m_3drUseAreaDescriptionPose_19;
	// System.Int32 Tango.TangoApplication::m_3drMinNumVertices
	int32_t ___m_3drMinNumVertices_20;
	// Tango.Tango3DReconstruction/UpdateMethod Tango.TangoApplication::m_3drUpdateMethod
	int32_t ___m_3drUpdateMethod_21;
	// System.Boolean Tango.TangoApplication::m_enableVideoOverlay
	bool ___m_enableVideoOverlay_22;
	// System.Boolean Tango.TangoApplication::m_videoOverlayUseTextureMethod
	bool ___m_videoOverlayUseTextureMethod_23;
	// System.Boolean Tango.TangoApplication::m_videoOverlayUseYUVTextureIdMethod
	bool ___m_videoOverlayUseYUVTextureIdMethod_24;
	// System.Boolean Tango.TangoApplication::m_videoOverlayUseByteBufferMethod
	bool ___m_videoOverlayUseByteBufferMethod_25;
	// System.Boolean Tango.TangoApplication::m_keepScreenAwake
	bool ___m_keepScreenAwake_26;
	// System.Boolean Tango.TangoApplication::m_adjustScreenResolution
	bool ___m_adjustScreenResolution_27;
	// System.Int32 Tango.TangoApplication::m_targetResolution
	int32_t ___m_targetResolution_28;
	// System.Boolean Tango.TangoApplication::m_allowOversizedScreenResolutions
	bool ___m_allowOversizedScreenResolutions_29;
	// System.Int32 Tango.TangoApplication::m_initialPointCloudMaxPoints
	int32_t ___m_initialPointCloudMaxPoints_30;
	// Tango.OnDisplayChangedEventHandler Tango.TangoApplication::OnDisplayChanged
	OnDisplayChangedEventHandler_t1075383569 * ___OnDisplayChanged_31;
	// Tango.Tango3DReconstruction Tango.TangoApplication::m_tango3DReconstruction
	Tango3DReconstruction_t1839722436 * ___m_tango3DReconstruction_37;
	// System.Collections.Generic.HashSet`1<Tango.TangoUxEnums/UxExceptionEventType> Tango.TangoApplication::m_unresolvedUxExceptions
	HashSet_1_t995112032 * ___m_unresolvedUxExceptions_38;
	// Tango.YUVTexture Tango.TangoApplication::m_yuvTexture
	YUVTexture_t2576165397 * ___m_yuvTexture_39;
	// System.Action`1<System.Boolean> Tango.TangoApplication::m_androidPermissionsEvent
	Action_1_t3627374100 * ___m_androidPermissionsEvent_40;
	// System.Action`1<System.Boolean> Tango.TangoApplication::m_tangoBindEvent
	Action_1_t3627374100 * ___m_tangoBindEvent_41;
	// System.Nullable`1<Tango.DMatrix4x4> Tango.TangoApplication::m_globalTLocal
	Nullable_1_t2601156776  ___m_globalTLocal_42;
	// Tango.TangoApplication/OnTangoConnectDelegate Tango.TangoApplication::m_tangoConnectEvent
	OnTangoConnectDelegate_t1579449014 * ___m_tangoConnectEvent_43;
	// Tango.TangoApplication/OnTangoDisconnectDelegate Tango.TangoApplication::m_tangoDisconnectEvent
	OnTangoDisconnectDelegate_t1172501576 * ___m_tangoDisconnectEvent_44;
	// Tango.TangoApplication/ITangoAndroidMessageManager Tango.TangoApplication::m_androidMessageManager
	Il2CppObject * ___m_androidMessageManager_45;
	// Tango.TangoApplication/TangoApplicationState Tango.TangoApplication::m_applicationState
	TangoApplicationState_t848537215 * ___m_applicationState_46;
	// Tango.TangoApplication/ITangoEventRegistrationManager Tango.TangoApplication::m_eventRegistrationManager
	Il2CppObject * ___m_eventRegistrationManager_47;
	// Tango.TangoApplication/ITangoPermissionsManager Tango.TangoApplication::m_permissionsManager
	Il2CppObject * ___m_permissionsManager_48;
	// Tango.TangoApplication/ITangoSetupTeardownManager Tango.TangoApplication::m_setupTeardownManager
	Il2CppObject * ___m_setupTeardownManager_49;
	// Tango.TangoApplication/ITangoDepthCameraManager Tango.TangoApplication::m_tangoDepthCameraManager
	Il2CppObject * ___m_tangoDepthCameraManager_50;

public:
	inline static int32_t get_offset_of_m_autoConnectToService_2() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_autoConnectToService_2)); }
	inline bool get_m_autoConnectToService_2() const { return ___m_autoConnectToService_2; }
	inline bool* get_address_of_m_autoConnectToService_2() { return &___m_autoConnectToService_2; }
	inline void set_m_autoConnectToService_2(bool value)
	{
		___m_autoConnectToService_2 = value;
	}

	inline static int32_t get_offset_of_m_allowOutOfDateTangoAPI_3() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_allowOutOfDateTangoAPI_3)); }
	inline bool get_m_allowOutOfDateTangoAPI_3() const { return ___m_allowOutOfDateTangoAPI_3; }
	inline bool* get_address_of_m_allowOutOfDateTangoAPI_3() { return &___m_allowOutOfDateTangoAPI_3; }
	inline void set_m_allowOutOfDateTangoAPI_3(bool value)
	{
		___m_allowOutOfDateTangoAPI_3 = value;
	}

	inline static int32_t get_offset_of_m_enableMotionTracking_4() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_enableMotionTracking_4)); }
	inline bool get_m_enableMotionTracking_4() const { return ___m_enableMotionTracking_4; }
	inline bool* get_address_of_m_enableMotionTracking_4() { return &___m_enableMotionTracking_4; }
	inline void set_m_enableMotionTracking_4(bool value)
	{
		___m_enableMotionTracking_4 = value;
	}

	inline static int32_t get_offset_of_m_motionTrackingAutoReset_5() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_motionTrackingAutoReset_5)); }
	inline bool get_m_motionTrackingAutoReset_5() const { return ___m_motionTrackingAutoReset_5; }
	inline bool* get_address_of_m_motionTrackingAutoReset_5() { return &___m_motionTrackingAutoReset_5; }
	inline void set_m_motionTrackingAutoReset_5(bool value)
	{
		___m_motionTrackingAutoReset_5 = value;
	}

	inline static int32_t get_offset_of_m_enableAreaDescriptions_6() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_enableAreaDescriptions_6)); }
	inline bool get_m_enableAreaDescriptions_6() const { return ___m_enableAreaDescriptions_6; }
	inline bool* get_address_of_m_enableAreaDescriptions_6() { return &___m_enableAreaDescriptions_6; }
	inline void set_m_enableAreaDescriptions_6(bool value)
	{
		___m_enableAreaDescriptions_6 = value;
	}

	inline static int32_t get_offset_of_m_areaDescriptionLearningMode_7() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_areaDescriptionLearningMode_7)); }
	inline bool get_m_areaDescriptionLearningMode_7() const { return ___m_areaDescriptionLearningMode_7; }
	inline bool* get_address_of_m_areaDescriptionLearningMode_7() { return &___m_areaDescriptionLearningMode_7; }
	inline void set_m_areaDescriptionLearningMode_7(bool value)
	{
		___m_areaDescriptionLearningMode_7 = value;
	}

	inline static int32_t get_offset_of_m_enableDriftCorrection_8() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_enableDriftCorrection_8)); }
	inline bool get_m_enableDriftCorrection_8() const { return ___m_enableDriftCorrection_8; }
	inline bool* get_address_of_m_enableDriftCorrection_8() { return &___m_enableDriftCorrection_8; }
	inline void set_m_enableDriftCorrection_8(bool value)
	{
		___m_enableDriftCorrection_8 = value;
	}

	inline static int32_t get_offset_of_m_enableDepth_9() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_enableDepth_9)); }
	inline bool get_m_enableDepth_9() const { return ___m_enableDepth_9; }
	inline bool* get_address_of_m_enableDepth_9() { return &___m_enableDepth_9; }
	inline void set_m_enableDepth_9(bool value)
	{
		___m_enableDepth_9 = value;
	}

	inline static int32_t get_offset_of_m_enableAreaLearning_10() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_enableAreaLearning_10)); }
	inline bool get_m_enableAreaLearning_10() const { return ___m_enableAreaLearning_10; }
	inline bool* get_address_of_m_enableAreaLearning_10() { return &___m_enableAreaLearning_10; }
	inline void set_m_enableAreaLearning_10(bool value)
	{
		___m_enableAreaLearning_10 = value;
	}

	inline static int32_t get_offset_of_m_enableADFLoading_11() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_enableADFLoading_11)); }
	inline bool get_m_enableADFLoading_11() const { return ___m_enableADFLoading_11; }
	inline bool* get_address_of_m_enableADFLoading_11() { return &___m_enableADFLoading_11; }
	inline void set_m_enableADFLoading_11(bool value)
	{
		___m_enableADFLoading_11 = value;
	}

	inline static int32_t get_offset_of_m_enableCloudADF_12() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_enableCloudADF_12)); }
	inline bool get_m_enableCloudADF_12() const { return ___m_enableCloudADF_12; }
	inline bool* get_address_of_m_enableCloudADF_12() { return &___m_enableCloudADF_12; }
	inline void set_m_enableCloudADF_12(bool value)
	{
		___m_enableCloudADF_12 = value;
	}

	inline static int32_t get_offset_of_m_enable3DReconstruction_13() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_enable3DReconstruction_13)); }
	inline bool get_m_enable3DReconstruction_13() const { return ___m_enable3DReconstruction_13; }
	inline bool* get_address_of_m_enable3DReconstruction_13() { return &___m_enable3DReconstruction_13; }
	inline void set_m_enable3DReconstruction_13(bool value)
	{
		___m_enable3DReconstruction_13 = value;
	}

	inline static int32_t get_offset_of_m_3drResolutionMeters_14() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_3drResolutionMeters_14)); }
	inline float get_m_3drResolutionMeters_14() const { return ___m_3drResolutionMeters_14; }
	inline float* get_address_of_m_3drResolutionMeters_14() { return &___m_3drResolutionMeters_14; }
	inline void set_m_3drResolutionMeters_14(float value)
	{
		___m_3drResolutionMeters_14 = value;
	}

	inline static int32_t get_offset_of_m_3drGenerateColor_15() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_3drGenerateColor_15)); }
	inline bool get_m_3drGenerateColor_15() const { return ___m_3drGenerateColor_15; }
	inline bool* get_address_of_m_3drGenerateColor_15() { return &___m_3drGenerateColor_15; }
	inline void set_m_3drGenerateColor_15(bool value)
	{
		___m_3drGenerateColor_15 = value;
	}

	inline static int32_t get_offset_of_m_3drGenerateNormal_16() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_3drGenerateNormal_16)); }
	inline bool get_m_3drGenerateNormal_16() const { return ___m_3drGenerateNormal_16; }
	inline bool* get_address_of_m_3drGenerateNormal_16() { return &___m_3drGenerateNormal_16; }
	inline void set_m_3drGenerateNormal_16(bool value)
	{
		___m_3drGenerateNormal_16 = value;
	}

	inline static int32_t get_offset_of_m_3drGenerateTexCoord_17() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_3drGenerateTexCoord_17)); }
	inline bool get_m_3drGenerateTexCoord_17() const { return ___m_3drGenerateTexCoord_17; }
	inline bool* get_address_of_m_3drGenerateTexCoord_17() { return &___m_3drGenerateTexCoord_17; }
	inline void set_m_3drGenerateTexCoord_17(bool value)
	{
		___m_3drGenerateTexCoord_17 = value;
	}

	inline static int32_t get_offset_of_m_3drSpaceClearing_18() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_3drSpaceClearing_18)); }
	inline bool get_m_3drSpaceClearing_18() const { return ___m_3drSpaceClearing_18; }
	inline bool* get_address_of_m_3drSpaceClearing_18() { return &___m_3drSpaceClearing_18; }
	inline void set_m_3drSpaceClearing_18(bool value)
	{
		___m_3drSpaceClearing_18 = value;
	}

	inline static int32_t get_offset_of_m_3drUseAreaDescriptionPose_19() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_3drUseAreaDescriptionPose_19)); }
	inline bool get_m_3drUseAreaDescriptionPose_19() const { return ___m_3drUseAreaDescriptionPose_19; }
	inline bool* get_address_of_m_3drUseAreaDescriptionPose_19() { return &___m_3drUseAreaDescriptionPose_19; }
	inline void set_m_3drUseAreaDescriptionPose_19(bool value)
	{
		___m_3drUseAreaDescriptionPose_19 = value;
	}

	inline static int32_t get_offset_of_m_3drMinNumVertices_20() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_3drMinNumVertices_20)); }
	inline int32_t get_m_3drMinNumVertices_20() const { return ___m_3drMinNumVertices_20; }
	inline int32_t* get_address_of_m_3drMinNumVertices_20() { return &___m_3drMinNumVertices_20; }
	inline void set_m_3drMinNumVertices_20(int32_t value)
	{
		___m_3drMinNumVertices_20 = value;
	}

	inline static int32_t get_offset_of_m_3drUpdateMethod_21() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_3drUpdateMethod_21)); }
	inline int32_t get_m_3drUpdateMethod_21() const { return ___m_3drUpdateMethod_21; }
	inline int32_t* get_address_of_m_3drUpdateMethod_21() { return &___m_3drUpdateMethod_21; }
	inline void set_m_3drUpdateMethod_21(int32_t value)
	{
		___m_3drUpdateMethod_21 = value;
	}

	inline static int32_t get_offset_of_m_enableVideoOverlay_22() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_enableVideoOverlay_22)); }
	inline bool get_m_enableVideoOverlay_22() const { return ___m_enableVideoOverlay_22; }
	inline bool* get_address_of_m_enableVideoOverlay_22() { return &___m_enableVideoOverlay_22; }
	inline void set_m_enableVideoOverlay_22(bool value)
	{
		___m_enableVideoOverlay_22 = value;
	}

	inline static int32_t get_offset_of_m_videoOverlayUseTextureMethod_23() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_videoOverlayUseTextureMethod_23)); }
	inline bool get_m_videoOverlayUseTextureMethod_23() const { return ___m_videoOverlayUseTextureMethod_23; }
	inline bool* get_address_of_m_videoOverlayUseTextureMethod_23() { return &___m_videoOverlayUseTextureMethod_23; }
	inline void set_m_videoOverlayUseTextureMethod_23(bool value)
	{
		___m_videoOverlayUseTextureMethod_23 = value;
	}

	inline static int32_t get_offset_of_m_videoOverlayUseYUVTextureIdMethod_24() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_videoOverlayUseYUVTextureIdMethod_24)); }
	inline bool get_m_videoOverlayUseYUVTextureIdMethod_24() const { return ___m_videoOverlayUseYUVTextureIdMethod_24; }
	inline bool* get_address_of_m_videoOverlayUseYUVTextureIdMethod_24() { return &___m_videoOverlayUseYUVTextureIdMethod_24; }
	inline void set_m_videoOverlayUseYUVTextureIdMethod_24(bool value)
	{
		___m_videoOverlayUseYUVTextureIdMethod_24 = value;
	}

	inline static int32_t get_offset_of_m_videoOverlayUseByteBufferMethod_25() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_videoOverlayUseByteBufferMethod_25)); }
	inline bool get_m_videoOverlayUseByteBufferMethod_25() const { return ___m_videoOverlayUseByteBufferMethod_25; }
	inline bool* get_address_of_m_videoOverlayUseByteBufferMethod_25() { return &___m_videoOverlayUseByteBufferMethod_25; }
	inline void set_m_videoOverlayUseByteBufferMethod_25(bool value)
	{
		___m_videoOverlayUseByteBufferMethod_25 = value;
	}

	inline static int32_t get_offset_of_m_keepScreenAwake_26() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_keepScreenAwake_26)); }
	inline bool get_m_keepScreenAwake_26() const { return ___m_keepScreenAwake_26; }
	inline bool* get_address_of_m_keepScreenAwake_26() { return &___m_keepScreenAwake_26; }
	inline void set_m_keepScreenAwake_26(bool value)
	{
		___m_keepScreenAwake_26 = value;
	}

	inline static int32_t get_offset_of_m_adjustScreenResolution_27() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_adjustScreenResolution_27)); }
	inline bool get_m_adjustScreenResolution_27() const { return ___m_adjustScreenResolution_27; }
	inline bool* get_address_of_m_adjustScreenResolution_27() { return &___m_adjustScreenResolution_27; }
	inline void set_m_adjustScreenResolution_27(bool value)
	{
		___m_adjustScreenResolution_27 = value;
	}

	inline static int32_t get_offset_of_m_targetResolution_28() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_targetResolution_28)); }
	inline int32_t get_m_targetResolution_28() const { return ___m_targetResolution_28; }
	inline int32_t* get_address_of_m_targetResolution_28() { return &___m_targetResolution_28; }
	inline void set_m_targetResolution_28(int32_t value)
	{
		___m_targetResolution_28 = value;
	}

	inline static int32_t get_offset_of_m_allowOversizedScreenResolutions_29() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_allowOversizedScreenResolutions_29)); }
	inline bool get_m_allowOversizedScreenResolutions_29() const { return ___m_allowOversizedScreenResolutions_29; }
	inline bool* get_address_of_m_allowOversizedScreenResolutions_29() { return &___m_allowOversizedScreenResolutions_29; }
	inline void set_m_allowOversizedScreenResolutions_29(bool value)
	{
		___m_allowOversizedScreenResolutions_29 = value;
	}

	inline static int32_t get_offset_of_m_initialPointCloudMaxPoints_30() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_initialPointCloudMaxPoints_30)); }
	inline int32_t get_m_initialPointCloudMaxPoints_30() const { return ___m_initialPointCloudMaxPoints_30; }
	inline int32_t* get_address_of_m_initialPointCloudMaxPoints_30() { return &___m_initialPointCloudMaxPoints_30; }
	inline void set_m_initialPointCloudMaxPoints_30(int32_t value)
	{
		___m_initialPointCloudMaxPoints_30 = value;
	}

	inline static int32_t get_offset_of_OnDisplayChanged_31() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___OnDisplayChanged_31)); }
	inline OnDisplayChangedEventHandler_t1075383569 * get_OnDisplayChanged_31() const { return ___OnDisplayChanged_31; }
	inline OnDisplayChangedEventHandler_t1075383569 ** get_address_of_OnDisplayChanged_31() { return &___OnDisplayChanged_31; }
	inline void set_OnDisplayChanged_31(OnDisplayChangedEventHandler_t1075383569 * value)
	{
		___OnDisplayChanged_31 = value;
		Il2CppCodeGenWriteBarrier(&___OnDisplayChanged_31, value);
	}

	inline static int32_t get_offset_of_m_tango3DReconstruction_37() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_tango3DReconstruction_37)); }
	inline Tango3DReconstruction_t1839722436 * get_m_tango3DReconstruction_37() const { return ___m_tango3DReconstruction_37; }
	inline Tango3DReconstruction_t1839722436 ** get_address_of_m_tango3DReconstruction_37() { return &___m_tango3DReconstruction_37; }
	inline void set_m_tango3DReconstruction_37(Tango3DReconstruction_t1839722436 * value)
	{
		___m_tango3DReconstruction_37 = value;
		Il2CppCodeGenWriteBarrier(&___m_tango3DReconstruction_37, value);
	}

	inline static int32_t get_offset_of_m_unresolvedUxExceptions_38() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_unresolvedUxExceptions_38)); }
	inline HashSet_1_t995112032 * get_m_unresolvedUxExceptions_38() const { return ___m_unresolvedUxExceptions_38; }
	inline HashSet_1_t995112032 ** get_address_of_m_unresolvedUxExceptions_38() { return &___m_unresolvedUxExceptions_38; }
	inline void set_m_unresolvedUxExceptions_38(HashSet_1_t995112032 * value)
	{
		___m_unresolvedUxExceptions_38 = value;
		Il2CppCodeGenWriteBarrier(&___m_unresolvedUxExceptions_38, value);
	}

	inline static int32_t get_offset_of_m_yuvTexture_39() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_yuvTexture_39)); }
	inline YUVTexture_t2576165397 * get_m_yuvTexture_39() const { return ___m_yuvTexture_39; }
	inline YUVTexture_t2576165397 ** get_address_of_m_yuvTexture_39() { return &___m_yuvTexture_39; }
	inline void set_m_yuvTexture_39(YUVTexture_t2576165397 * value)
	{
		___m_yuvTexture_39 = value;
		Il2CppCodeGenWriteBarrier(&___m_yuvTexture_39, value);
	}

	inline static int32_t get_offset_of_m_androidPermissionsEvent_40() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_androidPermissionsEvent_40)); }
	inline Action_1_t3627374100 * get_m_androidPermissionsEvent_40() const { return ___m_androidPermissionsEvent_40; }
	inline Action_1_t3627374100 ** get_address_of_m_androidPermissionsEvent_40() { return &___m_androidPermissionsEvent_40; }
	inline void set_m_androidPermissionsEvent_40(Action_1_t3627374100 * value)
	{
		___m_androidPermissionsEvent_40 = value;
		Il2CppCodeGenWriteBarrier(&___m_androidPermissionsEvent_40, value);
	}

	inline static int32_t get_offset_of_m_tangoBindEvent_41() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_tangoBindEvent_41)); }
	inline Action_1_t3627374100 * get_m_tangoBindEvent_41() const { return ___m_tangoBindEvent_41; }
	inline Action_1_t3627374100 ** get_address_of_m_tangoBindEvent_41() { return &___m_tangoBindEvent_41; }
	inline void set_m_tangoBindEvent_41(Action_1_t3627374100 * value)
	{
		___m_tangoBindEvent_41 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoBindEvent_41, value);
	}

	inline static int32_t get_offset_of_m_globalTLocal_42() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_globalTLocal_42)); }
	inline Nullable_1_t2601156776  get_m_globalTLocal_42() const { return ___m_globalTLocal_42; }
	inline Nullable_1_t2601156776 * get_address_of_m_globalTLocal_42() { return &___m_globalTLocal_42; }
	inline void set_m_globalTLocal_42(Nullable_1_t2601156776  value)
	{
		___m_globalTLocal_42 = value;
	}

	inline static int32_t get_offset_of_m_tangoConnectEvent_43() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_tangoConnectEvent_43)); }
	inline OnTangoConnectDelegate_t1579449014 * get_m_tangoConnectEvent_43() const { return ___m_tangoConnectEvent_43; }
	inline OnTangoConnectDelegate_t1579449014 ** get_address_of_m_tangoConnectEvent_43() { return &___m_tangoConnectEvent_43; }
	inline void set_m_tangoConnectEvent_43(OnTangoConnectDelegate_t1579449014 * value)
	{
		___m_tangoConnectEvent_43 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoConnectEvent_43, value);
	}

	inline static int32_t get_offset_of_m_tangoDisconnectEvent_44() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_tangoDisconnectEvent_44)); }
	inline OnTangoDisconnectDelegate_t1172501576 * get_m_tangoDisconnectEvent_44() const { return ___m_tangoDisconnectEvent_44; }
	inline OnTangoDisconnectDelegate_t1172501576 ** get_address_of_m_tangoDisconnectEvent_44() { return &___m_tangoDisconnectEvent_44; }
	inline void set_m_tangoDisconnectEvent_44(OnTangoDisconnectDelegate_t1172501576 * value)
	{
		___m_tangoDisconnectEvent_44 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoDisconnectEvent_44, value);
	}

	inline static int32_t get_offset_of_m_androidMessageManager_45() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_androidMessageManager_45)); }
	inline Il2CppObject * get_m_androidMessageManager_45() const { return ___m_androidMessageManager_45; }
	inline Il2CppObject ** get_address_of_m_androidMessageManager_45() { return &___m_androidMessageManager_45; }
	inline void set_m_androidMessageManager_45(Il2CppObject * value)
	{
		___m_androidMessageManager_45 = value;
		Il2CppCodeGenWriteBarrier(&___m_androidMessageManager_45, value);
	}

	inline static int32_t get_offset_of_m_applicationState_46() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_applicationState_46)); }
	inline TangoApplicationState_t848537215 * get_m_applicationState_46() const { return ___m_applicationState_46; }
	inline TangoApplicationState_t848537215 ** get_address_of_m_applicationState_46() { return &___m_applicationState_46; }
	inline void set_m_applicationState_46(TangoApplicationState_t848537215 * value)
	{
		___m_applicationState_46 = value;
		Il2CppCodeGenWriteBarrier(&___m_applicationState_46, value);
	}

	inline static int32_t get_offset_of_m_eventRegistrationManager_47() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_eventRegistrationManager_47)); }
	inline Il2CppObject * get_m_eventRegistrationManager_47() const { return ___m_eventRegistrationManager_47; }
	inline Il2CppObject ** get_address_of_m_eventRegistrationManager_47() { return &___m_eventRegistrationManager_47; }
	inline void set_m_eventRegistrationManager_47(Il2CppObject * value)
	{
		___m_eventRegistrationManager_47 = value;
		Il2CppCodeGenWriteBarrier(&___m_eventRegistrationManager_47, value);
	}

	inline static int32_t get_offset_of_m_permissionsManager_48() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_permissionsManager_48)); }
	inline Il2CppObject * get_m_permissionsManager_48() const { return ___m_permissionsManager_48; }
	inline Il2CppObject ** get_address_of_m_permissionsManager_48() { return &___m_permissionsManager_48; }
	inline void set_m_permissionsManager_48(Il2CppObject * value)
	{
		___m_permissionsManager_48 = value;
		Il2CppCodeGenWriteBarrier(&___m_permissionsManager_48, value);
	}

	inline static int32_t get_offset_of_m_setupTeardownManager_49() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_setupTeardownManager_49)); }
	inline Il2CppObject * get_m_setupTeardownManager_49() const { return ___m_setupTeardownManager_49; }
	inline Il2CppObject ** get_address_of_m_setupTeardownManager_49() { return &___m_setupTeardownManager_49; }
	inline void set_m_setupTeardownManager_49(Il2CppObject * value)
	{
		___m_setupTeardownManager_49 = value;
		Il2CppCodeGenWriteBarrier(&___m_setupTeardownManager_49, value);
	}

	inline static int32_t get_offset_of_m_tangoDepthCameraManager_50() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959, ___m_tangoDepthCameraManager_50)); }
	inline Il2CppObject * get_m_tangoDepthCameraManager_50() const { return ___m_tangoDepthCameraManager_50; }
	inline Il2CppObject ** get_address_of_m_tangoDepthCameraManager_50() { return &___m_tangoDepthCameraManager_50; }
	inline void set_m_tangoDepthCameraManager_50(Il2CppObject * value)
	{
		___m_tangoDepthCameraManager_50 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoDepthCameraManager_50, value);
	}
};

struct TangoApplication_t278578959_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<Tango.TangoUxEnums/UxExceptionEventType> Tango.TangoApplication::SCREEN_SLEEPABLE_UX_EXCEPTIONS
	HashSet_1_t995112032 * ___SCREEN_SLEEPABLE_UX_EXCEPTIONS_34;
	// System.String Tango.TangoApplication::m_tangoServiceVersion
	String_t* ___m_tangoServiceVersion_35;
	// System.Single Tango.TangoApplication::m_screenLandscapeAspectRatio
	float ___m_screenLandscapeAspectRatio_36;

public:
	inline static int32_t get_offset_of_SCREEN_SLEEPABLE_UX_EXCEPTIONS_34() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959_StaticFields, ___SCREEN_SLEEPABLE_UX_EXCEPTIONS_34)); }
	inline HashSet_1_t995112032 * get_SCREEN_SLEEPABLE_UX_EXCEPTIONS_34() const { return ___SCREEN_SLEEPABLE_UX_EXCEPTIONS_34; }
	inline HashSet_1_t995112032 ** get_address_of_SCREEN_SLEEPABLE_UX_EXCEPTIONS_34() { return &___SCREEN_SLEEPABLE_UX_EXCEPTIONS_34; }
	inline void set_SCREEN_SLEEPABLE_UX_EXCEPTIONS_34(HashSet_1_t995112032 * value)
	{
		___SCREEN_SLEEPABLE_UX_EXCEPTIONS_34 = value;
		Il2CppCodeGenWriteBarrier(&___SCREEN_SLEEPABLE_UX_EXCEPTIONS_34, value);
	}

	inline static int32_t get_offset_of_m_tangoServiceVersion_35() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959_StaticFields, ___m_tangoServiceVersion_35)); }
	inline String_t* get_m_tangoServiceVersion_35() const { return ___m_tangoServiceVersion_35; }
	inline String_t** get_address_of_m_tangoServiceVersion_35() { return &___m_tangoServiceVersion_35; }
	inline void set_m_tangoServiceVersion_35(String_t* value)
	{
		___m_tangoServiceVersion_35 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoServiceVersion_35, value);
	}

	inline static int32_t get_offset_of_m_screenLandscapeAspectRatio_36() { return static_cast<int32_t>(offsetof(TangoApplication_t278578959_StaticFields, ___m_screenLandscapeAspectRatio_36)); }
	inline float get_m_screenLandscapeAspectRatio_36() const { return ___m_screenLandscapeAspectRatio_36; }
	inline float* get_address_of_m_screenLandscapeAspectRatio_36() { return &___m_screenLandscapeAspectRatio_36; }
	inline void set_m_screenLandscapeAspectRatio_36(float value)
	{
		___m_screenLandscapeAspectRatio_36 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
