﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tango_TangoEventProvider2398726836.h"
#include "AssemblyU2DCSharp_Tango_TangoEventProvider_APIOnEv2464565050.h"
#include "AssemblyU2DCSharp_Tango_TangoEventProvider_API2500881492.h"
#include "AssemblyU2DCSharp_Tango_VideoOverlayProvider1400720988.h"
#include "AssemblyU2DCSharp_Tango_VideoOverlayProvider_APIOn3522420021.h"
#include "AssemblyU2DCSharp_Tango_VideoOverlayProvider_APIOn2067566777.h"
#include "AssemblyU2DCSharp_Tango_VideoOverlayProvider_API1450716194.h"
#include "AssemblyU2DCSharp_Tango_YUVTexture2576165397.h"
#include "AssemblyU2DCSharp_ADMGUIController1723854221.h"
#include "AssemblyU2DCSharp_ADMGUIController_U3CRefreshAreaD3661662196.h"
#include "AssemblyU2DCSharp_ADMGUIController_U3C_DoImportAre2422734253.h"
#include "AssemblyU2DCSharp_ADMGUIController_U3C_DoExportAre1769353053.h"
#include "AssemblyU2DCSharp_ADMGUIController_U3C_DoSaveCurre2439469346.h"
#include "AssemblyU2DCSharp_ADMGUIController_U3C_DoSaveCurre3411365485.h"
#include "AssemblyU2DCSharp_ADMLockY1470712298.h"
#include "AssemblyU2DCSharp_ADMQualityCamera2032242646.h"
#include "AssemblyU2DCSharp_ADMQualityManager1949231632.h"
#include "AssemblyU2DCSharp_ADMQualityManager_QualityCell1904740466.h"
#include "AssemblyU2DCSharp_AreaDescriptionPicker696588977.h"
#include "AssemblyU2DCSharp_AreaDescriptionPicker_U3C_Popula3866132252.h"
#include "AssemblyU2DCSharp_AreaLearningInGameController4091715694.h"
#include "AssemblyU2DCSharp_AreaLearningInGameController_Mar3379064849.h"
#include "AssemblyU2DCSharp_AreaLearningInGameController_U3C3205144073.h"
#include "AssemblyU2DCSharp_AreaLearningInGameController_U3C3698976680.h"
#include "AssemblyU2DCSharp_AreaLearningInGameController_U3C_141033691.h"
#include "AssemblyU2DCSharp_RelocalizingOverlay2716187845.h"
#include "AssemblyU2DCSharp_ARGUIController3357238990.h"
#include "AssemblyU2DCSharp_ARGUIController_U3C_WaitForDepth4144767716.h"
#include "AssemblyU2DCSharp_ARMarker1554260723.h"
#include "AssemblyU2DCSharp_ARTouchEffect1514194827.h"
#include "AssemblyU2DCSharp_AreaDescriptionListElement2319994151.h"
#include "AssemblyU2DCSharp_FPSCounter921841495.h"
#include "AssemblyU2DCSharp_SceneSwitcher2756713117.h"
#include "AssemblyU2DCSharp_TangoDynamicMesh714838395.h"
#include "AssemblyU2DCSharp_TangoDynamicMesh_MeshSegmentDeleg389850259.h"
#include "AssemblyU2DCSharp_TangoDynamicMesh_TangoSingleDyna2600495423.h"
#include "AssemblyU2DCSharp_DetectTangoCoreGUIController256302334.h"
#include "AssemblyU2DCSharp_TangoPresentController3947495060.h"
#include "AssemblyU2DCSharp_TangoPresentController_TangoState749463897.h"
#include "AssemblyU2DCSharp_TangoFloorFindingUIController2977415512.h"
#include "AssemblyU2DCSharp_MeshBuilderWithColorGUIController346325210.h"
#include "AssemblyU2DCSharp_BallThrower1574671556.h"
#include "AssemblyU2DCSharp_MeshBuilderWithPhysicsGUIControll954408520.h"
#include "AssemblyU2DCSharp_TopDownFollow1479984074.h"
#include "AssemblyU2DCSharp_MeshOcclusionAreaDescriptionList3382731569.h"
#include "AssemblyU2DCSharp_MeshOcclusionCameraDepthTexture40066095.h"
#include "AssemblyU2DCSharp_MeshOcclusionUIController1277167912.h"
#include "AssemblyU2DCSharp_MeshOcclusionUIController_AreaDes164498509.h"
#include "AssemblyU2DCSharp_MeshOcclusionUIController_U3C_Po3943986952.h"
#include "AssemblyU2DCSharp_MeshOcclusionUIController_U3C_Do2571324904.h"
#include "AssemblyU2DCSharp_MeshOcclusionUIController_U3C_Do2389158829.h"
#include "AssemblyU2DCSharp_MarkerDetectionUIController1057187083.h"
#include "AssemblyU2DCSharp_MarkerVisualizationObject2038234659.h"
#include "AssemblyU2DCSharp_MotionTrackingGUIController549681126.h"
#include "AssemblyU2DCSharp_MotionTrackingRotate2782832504.h"
#include "AssemblyU2DCSharp_PointCloudFPSCounter3320399270.h"
#include "AssemblyU2DCSharp_PointCloudGUIController2325220810.h"
#include "AssemblyU2DCSharp_PointToPointGUIController1162740098.h"
#include "AssemblyU2DCSharp_PointToPointGUIController_U3C_Wa3470368388.h"
#include "AssemblyU2DCSharp_SimpleARGUIController4051396900.h"
#include "AssemblyU2DCSharp_SimpleAROrbit1468276009.h"
#include "AssemblyU2DCSharp_SimpleARRotate3751492974.h"
#include "AssemblyU2DCSharp_Tango_TangoSupport856097654.h"
#include "AssemblyU2DCSharp_Tango_TangoSupport_MarkerType583861412.h"
#include "AssemblyU2DCSharp_Tango_TangoSupport_Marker1162551292.h"
#include "AssemblyU2DCSharp_Tango_TangoSupport_APIMarker4269261744.h"
#include "AssemblyU2DCSharp_Tango_TangoSupport_APIMarkerParam725008049.h"
#include "AssemblyU2DCSharp_Tango_TangoSupport_APIMarkerList1654514504.h"
#include "AssemblyU2DCSharp_Tango_TangoSupport_TangoSupportAP432952986.h"
#include "AssemblyU2DCSharp_Tango_TangoUxEnums3202003220.h"
#include "AssemblyU2DCSharp_Tango_TangoUxEnums_UxExceptionEv2661651178.h"
#include "AssemblyU2DCSharp_Tango_TangoUxEnums_UxExceptionEv1464923600.h"
#include "AssemblyU2DCSharp_Tango_TangoUxEnums_UxHoldPosture3601679880.h"
#include "AssemblyU2DCSharp_Tango_UxExceptionEvent2503396180.h"
#include "AssemblyU2DCSharp_UxExceptionEventListener3690928406.h"
#include "AssemblyU2DCSharp_UxExceptionEventListener_OnUxExc2040994544.h"
#include "AssemblyU2DCSharp_Tango_TangoUx746396440.h"
#include "AssemblyU2DCSharp_Images563431294.h"
#include "AssemblyU2DCSharp_Reporter3561640551.h"
#include "AssemblyU2DCSharp_Reporter__LogType4109028099.h"
#include "AssemblyU2DCSharp_Reporter_Sample3185432476.h"
#include "AssemblyU2DCSharp_Reporter_Log3604182180.h"
#include "AssemblyU2DCSharp_Reporter_ReportView608488827.h"
#include "AssemblyU2DCSharp_Reporter_DetailView904843682.h"
#include "AssemblyU2DCSharp_Reporter_U3CreadInfoU3Ec__Iterat2714882631.h"
#include "AssemblyU2DCSharp_ReporterGUI402918452.h"
#include "AssemblyU2DCSharp_ReporterMessageReceiver1067427633.h"
#include "AssemblyU2DCSharp_Rotate4255939431.h"
#include "AssemblyU2DCSharp_TestReporter864384665.h"
#include "AssemblyU2DCSharp_UnityEngine_XR_iOS_ConnectToEdit1997139904.h"
#include "AssemblyU2DCSharp_UnityEngine_XR_iOS_ConnectionMes2549772895.h"
#include "AssemblyU2DCSharp_UnityEngine_XR_iOS_SubMessageIds3304728981.h"
#include "AssemblyU2DCSharp_UnityEngine_XR_iOS_EditorHitTest2885724780.h"
#include "AssemblyU2DCSharp_Utils_ObjectSerializationExtensi2339960184.h"
#include "AssemblyU2DCSharp_Utils_SerializableVector44294681242.h"
#include "AssemblyU2DCSharp_Utils_serializableUnityARMatrix41608204732.h"
#include "AssemblyU2DCSharp_Utils_serializableUnityARCamera2284676354.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (TangoEventProvider_t2398726836), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (APIOnEventAvailable_t2464565050), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (API_t2500881492)+ sizeof (Il2CppObject), sizeof(API_t2500881492 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (VideoOverlayProvider_t1400720988), -1, sizeof(VideoOverlayProvider_t1400720988_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2403[1] = 
{
	VideoOverlayProvider_t1400720988_StaticFields::get_offset_of_CLASS_NAME_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (APIOnImageAvailable_t3522420021), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (APIOnTextureAvailable_t2067566777), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (API_t1450716194)+ sizeof (Il2CppObject), sizeof(API_t1450716194 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (YUVTexture_t2576165397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[3] = 
{
	YUVTexture_t2576165397::get_offset_of_m_videoOverlayTextureY_0(),
	YUVTexture_t2576165397::get_offset_of_m_videoOverlayTextureCb_1(),
	YUVTexture_t2576165397::get_offset_of_m_videoOverlayTextureCr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (ADMGUIController_t1723854221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2408[22] = 
{
	ADMGUIController_t1723854221::get_offset_of_m_managementRoot_2(),
	ADMGUIController_t1723854221::get_offset_of_m_qualityRoot_3(),
	ADMGUIController_t1723854221::get_offset_of_m_listParent_4(),
	ADMGUIController_t1723854221::get_offset_of_m_listElement_5(),
	ADMGUIController_t1723854221::get_offset_of_m_listEmptyText_6(),
	ADMGUIController_t1723854221::get_offset_of_m_detailsParent_7(),
	ADMGUIController_t1723854221::get_offset_of_m_detailsDate_8(),
	ADMGUIController_t1723854221::get_offset_of_m_detailsEditableName_9(),
	ADMGUIController_t1723854221::get_offset_of_m_detailsEditablePosX_10(),
	ADMGUIController_t1723854221::get_offset_of_m_detailsEditablePosY_11(),
	ADMGUIController_t1723854221::get_offset_of_m_detailsEditablePosZ_12(),
	ADMGUIController_t1723854221::get_offset_of_m_detailsEditableRotQX_13(),
	ADMGUIController_t1723854221::get_offset_of_m_detailsEditableRotQY_14(),
	ADMGUIController_t1723854221::get_offset_of_m_detailsEditableRotQZ_15(),
	ADMGUIController_t1723854221::get_offset_of_m_detailsEditableRotQW_16(),
	ADMGUIController_t1723854221::get_offset_of_m_deltaPoseController_17(),
	ADMGUIController_t1723854221::get_offset_of_m_savingTextParent_18(),
	ADMGUIController_t1723854221::get_offset_of_m_savingText_19(),
	ADMGUIController_t1723854221::get_offset_of_m_tangoApplication_20(),
	ADMGUIController_t1723854221::get_offset_of_m_selectedAreaDescription_21(),
	ADMGUIController_t1723854221::get_offset_of_m_selectedMetadata_22(),
	ADMGUIController_t1723854221::get_offset_of_m_saveThread_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (U3CRefreshAreaDescriptionListU3Ec__AnonStorey3_t3661662196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[2] = 
{
	U3CRefreshAreaDescriptionListU3Ec__AnonStorey3_t3661662196::get_offset_of_lambdaParam_0(),
	U3CRefreshAreaDescriptionListU3Ec__AnonStorey3_t3661662196::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (U3C_DoImportAreaDescriptionU3Ec__Iterator0_t2422734253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2410[4] = 
{
	U3C_DoImportAreaDescriptionU3Ec__Iterator0_t2422734253::get_offset_of_U3CkbU3E__0_0(),
	U3C_DoImportAreaDescriptionU3Ec__Iterator0_t2422734253::get_offset_of_U24current_1(),
	U3C_DoImportAreaDescriptionU3Ec__Iterator0_t2422734253::get_offset_of_U24disposing_2(),
	U3C_DoImportAreaDescriptionU3Ec__Iterator0_t2422734253::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (U3C_DoExportAreaDescriptionU3Ec__Iterator1_t1769353053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2411[5] = 
{
	U3C_DoExportAreaDescriptionU3Ec__Iterator1_t1769353053::get_offset_of_U3CkbU3E__0_0(),
	U3C_DoExportAreaDescriptionU3Ec__Iterator1_t1769353053::get_offset_of_areaDescription_1(),
	U3C_DoExportAreaDescriptionU3Ec__Iterator1_t1769353053::get_offset_of_U24current_2(),
	U3C_DoExportAreaDescriptionU3Ec__Iterator1_t1769353053::get_offset_of_U24disposing_3(),
	U3C_DoExportAreaDescriptionU3Ec__Iterator1_t1769353053::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator2_t2439469346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2412[6] = 
{
	U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator2_t2439469346::get_offset_of_U3CkbU3E__0_0(),
	U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator2_t2439469346::get_offset_of_U24this_1(),
	U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator2_t2439469346::get_offset_of_U24current_2(),
	U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator2_t2439469346::get_offset_of_U24disposing_3(),
	U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator2_t2439469346::get_offset_of_U24PC_4(),
	U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator2_t2439469346::get_offset_of_U24locvar0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (U3C_DoSaveCurrentAreaDescriptionU3Ec__AnonStorey4_t3411365485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2413[2] = 
{
	U3C_DoSaveCurrentAreaDescriptionU3Ec__AnonStorey4_t3411365485::get_offset_of_fileNameFromKeyboard_0(),
	U3C_DoSaveCurrentAreaDescriptionU3Ec__AnonStorey4_t3411365485::get_offset_of_U3CU3Ef__refU242_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (ADMLockY_t1470712298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2414[1] = 
{
	ADMLockY_t1470712298::get_offset_of_m_lockedY_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (ADMQualityCamera_t2032242646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2415[5] = 
{
	0,
	0,
	0,
	ADMQualityCamera_t2032242646::get_offset_of_m_qualityManager_5(),
	ADMQualityCamera_t2032242646::get_offset_of_m_camera_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (ADMQualityManager_t1949231632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2416[11] = 
{
	ADMQualityManager_t1949231632::get_offset_of_m_poseController_2(),
	ADMQualityManager_t1949231632::get_offset_of_m_cellVisualsPrefab_3(),
	ADMQualityManager_t1949231632::get_offset_of_m_badQualityTransformParent_4(),
	ADMQualityManager_t1949231632::get_offset_of_m_qualityText_5(),
	0,
	0,
	0,
	0,
	ADMQualityManager_t1949231632::get_offset_of_m_cellQualities_10(),
	ADMQualityManager_t1949231632::get_offset_of_m_cellsOrigin_11(),
	ADMQualityManager_t1949231632::get_offset_of_m_badQualityTransformTime_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (QualityCell_t1904740466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2417[2] = 
{
	QualityCell_t1904740466::get_offset_of_m_angleVisited_0(),
	QualityCell_t1904740466::get_offset_of_m_visuals_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (AreaDescriptionPicker_t696588977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2418[9] = 
{
	AreaDescriptionPicker_t696588977::get_offset_of_m_listElement_2(),
	AreaDescriptionPicker_t696588977::get_offset_of_m_listContentParent_3(),
	AreaDescriptionPicker_t696588977::get_offset_of_m_toggleGroup_4(),
	AreaDescriptionPicker_t696588977::get_offset_of_m_enableLearningToggle_5(),
	AreaDescriptionPicker_t696588977::get_offset_of_m_poseController_6(),
	AreaDescriptionPicker_t696588977::get_offset_of_m_gameControlPanel_7(),
	AreaDescriptionPicker_t696588977::get_offset_of_m_guiController_8(),
	AreaDescriptionPicker_t696588977::get_offset_of_m_tangoApplication_9(),
	AreaDescriptionPicker_t696588977::get_offset_of_m_curAreaDescriptionUUID_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (U3C_PopulateListU3Ec__AnonStorey0_t3866132252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[2] = 
{
	U3C_PopulateListU3Ec__AnonStorey0_t3866132252::get_offset_of_lambdaParam_0(),
	U3C_PopulateListU3Ec__AnonStorey0_t3866132252::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (AreaLearningInGameController_t4091715694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[16] = 
{
	AreaLearningInGameController_t4091715694::get_offset_of_m_markPrefabs_2(),
	AreaLearningInGameController_t4091715694::get_offset_of_m_pointCloud_3(),
	AreaLearningInGameController_t4091715694::get_offset_of_m_canvas_4(),
	AreaLearningInGameController_t4091715694::get_offset_of_m_prefabTouchEffect_5(),
	AreaLearningInGameController_t4091715694::get_offset_of_m_savingText_6(),
	AreaLearningInGameController_t4091715694::get_offset_of_m_curAreaDescription_7(),
	AreaLearningInGameController_t4091715694::get_offset_of_m_findPlaneWaitingForDepth_8(),
	AreaLearningInGameController_t4091715694::get_offset_of_m_poseController_9(),
	AreaLearningInGameController_t4091715694::get_offset_of_m_markerList_10(),
	AreaLearningInGameController_t4091715694::get_offset_of_newMarkObject_11(),
	AreaLearningInGameController_t4091715694::get_offset_of_m_currentMarkType_12(),
	AreaLearningInGameController_t4091715694::get_offset_of_m_selectedMarker_13(),
	AreaLearningInGameController_t4091715694::get_offset_of_m_selectedRect_14(),
	AreaLearningInGameController_t4091715694::get_offset_of_m_initialized_15(),
	AreaLearningInGameController_t4091715694::get_offset_of_m_tangoApplication_16(),
	AreaLearningInGameController_t4091715694::get_offset_of_m_saveThread_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (MarkerData_t3379064849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2421[3] = 
{
	MarkerData_t3379064849::get_offset_of_m_type_0(),
	MarkerData_t3379064849::get_offset_of_m_position_1(),
	MarkerData_t3379064849::get_offset_of_m_orientation_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2422[6] = 
{
	U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073::get_offset_of_U3CkbU3E__0_0(),
	U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073::get_offset_of_U3CsaveConfirmedU3E__0_1(),
	U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073::get_offset_of_U24this_2(),
	U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073::get_offset_of_U24current_3(),
	U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073::get_offset_of_U24disposing_4(),
	U3C_DoSaveCurrentAreaDescriptionU3Ec__Iterator0_t3205144073::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (U3C_DoSaveCurrentAreaDescriptionU3Ec__AnonStorey2_t3698976680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2423[2] = 
{
	U3C_DoSaveCurrentAreaDescriptionU3Ec__AnonStorey2_t3698976680::get_offset_of_name_0(),
	U3C_DoSaveCurrentAreaDescriptionU3Ec__AnonStorey2_t3698976680::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2424[13] = 
{
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691::get_offset_of_U3CcamU3E__0_0(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691::get_offset_of_touchPosition_1(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691::get_offset_of_U3CplaneCenterU3E__0_2(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691::get_offset_of_U3CplaneU3E__0_3(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691::get_offset_of_U3CupU3E__0_4(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691::get_offset_of_U3CforwardU3E__1_5(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691::get_offset_of_U3CmarkerScriptU3E__0_6(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691::get_offset_of_U3CuwTDeviceU3E__0_7(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691::get_offset_of_U3CuwTMarkerU3E__0_8(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691::get_offset_of_U24this_9(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691::get_offset_of_U24current_10(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691::get_offset_of_U24disposing_11(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator1_t141033691::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (RelocalizingOverlay_t2716187845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2425[2] = 
{
	RelocalizingOverlay_t2716187845::get_offset_of_m_relocalizationOverlay_2(),
	RelocalizingOverlay_t2716187845::get_offset_of_m_tangoApplication_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (ARGUIController_t3357238990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2426[45] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ARGUIController_t3357238990::get_offset_of_m_prefabMarker_28(),
	ARGUIController_t3357238990::get_offset_of_m_prefabTouchEffect_29(),
	ARGUIController_t3357238990::get_offset_of_m_canvas_30(),
	ARGUIController_t3357238990::get_offset_of_m_pointCloud_31(),
	0,
	ARGUIController_t3357238990::get_offset_of_m_fpsText_33(),
	ARGUIController_t3357238990::get_offset_of_m_currentFPS_34(),
	ARGUIController_t3357238990::get_offset_of_m_framesSinceUpdate_35(),
	ARGUIController_t3357238990::get_offset_of_m_accumulation_36(),
	ARGUIController_t3357238990::get_offset_of_m_currentTime_37(),
	ARGUIController_t3357238990::get_offset_of_m_tangoApplication_38(),
	ARGUIController_t3357238990::get_offset_of_m_tangoPose_39(),
	ARGUIController_t3357238990::get_offset_of_m_tangoServiceVersion_40(),
	ARGUIController_t3357238990::get_offset_of_m_arCameraPostProcess_41(),
	ARGUIController_t3357238990::get_offset_of_m_findPlaneWaitingForDepth_42(),
	ARGUIController_t3357238990::get_offset_of_m_selectedMarker_43(),
	ARGUIController_t3357238990::get_offset_of_m_selectedRect_44(),
	ARGUIController_t3357238990::get_offset_of_m_hideAllRect_45(),
	ARGUIController_t3357238990::get_offset_of_m_showDebug_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2427[10] = 
{
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716::get_offset_of_U3CcamU3E__0_0(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716::get_offset_of_touchPosition_1(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716::get_offset_of_U3CplaneCenterU3E__0_2(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716::get_offset_of_U3CplaneU3E__0_3(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716::get_offset_of_U3CupU3E__0_4(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716::get_offset_of_U3CforwardU3E__1_5(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716::get_offset_of_U24this_6(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716::get_offset_of_U24current_7(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716::get_offset_of_U24disposing_8(),
	U3C_WaitForDepthAndFindPlaneU3Ec__Iterator0_t4144767716::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (ARMarker_t1554260723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2428[4] = 
{
	ARMarker_t1554260723::get_offset_of_m_type_2(),
	ARMarker_t1554260723::get_offset_of_m_timestamp_3(),
	ARMarker_t1554260723::get_offset_of_m_deviceTMarker_4(),
	ARMarker_t1554260723::get_offset_of_m_anim_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (ARTouchEffect_t1514194827), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (AreaDescriptionListElement_t2319994151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2430[3] = 
{
	AreaDescriptionListElement_t2319994151::get_offset_of_m_toggle_2(),
	AreaDescriptionListElement_t2319994151::get_offset_of_m_areaDescriptionName_3(),
	AreaDescriptionListElement_t2319994151::get_offset_of_m_areaDescriptionUUID_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (FPSCounter_t921841495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2431[12] = 
{
	FPSCounter_t921841495::get_offset_of_m_FPSLabelOffsetX_2(),
	FPSCounter_t921841495::get_offset_of_m_FPSLabelOffsetY_3(),
	0,
	0,
	0,
	FPSCounter_t921841495::get_offset_of_m_updateFrequency_7(),
	FPSCounter_t921841495::get_offset_of_m_fpsText_8(),
	FPSCounter_t921841495::get_offset_of_m_currentFPS_9(),
	FPSCounter_t921841495::get_offset_of_m_framesSinceUpdate_10(),
	FPSCounter_t921841495::get_offset_of_m_accumulation_11(),
	FPSCounter_t921841495::get_offset_of_m_currentTime_12(),
	FPSCounter_t921841495::get_offset_of_m_tangoApplication_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (SceneSwitcher_t2756713117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2432[6] = 
{
	SceneSwitcher_t2756713117::get_offset_of_m_onBeforeLoadScene_2(),
	0,
	0,
	0,
	0,
	SceneSwitcher_t2756713117::get_offset_of_m_sceneNames_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (TangoDynamicMesh_t714838395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2433[24] = 
{
	TangoDynamicMesh_t714838395::get_offset_of_m_enableDebugUI_2(),
	TangoDynamicMesh_t714838395::get_offset_of_m_enableSelectiveMeshing_3(),
	TangoDynamicMesh_t714838395::get_offset_of_m_meshSegmentCreated_4(),
	TangoDynamicMesh_t714838395::get_offset_of_m_meshSegmentUpdated_5(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	TangoDynamicMesh_t714838395::get_offset_of_m_minDirectionCheck_13(),
	TangoDynamicMesh_t714838395::get_offset_of_m_tangoApplication_14(),
	TangoDynamicMesh_t714838395::get_offset_of_m_meshRenderer_15(),
	TangoDynamicMesh_t714838395::get_offset_of_m_meshCollider_16(),
	TangoDynamicMesh_t714838395::get_offset_of_m_meshes_17(),
	TangoDynamicMesh_t714838395::get_offset_of_m_gridIndexToUpdate_18(),
	TangoDynamicMesh_t714838395::get_offset_of_m_gridUpdateBacklog_19(),
	TangoDynamicMesh_t714838395::get_offset_of_m_debugTotalVertices_20(),
	TangoDynamicMesh_t714838395::get_offset_of_m_debugTotalTriangles_21(),
	TangoDynamicMesh_t714838395::get_offset_of_m_debugRemeshingTime_22(),
	TangoDynamicMesh_t714838395::get_offset_of_m_debugRemeshingCount_23(),
	TangoDynamicMesh_t714838395::get_offset_of_m_gridIndexConfigs_24(),
	TangoDynamicMesh_t714838395::get_offset_of_m_bounds_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (MeshSegmentDelegate_t389850259), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (TangoSingleDynamicMesh_t2600495423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2435[10] = 
{
	TangoSingleDynamicMesh_t2600495423::get_offset_of_m_mesh_2(),
	TangoSingleDynamicMesh_t2600495423::get_offset_of_m_meshCollider_3(),
	TangoSingleDynamicMesh_t2600495423::get_offset_of_m_needsToGrow_4(),
	TangoSingleDynamicMesh_t2600495423::get_offset_of_m_vertices_5(),
	TangoSingleDynamicMesh_t2600495423::get_offset_of_m_uv_6(),
	TangoSingleDynamicMesh_t2600495423::get_offset_of_m_colors_7(),
	TangoSingleDynamicMesh_t2600495423::get_offset_of_m_triangles_8(),
	TangoSingleDynamicMesh_t2600495423::get_offset_of_m_completed_9(),
	TangoSingleDynamicMesh_t2600495423::get_offset_of_m_observations_10(),
	TangoSingleDynamicMesh_t2600495423::get_offset_of_m_directions_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (DetectTangoCoreGUIController_t256302334), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (TangoPresentController_t3947495060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2437[4] = 
{
	TangoPresentController_t3947495060::get_offset_of_m_editorPlayModeState_2(),
	TangoPresentController_t3947495060::get_offset_of_m_enableIfTangoPresent_3(),
	TangoPresentController_t3947495060::get_offset_of_m_enableIfTangoOutOfDate_4(),
	TangoPresentController_t3947495060::get_offset_of_m_disableIfTangoPresent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (TangoState_t749463897)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2438[4] = 
{
	TangoState_t749463897::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (TangoFloorFindingUIController_t2977415512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2439[5] = 
{
	TangoFloorFindingUIController_t2977415512::get_offset_of_m_marker_2(),
	TangoFloorFindingUIController_t2977415512::get_offset_of_m_tangoApplication_3(),
	TangoFloorFindingUIController_t2977415512::get_offset_of_m_pointCloud_4(),
	TangoFloorFindingUIController_t2977415512::get_offset_of_m_pointCloudFloor_5(),
	TangoFloorFindingUIController_t2977415512::get_offset_of_m_findingFloor_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (MeshBuilderWithColorGUIController_t346325210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2440[4] = 
{
	MeshBuilderWithColorGUIController_t346325210::get_offset_of_m_enableSelectiveMeshing_2(),
	MeshBuilderWithColorGUIController_t346325210::get_offset_of_m_isEnabled_3(),
	MeshBuilderWithColorGUIController_t346325210::get_offset_of_m_tangoApplication_4(),
	MeshBuilderWithColorGUIController_t346325210::get_offset_of_m_dynamicMesh_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (BallThrower_t1574671556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2441[5] = 
{
	BallThrower_t1574671556::get_offset_of_ballPrefab_2(),
	BallThrower_t1574671556::get_offset_of_mainCamera_3(),
	BallThrower_t1574671556::get_offset_of_forwardVelocity_4(),
	BallThrower_t1574671556::get_offset_of_ballArray_5(),
	BallThrower_t1574671556::get_offset_of_currentBallID_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (MeshBuilderWithPhysicsGUIController_t954408520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2442[4] = 
{
	MeshBuilderWithPhysicsGUIController_t954408520::get_offset_of_m_enableSelectiveMeshing_2(),
	MeshBuilderWithPhysicsGUIController_t954408520::get_offset_of_m_isEnabled_3(),
	MeshBuilderWithPhysicsGUIController_t954408520::get_offset_of_m_tangoApplication_4(),
	MeshBuilderWithPhysicsGUIController_t954408520::get_offset_of_m_dynamicMesh_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (TopDownFollow_t1479984074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2443[4] = 
{
	TopDownFollow_t1479984074::get_offset_of_followTarget_2(),
	TopDownFollow_t1479984074::get_offset_of_followYaw_3(),
	TopDownFollow_t1479984074::get_offset_of_pos_4(),
	TopDownFollow_t1479984074::get_offset_of_rotation_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (MeshOcclusionAreaDescriptionListElement_t3382731569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2444[4] = 
{
	MeshOcclusionAreaDescriptionListElement_t3382731569::get_offset_of_m_toggle_2(),
	MeshOcclusionAreaDescriptionListElement_t3382731569::get_offset_of_m_areaDescriptionName_3(),
	MeshOcclusionAreaDescriptionListElement_t3382731569::get_offset_of_m_areaDescriptionUUID_4(),
	MeshOcclusionAreaDescriptionListElement_t3382731569::get_offset_of_m_hasMeshData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (MeshOcclusionCameraDepthTexture_t40066095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2445[2] = 
{
	MeshOcclusionCameraDepthTexture_t40066095::get_offset_of_m_parentCamera_2(),
	MeshOcclusionCameraDepthTexture_t40066095::get_offset_of_m_camera_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (MeshOcclusionUIController_t1277167912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2446[26] = 
{
	MeshOcclusionUIController_t1277167912::get_offset_of_m_markerObject_2(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_meshBuildPanel_3(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_meshInteractionPanel_4(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_viewMeshButton_5(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_hideMeshButton_6(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_relocalizeImage_7(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_savingText_8(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_createSelectedButton_9(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_startGameButton_10(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_areaDescriptionLoaderPanel_11(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_listElement_12(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_listContentParent_13(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_toggleGroup_14(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_depthMaskMat_15(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_visibleMat_16(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_tangoDynamicMesh_17(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_meshFromFile_18(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_curAreaDescription_19(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_poseController_20(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_tangoApplication_21(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_saveThread_22(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_initialized_23(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_3dReconstruction_24(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_menuOpen_25(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_savedUUID_26(),
	MeshOcclusionUIController_t1277167912::get_offset_of_m_meshSavePath_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (AreaDescriptionMesh_t164498509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2447[3] = 
{
	AreaDescriptionMesh_t164498509::get_offset_of_m_uuid_0(),
	AreaDescriptionMesh_t164498509::get_offset_of_m_vertices_1(),
	AreaDescriptionMesh_t164498509::get_offset_of_m_triangles_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (U3C_PopulateAreaDescriptionUIListU3Ec__AnonStorey2_t3943986952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2448[2] = 
{
	U3C_PopulateAreaDescriptionUIListU3Ec__AnonStorey2_t3943986952::get_offset_of_lambdaParam_0(),
	U3C_PopulateAreaDescriptionUIListU3Ec__AnonStorey2_t3943986952::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (U3C_DoSaveAreaDescriptionAndMeshU3Ec__Iterator0_t2571324904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2449[4] = 
{
	U3C_DoSaveAreaDescriptionAndMeshU3Ec__Iterator0_t2571324904::get_offset_of_U24this_0(),
	U3C_DoSaveAreaDescriptionAndMeshU3Ec__Iterator0_t2571324904::get_offset_of_U24current_1(),
	U3C_DoSaveAreaDescriptionAndMeshU3Ec__Iterator0_t2571324904::get_offset_of_U24disposing_2(),
	U3C_DoSaveAreaDescriptionAndMeshU3Ec__Iterator0_t2571324904::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (U3C_DoSaveTangoDynamicMeshU3Ec__Iterator1_t2389158829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2450[4] = 
{
	U3C_DoSaveTangoDynamicMeshU3Ec__Iterator1_t2389158829::get_offset_of_U24this_0(),
	U3C_DoSaveTangoDynamicMeshU3Ec__Iterator1_t2389158829::get_offset_of_U24current_1(),
	U3C_DoSaveTangoDynamicMeshU3Ec__Iterator1_t2389158829::get_offset_of_U24disposing_2(),
	U3C_DoSaveTangoDynamicMeshU3Ec__Iterator1_t2389158829::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (MarkerDetectionUIController_t1057187083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2451[5] = 
{
	MarkerDetectionUIController_t1057187083::get_offset_of_m_markerPrefab_2(),
	0,
	MarkerDetectionUIController_t1057187083::get_offset_of_m_markerObjects_4(),
	MarkerDetectionUIController_t1057187083::get_offset_of_m_markerList_5(),
	MarkerDetectionUIController_t1057187083::get_offset_of_m_tangoApplication_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (MarkerVisualizationObject_t2038234659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2452[1] = 
{
	MarkerVisualizationObject_t2038234659::get_offset_of_m_rect_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (MotionTrackingGUIController_t549681126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2453[1] = 
{
	MotionTrackingGUIController_t549681126::get_offset_of_m_poseController_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (MotionTrackingRotate_t2782832504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2454[1] = 
{
	MotionTrackingRotate_t2782832504::get_offset_of_m_rotationSpeed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (PointCloudFPSCounter_t3320399270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2455[7] = 
{
	PointCloudFPSCounter_t3320399270::get_offset_of_m_updateFrequency_2(),
	PointCloudFPSCounter_t3320399270::get_offset_of_m_FPSText_3(),
	PointCloudFPSCounter_t3320399270::get_offset_of_m_currentFPS_4(),
	PointCloudFPSCounter_t3320399270::get_offset_of_m_framesSinceUpdate_5(),
	PointCloudFPSCounter_t3320399270::get_offset_of_m_accumulation_6(),
	PointCloudFPSCounter_t3320399270::get_offset_of_m_currentTime_7(),
	PointCloudFPSCounter_t3320399270::get_offset_of_m_tangoApplication_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (PointCloudGUIController_t2325220810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2456[35] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	PointCloudGUIController_t2325220810::get_offset_of_m_tangoPoseController_28(),
	PointCloudGUIController_t2325220810::get_offset_of_m_pointcloud_29(),
	0,
	PointCloudGUIController_t2325220810::get_offset_of_m_fpsText_31(),
	PointCloudGUIController_t2325220810::get_offset_of_m_currentFPS_32(),
	PointCloudGUIController_t2325220810::get_offset_of_m_framesSinceUpdate_33(),
	PointCloudGUIController_t2325220810::get_offset_of_m_accumulation_34(),
	PointCloudGUIController_t2325220810::get_offset_of_m_currentTime_35(),
	PointCloudGUIController_t2325220810::get_offset_of_m_tangoApplication_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (PointToPointGUIController_t1162740098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2457[12] = 
{
	0,
	0,
	0,
	0,
	PointToPointGUIController_t1162740098::get_offset_of_m_pointCloud_6(),
	PointToPointGUIController_t1162740098::get_offset_of_m_lineRenderer_7(),
	PointToPointGUIController_t1162740098::get_offset_of_m_tangoApplication_8(),
	PointToPointGUIController_t1162740098::get_offset_of_m_waitingForDepth_9(),
	PointToPointGUIController_t1162740098::get_offset_of_m_startPoint_10(),
	PointToPointGUIController_t1162740098::get_offset_of_m_endPoint_11(),
	PointToPointGUIController_t1162740098::get_offset_of_m_distance_12(),
	PointToPointGUIController_t1162740098::get_offset_of_m_distanceText_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (U3C_WaitForDepthU3Ec__Iterator0_t3470368388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2458[7] = 
{
	U3C_WaitForDepthU3Ec__Iterator0_t3470368388::get_offset_of_U3CcamU3E__0_0(),
	U3C_WaitForDepthU3Ec__Iterator0_t3470368388::get_offset_of_touchPosition_1(),
	U3C_WaitForDepthU3Ec__Iterator0_t3470368388::get_offset_of_U3CpointIndexU3E__0_2(),
	U3C_WaitForDepthU3Ec__Iterator0_t3470368388::get_offset_of_U24this_3(),
	U3C_WaitForDepthU3Ec__Iterator0_t3470368388::get_offset_of_U24current_4(),
	U3C_WaitForDepthU3Ec__Iterator0_t3470368388::get_offset_of_U24disposing_5(),
	U3C_WaitForDepthU3Ec__Iterator0_t3470368388::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (SimpleARGUIController_t4051396900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2459[1] = 
{
	SimpleARGUIController_t4051396900::get_offset_of_m_poseController_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (SimpleAROrbit_t1468276009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2460[3] = 
{
	SimpleAROrbit_t1468276009::get_offset_of_m_earth_2(),
	SimpleAROrbit_t1468276009::get_offset_of_m_radius_3(),
	SimpleAROrbit_t1468276009::get_offset_of_m_orbitSpeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (SimpleARRotate_t3751492974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2461[1] = 
{
	SimpleARRotate_t3751492974::get_offset_of_m_rotationSpeed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (TangoSupport_t856097654), -1, sizeof(TangoSupport_t856097654_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2462[10] = 
{
	TangoSupport_t856097654_StaticFields::get_offset_of_UNITY_WORLD_T_START_SERVICE_0(),
	TangoSupport_t856097654_StaticFields::get_offset_of_DEVICE_T_UNITY_CAMERA_1(),
	TangoSupport_t856097654_StaticFields::get_offset_of_COLOR_CAMERA_T_UNITY_CAMERA_2(),
	TangoSupport_t856097654_StaticFields::get_offset_of_m_colorCameraPoseRotation_3(),
	TangoSupport_t856097654_StaticFields::get_offset_of_m_devicePoseRotation_4(),
	0,
	0,
	TangoSupport_t856097654_StaticFields::get_offset_of_ROTATION270_T_DEFAULT_7(),
	TangoSupport_t856097654_StaticFields::get_offset_of_ROTATION180_T_DEFAULT_8(),
	TangoSupport_t856097654_StaticFields::get_offset_of_ROTATION90_T_DEFAULT_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (MarkerType_t583861412)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2463[3] = 
{
	MarkerType_t583861412::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (Marker_t1162551292)+ sizeof (Il2CppObject), sizeof(Marker_t1162551292_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2464[13] = 
{
	Marker_t1162551292::get_offset_of_m_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Marker_t1162551292::get_offset_of_m_timestamp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Marker_t1162551292::get_offset_of_m_content_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Marker_t1162551292::get_offset_of_m_corner2DP0_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Marker_t1162551292::get_offset_of_m_corner2DP1_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Marker_t1162551292::get_offset_of_m_corner2DP2_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Marker_t1162551292::get_offset_of_m_corner2DP3_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Marker_t1162551292::get_offset_of_m_corner3DP0_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Marker_t1162551292::get_offset_of_m_corner3DP1_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Marker_t1162551292::get_offset_of_m_corner3DP2_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Marker_t1162551292::get_offset_of_m_corner3DP3_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Marker_t1162551292::get_offset_of_m_translation_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Marker_t1162551292::get_offset_of_m_orientation_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (APIMarker_t4269261744)+ sizeof (Il2CppObject), sizeof(APIMarker_t4269261744_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2465[14] = 
{
	APIMarker_t4269261744::get_offset_of_m_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMarker_t4269261744::get_offset_of_m_timestamp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMarker_t4269261744::get_offset_of_m_content_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMarker_t4269261744::get_offset_of_m_contentSize_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMarker_t4269261744::get_offset_of_m_corner2DP0_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMarker_t4269261744::get_offset_of_m_corner2DP1_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMarker_t4269261744::get_offset_of_m_corner2DP2_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMarker_t4269261744::get_offset_of_m_corner2DP3_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMarker_t4269261744::get_offset_of_m_corner3DP0_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMarker_t4269261744::get_offset_of_m_corner3DP1_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMarker_t4269261744::get_offset_of_m_corner3DP2_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMarker_t4269261744::get_offset_of_m_corner3DP3_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMarker_t4269261744::get_offset_of_m_translation_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMarker_t4269261744::get_offset_of_m_rotation_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (APIMarkerParam_t725008049)+ sizeof (Il2CppObject), sizeof(APIMarkerParam_t725008049 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2466[2] = 
{
	APIMarkerParam_t725008049::get_offset_of_m_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMarkerParam_t725008049::get_offset_of_m_markerSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (APIMarkerList_t1654514504)+ sizeof (Il2CppObject), sizeof(APIMarkerList_t1654514504 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2467[2] = 
{
	APIMarkerList_t1654514504::get_offset_of_markers_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	APIMarkerList_t1654514504::get_offset_of_markerCount_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (TangoSupportAPI_t432952986)+ sizeof (Il2CppObject), sizeof(TangoSupportAPI_t432952986 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (TangoUxEnums_t3202003220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (UxExceptionEventType_t2661651178)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2470[12] = 
{
	UxExceptionEventType_t2661651178::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (UxExceptionEventStatus_t1464923600)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2471[4] = 
{
	UxExceptionEventStatus_t1464923600::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (UxHoldPostureType_t3601679880)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2472[5] = 
{
	UxHoldPostureType_t3601679880::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (UxExceptionEvent_t2503396180)+ sizeof (Il2CppObject), sizeof(UxExceptionEvent_t2503396180 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2473[3] = 
{
	UxExceptionEvent_t2503396180::get_offset_of_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UxExceptionEvent_t2503396180::get_offset_of_value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UxExceptionEvent_t2503396180::get_offset_of_status_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (UxExceptionEventListener_t3690928406), -1, sizeof(UxExceptionEventListener_t3690928406_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2476[5] = 
{
	UxExceptionEventListener_t3690928406_StaticFields::get_offset_of_m_instance_1(),
	UxExceptionEventListener_t3690928406_StaticFields::get_offset_of_m_lockObject_2(),
	UxExceptionEventListener_t3690928406_StaticFields::get_offset_of_m_tangoPendingEventQueue_3(),
	UxExceptionEventListener_t3690928406_StaticFields::get_offset_of_m_onUxExceptionEvent_4(),
	UxExceptionEventListener_t3690928406_StaticFields::get_offset_of_m_onUxExceptionEventMultithreadedAvailable_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (OnUxExceptionEventHandler_t2040994544), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (TangoUx_t746396440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2478[6] = 
{
	TangoUx_t746396440::get_offset_of_m_enableUXLibrary_2(),
	TangoUx_t746396440::get_offset_of_m_drawDefaultUXExceptions_3(),
	TangoUx_t746396440::get_offset_of_m_showConnectionScreen_4(),
	TangoUx_t746396440::get_offset_of_m_holdPosture_5(),
	TangoUx_t746396440::get_offset_of_m_tangoApplication_6(),
	TangoUx_t746396440::get_offset_of_m_isTangoUxStarted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (Images_t563431294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2480[26] = 
{
	Images_t563431294::get_offset_of_clearImage_0(),
	Images_t563431294::get_offset_of_collapseImage_1(),
	Images_t563431294::get_offset_of_clearOnNewSceneImage_2(),
	Images_t563431294::get_offset_of_showTimeImage_3(),
	Images_t563431294::get_offset_of_showSceneImage_4(),
	Images_t563431294::get_offset_of_userImage_5(),
	Images_t563431294::get_offset_of_showMemoryImage_6(),
	Images_t563431294::get_offset_of_softwareImage_7(),
	Images_t563431294::get_offset_of_dateImage_8(),
	Images_t563431294::get_offset_of_showFpsImage_9(),
	Images_t563431294::get_offset_of_infoImage_10(),
	Images_t563431294::get_offset_of_searchImage_11(),
	Images_t563431294::get_offset_of_closeImage_12(),
	Images_t563431294::get_offset_of_buildFromImage_13(),
	Images_t563431294::get_offset_of_systemInfoImage_14(),
	Images_t563431294::get_offset_of_graphicsInfoImage_15(),
	Images_t563431294::get_offset_of_backImage_16(),
	Images_t563431294::get_offset_of_logImage_17(),
	Images_t563431294::get_offset_of_warningImage_18(),
	Images_t563431294::get_offset_of_errorImage_19(),
	Images_t563431294::get_offset_of_barImage_20(),
	Images_t563431294::get_offset_of_button_activeImage_21(),
	Images_t563431294::get_offset_of_even_logImage_22(),
	Images_t563431294::get_offset_of_odd_logImage_23(),
	Images_t563431294::get_offset_of_selectedImage_24(),
	Images_t563431294::get_offset_of_reporterScrollerSkin_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (Reporter_t3561640551), -1, sizeof(Reporter_t3561640551_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2481[147] = 
{
	Reporter_t3561640551::get_offset_of_samples_2(),
	Reporter_t3561640551::get_offset_of_logs_3(),
	Reporter_t3561640551::get_offset_of_collapsedLogs_4(),
	Reporter_t3561640551::get_offset_of_currentLog_5(),
	Reporter_t3561640551::get_offset_of_logsDic_6(),
	Reporter_t3561640551::get_offset_of_cachedString_7(),
	Reporter_t3561640551::get_offset_of_show_8(),
	Reporter_t3561640551::get_offset_of_collapse_9(),
	Reporter_t3561640551::get_offset_of_clearOnNewSceneLoaded_10(),
	Reporter_t3561640551::get_offset_of_showTime_11(),
	Reporter_t3561640551::get_offset_of_showScene_12(),
	Reporter_t3561640551::get_offset_of_showMemory_13(),
	Reporter_t3561640551::get_offset_of_showFps_14(),
	Reporter_t3561640551::get_offset_of_showGraph_15(),
	Reporter_t3561640551::get_offset_of_showLog_16(),
	Reporter_t3561640551::get_offset_of_showWarning_17(),
	Reporter_t3561640551::get_offset_of_showError_18(),
	Reporter_t3561640551::get_offset_of_numOfLogs_19(),
	Reporter_t3561640551::get_offset_of_numOfLogsWarning_20(),
	Reporter_t3561640551::get_offset_of_numOfLogsError_21(),
	Reporter_t3561640551::get_offset_of_numOfCollapsedLogs_22(),
	Reporter_t3561640551::get_offset_of_numOfCollapsedLogsWarning_23(),
	Reporter_t3561640551::get_offset_of_numOfCollapsedLogsError_24(),
	Reporter_t3561640551::get_offset_of_showClearOnNewSceneLoadedButton_25(),
	Reporter_t3561640551::get_offset_of_showTimeButton_26(),
	Reporter_t3561640551::get_offset_of_showSceneButton_27(),
	Reporter_t3561640551::get_offset_of_showMemButton_28(),
	Reporter_t3561640551::get_offset_of_showFpsButton_29(),
	Reporter_t3561640551::get_offset_of_showSearchText_30(),
	Reporter_t3561640551::get_offset_of_buildDate_31(),
	Reporter_t3561640551::get_offset_of_logDate_32(),
	Reporter_t3561640551::get_offset_of_logsMemUsage_33(),
	Reporter_t3561640551::get_offset_of_graphMemUsage_34(),
	Reporter_t3561640551::get_offset_of_gcTotalMemory_35(),
	Reporter_t3561640551::get_offset_of_UserData_36(),
	Reporter_t3561640551::get_offset_of_fps_37(),
	Reporter_t3561640551::get_offset_of_fpsText_38(),
	Reporter_t3561640551::get_offset_of_currentView_39(),
	Reporter_t3561640551_StaticFields::get_offset_of_created_40(),
	Reporter_t3561640551::get_offset_of_images_41(),
	Reporter_t3561640551::get_offset_of_clearContent_42(),
	Reporter_t3561640551::get_offset_of_collapseContent_43(),
	Reporter_t3561640551::get_offset_of_clearOnNewSceneContent_44(),
	Reporter_t3561640551::get_offset_of_showTimeContent_45(),
	Reporter_t3561640551::get_offset_of_showSceneContent_46(),
	Reporter_t3561640551::get_offset_of_userContent_47(),
	Reporter_t3561640551::get_offset_of_showMemoryContent_48(),
	Reporter_t3561640551::get_offset_of_softwareContent_49(),
	Reporter_t3561640551::get_offset_of_dateContent_50(),
	Reporter_t3561640551::get_offset_of_showFpsContent_51(),
	Reporter_t3561640551::get_offset_of_infoContent_52(),
	Reporter_t3561640551::get_offset_of_searchContent_53(),
	Reporter_t3561640551::get_offset_of_closeContent_54(),
	Reporter_t3561640551::get_offset_of_buildFromContent_55(),
	Reporter_t3561640551::get_offset_of_systemInfoContent_56(),
	Reporter_t3561640551::get_offset_of_graphicsInfoContent_57(),
	Reporter_t3561640551::get_offset_of_backContent_58(),
	Reporter_t3561640551::get_offset_of_logContent_59(),
	Reporter_t3561640551::get_offset_of_warningContent_60(),
	Reporter_t3561640551::get_offset_of_errorContent_61(),
	Reporter_t3561640551::get_offset_of_barStyle_62(),
	Reporter_t3561640551::get_offset_of_buttonActiveStyle_63(),
	Reporter_t3561640551::get_offset_of_nonStyle_64(),
	Reporter_t3561640551::get_offset_of_lowerLeftFontStyle_65(),
	Reporter_t3561640551::get_offset_of_backStyle_66(),
	Reporter_t3561640551::get_offset_of_evenLogStyle_67(),
	Reporter_t3561640551::get_offset_of_oddLogStyle_68(),
	Reporter_t3561640551::get_offset_of_logButtonStyle_69(),
	Reporter_t3561640551::get_offset_of_selectedLogStyle_70(),
	Reporter_t3561640551::get_offset_of_selectedLogFontStyle_71(),
	Reporter_t3561640551::get_offset_of_stackLabelStyle_72(),
	Reporter_t3561640551::get_offset_of_scrollerStyle_73(),
	Reporter_t3561640551::get_offset_of_searchStyle_74(),
	Reporter_t3561640551::get_offset_of_sliderBackStyle_75(),
	Reporter_t3561640551::get_offset_of_sliderThumbStyle_76(),
	Reporter_t3561640551::get_offset_of_toolbarScrollerSkin_77(),
	Reporter_t3561640551::get_offset_of_logScrollerSkin_78(),
	Reporter_t3561640551::get_offset_of_graphScrollerSkin_79(),
	Reporter_t3561640551::get_offset_of_size_80(),
	Reporter_t3561640551::get_offset_of_maxSize_81(),
	Reporter_t3561640551::get_offset_of_numOfCircleToShow_82(),
	Reporter_t3561640551_StaticFields::get_offset_of_scenes_83(),
	Reporter_t3561640551::get_offset_of_currentScene_84(),
	Reporter_t3561640551::get_offset_of_filterText_85(),
	Reporter_t3561640551::get_offset_of_deviceModel_86(),
	Reporter_t3561640551::get_offset_of_deviceType_87(),
	Reporter_t3561640551::get_offset_of_deviceName_88(),
	Reporter_t3561640551::get_offset_of_graphicsMemorySize_89(),
	Reporter_t3561640551::get_offset_of_maxTextureSize_90(),
	Reporter_t3561640551::get_offset_of_systemMemorySize_91(),
	Reporter_t3561640551::get_offset_of_Initialized_92(),
	Reporter_t3561640551::get_offset_of_screenRect_93(),
	Reporter_t3561640551::get_offset_of_toolBarRect_94(),
	Reporter_t3561640551::get_offset_of_logsRect_95(),
	Reporter_t3561640551::get_offset_of_stackRect_96(),
	Reporter_t3561640551::get_offset_of_graphRect_97(),
	Reporter_t3561640551::get_offset_of_graphMinRect_98(),
	Reporter_t3561640551::get_offset_of_graphMaxRect_99(),
	Reporter_t3561640551::get_offset_of_buttomRect_100(),
	Reporter_t3561640551::get_offset_of_stackRectTopLeft_101(),
	Reporter_t3561640551::get_offset_of_detailRect_102(),
	Reporter_t3561640551::get_offset_of_scrollPosition_103(),
	Reporter_t3561640551::get_offset_of_scrollPosition2_104(),
	Reporter_t3561640551::get_offset_of_toolbarScrollPosition_105(),
	Reporter_t3561640551::get_offset_of_selectedLog_106(),
	Reporter_t3561640551::get_offset_of_toolbarOldDrag_107(),
	Reporter_t3561640551::get_offset_of_oldDrag_108(),
	Reporter_t3561640551::get_offset_of_oldDrag2_109(),
	Reporter_t3561640551::get_offset_of_oldDrag3_110(),
	Reporter_t3561640551::get_offset_of_startIndex_111(),
	Reporter_t3561640551::get_offset_of_countRect_112(),
	Reporter_t3561640551::get_offset_of_timeRect_113(),
	Reporter_t3561640551::get_offset_of_timeLabelRect_114(),
	Reporter_t3561640551::get_offset_of_sceneRect_115(),
	Reporter_t3561640551::get_offset_of_sceneLabelRect_116(),
	Reporter_t3561640551::get_offset_of_memoryRect_117(),
	Reporter_t3561640551::get_offset_of_memoryLabelRect_118(),
	Reporter_t3561640551::get_offset_of_fpsRect_119(),
	Reporter_t3561640551::get_offset_of_fpsLabelRect_120(),
	Reporter_t3561640551::get_offset_of_tempContent_121(),
	Reporter_t3561640551::get_offset_of_infoScrollPosition_122(),
	Reporter_t3561640551::get_offset_of_oldInfoDrag_123(),
	Reporter_t3561640551::get_offset_of_tempRect_124(),
	Reporter_t3561640551::get_offset_of_graphSize_125(),
	Reporter_t3561640551::get_offset_of_startFrame_126(),
	Reporter_t3561640551::get_offset_of_currentFrame_127(),
	Reporter_t3561640551::get_offset_of_tempVector1_128(),
	Reporter_t3561640551::get_offset_of_tempVector2_129(),
	Reporter_t3561640551::get_offset_of_graphScrollerPos_130(),
	Reporter_t3561640551::get_offset_of_maxFpsValue_131(),
	Reporter_t3561640551::get_offset_of_minFpsValue_132(),
	Reporter_t3561640551::get_offset_of_maxMemoryValue_133(),
	Reporter_t3561640551::get_offset_of_minMemoryValue_134(),
	Reporter_t3561640551::get_offset_of_gestureDetector_135(),
	Reporter_t3561640551::get_offset_of_gestureSum_136(),
	Reporter_t3561640551::get_offset_of_gestureLength_137(),
	Reporter_t3561640551::get_offset_of_gestureCount_138(),
	Reporter_t3561640551::get_offset_of_lastClickTime_139(),
	Reporter_t3561640551::get_offset_of_startPos_140(),
	Reporter_t3561640551::get_offset_of_downPos_141(),
	Reporter_t3561640551::get_offset_of_mousePosition_142(),
	Reporter_t3561640551::get_offset_of_frames_143(),
	Reporter_t3561640551::get_offset_of_firstTime_144(),
	Reporter_t3561640551::get_offset_of_lastUpdate_145(),
	0,
	0,
	Reporter_t3561640551::get_offset_of_threadedLogs_148(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (_LogType_t4109028099)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2482[6] = 
{
	_LogType_t4109028099::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (Sample_t3185432476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2483[5] = 
{
	Sample_t3185432476::get_offset_of_time_0(),
	Sample_t3185432476::get_offset_of_loadedScene_1(),
	Sample_t3185432476::get_offset_of_memory_2(),
	Sample_t3185432476::get_offset_of_fps_3(),
	Sample_t3185432476::get_offset_of_fpsText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (Log_t3604182180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2484[5] = 
{
	Log_t3604182180::get_offset_of_count_0(),
	Log_t3604182180::get_offset_of_logType_1(),
	Log_t3604182180::get_offset_of_condition_2(),
	Log_t3604182180::get_offset_of_stacktrace_3(),
	Log_t3604182180::get_offset_of_sampleId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (ReportView_t608488827)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2485[5] = 
{
	ReportView_t608488827::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (DetailView_t904843682)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2486[4] = 
{
	DetailView_t904843682::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (U3CreadInfoU3Ec__Iterator0_t2714882631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2487[7] = 
{
	U3CreadInfoU3Ec__Iterator0_t2714882631::get_offset_of_U3CprefFileU3E__0_0(),
	U3CreadInfoU3Ec__Iterator0_t2714882631::get_offset_of_U3CurlU3E__0_1(),
	U3CreadInfoU3Ec__Iterator0_t2714882631::get_offset_of_U3CwwwU3E__0_2(),
	U3CreadInfoU3Ec__Iterator0_t2714882631::get_offset_of_U24this_3(),
	U3CreadInfoU3Ec__Iterator0_t2714882631::get_offset_of_U24current_4(),
	U3CreadInfoU3Ec__Iterator0_t2714882631::get_offset_of_U24disposing_5(),
	U3CreadInfoU3Ec__Iterator0_t2714882631::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (ReporterGUI_t402918452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2488[1] = 
{
	ReporterGUI_t402918452::get_offset_of_reporter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (ReporterMessageReceiver_t1067427633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2489[1] = 
{
	ReporterMessageReceiver_t1067427633::get_offset_of_reporter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (Rotate_t4255939431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2490[1] = 
{
	Rotate_t4255939431::get_offset_of_angle_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (TestReporter_t864384665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2491[14] = 
{
	TestReporter_t864384665::get_offset_of_logTestCount_2(),
	TestReporter_t864384665::get_offset_of_threadLogTestCount_3(),
	TestReporter_t864384665::get_offset_of_logEverySecond_4(),
	TestReporter_t864384665::get_offset_of_currentLogTestCount_5(),
	TestReporter_t864384665::get_offset_of_reporter_6(),
	TestReporter_t864384665::get_offset_of_style_7(),
	TestReporter_t864384665::get_offset_of_rect1_8(),
	TestReporter_t864384665::get_offset_of_rect2_9(),
	TestReporter_t864384665::get_offset_of_rect3_10(),
	TestReporter_t864384665::get_offset_of_rect4_11(),
	TestReporter_t864384665::get_offset_of_rect5_12(),
	TestReporter_t864384665::get_offset_of_rect6_13(),
	TestReporter_t864384665::get_offset_of_thread_14(),
	TestReporter_t864384665::get_offset_of_elapsed_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (ConnectToEditor_t1997139904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2492[4] = 
{
	ConnectToEditor_t1997139904::get_offset_of_playerConnection_2(),
	ConnectToEditor_t1997139904::get_offset_of_m_session_3(),
	ConnectToEditor_t1997139904::get_offset_of_editorID_4(),
	ConnectToEditor_t1997139904::get_offset_of_frameBufferTex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (ConnectionMessageIds_t2549772895), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (SubMessageIds_t3304728981), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (EditorHitTest_t2885724780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2495[3] = 
{
	EditorHitTest_t2885724780::get_offset_of_m_HitTransform_2(),
	EditorHitTest_t2885724780::get_offset_of_maxRayDistance_3(),
	EditorHitTest_t2885724780::get_offset_of_collisionLayerMask_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (ObjectSerializationExtension_t2339960184), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (SerializableVector4_t4294681242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2497[4] = 
{
	SerializableVector4_t4294681242::get_offset_of_x_0(),
	SerializableVector4_t4294681242::get_offset_of_y_1(),
	SerializableVector4_t4294681242::get_offset_of_z_2(),
	SerializableVector4_t4294681242::get_offset_of_w_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (serializableUnityARMatrix4x4_t1608204732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2498[4] = 
{
	serializableUnityARMatrix4x4_t1608204732::get_offset_of_column0_0(),
	serializableUnityARMatrix4x4_t1608204732::get_offset_of_column1_1(),
	serializableUnityARMatrix4x4_t1608204732::get_offset_of_column2_2(),
	serializableUnityARMatrix4x4_t1608204732::get_offset_of_column3_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (serializableUnityARCamera_t2284676354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2499[8] = 
{
	serializableUnityARCamera_t2284676354::get_offset_of_worldTransform_0(),
	serializableUnityARCamera_t2284676354::get_offset_of_projectionMatrix_1(),
	serializableUnityARCamera_t2284676354::get_offset_of_trackingState_2(),
	serializableUnityARCamera_t2284676354::get_offset_of_trackingReason_3(),
	serializableUnityARCamera_t2284676354::get_offset_of_videoParams_4(),
	serializableUnityARCamera_t2284676354::get_offset_of_lightEstimation_5(),
	serializableUnityARCamera_t2284676354::get_offset_of_pointCloud_6(),
	serializableUnityARCamera_t2284676354::get_offset_of_displayTransform_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
