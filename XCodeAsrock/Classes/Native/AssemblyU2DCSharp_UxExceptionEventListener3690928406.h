﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_AndroidJavaProxy4274989947.h"

// UxExceptionEventListener
struct UxExceptionEventListener_t3690928406;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Queue`1<Tango.UxExceptionEvent>
struct Queue_1_t2323053015;
// UxExceptionEventListener/OnUxExceptionEventHandler
struct OnUxExceptionEventHandler_t2040994544;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UxExceptionEventListener
struct  UxExceptionEventListener_t3690928406  : public AndroidJavaProxy_t4274989947
{
public:

public:
};

struct UxExceptionEventListener_t3690928406_StaticFields
{
public:
	// UxExceptionEventListener UxExceptionEventListener::m_instance
	UxExceptionEventListener_t3690928406 * ___m_instance_1;
	// System.Object UxExceptionEventListener::m_lockObject
	Il2CppObject * ___m_lockObject_2;
	// System.Collections.Generic.Queue`1<Tango.UxExceptionEvent> UxExceptionEventListener::m_tangoPendingEventQueue
	Queue_1_t2323053015 * ___m_tangoPendingEventQueue_3;
	// UxExceptionEventListener/OnUxExceptionEventHandler UxExceptionEventListener::m_onUxExceptionEvent
	OnUxExceptionEventHandler_t2040994544 * ___m_onUxExceptionEvent_4;
	// UxExceptionEventListener/OnUxExceptionEventHandler UxExceptionEventListener::m_onUxExceptionEventMultithreadedAvailable
	OnUxExceptionEventHandler_t2040994544 * ___m_onUxExceptionEventMultithreadedAvailable_5;

public:
	inline static int32_t get_offset_of_m_instance_1() { return static_cast<int32_t>(offsetof(UxExceptionEventListener_t3690928406_StaticFields, ___m_instance_1)); }
	inline UxExceptionEventListener_t3690928406 * get_m_instance_1() const { return ___m_instance_1; }
	inline UxExceptionEventListener_t3690928406 ** get_address_of_m_instance_1() { return &___m_instance_1; }
	inline void set_m_instance_1(UxExceptionEventListener_t3690928406 * value)
	{
		___m_instance_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_instance_1, value);
	}

	inline static int32_t get_offset_of_m_lockObject_2() { return static_cast<int32_t>(offsetof(UxExceptionEventListener_t3690928406_StaticFields, ___m_lockObject_2)); }
	inline Il2CppObject * get_m_lockObject_2() const { return ___m_lockObject_2; }
	inline Il2CppObject ** get_address_of_m_lockObject_2() { return &___m_lockObject_2; }
	inline void set_m_lockObject_2(Il2CppObject * value)
	{
		___m_lockObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_lockObject_2, value);
	}

	inline static int32_t get_offset_of_m_tangoPendingEventQueue_3() { return static_cast<int32_t>(offsetof(UxExceptionEventListener_t3690928406_StaticFields, ___m_tangoPendingEventQueue_3)); }
	inline Queue_1_t2323053015 * get_m_tangoPendingEventQueue_3() const { return ___m_tangoPendingEventQueue_3; }
	inline Queue_1_t2323053015 ** get_address_of_m_tangoPendingEventQueue_3() { return &___m_tangoPendingEventQueue_3; }
	inline void set_m_tangoPendingEventQueue_3(Queue_1_t2323053015 * value)
	{
		___m_tangoPendingEventQueue_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoPendingEventQueue_3, value);
	}

	inline static int32_t get_offset_of_m_onUxExceptionEvent_4() { return static_cast<int32_t>(offsetof(UxExceptionEventListener_t3690928406_StaticFields, ___m_onUxExceptionEvent_4)); }
	inline OnUxExceptionEventHandler_t2040994544 * get_m_onUxExceptionEvent_4() const { return ___m_onUxExceptionEvent_4; }
	inline OnUxExceptionEventHandler_t2040994544 ** get_address_of_m_onUxExceptionEvent_4() { return &___m_onUxExceptionEvent_4; }
	inline void set_m_onUxExceptionEvent_4(OnUxExceptionEventHandler_t2040994544 * value)
	{
		___m_onUxExceptionEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_onUxExceptionEvent_4, value);
	}

	inline static int32_t get_offset_of_m_onUxExceptionEventMultithreadedAvailable_5() { return static_cast<int32_t>(offsetof(UxExceptionEventListener_t3690928406_StaticFields, ___m_onUxExceptionEventMultithreadedAvailable_5)); }
	inline OnUxExceptionEventHandler_t2040994544 * get_m_onUxExceptionEventMultithreadedAvailable_5() const { return ___m_onUxExceptionEventMultithreadedAvailable_5; }
	inline OnUxExceptionEventHandler_t2040994544 ** get_address_of_m_onUxExceptionEventMultithreadedAvailable_5() { return &___m_onUxExceptionEventMultithreadedAvailable_5; }
	inline void set_m_onUxExceptionEventMultithreadedAvailable_5(OnUxExceptionEventHandler_t2040994544 * value)
	{
		___m_onUxExceptionEventMultithreadedAvailable_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_onUxExceptionEventMultithreadedAvailable_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
