﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_IntPtr2504060609.h"

// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4251328308;
// AndroidLifecycleCallbacks
struct AndroidLifecycleCallbacks_t1531251569;
// AndroidHelper/TangoServiceLifecycleListener
struct TangoServiceLifecycleListener_t431297296;
// UnityEngine.jvalue[]
struct jvalueU5BU5D_t2851849116;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidHelper
struct  AndroidHelper_t1537643947  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct AndroidHelper_t1537643947_StaticFields
{
public:
	// UnityEngine.AndroidJavaObject AndroidHelper::m_unityActivity
	AndroidJavaObject_t4251328308 * ___m_unityActivity_2;
	// AndroidLifecycleCallbacks AndroidHelper::m_callbacks
	AndroidLifecycleCallbacks_t1531251569 * ___m_callbacks_3;
	// AndroidHelper/TangoServiceLifecycleListener AndroidHelper::m_tangoServiceLifecycle
	TangoServiceLifecycleListener_t431297296 * ___m_tangoServiceLifecycle_5;
	// UnityEngine.AndroidJavaObject AndroidHelper::m_tangoUxHelper
	AndroidJavaObject_t4251328308 * ___m_tangoUxHelper_6;
	// System.IntPtr AndroidHelper::m_tangoUxHelper_class
	IntPtr_t ___m_tangoUxHelper_class_7;
	// System.IntPtr AndroidHelper::m_tangoUxHelper_obj
	IntPtr_t ___m_tangoUxHelper_obj_8;
	// System.IntPtr AndroidHelper::m_tangoUxHelper_processPoseDataStatus
	IntPtr_t ___m_tangoUxHelper_processPoseDataStatus_9;

public:
	inline static int32_t get_offset_of_m_unityActivity_2() { return static_cast<int32_t>(offsetof(AndroidHelper_t1537643947_StaticFields, ___m_unityActivity_2)); }
	inline AndroidJavaObject_t4251328308 * get_m_unityActivity_2() const { return ___m_unityActivity_2; }
	inline AndroidJavaObject_t4251328308 ** get_address_of_m_unityActivity_2() { return &___m_unityActivity_2; }
	inline void set_m_unityActivity_2(AndroidJavaObject_t4251328308 * value)
	{
		___m_unityActivity_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_unityActivity_2, value);
	}

	inline static int32_t get_offset_of_m_callbacks_3() { return static_cast<int32_t>(offsetof(AndroidHelper_t1537643947_StaticFields, ___m_callbacks_3)); }
	inline AndroidLifecycleCallbacks_t1531251569 * get_m_callbacks_3() const { return ___m_callbacks_3; }
	inline AndroidLifecycleCallbacks_t1531251569 ** get_address_of_m_callbacks_3() { return &___m_callbacks_3; }
	inline void set_m_callbacks_3(AndroidLifecycleCallbacks_t1531251569 * value)
	{
		___m_callbacks_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_callbacks_3, value);
	}

	inline static int32_t get_offset_of_m_tangoServiceLifecycle_5() { return static_cast<int32_t>(offsetof(AndroidHelper_t1537643947_StaticFields, ___m_tangoServiceLifecycle_5)); }
	inline TangoServiceLifecycleListener_t431297296 * get_m_tangoServiceLifecycle_5() const { return ___m_tangoServiceLifecycle_5; }
	inline TangoServiceLifecycleListener_t431297296 ** get_address_of_m_tangoServiceLifecycle_5() { return &___m_tangoServiceLifecycle_5; }
	inline void set_m_tangoServiceLifecycle_5(TangoServiceLifecycleListener_t431297296 * value)
	{
		___m_tangoServiceLifecycle_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoServiceLifecycle_5, value);
	}

	inline static int32_t get_offset_of_m_tangoUxHelper_6() { return static_cast<int32_t>(offsetof(AndroidHelper_t1537643947_StaticFields, ___m_tangoUxHelper_6)); }
	inline AndroidJavaObject_t4251328308 * get_m_tangoUxHelper_6() const { return ___m_tangoUxHelper_6; }
	inline AndroidJavaObject_t4251328308 ** get_address_of_m_tangoUxHelper_6() { return &___m_tangoUxHelper_6; }
	inline void set_m_tangoUxHelper_6(AndroidJavaObject_t4251328308 * value)
	{
		___m_tangoUxHelper_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoUxHelper_6, value);
	}

	inline static int32_t get_offset_of_m_tangoUxHelper_class_7() { return static_cast<int32_t>(offsetof(AndroidHelper_t1537643947_StaticFields, ___m_tangoUxHelper_class_7)); }
	inline IntPtr_t get_m_tangoUxHelper_class_7() const { return ___m_tangoUxHelper_class_7; }
	inline IntPtr_t* get_address_of_m_tangoUxHelper_class_7() { return &___m_tangoUxHelper_class_7; }
	inline void set_m_tangoUxHelper_class_7(IntPtr_t value)
	{
		___m_tangoUxHelper_class_7 = value;
	}

	inline static int32_t get_offset_of_m_tangoUxHelper_obj_8() { return static_cast<int32_t>(offsetof(AndroidHelper_t1537643947_StaticFields, ___m_tangoUxHelper_obj_8)); }
	inline IntPtr_t get_m_tangoUxHelper_obj_8() const { return ___m_tangoUxHelper_obj_8; }
	inline IntPtr_t* get_address_of_m_tangoUxHelper_obj_8() { return &___m_tangoUxHelper_obj_8; }
	inline void set_m_tangoUxHelper_obj_8(IntPtr_t value)
	{
		___m_tangoUxHelper_obj_8 = value;
	}

	inline static int32_t get_offset_of_m_tangoUxHelper_processPoseDataStatus_9() { return static_cast<int32_t>(offsetof(AndroidHelper_t1537643947_StaticFields, ___m_tangoUxHelper_processPoseDataStatus_9)); }
	inline IntPtr_t get_m_tangoUxHelper_processPoseDataStatus_9() const { return ___m_tangoUxHelper_processPoseDataStatus_9; }
	inline IntPtr_t* get_address_of_m_tangoUxHelper_processPoseDataStatus_9() { return &___m_tangoUxHelper_processPoseDataStatus_9; }
	inline void set_m_tangoUxHelper_processPoseDataStatus_9(IntPtr_t value)
	{
		___m_tangoUxHelper_processPoseDataStatus_9 = value;
	}
};

struct AndroidHelper_t1537643947_ThreadStaticFields
{
public:
	// UnityEngine.jvalue[] AndroidHelper::val
	jvalueU5BU5D_t2851849116* ___val_10;
	// System.Int32 AndroidHelper::jniAttached
	int32_t ___jniAttached_11;

public:
	inline static int32_t get_offset_of_val_10() { return static_cast<int32_t>(offsetof(AndroidHelper_t1537643947_ThreadStaticFields, ___val_10)); }
	inline jvalueU5BU5D_t2851849116* get_val_10() const { return ___val_10; }
	inline jvalueU5BU5D_t2851849116** get_address_of_val_10() { return &___val_10; }
	inline void set_val_10(jvalueU5BU5D_t2851849116* value)
	{
		___val_10 = value;
		Il2CppCodeGenWriteBarrier(&___val_10, value);
	}

	inline static int32_t get_offset_of_jniAttached_11() { return static_cast<int32_t>(offsetof(AndroidHelper_t1537643947_ThreadStaticFields, ___jniAttached_11)); }
	inline int32_t get_jniAttached_11() const { return ___jniAttached_11; }
	inline int32_t* get_address_of_jniAttached_11() { return &___jniAttached_11; }
	inline void set_jniAttached_11(int32_t value)
	{
		___jniAttached_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
