﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.MeshCollider
struct MeshCollider_t2718867283;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651;
// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TangoDynamicMesh/TangoSingleDynamicMesh
struct  TangoSingleDynamicMesh_t2600495423  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Mesh TangoDynamicMesh/TangoSingleDynamicMesh::m_mesh
	Mesh_t1356156583 * ___m_mesh_2;
	// UnityEngine.MeshCollider TangoDynamicMesh/TangoSingleDynamicMesh::m_meshCollider
	MeshCollider_t2718867283 * ___m_meshCollider_3;
	// System.Boolean TangoDynamicMesh/TangoSingleDynamicMesh::m_needsToGrow
	bool ___m_needsToGrow_4;
	// UnityEngine.Vector3[] TangoDynamicMesh/TangoSingleDynamicMesh::m_vertices
	Vector3U5BU5D_t1172311765* ___m_vertices_5;
	// UnityEngine.Vector2[] TangoDynamicMesh/TangoSingleDynamicMesh::m_uv
	Vector2U5BU5D_t686124026* ___m_uv_6;
	// UnityEngine.Color32[] TangoDynamicMesh/TangoSingleDynamicMesh::m_colors
	Color32U5BU5D_t30278651* ___m_colors_7;
	// System.Int32[] TangoDynamicMesh/TangoSingleDynamicMesh::m_triangles
	Int32U5BU5D_t3030399641* ___m_triangles_8;
	// System.Boolean TangoDynamicMesh/TangoSingleDynamicMesh::m_completed
	bool ___m_completed_9;
	// System.Int32 TangoDynamicMesh/TangoSingleDynamicMesh::m_observations
	int32_t ___m_observations_10;
	// System.Byte TangoDynamicMesh/TangoSingleDynamicMesh::m_directions
	uint8_t ___m_directions_11;

public:
	inline static int32_t get_offset_of_m_mesh_2() { return static_cast<int32_t>(offsetof(TangoSingleDynamicMesh_t2600495423, ___m_mesh_2)); }
	inline Mesh_t1356156583 * get_m_mesh_2() const { return ___m_mesh_2; }
	inline Mesh_t1356156583 ** get_address_of_m_mesh_2() { return &___m_mesh_2; }
	inline void set_m_mesh_2(Mesh_t1356156583 * value)
	{
		___m_mesh_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_mesh_2, value);
	}

	inline static int32_t get_offset_of_m_meshCollider_3() { return static_cast<int32_t>(offsetof(TangoSingleDynamicMesh_t2600495423, ___m_meshCollider_3)); }
	inline MeshCollider_t2718867283 * get_m_meshCollider_3() const { return ___m_meshCollider_3; }
	inline MeshCollider_t2718867283 ** get_address_of_m_meshCollider_3() { return &___m_meshCollider_3; }
	inline void set_m_meshCollider_3(MeshCollider_t2718867283 * value)
	{
		___m_meshCollider_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_meshCollider_3, value);
	}

	inline static int32_t get_offset_of_m_needsToGrow_4() { return static_cast<int32_t>(offsetof(TangoSingleDynamicMesh_t2600495423, ___m_needsToGrow_4)); }
	inline bool get_m_needsToGrow_4() const { return ___m_needsToGrow_4; }
	inline bool* get_address_of_m_needsToGrow_4() { return &___m_needsToGrow_4; }
	inline void set_m_needsToGrow_4(bool value)
	{
		___m_needsToGrow_4 = value;
	}

	inline static int32_t get_offset_of_m_vertices_5() { return static_cast<int32_t>(offsetof(TangoSingleDynamicMesh_t2600495423, ___m_vertices_5)); }
	inline Vector3U5BU5D_t1172311765* get_m_vertices_5() const { return ___m_vertices_5; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_vertices_5() { return &___m_vertices_5; }
	inline void set_m_vertices_5(Vector3U5BU5D_t1172311765* value)
	{
		___m_vertices_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_vertices_5, value);
	}

	inline static int32_t get_offset_of_m_uv_6() { return static_cast<int32_t>(offsetof(TangoSingleDynamicMesh_t2600495423, ___m_uv_6)); }
	inline Vector2U5BU5D_t686124026* get_m_uv_6() const { return ___m_uv_6; }
	inline Vector2U5BU5D_t686124026** get_address_of_m_uv_6() { return &___m_uv_6; }
	inline void set_m_uv_6(Vector2U5BU5D_t686124026* value)
	{
		___m_uv_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_uv_6, value);
	}

	inline static int32_t get_offset_of_m_colors_7() { return static_cast<int32_t>(offsetof(TangoSingleDynamicMesh_t2600495423, ___m_colors_7)); }
	inline Color32U5BU5D_t30278651* get_m_colors_7() const { return ___m_colors_7; }
	inline Color32U5BU5D_t30278651** get_address_of_m_colors_7() { return &___m_colors_7; }
	inline void set_m_colors_7(Color32U5BU5D_t30278651* value)
	{
		___m_colors_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_colors_7, value);
	}

	inline static int32_t get_offset_of_m_triangles_8() { return static_cast<int32_t>(offsetof(TangoSingleDynamicMesh_t2600495423, ___m_triangles_8)); }
	inline Int32U5BU5D_t3030399641* get_m_triangles_8() const { return ___m_triangles_8; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_triangles_8() { return &___m_triangles_8; }
	inline void set_m_triangles_8(Int32U5BU5D_t3030399641* value)
	{
		___m_triangles_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_triangles_8, value);
	}

	inline static int32_t get_offset_of_m_completed_9() { return static_cast<int32_t>(offsetof(TangoSingleDynamicMesh_t2600495423, ___m_completed_9)); }
	inline bool get_m_completed_9() const { return ___m_completed_9; }
	inline bool* get_address_of_m_completed_9() { return &___m_completed_9; }
	inline void set_m_completed_9(bool value)
	{
		___m_completed_9 = value;
	}

	inline static int32_t get_offset_of_m_observations_10() { return static_cast<int32_t>(offsetof(TangoSingleDynamicMesh_t2600495423, ___m_observations_10)); }
	inline int32_t get_m_observations_10() const { return ___m_observations_10; }
	inline int32_t* get_address_of_m_observations_10() { return &___m_observations_10; }
	inline void set_m_observations_10(int32_t value)
	{
		___m_observations_10 = value;
	}

	inline static int32_t get_offset_of_m_directions_11() { return static_cast<int32_t>(offsetof(TangoSingleDynamicMesh_t2600495423, ___m_directions_11)); }
	inline uint8_t get_m_directions_11() const { return ___m_directions_11; }
	inline uint8_t* get_address_of_m_directions_11() { return &___m_directions_11; }
	inline void set_m_directions_11(uint8_t value)
	{
		___m_directions_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
