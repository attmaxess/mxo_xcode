﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Tango.TangoApplication
struct TangoApplication_t278578959;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tango.TangoApplication/TangoEventRegistrationManager
struct  TangoEventRegistrationManager_t3622516502  : public Il2CppObject
{
public:
	// Tango.TangoApplication Tango.TangoApplication/TangoEventRegistrationManager::m_tangoApplication
	TangoApplication_t278578959 * ___m_tangoApplication_0;

public:
	inline static int32_t get_offset_of_m_tangoApplication_0() { return static_cast<int32_t>(offsetof(TangoEventRegistrationManager_t3622516502, ___m_tangoApplication_0)); }
	inline TangoApplication_t278578959 * get_m_tangoApplication_0() const { return ___m_tangoApplication_0; }
	inline TangoApplication_t278578959 ** get_address_of_m_tangoApplication_0() { return &___m_tangoApplication_0; }
	inline void set_m_tangoApplication_0(TangoApplication_t278578959 * value)
	{
		___m_tangoApplication_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_tangoApplication_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
