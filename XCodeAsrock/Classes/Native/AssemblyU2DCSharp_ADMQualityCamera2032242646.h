﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// ADMQualityManager
struct ADMQualityManager_t1949231632;
// UnityEngine.Camera
struct Camera_t189460977;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ADMQualityCamera
struct  ADMQualityCamera_t2032242646  : public MonoBehaviour_t1158329972
{
public:
	// ADMQualityManager ADMQualityCamera::m_qualityManager
	ADMQualityManager_t1949231632 * ___m_qualityManager_5;
	// UnityEngine.Camera ADMQualityCamera::m_camera
	Camera_t189460977 * ___m_camera_6;

public:
	inline static int32_t get_offset_of_m_qualityManager_5() { return static_cast<int32_t>(offsetof(ADMQualityCamera_t2032242646, ___m_qualityManager_5)); }
	inline ADMQualityManager_t1949231632 * get_m_qualityManager_5() const { return ___m_qualityManager_5; }
	inline ADMQualityManager_t1949231632 ** get_address_of_m_qualityManager_5() { return &___m_qualityManager_5; }
	inline void set_m_qualityManager_5(ADMQualityManager_t1949231632 * value)
	{
		___m_qualityManager_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_qualityManager_5, value);
	}

	inline static int32_t get_offset_of_m_camera_6() { return static_cast<int32_t>(offsetof(ADMQualityCamera_t2032242646, ___m_camera_6)); }
	inline Camera_t189460977 * get_m_camera_6() const { return ___m_camera_6; }
	inline Camera_t189460977 ** get_address_of_m_camera_6() { return &___m_camera_6; }
	inline void set_m_camera_6(Camera_t189460977 * value)
	{
		___m_camera_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_camera_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
